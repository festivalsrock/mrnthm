<?php
$type_gabarit = get_field('type_gabarit');

if(is_preview()) {
    $type_page = 'artist-detail';
    get_header();
    echo '<div class="template-artist-details">';
    get_template_part('views/partials/gabarit-'.$type_gabarit);
    echo '</div>';
    get_footer();
}else {
    if(isset($_REQUEST['seo'])) {
        add_filter( 'wpseo_canonical', '__return_false', 10 );
        echo '<title>';
        wp_title('');
        echo '</title>';
        do_action("wpseo_head");
    }
    header("Status: 301 Moved Permanently", false, 301);
    header("Location: /");
}
