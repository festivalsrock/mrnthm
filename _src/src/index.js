'use strict'

var $ = require('jquery');
var platform = require('platform');
var menu = require('./ui/menu');
var lazyLoading = require('./ui/lazy-loading');
var videoPlayer = require('./tools/video-player');
var slider = require('./ui/slider');
var related = require('./ui/related');
var artistList = require('./ui/artist-list');
var creation = require('./ui/creation');
var aboutUs = require('./ui/about-us');
var playlist = require('./ui/playlist');
var transitions = require('./ui/transitions');
var parallax = require('./ui/parallax');
var gabarit = require('./ui/gabarit');

var App = {

    init: function () {
        console.log('App init()');

        var os = platform.ua.toLowerCase().indexOf('mac') > -1 ? 'mac' : 'win';
        var elBody = document.body;
        var aPlatform = platform.name.split(' ');
        try {
            for(var i = 0, nb = aPlatform.length; i<nb; ++i) {
                elBody.classList.add(aPlatform[i].toLowerCase());
            }
            elBody.classList.add(os);
        } catch (e) {
            console.log('Handle error platform', e);
        }

        var isDesktop = document.body.classList.contains('desktop');
        var isPortrait = $(window).width() < $(window).height();

        lazyLoading.init();
        transitions.init(isDesktop);
        menu.init();
        videoPlayer.init();
        slider.init(isDesktop, isPortrait);
        related.init();
        artistList.init(isDesktop);
        gabarit.init(isDesktop);
        creation.init(isDesktop);
        aboutUs.init();
        playlist.init();
        parallax.init(isDesktop);
    }
};

console.log('window.onload - started');

// Faster than onload, return the event when DOM is loaded (not the external files)
document.addEventListener('DOMContentLoaded', (evt) => {
    console.log('document - DOMContentLoaded');
    App.init();
})

window.onload = function() {
    console.log('window.onload - finished');
    // App.init();
};

// $(document).ready(function () {
//     App.init();
// });
