
// ?name=test
// getSearchParam('name')  // return 'test'
function getSearchParam (sVar) {
    return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
  


module.exports = {
    getSearchParam: getSearchParam
};