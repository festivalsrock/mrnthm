// <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97818201-1', 'auto');
  // ga('send', 'pageview');

// </script>
//

var send = (category, action, label) => {
    console.info(`→ analytics  ${event}, ${category}, ${action}, ${label}`);
    ga('send', {
        hitType: 'event',
        eventCategory: category, //'Videos',
        eventAction: action, //'play',
        eventLabel: label //'Fall Campaign'
    });
}


function trackWithValue(cat, action, value, label)
{
	//console.log('tracked event with value : ' + action);
    if(window.ga)
    {
        console.info('► analytics : ' + cat + ' / ' + action + ' / ' + label + ' / ' + value);

        if(!label)
        	window.ga('send', 'event', cat, action, null, value);
        else
        	window.ga('send', 'event', cat, action, label, value);


    }
}

function track(cat, action, label){
	//console.log('tracked event : ' + action);
    if(window.ga)
    {
        console.info('► analytics : ' + cat + ' / ' + action + ' / ' + label);

        if(!label)
        	window.ga('send', 'event', cat, action);
        else
        	window.ga('send', 'event', cat, action, label);
    }
}


module.exports = {
	track:track,
	trackWithValue:trackWithValue,
    send: send
}
