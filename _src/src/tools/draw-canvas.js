var canvas,
    ctx,
    w,
    h;

var aImages,
    nbImages,
    index = 0,
    grid = [],
    hasActive,
    name;

var init = (canvas, w, h, images, _name) => {
    /* = document.querySelector("canvas")*/
    name = _name;
    console.log('Drawcanvas init '+name);
    console.log(images);
    canvas = canvas;
    ctx = canvas.getContext('2d');
    w = canvas.width;
    h = canvas.height;
    index = 0;
    hasActive = true;
    grid = [];
    aImages = images;
    nbImages = images.length;
    for (var i = 0; i < nbImages; i++) {
        let image = new Image();
        image.src = aImages[i];
        grid.push(new Rectangle(ctx, 0, 0, w, h, '#79f', 0.03, image));
    }

    // Init first image (poster like)
    let image = new Image();
    image.src = aImages[0];
    ctx.drawImage(image, 0, 0, w, h);
}

// prototype methods that will be shared
Rectangle.prototype = {

    trigger: function() { // start this rectangle
        this.triggered = true
    },

    update: function() {
        if (this.triggered && !this.done) { // only if active
            this.alpha += this.speed; // update alpha
            this.done = (this.alpha >= 1); // update status
        }
        // console.log('update alpha : '+this.alpha);
        this.ctx.globalCompositeOperation = 'source-over';
        this.ctx.globalAlpha = Math.min(1, this.alpha);
        this.ctx.drawImage(this.image, 0, 0, this.width, this.height);
        // https://developer.mozilla.org/fr/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
    }
};

// Square-monkey object
function Rectangle(ctx, x, y, w, h, color, speed, image) {
    this.ctx = ctx;
    this.x = x;
    this.y = y;
    this.height = h;
    this.width = w;
    // this.color = color;
    this.image = image;

    this.alpha = 0; // current alpha for this instance
    this.speed = speed; // increment for alpha per frame
    this.triggered = false; // is running
    this.done = false; // has finished
}

// Transition
// http://stackoverflow.com/questions/26620497/animated-image-transition-with-html5-canvas

var loop = () => {

    ctx.globalAlpha = 1;
    // console.log('loop() '+name+' : index='+index);
    // console.log(' is done ?'+grid[index].done+' alpha ? '+grid[index].alpha);
    if(!hasActive)
        return;

    if(grid[index].done)
        index++;

    // console.log('done : '+grid[index].done);
    if(index < nbImages)
    {
        grid[index].trigger();
        grid[index].update();
        if (!grid[index].done) {
            hasActive = true;
        }
    }
    else {
        hasActive = false;
    }

    if (hasActive)
    {
        requestAnimationFrame(loop);
    }
}

var progress = () => {
    // console.log('progress : '+((index+1) / nbImages));
    return (index+1) / nbImages;
}

var stop = () => {
    // index = nbImages;
    hasActive = false;
}

module.exports = {
    init: init,
    loop: loop,
    progress:progress,
    stop:stop
}
