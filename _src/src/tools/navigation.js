
/*
This class handles only the mecanic, not the display

This method starts the hashchange event listener
1- instanciate the navigation
2- add UI object
3- call the method start
 */

var start = () => {
    console.log('HASH DEFAULT : '+window.location.hash);

    var onHashChange = (event) => {
        console.log('HASH CHANGED : '+window.location.hash+' => '+window.location.hash.substr(1));
        showUI(window.location.hash.substr(1));
    }

    onHashChange();
    window.addEventListener('hashchange', onHashChange, false);
}



var aUIs = [];
var aUINames = [];
var uiToClose = '';
var openedUI = '';
var pendingUIToOpen = '';

/*
uniqueName : unique name identifier to associate to the objectUI
objectUI : Object with method [open(onEndOpenCallback), close(onEndCloseCallback)]
 */
var addUI = (uniqueName, objectUI) => {
    // console.log('addUI : ' + uniqueName);
    if (aUIs[uniqueName] === undefined)
    {
        aUIs[uniqueName] = objectUI;
        aUINames.push(uniqueName); // Needed in case there is no match in the hash url
    }
}

var onEndClose = () => {
    if (openedUI != '')
        openedUI = '';
    showUI(pendingUIToOpen);
}

var onEndOpen = () => {
}

var showUI = (uniqueName) => {
    console.log('showUI : ' + uniqueName);
    if(aUIs[uniqueName] != undefined)
    {
        pendingUIToOpen = uniqueName;
        if (openedUI != '') {
            console.log('showUI--close : ' + openedUI);
            aUIs[openedUI].close(onEndClose);
        } else {
            console.log('showUI--open : ' + pendingUIToOpen);
            openedUI = pendingUIToOpen;
            aUIs[pendingUIToOpen].open(onEndOpen);
            if (window.scrollY) {
                window.scroll(0, 0);
            }
        }
    }
    else {
        // Check if is in-game ( #game/1 )
        if(aUIs[uniqueName.split('/')[0]] == undefined)
        {
            console.log('UI : '+uniqueName+' doesn\'t exist');
            window.location.hash = aUINames[0];
        } else {
            console.log('UI : '+uniqueName+' exists, do nothing');
        }
    }
}

var closeUI = (uniqueName) => {
    console.log('closeUI : ' + uniqueName);
    aUIs[uniqueName].close();
}


module.exports = () => {

    // console.log('INSTANCE OF NAVIGATION CTRL');

    // Add oldURL and newURL parameters
    if(!window.HashChangeEvent)(function(){
    	var lastURL=document.URL;
    	window.addEventListener('hashchange',function(event){
    		Object.defineProperty(event,'oldURL',{enumerable:true,configurable:true,value:lastURL});
    		Object.defineProperty(event,'newURL',{enumerable:true,configurable:true,value:document.URL});
    		lastURL=document.URL;
    	});
    }());

    return {
        addUI: addUI,
        showUI: showUI,
        closeUI: closeUI,
        start:start
    };

}
