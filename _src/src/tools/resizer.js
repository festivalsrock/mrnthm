const $				= require('jquery');

var init = (_resizee, _holder, aspectRatio) => {
	
	setInterval(() => {
		let holder = $(_holder);
		let resizee = $(_resizee);

		if(!holder || !resizee)
		{
			console.log(holder + ' or ' +resizee + ' not found');
		}

		let w = holder.width();
		let h = holder.height();

		let newAspectRatio = (w/h);
		if( newAspectRatio < aspectRatio)
		{
			h = w/aspectRatio;
		}
		else
		{
			w = h*aspectRatio;
			
		}

		resizee.width(w).height(h);
	},33);
}

var adaptToHeight = (_elem, _elemOriginalHeight, onResize) => {
	setInterval(() => {
		let elem = $(_elem);
		let h = elem.height();
		let heightRatio = h/_elemOriginalHeight;

		if( heightRatio > 1 && heightRatio < 1.05)
			heightRatio = 1;
		
		elem.css('transform', `scale(${heightRatio})`);
		
		if(onResize)
			onResize(heightRatio);

	},33);

}

// No setInterval on mobile, window size is fixed
var adaptToWidth = ($elem, _widthToFitIn, onResize) => {
	var nScale = _widthToFitIn / $elem.width();
	nScale = Math.min(nScale, 1);
	onResize(nScale);
}

var listen = (cbk, param) => {	
	var resizeTimer;
	window.addEventListener('resize', (evt) => {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			cbk(param);               
		}, 300);
	});
}


module.exports = {
	init:init,
	listen: listen,
	adaptToHeight:adaptToHeight,
	adaptToWidth:adaptToWidth
};