const $	= require('jquery');
const analytics		= require('./analytics');


var initFB = (btFB, url, resultProfil) => {

    $(btFB).unbind('click').click(function(){
        let esc_url = encodeURIComponent(url);
        // console.log('CLICK FB : '+esc_url);

        if(resultProfil == 4) {
            analytics.track('Share', 'Result_Thoughtful_on_FB');
        } else if(resultProfil == 3) {
            analytics.track('Share', 'Result_Intuitive_on_FB');
        } else if(resultProfil == 2) {
            analytics.track('Share', 'Result_Spontaneous_on_FB');
        } else if(resultProfil == 1) {
            analytics.track('Share', 'Result_Audacious_on_FB');
        } else {
            analytics.track('Share', 'Website_on_FB');
        }

        window.open(`http://www.facebook.com/sharer/sharer.php?u=${esc_url}`, 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
        return false;
    });
}

var initTwitter = (btTwitter, url, resultProfil) => {

    $(btTwitter).unbind('click').click(function(){
        let esc_url = encodeURIComponent(url);
        // console.log('CLICK TWIT : '+esc_url);

        if(resultProfil == 4) {
            analytics.track('Share', 'Result_Thoughtful_on_Twitter');
        } else if(resultProfil == 3) {
            analytics.track('Share', 'Result_Intuitive_on_Twitter');
        } else if(resultProfil == 2) {
            analytics.track('Share', 'Result_Spontaneous_on_Twitter');
        } else if(resultProfil == 1) {
            analytics.track('Share', 'Result_Audacious_on_Twitter');
        } else {
            analytics.track('Share', 'Website_on_Twitter');
        }

        window.open(`https://twitter.com/home?status=${esc_url}`, 'twitter_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
        return false;
    });
}

module.exports = function(){
    return {
        initFB:initFB,
        initTwitter:initTwitter
    }
};
