const $	= require('jquery');

module.exports = function createApi(remote_url){

	return{
		call(action, params, onSuccess, onError){

			if(!onError)
				onError = (data) => console.log(data);

			params.action = action;
			params.url = `${remote_url}/remote/`;

			$.ajax({
			  url: params.url,
			  data: params,
			  success: (data) => { data.params = params; onSuccess(data); },
			  error: (data) => { data.params = params; onError(data); },
			  crossDomain: true,
			  dataType: 'jsonp'
			});
		}
	}
}
	