
var createjs = window.createjs;


function createMovieClip(lib, target, opt)
{
	function LoadGFonts(families) {
		var googleObject = {type: 'Google', loadedFonts: 0, totalFonts: families.length, callOnLoad: ht_door.gfontAvailable};
		for(var i =0; i < families.length; i++)
			isFontAvailable(gFontsFamilies[i], googleObject);
	}
	function isFontAvailable(font, obj) {
		var timeOut = 5000;
		var delay = 200;
		var interval = 0;
		var timeElapsed = 0;
		function checkFont() {
			var node = document.createElement('span');
			node.innerHTML = 'giItT1WQy@!-/#';
			node.style.position      = 'absolute';
			node.style.left          = '-1000px';
			node.style.top           = '-1000px';
			// Large font size makes even subtle changes obvious
			node.style.fontSize      = '300px';
			node.style.fontFamily    = 'sans-serif';
			node.style.fontVariant   = 'normal';
			node.style.fontStyle     = 'normal';
			node.style.fontWeight    = 'normal';
			node.style.letterSpacing = '0';
			document.body.appendChild(node);
			var width = node.offsetWidth;
			node.style.fontFamily = font+','+node.style.fontFamily;
			// Compare current width with original width
			var returnVal = false;
			if((node && node.offsetWidth != width) || timeElapsed >=timeOut) {
				obj.loadedFonts++;
				if(interval)
					clearInterval(interval);
				obj.callOnLoad(font, obj.totalFonts);
				returnVal = true;
			}
			if(node) {
				node.parentNode.removeChild(node);
				node = null;
			}
			timeElapsed += delay;
			return returnVal;
		}
		if(!checkFont()) {
			interval = setInterval(checkFont, delay);
		}
	}

	var gFontsFamilies = ['Roboto'];

	var canvas, stage, exportRoot;

	var lib_root = lib;

	var create_anim_func = eval(`create_main_${lib_root}`);
	var anim_id = `main_${lib_root}`;
	canvas = document.getElementById(target);

	try {
			if(!(typeof gFontsFamilies === 'undefined' || gFontsFamilies === null))
				LoadGFonts(gFontsFamilies);
			if(!(typeof totalTypekitFonts === 'undefined' || totalTypekitFonts === null)) {
				var typekitObject = {type: 'Typekit', loadedFonts: 0, totalFonts: totalTypekitFonts, callOnLoad: ht_door.tfontAvailable};
				Typekit.load({
				async: true,
				'fontactive': function(family) {
					isFontAvailable(family, typekitObject);
					}
				});
			}
		} catch(e) {};

	window.images = window.images||{};
	var loader = new createjs.LoadQueue(false);
	loader.addEventListener('fileload', handleFileLoad);
	loader.addEventListener('complete', handleComplete);
	loader.loadManifest(window[lib_root].properties.manifest,true,window.assetBasePath);

	function handleFileLoad(evt) {
		if (evt.item.type == 'image') { images[evt.item.id] = evt.result; }
	}

	function handleComplete(evt) {
		//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
		var queue = evt.target;
		var ssMetadata = window[lib_root].ssMetadata;
		for(var i=0; i<ssMetadata.length; i++) {
			ss[ssMetadata[i].name] = new createjs.SpriteSheet( {'images': [queue.getResult(ssMetadata[i].name)], 'frames': ssMetadata[i].frames} )
		}
		stage = new createjs.Stage(canvas);
		//window[anim_id] =
		exportRoot = new window[lib_root][lib_root]();
		exportRoot[anim_id] = create_anim_func(stage,opt,InjectImageAsset,LoadImages);
		stage.addChild(exportRoot);
		stage.enableMouseOver();
		//Registers the "tick" event listener.
		createjs.Ticker.setFPS(window[lib_root].properties.fps);
		createjs.Ticker.addEventListener('tick', stage);
		//Code to support hidpi screens and responsive scaling.
		(function(isResp, respDim, isScale, scaleType) {
			var lastW, lastH, lastS=1;
			window.addEventListener('resize', resizeCanvas);
			resizeCanvas();
			function resizeCanvas() {
				var w = window[lib_root].properties.width, h = window[lib_root].properties.height;
				var iw = window.innerWidth, ih=window.innerHeight;
				var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
				if(isResp) {
					if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
						sRatio = lastS;
					}
					else if(!isScale) {
						if(iw<w || ih<h)
							sRatio = Math.min(xRatio, yRatio);
					}
					else if(scaleType==1) {
						sRatio = Math.min(xRatio, yRatio);
					}
					else if(scaleType==2) {
						sRatio = Math.max(xRatio, yRatio);
					}
				}
				canvas.width = w*pRatio*sRatio;
				canvas.height = h*pRatio*sRatio;
				canvas.style.width = w*sRatio+'px';
				canvas.style.height = h*sRatio+'px';
				stage.scaleX = pRatio*sRatio;
				stage.scaleY = pRatio*sRatio;
				lastW = iw; lastH = ih; lastS = sRatio;
			}
		})(false,'both',false,1);
	}

	var loadedImages = {};
	var createJs;

	function InjectDynamicAssets(cjs)
	{
		createJs = cjs;
	};

	function InjectImageAsset(lib, src, id, width, height)
	{
		window.dynamicLib.manifest.push( {src:src, id:id} );

		var p;
		(window.dynamicLib[id] = function()
		{
			this.initialize(loadedImages[id]);
		}).prototype = p = new createjs.Bitmap();
		p.nominalBounds = new createjs.Rectangle(0, 0, width, height);
	};

	function LoadImages(handleComplete)
	{
		var loader = new createjs.LoadQueue(false);
		loader.addEventListener('complete', handleComplete);
		loader.addEventListener('fileload', function(e) { loadedImages[e.item.id] = e.result; });
		loader.loadManifest(window.dynamicLib.manifest);
	};

}

var init = (lib, target, opt) => {
	//createMovieClip(lib,target,opt);
}



module.exports = {
	init:init
};
