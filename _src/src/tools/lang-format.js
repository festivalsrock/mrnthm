/*
Convert the lines af an object or a string to html text
[b] [/b] will be bold
\n will be replaced with <br/>

Return a String or an Object
 */
module.exports = function formatToHTML(textObject) {

    // console.log('//////////// TEXT BEFORE');
    console.log(typeof textObject);

    function isString (obj) {
      return (Object.prototype.toString.call(obj) === '[object String]');
    }

    if (isString(textObject)) {
        // bold  [b]bold[/b]
        var txtTemp = textObject.replace(new RegExp(/\[b\](.*?)\[\/b\]/, 'g'), '<b>$1</b>');
        // \n
        txtTemp = txtTemp.replace(new RegExp(/\n/, 'g'), '<br/>');
        return txtTemp;
    }
    else {
        var sLine;
        for (var element in textObject) {
            // console.log(element + ' ' + textObject[element]);
            // bold  [b]bold[/b]
            sLine = textObject[element].replace(new RegExp(/\[b\](.*?)\[\/b\]/, 'g'), '<b>$1</b>');
            // \n
            textObject[element] = sLine.replace(new RegExp(/\n/, 'g'), '<br/>');
            // console.log(element + ' ' + textObject[element]);
        }
        return textObject;
    }

}
