import { log } from 'util';

var $ = require('jquery');
var videojs = require('video.js');
// var videojsHLS = require('videojs-contrib-hls');

window.HELP_IMPROVE_VIDEOJS = false;


// See documentation
// https://developer.vimeo.com/api/authentication
// Authentication made with HTML Headers in PHP
// https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627
// https://player.vimeo.com/external/125947740.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627



// TODO: Add video in artist-detail.php cover, with background cover
// https://github.com/matthojo/videojs-Background/blob/master/lib/videojs-Background.js

var init = function() {
    console.log('VideoPlayer init()');

    var isHome = document.getElementsByClassName('template-homepage').length > 0;
    var isArtistList = document.getElementsByClassName('template-artist-list').length > 0;

    
        
    // videojs.hook('setup', function(player) {
    //     console.log('VIDEOJS HOOK', player.el_);
        
    //     console.log(player.el_.style.width);
    //     console.log(player.el_.style.height);
    //     player.el_.style.width = Math.round(player.el_.offsetWidth) + 'px';
    //     player.el_.style.height = Math.round(player.el_.offsetHeight) + 'px';
    //     console.log(player.el_.style.width);
    //     console.log(player.el_.style.height);
    // });

    if(!isHome) {

        // muteSmallVideos();

        // Init only video-js elements, not other gabarit video
        var aVideos = document.querySelectorAll('.video-js');
        for(var i = 0, L = aVideos.length; i < L; ++i) {
            let isCover = aVideos[i].parentNode.id == 'artist-cover';

            var oVideo = videojs(aVideos[i], {
                controls: !isCover,
                // autoplay: isArtistList,
                // preload: 'auto',
                // loop: false,
                controlBar: {
                    muteToggle: true
                }
            }, () => {
                console.log('> video player ready');
            });

            if(isCover) {
                oVideo.ready(() => {
                    var $videoHeader = $('.js-artist-video-header video');
                    if($videoHeader.length > 0) {
                        var $artistCover = $('#artist-cover');
                        var centerVideo = function() {
                            $videoHeader.css({transform: `translateY(-${($videoHeader.height() - $artistCover.height()) * 0.5}px)`});
                        };        
                        centerVideo();
                        $(window).on('resize', () => {
                            centerVideo();
                        });        
                    }
                });
            } else {
                oVideo.one('click', (evt)=> {
                    $(evt.currentTarget).parents('.gabarit').find('.title-project').hide();
                });
            }
        }

        // Video Simple
        var aVideoSimple = document.querySelectorAll('.simple-video-control');
        var videoSimple, styleTransform;
        for(var j = 0, M = aVideoSimple.length; j < M; ++j) {
            videoSimple = aVideoSimple[j];
            videoSimple.classList.add('pause');
            // styleTransform = window.getComputedStyle(videoSimple.previousSibling, null);
            // videoSimple.style.transform = styleTransform.getPropertyValue('transform');
            videoSimple.parentNode.style.position = 'relative';
            videoSimple.addEventListener('click', (evt) => {
                clickMiniButton(evt.target);
            });
        }
    }
}

var muteSmallVideos = function() {
    var videos = document.getElementsByClassName('simple-video');
    console.log('mute video', videos);
    console.log('Nb videos', videos.length);
    for(var i=0, nb = videos.length; i < nb; ++i) {
        videos[i].muted = true;
    }
}

var startVideoCover = function(el) {
    if(el) {
        console.log('startVideoCover', el);
        var player = videojs(el, {
            controls: false,
            loop: true
            // controlBar: {
            //     muteToggle: true
            // }
        });
        player.currentTime(0);
        player.play();
        player.muted(true);
    }
}
var startVideoHome = function(el) {
    if(el) {
        // console.log('startVideoHome', el);
        // el.style.width = Math.round(el.offsetWidth - 1) + 'px';
        // el.style.height = Math.round(el.offsetHeight - 1) + 'px';
        var player = videojs(el, {
            controls: false,
            loop: true
            // controlBar: {
            //     muteToggle: true
            // }
        });
        player.currentTime(0);
        player.muted(true);
        player.play();
    }
}

var startVideo = function(el) {
    if(el) {
        console.log('startVideo', el);
        let w = el.style.width;

        console.log('WIDTH', w);
        var player = videojs(el, {
            controls: true,
            loop: false,
            controlBar: {
                muteToggle: true
            }
        });
        player.currentTime(0);
        player.play();
        player.muted(false);
    }
}

var stopVideo = function(el) {
    if(el)
        videojs(el).pause();
}


var clickMiniButton = (el) => {
    console.log('clickMiniButton', el);
    var video = el.previousSibling;
    if(video.paused) {
        video.play();
        el.classList.remove('play');
        el.classList.add('pause');
    } else {
        video.pause();
        el.classList.add('play');
        el.classList.remove('pause');
    }    
}



module.exports = {
    init: init,
    startVideoHome: startVideoHome,
    startVideoCover: startVideoCover,
    startVideo: startVideo,
    stopVideo: stopVideo,
    muteSmallVideos: muteSmallVideos
};

