const langs	= require('../lg.json');

module.exports = function createLang(name){
	var lang = langs[name];

	if(!lang)
	{
		console.log(`language: ${name} NOT FOUND!`, lang);
		lang = langs['en_GB'] ;
	}
	return lang;
}