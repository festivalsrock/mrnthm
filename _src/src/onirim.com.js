(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

exports.__esModule = true;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

// istanbul ignore next

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

var _handlebarsBase = require('./handlebars/base');

var base = _interopRequireWildcard(_handlebarsBase);

// Each of these augment the Handlebars object. No need to setup here.
// (This is done to easily share code between commonjs and browse envs)

var _handlebarsSafeString = require('./handlebars/safe-string');

var _handlebarsSafeString2 = _interopRequireDefault(_handlebarsSafeString);

var _handlebarsException = require('./handlebars/exception');

var _handlebarsException2 = _interopRequireDefault(_handlebarsException);

var _handlebarsUtils = require('./handlebars/utils');

var Utils = _interopRequireWildcard(_handlebarsUtils);

var _handlebarsRuntime = require('./handlebars/runtime');

var runtime = _interopRequireWildcard(_handlebarsRuntime);

var _handlebarsNoConflict = require('./handlebars/no-conflict');

var _handlebarsNoConflict2 = _interopRequireDefault(_handlebarsNoConflict);

// For compatibility and usage outside of module systems, make the Handlebars object a namespace
function create() {
  var hb = new base.HandlebarsEnvironment();

  Utils.extend(hb, base);
  hb.SafeString = _handlebarsSafeString2['default'];
  hb.Exception = _handlebarsException2['default'];
  hb.Utils = Utils;
  hb.escapeExpression = Utils.escapeExpression;

  hb.VM = runtime;
  hb.template = function (spec) {
    return runtime.template(spec, hb);
  };

  return hb;
}

var inst = create();
inst.create = create;

_handlebarsNoConflict2['default'](inst);

inst['default'] = inst;

exports['default'] = inst;
module.exports = exports['default'];


},{"./handlebars/base":2,"./handlebars/exception":5,"./handlebars/no-conflict":15,"./handlebars/runtime":16,"./handlebars/safe-string":17,"./handlebars/utils":18}],2:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.HandlebarsEnvironment = HandlebarsEnvironment;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _utils = require('./utils');

var _exception = require('./exception');

var _exception2 = _interopRequireDefault(_exception);

var _helpers = require('./helpers');

var _decorators = require('./decorators');

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var VERSION = '4.0.5';
exports.VERSION = VERSION;
var COMPILER_REVISION = 7;

exports.COMPILER_REVISION = COMPILER_REVISION;
var REVISION_CHANGES = {
  1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
  2: '== 1.0.0-rc.3',
  3: '== 1.0.0-rc.4',
  4: '== 1.x.x',
  5: '== 2.0.0-alpha.x',
  6: '>= 2.0.0-beta.1',
  7: '>= 4.0.0'
};

exports.REVISION_CHANGES = REVISION_CHANGES;
var objectType = '[object Object]';

function HandlebarsEnvironment(helpers, partials, decorators) {
  this.helpers = helpers || {};
  this.partials = partials || {};
  this.decorators = decorators || {};

  _helpers.registerDefaultHelpers(this);
  _decorators.registerDefaultDecorators(this);
}

HandlebarsEnvironment.prototype = {
  constructor: HandlebarsEnvironment,

  logger: _logger2['default'],
  log: _logger2['default'].log,

  registerHelper: function registerHelper(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple helpers');
      }
      _utils.extend(this.helpers, name);
    } else {
      this.helpers[name] = fn;
    }
  },
  unregisterHelper: function unregisterHelper(name) {
    delete this.helpers[name];
  },

  registerPartial: function registerPartial(name, partial) {
    if (_utils.toString.call(name) === objectType) {
      _utils.extend(this.partials, name);
    } else {
      if (typeof partial === 'undefined') {
        throw new _exception2['default']('Attempting to register a partial called "' + name + '" as undefined');
      }
      this.partials[name] = partial;
    }
  },
  unregisterPartial: function unregisterPartial(name) {
    delete this.partials[name];
  },

  registerDecorator: function registerDecorator(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple decorators');
      }
      _utils.extend(this.decorators, name);
    } else {
      this.decorators[name] = fn;
    }
  },
  unregisterDecorator: function unregisterDecorator(name) {
    delete this.decorators[name];
  }
};

var log = _logger2['default'].log;

exports.log = log;
exports.createFrame = _utils.createFrame;
exports.logger = _logger2['default'];


},{"./decorators":3,"./exception":5,"./helpers":6,"./logger":14,"./utils":18}],3:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.registerDefaultDecorators = registerDefaultDecorators;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _decoratorsInline = require('./decorators/inline');

var _decoratorsInline2 = _interopRequireDefault(_decoratorsInline);

function registerDefaultDecorators(instance) {
  _decoratorsInline2['default'](instance);
}


},{"./decorators/inline":4}],4:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _utils = require('../utils');

exports['default'] = function (instance) {
  instance.registerDecorator('inline', function (fn, props, container, options) {
    var ret = fn;
    if (!props.partials) {
      props.partials = {};
      ret = function (context, options) {
        // Create a new partials stack frame prior to exec.
        var original = container.partials;
        container.partials = _utils.extend({}, original, props.partials);
        var ret = fn(context, options);
        container.partials = original;
        return ret;
      };
    }

    props.partials[options.args[0]] = options.fn;

    return ret;
  });
};

module.exports = exports['default'];


},{"../utils":18}],5:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

function Exception(message, node) {
  var loc = node && node.loc,
      line = undefined,
      column = undefined;
  if (loc) {
    line = loc.start.line;
    column = loc.start.column;

    message += ' - ' + line + ':' + column;
  }

  var tmp = Error.prototype.constructor.call(this, message);

  // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
  for (var idx = 0; idx < errorProps.length; idx++) {
    this[errorProps[idx]] = tmp[errorProps[idx]];
  }

  /* istanbul ignore else */
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, Exception);
  }

  if (loc) {
    this.lineNumber = line;
    this.column = column;
  }
}

Exception.prototype = new Error();

exports['default'] = Exception;
module.exports = exports['default'];


},{}],6:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.registerDefaultHelpers = registerDefaultHelpers;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _helpersBlockHelperMissing = require('./helpers/block-helper-missing');

var _helpersBlockHelperMissing2 = _interopRequireDefault(_helpersBlockHelperMissing);

var _helpersEach = require('./helpers/each');

var _helpersEach2 = _interopRequireDefault(_helpersEach);

var _helpersHelperMissing = require('./helpers/helper-missing');

var _helpersHelperMissing2 = _interopRequireDefault(_helpersHelperMissing);

var _helpersIf = require('./helpers/if');

var _helpersIf2 = _interopRequireDefault(_helpersIf);

var _helpersLog = require('./helpers/log');

var _helpersLog2 = _interopRequireDefault(_helpersLog);

var _helpersLookup = require('./helpers/lookup');

var _helpersLookup2 = _interopRequireDefault(_helpersLookup);

var _helpersWith = require('./helpers/with');

var _helpersWith2 = _interopRequireDefault(_helpersWith);

function registerDefaultHelpers(instance) {
  _helpersBlockHelperMissing2['default'](instance);
  _helpersEach2['default'](instance);
  _helpersHelperMissing2['default'](instance);
  _helpersIf2['default'](instance);
  _helpersLog2['default'](instance);
  _helpersLookup2['default'](instance);
  _helpersWith2['default'](instance);
}


},{"./helpers/block-helper-missing":7,"./helpers/each":8,"./helpers/helper-missing":9,"./helpers/if":10,"./helpers/log":11,"./helpers/lookup":12,"./helpers/with":13}],7:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _utils = require('../utils');

exports['default'] = function (instance) {
  instance.registerHelper('blockHelperMissing', function (context, options) {
    var inverse = options.inverse,
        fn = options.fn;

    if (context === true) {
      return fn(this);
    } else if (context === false || context == null) {
      return inverse(this);
    } else if (_utils.isArray(context)) {
      if (context.length > 0) {
        if (options.ids) {
          options.ids = [options.name];
        }

        return instance.helpers.each(context, options);
      } else {
        return inverse(this);
      }
    } else {
      if (options.data && options.ids) {
        var data = _utils.createFrame(options.data);
        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.name);
        options = { data: data };
      }

      return fn(context, options);
    }
  });
};

module.exports = exports['default'];


},{"../utils":18}],8:[function(require,module,exports){
'use strict';

exports.__esModule = true;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _utils = require('../utils');

var _exception = require('../exception');

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('each', function (context, options) {
    if (!options) {
      throw new _exception2['default']('Must pass iterator to #each');
    }

    var fn = options.fn,
        inverse = options.inverse,
        i = 0,
        ret = '',
        data = undefined,
        contextPath = undefined;

    if (options.data && options.ids) {
      contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
    }

    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    if (options.data) {
      data = _utils.createFrame(options.data);
    }

    function execIteration(field, index, last) {
      if (data) {
        data.key = field;
        data.index = index;
        data.first = index === 0;
        data.last = !!last;

        if (contextPath) {
          data.contextPath = contextPath + field;
        }
      }

      ret = ret + fn(context[field], {
        data: data,
        blockParams: _utils.blockParams([context[field], field], [contextPath + field, null])
      });
    }

    if (context && typeof context === 'object') {
      if (_utils.isArray(context)) {
        for (var j = context.length; i < j; i++) {
          if (i in context) {
            execIteration(i, i, i === context.length - 1);
          }
        }
      } else {
        var priorKey = undefined;

        for (var key in context) {
          if (context.hasOwnProperty(key)) {
            // We're running the iterations one step out of sync so we can detect
            // the last iteration without have to scan the object twice and create
            // an itermediate keys array.
            if (priorKey !== undefined) {
              execIteration(priorKey, i - 1);
            }
            priorKey = key;
            i++;
          }
        }
        if (priorKey !== undefined) {
          execIteration(priorKey, i - 1, true);
        }
      }
    }

    if (i === 0) {
      ret = inverse(this);
    }

    return ret;
  });
};

module.exports = exports['default'];


},{"../exception":5,"../utils":18}],9:[function(require,module,exports){
'use strict';

exports.__esModule = true;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _exception = require('../exception');

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('helperMissing', function () /* [args, ]options */{
    if (arguments.length === 1) {
      // A missing field in a {{foo}} construct.
      return undefined;
    } else {
      // Someone is actually trying to call something, blow up.
      throw new _exception2['default']('Missing helper: "' + arguments[arguments.length - 1].name + '"');
    }
  });
};

module.exports = exports['default'];


},{"../exception":5}],10:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _utils = require('../utils');

exports['default'] = function (instance) {
  instance.registerHelper('if', function (conditional, options) {
    if (_utils.isFunction(conditional)) {
      conditional = conditional.call(this);
    }

    // Default behavior is to render the positive path if the value is truthy and not empty.
    // The `includeZero` option may be set to treat the condtional as purely not empty based on the
    // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
    if (!options.hash.includeZero && !conditional || _utils.isEmpty(conditional)) {
      return options.inverse(this);
    } else {
      return options.fn(this);
    }
  });

  instance.registerHelper('unless', function (conditional, options) {
    return instance.helpers['if'].call(this, conditional, { fn: options.inverse, inverse: options.fn, hash: options.hash });
  });
};

module.exports = exports['default'];


},{"../utils":18}],11:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('log', function () /* message, options */{
    var args = [undefined],
        options = arguments[arguments.length - 1];
    for (var i = 0; i < arguments.length - 1; i++) {
      args.push(arguments[i]);
    }

    var level = 1;
    if (options.hash.level != null) {
      level = options.hash.level;
    } else if (options.data && options.data.level != null) {
      level = options.data.level;
    }
    args[0] = level;

    instance.log.apply(instance, args);
  });
};

module.exports = exports['default'];


},{}],12:[function(require,module,exports){
'use strict';

exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('lookup', function (obj, field) {
    return obj && obj[field];
  });
};

module.exports = exports['default'];


},{}],13:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _utils = require('../utils');

exports['default'] = function (instance) {
  instance.registerHelper('with', function (context, options) {
    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    var fn = options.fn;

    if (!_utils.isEmpty(context)) {
      var data = options.data;
      if (options.data && options.ids) {
        data = _utils.createFrame(options.data);
        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]);
      }

      return fn(context, {
        data: data,
        blockParams: _utils.blockParams([context], [data && data.contextPath])
      });
    } else {
      return options.inverse(this);
    }
  });
};

module.exports = exports['default'];


},{"../utils":18}],14:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var _utils = require('./utils');

var logger = {
  methodMap: ['debug', 'info', 'warn', 'error'],
  level: 'info',

  // Maps a given level value to the `methodMap` indexes above.
  lookupLevel: function lookupLevel(level) {
    if (typeof level === 'string') {
      var levelMap = _utils.indexOf(logger.methodMap, level.toLowerCase());
      if (levelMap >= 0) {
        level = levelMap;
      } else {
        level = parseInt(level, 10);
      }
    }

    return level;
  },

  // Can be overridden in the host environment
  log: function log(level) {
    level = logger.lookupLevel(level);

    if (typeof console !== 'undefined' && logger.lookupLevel(logger.level) <= level) {
      var method = logger.methodMap[level];
      if (!console[method]) {
        // eslint-disable-line no-console
        method = 'log';
      }

      for (var _len = arguments.length, message = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        message[_key - 1] = arguments[_key];
      }

      console[method].apply(console, message); // eslint-disable-line no-console
    }
  }
};

exports['default'] = logger;
module.exports = exports['default'];


},{"./utils":18}],15:[function(require,module,exports){
(function (global){
/* global window */
'use strict';

exports.__esModule = true;

exports['default'] = function (Handlebars) {
  /* istanbul ignore next */
  var root = typeof global !== 'undefined' ? global : window,
      $Handlebars = root.Handlebars;
  /* istanbul ignore next */
  Handlebars.noConflict = function () {
    if (root.Handlebars === Handlebars) {
      root.Handlebars = $Handlebars;
    }
    return Handlebars;
  };
};

module.exports = exports['default'];


}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],16:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.checkRevision = checkRevision;
exports.template = template;
exports.wrapProgram = wrapProgram;
exports.resolvePartial = resolvePartial;
exports.invokePartial = invokePartial;
exports.noop = noop;
// istanbul ignore next

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

// istanbul ignore next

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

var _utils = require('./utils');

var Utils = _interopRequireWildcard(_utils);

var _exception = require('./exception');

var _exception2 = _interopRequireDefault(_exception);

var _base = require('./base');

function checkRevision(compilerInfo) {
  var compilerRevision = compilerInfo && compilerInfo[0] || 1,
      currentRevision = _base.COMPILER_REVISION;

  if (compilerRevision !== currentRevision) {
    if (compilerRevision < currentRevision) {
      var runtimeVersions = _base.REVISION_CHANGES[currentRevision],
          compilerVersions = _base.REVISION_CHANGES[compilerRevision];
      throw new _exception2['default']('Template was precompiled with an older version of Handlebars than the current runtime. ' + 'Please update your precompiler to a newer version (' + runtimeVersions + ') or downgrade your runtime to an older version (' + compilerVersions + ').');
    } else {
      // Use the embedded version info since the runtime doesn't know about this revision yet
      throw new _exception2['default']('Template was precompiled with a newer version of Handlebars than the current runtime. ' + 'Please update your runtime to a newer version (' + compilerInfo[1] + ').');
    }
  }
}

function template(templateSpec, env) {
  /* istanbul ignore next */
  if (!env) {
    throw new _exception2['default']('No environment passed to template');
  }
  if (!templateSpec || !templateSpec.main) {
    throw new _exception2['default']('Unknown template object: ' + typeof templateSpec);
  }

  templateSpec.main.decorator = templateSpec.main_d;

  // Note: Using env.VM references rather than local var references throughout this section to allow
  // for external users to override these as psuedo-supported APIs.
  env.VM.checkRevision(templateSpec.compiler);

  function invokePartialWrapper(partial, context, options) {
    if (options.hash) {
      context = Utils.extend({}, context, options.hash);
      if (options.ids) {
        options.ids[0] = true;
      }
    }

    partial = env.VM.resolvePartial.call(this, partial, context, options);
    var result = env.VM.invokePartial.call(this, partial, context, options);

    if (result == null && env.compile) {
      options.partials[options.name] = env.compile(partial, templateSpec.compilerOptions, env);
      result = options.partials[options.name](context, options);
    }
    if (result != null) {
      if (options.indent) {
        var lines = result.split('\n');
        for (var i = 0, l = lines.length; i < l; i++) {
          if (!lines[i] && i + 1 === l) {
            break;
          }

          lines[i] = options.indent + lines[i];
        }
        result = lines.join('\n');
      }
      return result;
    } else {
      throw new _exception2['default']('The partial ' + options.name + ' could not be compiled when running in runtime-only mode');
    }
  }

  // Just add water
  var container = {
    strict: function strict(obj, name) {
      if (!(name in obj)) {
        throw new _exception2['default']('"' + name + '" not defined in ' + obj);
      }
      return obj[name];
    },
    lookup: function lookup(depths, name) {
      var len = depths.length;
      for (var i = 0; i < len; i++) {
        if (depths[i] && depths[i][name] != null) {
          return depths[i][name];
        }
      }
    },
    lambda: function lambda(current, context) {
      return typeof current === 'function' ? current.call(context) : current;
    },

    escapeExpression: Utils.escapeExpression,
    invokePartial: invokePartialWrapper,

    fn: function fn(i) {
      var ret = templateSpec[i];
      ret.decorator = templateSpec[i + '_d'];
      return ret;
    },

    programs: [],
    program: function program(i, data, declaredBlockParams, blockParams, depths) {
      var programWrapper = this.programs[i],
          fn = this.fn(i);
      if (data || depths || blockParams || declaredBlockParams) {
        programWrapper = wrapProgram(this, i, fn, data, declaredBlockParams, blockParams, depths);
      } else if (!programWrapper) {
        programWrapper = this.programs[i] = wrapProgram(this, i, fn);
      }
      return programWrapper;
    },

    data: function data(value, depth) {
      while (value && depth--) {
        value = value._parent;
      }
      return value;
    },
    merge: function merge(param, common) {
      var obj = param || common;

      if (param && common && param !== common) {
        obj = Utils.extend({}, common, param);
      }

      return obj;
    },

    noop: env.VM.noop,
    compilerInfo: templateSpec.compiler
  };

  function ret(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    var data = options.data;

    ret._setup(options);
    if (!options.partial && templateSpec.useData) {
      data = initData(context, data);
    }
    var depths = undefined,
        blockParams = templateSpec.useBlockParams ? [] : undefined;
    if (templateSpec.useDepths) {
      if (options.depths) {
        depths = context !== options.depths[0] ? [context].concat(options.depths) : options.depths;
      } else {
        depths = [context];
      }
    }

    function main(context /*, options*/) {
      return '' + templateSpec.main(container, context, container.helpers, container.partials, data, blockParams, depths);
    }
    main = executeDecorators(templateSpec.main, main, container, options.depths || [], data, blockParams);
    return main(context, options);
  }
  ret.isTop = true;

  ret._setup = function (options) {
    if (!options.partial) {
      container.helpers = container.merge(options.helpers, env.helpers);

      if (templateSpec.usePartial) {
        container.partials = container.merge(options.partials, env.partials);
      }
      if (templateSpec.usePartial || templateSpec.useDecorators) {
        container.decorators = container.merge(options.decorators, env.decorators);
      }
    } else {
      container.helpers = options.helpers;
      container.partials = options.partials;
      container.decorators = options.decorators;
    }
  };

  ret._child = function (i, data, blockParams, depths) {
    if (templateSpec.useBlockParams && !blockParams) {
      throw new _exception2['default']('must pass block params');
    }
    if (templateSpec.useDepths && !depths) {
      throw new _exception2['default']('must pass parent depths');
    }

    return wrapProgram(container, i, templateSpec[i], data, 0, blockParams, depths);
  };
  return ret;
}

function wrapProgram(container, i, fn, data, declaredBlockParams, blockParams, depths) {
  function prog(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

    var currentDepths = depths;
    if (depths && context !== depths[0]) {
      currentDepths = [context].concat(depths);
    }

    return fn(container, context, container.helpers, container.partials, options.data || data, blockParams && [options.blockParams].concat(blockParams), currentDepths);
  }

  prog = executeDecorators(fn, prog, container, depths, data, blockParams);

  prog.program = i;
  prog.depth = depths ? depths.length : 0;
  prog.blockParams = declaredBlockParams || 0;
  return prog;
}

function resolvePartial(partial, context, options) {
  if (!partial) {
    if (options.name === '@partial-block') {
      partial = options.data['partial-block'];
    } else {
      partial = options.partials[options.name];
    }
  } else if (!partial.call && !options.name) {
    // This is a dynamic partial that returned a string
    options.name = partial;
    partial = options.partials[partial];
  }
  return partial;
}

function invokePartial(partial, context, options) {
  options.partial = true;
  if (options.ids) {
    options.data.contextPath = options.ids[0] || options.data.contextPath;
  }

  var partialBlock = undefined;
  if (options.fn && options.fn !== noop) {
    options.data = _base.createFrame(options.data);
    partialBlock = options.data['partial-block'] = options.fn;

    if (partialBlock.partials) {
      options.partials = Utils.extend({}, options.partials, partialBlock.partials);
    }
  }

  if (partial === undefined && partialBlock) {
    partial = partialBlock;
  }

  if (partial === undefined) {
    throw new _exception2['default']('The partial ' + options.name + ' could not be found');
  } else if (partial instanceof Function) {
    return partial(context, options);
  }
}

function noop() {
  return '';
}

function initData(context, data) {
  if (!data || !('root' in data)) {
    data = data ? _base.createFrame(data) : {};
    data.root = context;
  }
  return data;
}

function executeDecorators(fn, prog, container, depths, data, blockParams) {
  if (fn.decorator) {
    var props = {};
    prog = fn.decorator(prog, props, container, depths && depths[0], data, blockParams, depths);
    Utils.extend(prog, props);
  }
  return prog;
}


},{"./base":2,"./exception":5,"./utils":18}],17:[function(require,module,exports){
// Build out our basic SafeString type
'use strict';

exports.__esModule = true;
function SafeString(string) {
  this.string = string;
}

SafeString.prototype.toString = SafeString.prototype.toHTML = function () {
  return '' + this.string;
};

exports['default'] = SafeString;
module.exports = exports['default'];


},{}],18:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.extend = extend;
exports.indexOf = indexOf;
exports.escapeExpression = escapeExpression;
exports.isEmpty = isEmpty;
exports.createFrame = createFrame;
exports.blockParams = blockParams;
exports.appendContextPath = appendContextPath;
var escape = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#x27;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

var badChars = /[&<>"'`=]/g,
    possible = /[&<>"'`=]/;

function escapeChar(chr) {
  return escape[chr];
}

function extend(obj /* , ...source */) {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
        obj[key] = arguments[i][key];
      }
    }
  }

  return obj;
}

var toString = Object.prototype.toString;

exports.toString = toString;
// Sourced from lodash
// https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
/* eslint-disable func-style */
var isFunction = function isFunction(value) {
  return typeof value === 'function';
};
// fallback for older versions of Chrome and Safari
/* istanbul ignore next */
if (isFunction(/x/)) {
  exports.isFunction = isFunction = function (value) {
    return typeof value === 'function' && toString.call(value) === '[object Function]';
  };
}
exports.isFunction = isFunction;

/* eslint-enable func-style */

/* istanbul ignore next */
var isArray = Array.isArray || function (value) {
  return value && typeof value === 'object' ? toString.call(value) === '[object Array]' : false;
};

exports.isArray = isArray;
// Older IE versions do not directly support indexOf so we must implement our own, sadly.

function indexOf(array, value) {
  for (var i = 0, len = array.length; i < len; i++) {
    if (array[i] === value) {
      return i;
    }
  }
  return -1;
}

function escapeExpression(string) {
  if (typeof string !== 'string') {
    // don't escape SafeStrings, since they're already safe
    if (string && string.toHTML) {
      return string.toHTML();
    } else if (string == null) {
      return '';
    } else if (!string) {
      return string + '';
    }

    // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.
    string = '' + string;
  }

  if (!possible.test(string)) {
    return string;
  }
  return string.replace(badChars, escapeChar);
}

function isEmpty(value) {
  if (!value && value !== 0) {
    return true;
  } else if (isArray(value) && value.length === 0) {
    return true;
  } else {
    return false;
  }
}

function createFrame(object) {
  var frame = extend({}, object);
  frame._parent = object;
  return frame;
}

function blockParams(params, ids) {
  params.path = ids;
  return params;
}

function appendContextPath(contextPath, id) {
  return (contextPath ? contextPath + '.' : '') + id;
}


},{}],19:[function(require,module,exports){
// Create a simple path alias to allow browserify to resolve
// the runtime on a supported path.
module.exports = require('./dist/cjs/handlebars.runtime')['default'];

},{"./dist/cjs/handlebars.runtime":1}],20:[function(require,module,exports){
module.exports = require("handlebars/runtime")["default"];

},{"handlebars/runtime":19}],21:[function(require,module,exports){
(function (process){
// Generated by CoffeeScript 1.7.1
(function() {
  var getNanoSeconds, hrtime, loadTime;

  if ((typeof performance !== "undefined" && performance !== null) && performance.now) {
    module.exports = function() {
      return performance.now();
    };
  } else if ((typeof process !== "undefined" && process !== null) && process.hrtime) {
    module.exports = function() {
      return (getNanoSeconds() - loadTime) / 1e6;
    };
    hrtime = process.hrtime;
    getNanoSeconds = function() {
      var hr;
      hr = hrtime();
      return hr[0] * 1e9 + hr[1];
    };
    loadTime = getNanoSeconds();
  } else if (Date.now) {
    module.exports = function() {
      return Date.now() - loadTime;
    };
    loadTime = Date.now();
  } else {
    module.exports = function() {
      return new Date().getTime() - loadTime;
    };
    loadTime = new Date().getTime();
  }

}).call(this);

}).call(this,require('_process'))

},{"_process":22}],22:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],23:[function(require,module,exports){
var now = require('performance-now')
  , global = typeof window === 'undefined' ? {} : window
  , vendors = ['moz', 'webkit']
  , suffix = 'AnimationFrame'
  , raf = global['request' + suffix]
  , caf = global['cancel' + suffix] || global['cancelRequest' + suffix]

for(var i = 0; i < vendors.length && !raf; i++) {
  raf = global[vendors[i] + 'Request' + suffix]
  caf = global[vendors[i] + 'Cancel' + suffix]
      || global[vendors[i] + 'CancelRequest' + suffix]
}

// Some versions of FF have rAF but not cAF
if(!raf || !caf) {
  var last = 0
    , id = 0
    , queue = []
    , frameDuration = 1000 / 60

  raf = function(callback) {
    if(queue.length === 0) {
      var _now = now()
        , next = Math.max(0, frameDuration - (_now - last))
      last = next + _now
      setTimeout(function() {
        var cp = queue.slice(0)
        // Clear queue here to prevent
        // callbacks from appending listeners
        // to the current frame's queue
        queue.length = 0
        for(var i = 0; i < cp.length; i++) {
          if(!cp[i].cancelled) {
            try{
              cp[i].callback(last)
            } catch(e) {
              setTimeout(function() { throw e }, 0)
            }
          }
        }
      }, Math.round(next))
    }
    queue.push({
      handle: ++id,
      callback: callback,
      cancelled: false
    })
    return id
  }

  caf = function(handle) {
    for(var i = 0; i < queue.length; i++) {
      if(queue[i].handle === handle) {
        queue[i].cancelled = true
      }
    }
  }
}

module.exports = function(fn) {
  // Wrap in a new function to prevent
  // `cancel` potentially being assigned
  // to the native rAF function
  return raf.call(global, fn)
}
module.exports.cancel = function() {
  caf.apply(global, arguments)
}

},{"performance-now":21}],24:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports["default"] = {
	components: {},
	mount: function mount(name, component) {
		if (typeof this.components[name] === "undefined") {
			this.components[name] = component;
		}
	}
};
module.exports = exports["default"];

},{}],25:[function(require,module,exports){
'use strict';

var EventDispatcher = require('utils/EventDispatcher');
var $setTimeout = require('utils/$setTimeout');
var _ = require('utils/utils');
var dom = require('utils/dom');

var optionalParam = /\((.*?)\)/g;
var namedParam = /(\(\?)?:\w+/g;
var splatParam = /\*\w+/g;
var escapeRegExp = /[\-{}\[\]+?.,\\\^$|#\s]/g;

var StateProvider = {

  states: {},

  _routeToRegExp: function _routeToRegExp(route) {
    route = route.replace(escapeRegExp, '\\$&').replace(optionalParam, '(?:$1)?').replace(namedParam, function (match, optional) {
      return optional ? match : '([^/?]+)';
    }).replace(splatParam, '([^?]*?)');
    return new RegExp('^' + route + '(?:\\?([\\s\\S]*))?$');
  },

  state: function state(stateName, options) {

    var options = _.extend({
      controller: null,
      route: null
    }, // deepState
    // stickyState: function(){},
    // deepStateRedirect: {
    //        default: { state: "foo.bar.baz.defaultSubState", params: { defaultStateParam1: "99" } },
    //        fn: function($dsr$) {}
    // }
    options || {});

    if (typeof options.controller == "object") {
      var controller = options.controller;
      var method = options.method || 'index';
      options.controller = controller[method].bind(controller);
    }

    //generate regular expression from route
    //extract namesParameters from route
    if (options.route) {
      options.regExp = this._routeToRegExp(options.route);
      options.paramList = this._extractParamsFromRoute(options.route);
    }

    this.states[stateName] = options;

    //chaining states
    return this;
  },

  initialize: function initialize(options) {
    this.lastState = null;
    this.base = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search;
    EventDispatcher.on('statechange', this.onStateChange, this);
    dom.addEventListener(window, 'hashchange', this._onHashChange.bind(this));
  },

  //state chnage is trigger as internal events.
  //if statechnage is triggered, ignore following hashchnage to avoid infinite loops
  onStateChange: function onStateChange(state, stateParams) {

    if (state in this.states) {

      //if the state as a route, save it to the url
      if (this.states[state].route) {
        clearTimeout(this.bypassTimer);
        this.bypass = true;
        window.location.hash = '#' + this.hrefFromParams(state, stateParams);
        this.bypassTimer = $setTimeout(function () {
          this.bypass = false;
        }, 50, this);
      }

      if (this.states[state].controller) {
        this.states[state].controller(stateParams);
      }
    }
  },

  _extractParamsFromRoute: function _extractParamsFromRoute(route) {
    var args = route.match(namedParam);
    var params = {};
    for (var i = 0; i < args.length; i++) {
      args[i] = args[i].replace(':', '');
    }
    return args;
  },

  hrefFromParams: function hrefFromParams(state, params) {

    if (!state in this.states) {
      return 'no state found :: ' + state;
    }

    if (!this.states[state].route) {
      return 'no route found';
    }

    var route = this.states[state].route;
    for (var p in params) {
      var namedParamRegExp = new RegExp('(\\(\\?)?:' + p, 'g');
      var optionalParamRegExp = new RegExp('\\(' + params[p] + '\\)', 'g');
      route = route.replace(namedParamRegExp, params[p]).replace(optionalParamRegExp, params[p]);
    }

    return route;
  },

  getHashState: function getHashState() {
    this._onHashChange();
  },

  _onHashChange: function _onHashChange() {

    if (this.bypass) {
      return;
    }

    var self = this,
        state = this.getState(),
        args = [];

    //this.state = state;

    for (var s in this.states) {

      if (this.states[s].regExp && this.states[s].regExp.test(state)) {
        state.replace(this.states[s].regExp, function () {
          for (var a = 1; a < arguments.length - 3; a++) {
            args.push(arguments[a]);
          }
        });
        var stateParams = {};
        for (var i = 0; i < args.length; i++) {
          stateParams[self.states[s].paramList[i]] = args[i];
        }
        //console.log("_onHashChange", s, state, this.states[s].controller );

        EventDispatcher.trigger('statechange', [s, stateParams]);

        // if (_.isFunction(this.states[s].controller)) {
        //   this.states[s].controller(stateParams);
        // }
        break;
      }
    }
  },

  getState: function getState() {
    return document.location.hash.replace(/^#\!?\/?/, '');
  }

};

module.exports = StateProvider;

},{"utils/$setTimeout":65,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],26:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports['default'] = {

  /**
   *  new event callback on the view chanel
   */
  on: function on(event, callback, ctx) {

    this.callbackStack = this.callbackStack || {};

    this.callbackStack[event] = this.callbackStack[event] || { evtName: event, evtCbArr: [] };
    this.callbackStack[event].evtCbArr.push({ fn: callback, ctx: typeof ctx != 'undefined' ? ctx : this });
  },

  /**
   *  new event callback on the view chanel
   */
  off: function off(event, callback) {

    if (typeof this.callbackStack === "undefined") {
      return;
    }

    if (typeof this.callbackStack[event] != 'undefined') {
      for (var i = 0, l = this.callbackStack[event].evtCbArr.length; i < l; i++) {
        if (this.callbackStack[event].evtCbArr[i].fn == callback) {
          this.callbackStack[event].evtCbArr.splice(i, 1);
          i--;
          l--;
        }
      }
    }
  },

  /**
   *  trigger event on the view chanel
   */
  trigger: function trigger(event, params) {

    if (typeof params != 'undefined') {
      var params = typeof params == 'object' && typeof params.length != 'undefined' ? params : [params];
    }

    if (this.callbackStack && typeof this.callbackStack[event] != 'undefined') {

      for (var i = 0, l = this.callbackStack[event].evtCbArr.length; i < l; i++) {
        var handler = this.callbackStack[event].evtCbArr[i];
        if (handler) {
          if (typeof params != 'undefined') {
            handler.fn.apply(handler.ctx, params);
          } else {
            handler.fn.call(handler.ctx);
          }
        }
      }
    }

    this.triggerMethod(event, params);
  },

  /**
   *  trigger eventMethod ( this[ 'on' + methodName ]() ) on the view
   */
  triggerMethod: function triggerMethod(event, params) {
    var eventMethodName = 'on' + event.charAt(0).toUpperCase() + event.slice(1);
    if (typeof this[eventMethodName] != 'undefined') {
      if (typeof params != 'undefined') {
        var params = typeof params == 'object' && typeof params.length != 'undefined' ? params : [params];
        this[eventMethodName].apply(this, params);
      } else {
        this[eventMethodName]();
      }
    }
  }

};
module.exports = exports['default'];

},{}],27:[function(require,module,exports){
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _utilsEventDispatcher = require('./utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

require("./components");

var _componentsCoreRoot = require("./components/core-root");

var _componentsCoreRoot2 = _interopRequireDefault(_componentsCoreRoot);

var support = require('utils/BrowserSupport');
var $setTimeout = require('utils/$setTimeout');
var dom = require('utils/dom');

var $Object = require('./core/$Object');

var requestAnimationFrame = window.requestAnimationFrame || require('raf');

var Application = {

  initialize: function initialize() {

    /**
     * DEMO OF HANDLER API
     */
    _utilsEventDispatcher2['default'].setHandler('application', (function (key) {
      return key !== "tokenKey" ? this[key] : key + 'is not allowed on application handler';
    }).bind(this));

    this.root = new _componentsCoreRoot2['default']({
      el: 'body',
      model: new $Object()
    });

    this.root.on('after:render', this.onAllRendereded, this);
    this.root.on('after:resize', this.onAllResized, this);
    this.root.on('after:show', this.onAllViewsReady, this);

    this.root.render();
  },

  /** 
   * we wait for all views and subviews to be rendered
   * if we need to edit the DOM after the render (ex: change data-attributes, retrieve dom node bu class etc)
   * we need to user afterRender. It is the same as 'componentDidMount' from React
   * 
   * when all is rendered(added to the dom) we trigger a resize from root to subnodes
   * this avoid the browser from triggering too many reflow/repain
   *  if parentView need information about the size of its children after the resize pass, it can use the afterResize method which will be triggered after all it's childview has resized
   *
   * 
   * when all has been resized we trigger a show from subnode to rootnode so that the browser will not need to invalute GPU texture if subview change the parentview
   * //TODO: check if this is really usefull
   *
   * 
   */

  onAllRendereded: function onAllRendereded() {

    console.log("onAllRendereded");
    this.root.off('after:render', this.onAllRendereded, this);
    this.root.triggerMethod('resize');
  },

  onAllResized: function onAllResized() {
    console.log("onAllResized");
    this.root.off('after:resize', this.onAllResized, this);
    this.root.triggerMethod('show');
  },

  onAllViewsReady: function onAllViewsReady() {
    console.log("onAllViewsReady");
    this.root.off('after:show', this.onAllViewsReady);
    this.lastFrame = Date.now();
    //this.root.triggerMethod('ready');
    requestAnimationFrame(this.render.bind(this));
  },

  /**
   * every component can bind updates to requestAnimationFrame
   * to imporve performances only one requestAnimationFrame callback is binded to root's render method
   * then the root will dispatch the 'enterframe' callback to ervery subviews
   * if subview is '.visible' then it's enterFrame() method will be called
   */
  render: function render() {
    requestAnimationFrame(this.render.bind(this));

    this.root.enterFrame();

    if (!this.ready) {

      this.ready = true;
      _utilsEventDispatcher2['default'].on("resizeend", this.onResizeEnd, this);
    }
  },

  onResizeEnd: function onResizeEnd() {
    this.root.triggerMethod('resize');
  }
};

module.exports = Application;

},{"./components":35,"./components/core-root":30,"./core/$Object":56,"./utils/EventDispatcher":68,"raf":23,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/dom":71}],28:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _core$Object = require('./core/$Object');

var _core$Object2 = _interopRequireDefault(_core$Object);

var ApplicationLayout = new _core$Object2['default']();

exports['default'] = ApplicationLayout;
module.exports = exports['default'];

},{"./core/$Object":56}],29:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsEventDispatcher = require('../../utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var support = require('../../utils/BrowserSupport');
var Component = require('../../core/Component');

var CoreCover = (function (_Component) {
  _inherits(CoreCover, _Component);

  function CoreCover(options) {
    _classCallCheck(this, CoreCover);

    _get(Object.getPrototypeOf(CoreCover.prototype), 'constructor', this).call(this, options);
  }

  _createClass(CoreCover, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(CoreCover.prototype), 'afterRender', this).call(this);

      if (this.$el instanceof HTMLImageElement) {

        var self = this;
        this.cacheImage = new Image();
        this.cacheImage.onload = function () {
          this.onload = null;
          self.onResize();
        };
        this.cacheImage.src = this.$el.src;
      }

      _utilsEventDispatcher2['default'].on('homesubnav:opened', this.onHomeSubNavOpened, this);
    }
  }, {
    key: 'onResize',
    value: function onResize() {

      _get(Object.getPrototypeOf(CoreCover.prototype), 'onResize', this).call(this);

      this.coverWidth = this.el.clientWidth;
      this.coverHeight = this.el.clientHeight;
      this.coverParent = this.el.parentNode;
      this.coverParentWidth = this.coverParent.clientWidth;
      this.coverParentHeight = this.coverParent.clientHeight;

      if (this.coverWidth !== 0 && this.coverHeight !== 0) {
        this.coverRatio = this.coverWidth / this.coverHeight;
        this.coverSwitcher = this.coverParentWidth / this.coverRatio > this.coverParentHeight;
        this.coverComputedWidth = this.coverSwitcher ? this.coverParentWidth : this.coverParentHeight * this.coverRatio;
        this.coverComputedHeight = this.coverSwitcher ? this.coverParentWidth / this.coverRatio : this.coverParentHeight;
        this.scale = this.coverComputedWidth / this.coverWidth;
        this.x = (this.coverParentWidth - this.coverComputedWidth) / 2;
        this.y = (this.coverParentHeight - this.coverComputedHeight) / 2;
      }

      this.$el.style[support.transformOrigin] = "0 0";
      this.$el.style[support.transform] = 'translate(' + this.x + 'px, ' + this.y + 'px) scale(' + this.scale + ')' + support.translateZ;
    }
  }, {
    key: 'onHomeSubNavOpened',
    value: function onHomeSubNavOpened() {

      //console.log('From Core cover: onHomeSubNavOpened');

    }
  }]);

  return CoreCover;
})(Component);

exports['default'] = CoreCover;
module.exports = exports['default'];

},{"../../core/Component":57,"../../utils/BrowserSupport":67,"../../utils/EventDispatcher":68}],30:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Component = require('../../core/Component');

var Root = (function (_Component) {
  _inherits(Root, _Component);

  function Root(options) {
    _classCallCheck(this, Root);

    _get(Object.getPrototypeOf(Root.prototype), 'constructor', this).call(this, options);
  }

  return Root;
})(Component);

exports['default'] = Root;
module.exports = exports['default'];

},{"../../core/Component":57}],31:[function(require,module,exports){
'use strict';

var EventDispatcher = require('utils/EventDispatcher');
var EventList = require('utils/EventList');
var support = require('utils/BrowserSupport');
var Component = require('core/Component');
var $setTimeout = require('utils/$setTimeout');
var _ = require('utils/utils');
var dom = require('utils/dom');

function SliderView(options) {

  Component.call(this, options);

  this.template = require('./template');

  this.props.set({
    value: 0,
    min: 0,
    max: 1,
    invert: false,
    orientation: "horizontal",
    onSlide: function onSlide() {},
    onStop: function onStop() {}
  });
}

SliderView.prototype = Object.create(Component.prototype);
SliderView.prototype.constructor = SliderView;

SliderView.prototype.afterRender = function () {

  Component.prototype.afterRender.call(this);

  this.range = this.props.max - this.props.min;
  this.sizeProp = this.props.orientation === "horizontal" ? 'width' : 'height';
  this.handlerSizeProp = this.props.orientation === "horizontal" ? 'handlerWidth' : 'handlerHeight';
  this.offsetProp = this.props.orientation === "horizontal" ? 'x' : 'y';
  this.handlerPositionProp = this.props.orientation === "horizontal" ? 'x' : 'y';
  this.offset = { x: 0, y: 0 };
  this.handler = { x: 0, y: 0 };

  this.bindMethods(["onSliderDown", "onPointerMove", "onPointerUp"]);

  this.$handler = this.$once('.js-handler');
  dom.addEventListener(this.el, support.pointerdown, this.onSliderDown);
  dom.addEventListener(document, support.pointermove, this.onPointerMove);
  dom.addEventListener(document, support.pointerup, this.onPointerUp);

  this.props.on('change:value', this.update, this);
  this.update();
};

SliderView.prototype.onSliderDown = function (event) {

  this.isPointerDown = true;
  var pageX = support.touch ? event.touches[0].pageX || event.changedTouches[0].pageX : event.clientX;
  var pageY = support.touch ? event.touches[0].pageY || event.changedTouches[0].pageY : event.clientY;
  this.track(pageX, pageY);
};

SliderView.prototype.onPointerMove = function (event) {

  if (!this.isPointerDown) {
    return;
  }
  if (event) {
    event.preventDefault();
  }
  var pageX = support.touch ? event.touches[0].pageX || event.changedTouches[0].pageX : event.clientX;
  var pageY = support.touch ? event.touches[0].pageY || event.changedTouches[0].pageY : event.clientY;
  this.track(pageX, pageY);
};

SliderView.prototype.onPointerUp = function () {
  if (!this.isPointerDown) {
    return;
  }
  this.isPointerDown = false;
  this.props.onStop(this.props.value);
};

SliderView.prototype.track = function (pageX, pageY) {

  var n = this.props.orientation === "horizontal" ? pageX : pageY;
  var q = this.props.invert ? this.offset[this.offsetProp] + this[this.sizeProp] - n : n - this.offset[this.offsetProp];

  if (q >= 0 && q <= this[this.sizeProp]) {
    this.handler[this.handlerPositionProp] = q;
    this.props.value = this.props.min + this.range / this[this.sizeProp] * q;
    this.props.onSlide(this.props.value);
  }
};

SliderView.prototype.onResize = function () {

  Component.prototype.onResize.call(this);

  this.height = this.el.clientHeight;
  this.width = this.el.clientWidth;

  this.hanlderWidth = this.$handler.clientWidth;
  this.hanlderHeight = this.$handler.clientHeight;

  this.hanlderHalfWidth = this.hanlderWidth >> 1;
  this.hanlderHalfHeight = this.hanlderHeight >> 1;

  var offset = this.getPosition(this.el, true);
  this.offset.x = offset.left;
  this.offset.y = offset.top;
};

SliderView.prototype.update = function () {
  this.range = this.props.max - this.props.min;
  this.handler[this.handlerPositionProp] = (this.props.value - this.props.min) / this.range * this[this.sizeProp];
};

SliderView.prototype.enterFrame = function () {
  Component.prototype.enterFrame.call(this);
  if (this.$handler && !isNaN(this.handler.x - this.hanlderHalfWidth)) {
    this.transform(this.$handler, {
      translateX: this.handler.x + 'px',
      translateY: this.handler.y + 'px',
      force3D: true
    });
  }
};

SliderView.prototype.destroy = function () {
  dom.removeEventListener(this.el, support.pointerdown, this.onSliderDown);
  dom.removeEventListener(document, support.pointermove, this.onPointerMove);
  dom.removeEventListener(document, support.pointerup, this.onPointerUp);
  this.props.off('change:value', this.update);
  Component.prototype.destroy.call(this);
};

module.exports = SliderView;

},{"./template":32,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/EventList":69,"utils/dom":71,"utils/utils":73}],32:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = require('hbsfy/runtime');
module.exports = HandlebarsCompiler.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"yt-seek js-handler\"></div>";
},"useData":true});

},{"hbsfy/runtime":20}],33:[function(require,module,exports){
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

//TODO add it to BrowerSupport
var EventDispatcher = require('utils/EventDispatcher');
var EventList = require('utils/EventList');
var support = require('utils/BrowserSupport');
var Component = require('core/Component');
var $setTimeout = require('utils/$setTimeout');
var dom = require('utils/dom');
var _ = require('utils/utils');
var requestFullScreen = require('utils/requestFullScreen');
var Slider = require('components/core-slider');

var UUID = 0;

function YoutubeCustomPlayer(options) {

    Component.call(this, options);

    this.visible = true;

    this.events.set({
        'click .yt-big-play-btn': 'togglePlay',
        'click .yt-mute-btn': 'toggleSound',
        'click .yt-play-btn': 'togglePlay',
        'click .yt-fullscreen-btn': 'toggleFullScreen'
    });

    this.template = require('./template');

    //TODO: merge attribute and props
    this.props.set({
        wmode: 'transparent',
        autoHideControls: false,
        src: null,
        controls: true,
        autoplay: 0,
        autoplayOnce: false,
        autoplayDelay: 0,
        mute: false,
        loop: false,
        poster: '',
        showinfo: 0,
        //origin: this.origin,
        rel: 0,
        stopAllVideos: false,
        uuid: 'yt-' + UUID,
        useNative: false,
        forceQuality: false,
        callback: function callback() {}
    });

    UUID++;

    this.paused = null;
    this.enabled = true;

    this.addClass('yt-player');
}

YoutubeCustomPlayer.prototype = Object.create(Component.prototype);
YoutubeCustomPlayer.prototype.constructor = YoutubeCustomPlayer;

YoutubeCustomPlayer.prototype.disable = function () {

    this.enabled = false;

    this.pause();

    if (document.getElementById(this.props.uuid + '-video')) {
        document.getElementById(this.props.uuid + '-video').style.display = "none";
    }

    Component.prototype.disable.call(this);
};

YoutubeCustomPlayer.prototype.enable = function () {

    this.enabled = true;

    if (this.props.autoplay && !this.props.autoplayOnce) {
        this.play();
    }

    if (document.getElementById(this.props.uuid + '-video')) {
        document.getElementById(this.props.uuid + '-video').style.display = "block";
    }

    Component.prototype.enable.call(this);
};

YoutubeCustomPlayer.prototype.render = function () {

    this.props.controls = this.props.controls == "false" || this.props.controls == "0" ? false : true;
    this.props.autoplay = this.props.autoplay == "false" || this.props.autoplay == "0" ? false : true;
    this.props.autoplayOnce = this.props.autoplayOnce == "false" || this.props.autoplayOnce == "0" ? false : true;
    this.props.mute = this.props.mute == "false" || this.props.mute == "0" ? false : true;

    if (this.props.src === null && !this.props.useNative) {
        _.warn("Sorry, props.src key must be specified");
        return;
    }

    //keep reference to this Player
    YoutubeCustomPlayer.players[this.props.uuid] = this;

    this.overrideTemplate();

    Component.prototype.render.call(this);
};

YoutubeCustomPlayer.prototype.overrideTemplate = function () {

    this.template = "";

    if (this.props.useNative) {
        this.template += '<video class="yt-video-wrapper js-native-video" poster="' + (this.props.poster || '') + '">';
        this.template += '    <content></content>';
        this.template += '</video>';
    } else {
        this.template += '<div class="yt-video-wrapper" id="' + (this.props.uuid || '') + '-video" style=""></div>';
    }

    this.template += '<div class="yt-poster-wrapper" style="background-image:url( ' + (this.props.poster || '') + ' ); background-size:cover; background-position:center center;>';
    //this.template += '    <img src="'+(this.props.poster||'')+'" class="yt-poster"/>';
    this.template += '</div>';
    this.template += '<div class="yt-loading-wrapper">';
    this.template += '    <div class="yt-loading"></div>';
    this.template += '</div>';
    this.template += '<div class="yt-big-play-btn u-show-tablet">Play</div>';
    this.template += '<span class="yt-bar yt-bar-top u-mp-hide"></span>';
    this.template += '<span class="yt-bar yt-bar-bottom u-mp-hide"></span>';

    if (this.props.controls) {
        this.template += '<div class="yt-wrapper-controls">';
        this.template += '    <div class="yt-mute-btn u-hide-tablet">';
        this.template += '        <span class="rect"></span>';
        this.template += '        <span class="rect"></span>';
        this.template += '        <span class="rect"></span>';
        this.template += '        <span class="rect"></span>';
        this.template += '        <span class="rect"></span>';
        this.template += '    </div>';
        this.template += '    <div class="yt-fullscreen-btn u-mp-hide">';
        this.template += '        <svg version="1.1" fill="#fff" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="155 40.9 100 80.2" style="enable-background:new 155 40.9 100 80.2;" xml:space="preserve">';
        this.template += '            <polygon points="183.3,121.1 155,121.1 155,92.8 162.8,92.8 162.8,113.3 183.3,113.3  "/>';
        this.template += '            <polygon points="162.8,69.2 155,69.2 155,40.9 183.3,40.9 183.3,48.7 162.8,48.7  "/>';
        this.template += '            <polygon points="255,69.2 247.2,69.2 247.2,48.7 226.7,48.7 226.7,40.9 255,40.9  "/>';
        this.template += '            <polygon points="255,121.1 226.7,121.1 226.7,113.3 247.2,113.3 247.2,92.8 255,92.8  "/>';
        this.template += '        </svg>';
        this.template += '    </div>';
        this.template += '    <div class="yt-play-btn o-wrapper--valign u-mp-hide"></div>';
        this.template += '    <div class="yt-skin-wrapper">';
        this.template += '        <div class="yt-controls-wrapper">';
        this.template += '            <div class="yt-time"></div>';
        this.template += '            <core-slider data-ref="timeline" class="yt-timeline" data-on-slide="onSliderSlide" data-on-stop="onSliderStop"></core-slider>';
        this.template += '            <div class="yt-mute-btn"></div>';
        this.template += '        </div>';
        this.template += '    </div>';
        this.template += '</div>';
    }
};

YoutubeCustomPlayer.prototype.afterRender = function () {

    if (this.props.src === null && !this.props.useNative) {
        return;
    }

    this.origin = window.location.origin;

    Component.prototype.afterRender.call(this);

    if (support.ipad) {
        dom.addClass('yt-ipad', document.getElementsByTagName('html')[0]);
    }

    if (support.iphone) {
        dom.addClass('yt-iphone', document.getElementsByTagName('html')[0]);
    }

    if (support.touch) {
        dom.addClass('yt-touch', document.getElementsByTagName('html')[0]);
    }

    if (requestFullScreen.requestFn) {
        dom.addClass('yt-support-fullscreen', document.getElementsByTagName('html')[0]);
    }

    this.ytplayer = null;
    this.paused = true;
    this.muted = false;
    this.seeksliding = false;
    this.duration = 0;
    this.currentTime = 0;
    this.isFullScreen = false;
    this.progressTimer = null;
    this.isFirstPlay = true;
    this.mouseMoveTimer = null;
    this.isControlsHidden = false;

    // this.test = dom.query('.js-play-auto');

    this.bindMethods(["onFullScrenChange", "onPlayerReady", "onPlayerStateChange", "onPlayerError"]);

    if (this.props.controls) {
        //TODO:data-bind
        this.$timecount = this.$once('.yt-time');
    }

    if (!this.props.useNative) {
        this._createYTPlayer = this.createYTPlayer.bind(this);
        YoutubeCustomPlayer.ready(this._createYTPlayer);
    } else {
        this.createHTML5Player();
        this.addClass('yt-native');
    }

    //shame
    // this.url = location.search.substring(1);

    // if(this.url != '' && Layout.page == 'director-details'){
    //   this.node = document.getElementById(this.url).children[0];
    // }

    //end shame
};

YoutubeCustomPlayer.prototype.onSliderSlide = function (ratio) {
    this.seeksliding = true;
    this.stopProgress();
};

YoutubeCustomPlayer.prototype.onSliderStop = function (ratio) {
    //console.log(  'YoutubeCustomPlayer.prototype.onSliderStop', ratio);
    this.seeksliding = false;
    this.seek(ratio * this.duration);
    this.startProgress();
};

YoutubeCustomPlayer.prototype.createYTPlayer = function () {

    this.ytplayer = new YT.Player(this.props.uuid + '-video', {
        height: '100%',
        width: '100%',
        videoId: this.props.src,
        playerVars: {
            wmode: this.props.wmode,
            controls: 0,
            //origin: this.origin,
            showinfo: this.props.showinfo ? 1 : 0,
            autoplay: 0,
            loop: 0, //dont use yt loop, use the player's loop mecanism (as this player may become a reusable video component)
            rel: this.props.rel,
            playlist: this.props.src,
            modestbranding: 1,
            html5: 1
        },
        events: {
            onReady: this.onPlayerReady,
            onStateChange: this.onPlayerStateChange,
            onError: this.onPlayerError
        }
    });
};

YoutubeCustomPlayer.prototype.createHTML5Player = function () {
    this.$videoTag = this.$once('.js-native-video');
    this.$videoTag.addEventListener('loadedmetadata', this.onPlayerReady, false);
    this.$videoTag.addEventListener('play', this.onPlayerStateChange, false);
    this.$videoTag.addEventListener('pause', this.onPlayerStateChange, false);
    this.$videoTag.addEventListener('ended', this.onPlayerStateChange, false);
    this.$videoTag.addEventListener('progress', this.onPlayerStateChange, false);
    this.$videoTag.addEventListener('error', this.onPlayerError, false);
};

YoutubeCustomPlayer.prototype.onPlayerReady = function (e) {

    //shame
    this.url = location.search.substring(1);
    //end shame

    this.ready = true;
    this.addClass('yt-ready');
    this.duration = this.getDuration();
    this.addListenners();
    this.props.callback(this);
    this.trigger('ready');
    this.onResize();

    EventDispatcher.publish('youtubeplayer:ready');

    if (support.touch && support.smallscreen || support.ipad) {} else {
        console.log('la', this.props.autoplay, this.enabled);

        if (this.props.autoplay && this.paused !== false && this.enabled) {

            if (this.props.autoplayDelay) {
                $setTimeout(function () {
                    this.play();
                }, this.props.autoplayDelay, this);
            }
            // else if( this.url != '' && Layout.page == 'director-details' ){

            // }
            else {
                    this.play();
                }
        }
    }
};

YoutubeCustomPlayer.prototype.onPlayerError = function (e) {
    this.trigger('error');
    console.log('onPlayerError');
};

YoutubeCustomPlayer.prototype.onPlayerStateChange = function (event) {

    console.log('onPlayerStateChange', event.data);

    if (this.ischangingQuality /*|| Layout.page === 'home'*/) {
            return;
        }

    // if( this.props.forceQuality && !this.props.useNative && event.target.getPlaybackQuality() !== this.props.forceQuality ){

    //     if ( event.data == YT.PlayerState.PLAYING ) {

    //         this.ischangingQuality = true;

    //         window.requestAnimationFrame(() =>
    //         {
    //             event.target.stopVideo();
    //             event.target.setPlaybackQuality( this.props.forceQuality );  // <-- WORKS!
    //             event.target.playVideo();
    //             this.ischangingQuality = false;
    //         })

    //     }
    // }

    if (this.props.mute == true) {
        this.mute();
    }
    if (event.data == -1) {//unstarted

    }
    if (event.type == "ended" || event.data == 0) {
        //ended
        this.onPlayerEnded();
    }
    if (event.type == "play" || event.data == 1) {
        //play
        this.removeClass('is-loading');
        this.onPlayerPlay();
    }
    if (event.type == "pause" || event.data == 2) {
        //pause
        this.onPlayerPause();
    }
    if (event.type == "progress") {
        //buffer
        this.onPlayerBuffer();
    }
};

YoutubeCustomPlayer.prototype.onPlayerBuffer = function () {
    this.trigger('buffer');
    this.addClass('is-loading');
};

YoutubeCustomPlayer.prototype.addListenners = function () {

    if (!this.props.controls) {
        return;
    }

    EventDispatcher.on('key:escape', this.cancelFullScreen, this);

    //show/hide controls on muser activity
    if (this.props.autoHideControls) {
        EventDispatcher.on('pointermove', this.toggleControls, this);
    }
};

YoutubeCustomPlayer.prototype.toggleSound = function (e) {
    if (this.muted) {
        this.unmute();
    } else {
        this.mute();
    }
};

YoutubeCustomPlayer.prototype.toggleControls = function () {
    clearTimeout(this.mouseMoveTimer);
    if (this.isControlsHidden) {
        this.isControlsHidden = false;
        this.removeClass('yt-no-controls');
    }
    this.mouseMoveTimer = $setTimeout(function () {
        this.isControlsHidden = true;
        this.addClass('yt-no-controls');
    }, 2000, this);
};

YoutubeCustomPlayer.prototype.onPlayerEnded = function () {
    this.onPlayerPause();
    this.trigger('ended');
};

YoutubeCustomPlayer.prototype.onPlayerPlay = function (node) {
    console.log('onPlayerPlay');
    this.addClass('is-playing');
    this.removeClass('is-paused');

    this.paused = false;
    this.startProgress();

    if (this.props.controls) {
        this.toggleControls();
    }

    if (this.isFirstPlay) {
        this.addClass('has-started');
        this.isFirstPlay = false;
        this.duration = this.getDuration();
    }

    this.trigger('play');
};

YoutubeCustomPlayer.prototype.onPlayerPause = function () {
    if (this.props.autoPlayOnce) {
        this.props.set('autoplay', false);
    }
    this.removeClass('is-playing');
    this.addClass('is-paused');
    this.paused = true;
    this.stopProgress();
    this.trigger('pause');
};

YoutubeCustomPlayer.prototype.togglePlay = function () {
    this[this.paused ? 'play' : 'pause']();
    //this.paused = this.paused ? false : true;
};

YoutubeCustomPlayer.prototype.toggleFullScreen = function () {
    if (!this.isFullScreen) {
        this.requestFullScreen();
    } else {
        this.cancelFullScreen();
    }
};

YoutubeCustomPlayer.prototype.requestFullScreen = function () {

    var self = this;

    this.isFullScreen = true;
    this.isPausedBeforeFullScreen = this.paused;

    this.pause();
    this.addClass('is-fullscreen');

    EventDispatcher.publish('video:enterfullscreen');

    var node = this.$el;
    while (node.parentNode && node.parentNode.getAttribute) {
        this.addClass('yt-branch', node.parentNode);
        node = node.parentNode;
    }

    if (requestFullScreen.requestFn) {
        this.el[requestFullScreen.requestFn]();
        dom.addEventListener(document, requestFullScreen.eventName, this.onFullScrenChange);
    } else {
        $setTimeout(function () {
            if (this.isPausedBeforeFullScreen == true) {
                this.pause();
            } else {
                this.play();
            }
        }, 0, this);
    }

    dom.addClass('yt-fullscreen', document.getElementsByTagName('body')[0]);

    this.onResize();
};

YoutubeCustomPlayer.prototype.onFullScrenChange = function () {

    if (!document[requestFullScreen.isFullScreen]) {
        dom.removeEventListener(document, requestFullScreen.eventName, this.onFullScrenChange);
        if (this.isFullScreen) {
            this.cancelFullScreen();
        }
    }

    if (this.isPausedBeforeFullScreen == true) {
        this.pause();
    } else {
        this.play();
    }
};

YoutubeCustomPlayer.prototype.cancelFullScreen = function () {

    this.isFullScreen = false;
    this.isPausedBeforeFullScreen = this.paused;

    this.pause();
    this.removeClass('is-fullscreen');

    var node = this.$el;
    while (node.parentNode && node.parentNode.getAttribute) {
        this.removeClass('yt-branch', node.parentNode);
        node = node.parentNode;
    }

    if (requestFullScreen.requestFn) {
        document[requestFullScreen.cancelFn]();
    } else {
        $setTimeout(function () {
            if (this.isPausedBeforeFullScreen == true) {
                this.pause();
            } else {
                this.play();
            }
        }, 0, this);
    }

    dom.removeClass('yt-fullscreen', document.getElementsByTagName('body')[0]);

    this.onResize();

    EventDispatcher.publish('video:leavefullscreen');
};

YoutubeCustomPlayer.prototype.play = function () {

    if (this.props.useNative) {
        if (this.$videoTag) {
            if (this.props.stopAllVideos) {
                for (var p in YoutubeCustomPlayer.players) {
                    if (typeof YoutubeCustomPlayer.players[p] != 'undefined' && YoutubeCustomPlayer.players[p] !== this) {
                        YoutubeCustomPlayer.players[p].pause();
                    }
                }
            }
            this.$videoTag.play();
        }
    } else {

        if (this.ytplayer && typeof this.ytplayer.playVideo != "undefined") {
            if (this.props.stopAllVideos) {
                for (var p in YoutubeCustomPlayer.players) {
                    if (typeof YoutubeCustomPlayer.players[p] != 'undefined' && YoutubeCustomPlayer.players[p] !== this) {
                        YoutubeCustomPlayer.players[p].pause();
                    }
                }
            }
            this.ytplayer.playVideo();
        }
    }
};

YoutubeCustomPlayer.prototype.changeSrc = function (id) {

    if (typeof this.ytplayer !== 'undefined') {
        this.ytplayer.loadVideoById(id);
        // this.ytplayer.playVideo(); 
        // this.onPlayerPlay()
        return false;
    }
};

YoutubeCustomPlayer.prototype.changePoster = function (poster) {

    this.props.poster = poster;
};

YoutubeCustomPlayer.prototype.pause = function () {

    if (this.paused) {
        return;
    }

    if (this.props.useNative) {
        if (this.$videoTag) {
            this.stopProgress();
            this.$videoTag.pause();
        }
    } else {
        if (this.ytplayer && typeof this.ytplayer.pauseVideo != "undefined") {
            this.stopProgress();

            this.ytplayer.pauseVideo();
        }
    }
};

YoutubeCustomPlayer.prototype.unmute = function () {
    if (this.props.useNative) {
        if (this.$videoTag) {
            this.$videoTag.muted = false;
            this.muted = false;
            this.removeClass('is-muted');
        }
    } else {
        if (this.ytplayer && typeof this.ytplayer.unMute != "undefined") {
            this.muted = false;
            this.removeClass('is-muted');
            this.ytplayer.unMute();
        }
    }
};

YoutubeCustomPlayer.prototype.mute = function () {
    if (this.props.useNative) {
        if (this.$videoTag) {
            this.$videoTag.muted = true;
            this.muted = true;
            this.addClass('is-muted');
        }
    } else {
        if (this.ytplayer && typeof this.ytplayer.mute != "undefined") {
            this.muted = true;
            this.ytplayer.mute();
            this.addClass('is-muted');
        }
    }
};

YoutubeCustomPlayer.prototype.seek = function (t) {
    if (this.props.useNative) {
        if (this.$videoTag) {
            this.$videoTag.currentTime = t;
        }
    } else {
        if (this.ytplayer && typeof this.ytplayer.seekTo != "undefined") {
            this.ytplayer.pauseVideo();
            this.ytplayer.seekTo(t, true);
            this.ytplayer.playVideo();
        }
    }
};

YoutubeCustomPlayer.prototype.startProgress = function () {
    clearTimeout(this.progressTimer);
    this.updateTime();
};

YoutubeCustomPlayer.prototype.stopProgress = function () {
    clearTimeout(this.progressTimer);
    this.progressTimer = null;
};

YoutubeCustomPlayer.prototype.updateTime = function (e, time) {

    this.currentTime = this.getCurrentTime();

    if (this.props.controls) {
        this.refs.timeline.props.set('value', this.currentTime / this.duration);
        //this.setTimeCount();
    }

    this.progressTimer = $setTimeout(function () {
        this.updateTime();
        //console.log('updateTime');
    }, 100, this);

    // this.trigger('timeupdate');

    if (this.props.loop && this.currentTime >= this.duration - 0.5) {
        this.seek(0);
    }
};

YoutubeCustomPlayer.prototype.setTimeCount = function () {
    var curr = this.formatTime(this.currentTime * 1000);
    var dur = this.formatTime(this.duration * 1000);
    this.$timecount.innerHTML = curr + '<span> / ' + dur + '</span>';
};

YoutubeCustomPlayer.prototype.formatTime = function (ms) {
    var min = ms / 1000 / 60 << 0,
        sec = ms / 1000 % 60 << 0,
        ret = "";
    ret += min < 10 ? "0" : "";
    ret += min + ":";
    ret += sec < 10 ? "0" : "";
    ret += sec;
    return ret;
};

YoutubeCustomPlayer.prototype.getCurrentTime = function () {
    if (this.props.useNative) {
        return this.$videoTag.currentTime;
    } else {
        return this.ytplayer && typeof this.ytplayer.getCurrentTime != "undefined" ? this.ytplayer.getCurrentTime() : 0;
    }
};

YoutubeCustomPlayer.prototype.getDuration = function () {
    if (this.props.useNative) {
        return this.$videoTag.duration;
    } else {
        return this.ytplayer && typeof this.ytplayer.getDuration != "undefined" ? this.ytplayer.getDuration() : 0;
    }
};

YoutubeCustomPlayer.prototype.destroy = function () {

    EventDispatcher.off('pointermove', this.toggleControls);
    EventDispatcher.off('key:escape', this.cancelFullScreen, this);

    if (this.ytplayer) {
        this.pause();
        this.ytplayer.destroy();
        delete YoutubeCustomPlayer.players[this.props.uuid];
    }

    Component.prototype.destroy.call(this);
};

YoutubeCustomPlayer.isReady = false;
YoutubeCustomPlayer.readyList = [];
YoutubeCustomPlayer.players = {};

YoutubeCustomPlayer.changeSrc = function (id) {
    for (var p in YoutubeCustomPlayer.players) {
        YoutubeCustomPlayer.players[p].changeSrc(id);
    }
};

YoutubeCustomPlayer.stopAllVideos = function () {
    for (var p in YoutubeCustomPlayer.players) {
        if (typeof YoutubeCustomPlayer.players[p] != 'undefined') {
            YoutubeCustomPlayer.players[p].pause();
        }
    }
};

YoutubeCustomPlayer.ready = function (callback) {
    if (YoutubeCustomPlayer.isReady) {
        callback();
    } else {
        YoutubeCustomPlayer.readyList.push(callback);
    }
};

YoutubeCustomPlayer.init = function () {
    YoutubeCustomPlayer.isReady = true;
    for (var i = 0, l = YoutubeCustomPlayer.readyList.length; i < l; i++) {
        YoutubeCustomPlayer.readyList[i]();
        delete YoutubeCustomPlayer.readyList[i];
    }
};

window.onYouTubeIframeAPIReady = function () {
    YoutubeCustomPlayer.init();
};

/** load YT API */
var tag = document.createElement('script');
tag.src = "http://youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

window.YoutubeCustomPlayer = YoutubeCustomPlayer;

module.exports = YoutubeCustomPlayer;

},{"./template":34,"applicationLayout":28,"components/core-slider":31,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/EventList":69,"utils/dom":71,"utils/requestFullScreen":72,"utils/utils":73}],34:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = require('hbsfy/runtime');
module.exports = HandlebarsCompiler.template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "    <video class=\"yt-video-wrapper js-native-video\" poster=\""
    + container.escapeExpression(((helper = (helper = helpers.poster || (depth0 != null ? depth0.poster : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"poster","hash":{},"data":data}) : helper)))
    + "\">\n        <content></content>\n    </video>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper;

  return "    <div class=\"yt-video-wrapper\" id=\""
    + container.escapeExpression(((helper = (helper = helpers.uuid || (depth0 != null ? depth0.uuid : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"uuid","hash":{},"data":data}) : helper)))
    + "-video\"></div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "\n<div class=\"yt-wrapper-controls\">\n\n    <div class=\"yt-mute-btn u-hide-tablet\">\n        <span class=\"rect\"></span>\n        <span class=\"rect\"></span>\n        <span class=\"rect\"></span>\n        <span class=\"rect\"></span>\n        <span class=\"rect\"></span>\n    </div>\n\n    <div class=\"yt-fullscreen-btn u-mp-hide\">\n        <svg version=\"1.1\" fill=\"#fff\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"155 40.9 100 80.2\" style=\"enable-background:new 155 40.9 100 80.2;\" xml:space=\"preserve\">\n            <polygon points=\"183.3,121.1 155,121.1 155,92.8 162.8,92.8 162.8,113.3 183.3,113.3  \"/>\n            <polygon points=\"162.8,69.2 155,69.2 155,40.9 183.3,40.9 183.3,48.7 162.8,48.7  \"/>\n            <polygon points=\"255,69.2 247.2,69.2 247.2,48.7 226.7,48.7 226.7,40.9 255,40.9  \"/>\n            <polygon points=\"255,121.1 226.7,121.1 226.7,113.3 247.2,113.3 247.2,92.8 255,92.8  \"/>\n        </svg>\n    </div>\n\n    <div class=\"yt-play-btn o-wrapper--valign u-mp-hide\">\n         \n    </div>\n\n    <div class=\"yt-skin-wrapper\">\n        <div class=\"yt-controls-wrapper\">\n            <div class=\"yt-time\"></div>\n           \n            <core-slider data-ref=\"timeline\" class=\"yt-timeline\" data-on-slide=\"onSliderSlide\" data-on-stop=\"onSliderStop\"></core-slider>\n            <div class=\"yt-mute-btn\"></div>\n            \n        </div>\n    </div>\n</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {};

  return ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.useNative : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "\n<div class=\"yt-poster-wrapper\">\n    <img src=\""
    + container.escapeExpression(((helper = (helper = helpers.poster || (depth0 != null ? depth0.poster : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"poster","hash":{},"data":data}) : helper)))
    + "\" class=\"yt-poster\"/>\n</div>\n<div class=\"yt-loading-wrapper\">\n    <div class=\"yt-loading\"></div>\n</div>\n\n<div class=\"yt-big-play-btn u-show-tablet u-mp-hide\">\n   Play\n</div>\n\n\n<span class=\"yt-bar yt-bar-top u-mp-hide\"></span>\n<span class=\"yt-bar yt-bar-bottom u-mp-hide\"></span>\n\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.controls : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});

},{"hbsfy/runtime":20}],35:[function(require,module,exports){
/**
 * imort component provider
 */
"use strict";

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _apiComponentProvider = require("api/ComponentProvider");

var _apiComponentProvider2 = _interopRequireDefault(_apiComponentProvider);

/**
 * RESUABLE COMPONENTS
 */

var _coreYoutube = require('./core-youtube');

var _coreYoutube2 = _interopRequireDefault(_coreYoutube);

var _coreSlider = require('./core-slider');

var _coreSlider2 = _interopRequireDefault(_coreSlider);

var _onirimApplication = require('./onirim-application');

var _onirimApplication2 = _interopRequireDefault(_onirimApplication);

var _onirimElevator = require('./onirim-elevator');

var _onirimElevator2 = _interopRequireDefault(_onirimElevator);

var _onirimGrid = require('./onirim-grid');

var _onirimGrid2 = _interopRequireDefault(_onirimGrid);

var _onirimGridVideo = require('./onirim-grid-video');

var _onirimGridVideo2 = _interopRequireDefault(_onirimGridVideo);

var _onirimListHover = require('./onirim-list-hover');

var _onirimListHover2 = _interopRequireDefault(_onirimListHover);

var _onirimYoutubeOverlay = require('./onirim-youtube-overlay');

var _onirimYoutubeOverlay2 = _interopRequireDefault(_onirimYoutubeOverlay);

var _onirimBurger = require('./onirim-burger');

var _onirimBurger2 = _interopRequireDefault(_onirimBurger);

var _onirimHome = require('./onirim-home');

var _onirimHome2 = _interopRequireDefault(_onirimHome);

var _onirimHomeSlides = require('./onirim-home-slides');

var _onirimHomeSlides2 = _interopRequireDefault(_onirimHomeSlides);

var _onirimSearch = require('./onirim-search');

var _onirimSearch2 = _interopRequireDefault(_onirimSearch);

var _onirimSlideshow = require('./onirim-slideshow');

var _onirimSlideshow2 = _interopRequireDefault(_onirimSlideshow);

var _onirimSubNav = require('./onirim-sub-nav');

var _onirimSubNav2 = _interopRequireDefault(_onirimSubNav);

var _onirimScrollObject = require('./onirim-scroll-object');

var _onirimScrollObject2 = _interopRequireDefault(_onirimScrollObject);

var _onirimHomeSubnav = require('./onirim-home-subnav');

var _onirimHomeSubnav2 = _interopRequireDefault(_onirimHomeSubnav);

var _onirimRootLink = require('./onirim-root-link');

var _onirimRootLink2 = _interopRequireDefault(_onirimRootLink);

var _onirimScrollContent = require('./onirim-scroll-content');

var _onirimScrollContent2 = _interopRequireDefault(_onirimScrollContent);

var _onirimDates = require('./onirim-dates');

var _onirimDates2 = _interopRequireDefault(_onirimDates);

var _onirimPage = require('./onirim-page');

var _onirimPage2 = _interopRequireDefault(_onirimPage);

var _coreCover = require('./core-cover');

var _coreCover2 = _interopRequireDefault(_coreCover);

var _onirimGradient = require('./onirim-gradient');

var _onirimGradient2 = _interopRequireDefault(_onirimGradient);

var _onirimFilters = require('./onirim-filters');

var _onirimFilters2 = _interopRequireDefault(_onirimFilters);

_apiComponentProvider2["default"].mount("core-youtube", _coreYoutube2["default"]);

_apiComponentProvider2["default"].mount("core-slider", _coreSlider2["default"]);

_apiComponentProvider2["default"].mount("onirim-application", _onirimApplication2["default"]);

_apiComponentProvider2["default"].mount("onirim-elevator", _onirimElevator2["default"]);

_apiComponentProvider2["default"].mount("onirim-grid", _onirimGrid2["default"]);

_apiComponentProvider2["default"].mount("onirim-grid-video", _onirimGridVideo2["default"]);

_apiComponentProvider2["default"].mount("onirim-list-hover", _onirimListHover2["default"]);

_apiComponentProvider2["default"].mount("onirim-youtube-overlay", _onirimYoutubeOverlay2["default"]);

_apiComponentProvider2["default"].mount("onirim-burger", _onirimBurger2["default"]);

_apiComponentProvider2["default"].mount("onirim-home", _onirimHome2["default"]);

_apiComponentProvider2["default"].mount("onirim-home-slides", _onirimHomeSlides2["default"]);

_apiComponentProvider2["default"].mount("onirim-search", _onirimSearch2["default"]);

_apiComponentProvider2["default"].mount("onirim-slideshow", _onirimSlideshow2["default"]);

_apiComponentProvider2["default"].mount("onirim-sub-nav", _onirimSubNav2["default"]);

_apiComponentProvider2["default"].mount("onirim-scroll-object", _onirimScrollObject2["default"]);

_apiComponentProvider2["default"].mount("onirim-home-subnav", _onirimHomeSubnav2["default"]);

_apiComponentProvider2["default"].mount("onirim-root-link", _onirimRootLink2["default"]);

_apiComponentProvider2["default"].mount("onirim-scroll-content", _onirimScrollContent2["default"]);

_apiComponentProvider2["default"].mount("onirim-dates", _onirimDates2["default"]);

_apiComponentProvider2["default"].mount("onirim-page", _onirimPage2["default"]);

_apiComponentProvider2["default"].mount("core-cover", _coreCover2["default"]);

_apiComponentProvider2["default"].mount("onirim-gradient", _onirimGradient2["default"]);

_apiComponentProvider2["default"].mount("onirim-filters", _onirimFilters2["default"]);

},{"./core-cover":29,"./core-slider":31,"./core-youtube":33,"./onirim-application":36,"./onirim-burger":37,"./onirim-dates":38,"./onirim-elevator":39,"./onirim-filters":40,"./onirim-gradient":41,"./onirim-grid":43,"./onirim-grid-video":42,"./onirim-home":46,"./onirim-home-slides":44,"./onirim-home-subnav":45,"./onirim-list-hover":47,"./onirim-page":48,"./onirim-root-link":49,"./onirim-scroll-content":50,"./onirim-scroll-object":51,"./onirim-search":52,"./onirim-slideshow":53,"./onirim-sub-nav":54,"./onirim-youtube-overlay":55,"api/ComponentProvider":24}],36:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimApplication = (function (_Component) {
  _inherits(OnirimApplication, _Component);

  function OnirimApplication(options) {
    _classCallCheck(this, OnirimApplication);

    _get(Object.getPrototypeOf(OnirimApplication.prototype), 'constructor', this).call(this, options);
  }

  /**
      constructor
      render //on instancie le template si il existe
      afterRender //le dom du composant est pret
      onResize// pur setter le layout du composant
      afterResize// si on a besoin dattendre que les composants enfants soient redimentionnés
      show //surement inutil
      afterShow//surmeent inutil
      enterFrame//la boucle de rendue du composant. n'est pas appelée si this.visible = false
  */

  _createClass(OnirimApplication, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimApplication.prototype), 'afterRender', this).call(this);

      _applicationLayout2['default'].set({
        $doc: document.documentElement || document,
        $win: window,
        $body: document.getElementsByTagName("body")[0],
        header: dom.query('.js-main-header'),
        winWidth: null,
        winHeight: null,
        lastWinWidth: null,
        lastWinHeight: null,
        halfWinHeight: null,
        halfWinWidth: null,
        thirdWinHeight: null,
        quarterWinWidth: null,
        quarterWinHeight: null,
        footer: dom.query('.js-footer')
      });

      this.$doc = _applicationLayout2['default'].$doc;
      this.$win = _applicationLayout2['default'].$win;

      //prevent back button loading cache http://stackoverflow.com/questions/8788802/prevent-safari-loading-from-cache-when-back-button-is-clicked

      if (dom.hasClass('is-leaving', _applicationLayout2['default'].$body)) {
        dom.removeClass('is-leaving', _applicationLayout2['default'].$body);
      } else {}

      this.$fakeScroll = document.getElementById('fake-scroll');

      this.$header = dom.query('.js-main-header');
      this.$footer = dom.query('.js-footer');
      this.$linkAnchor = dom.query('.js-link-anchor');
      this.$page = dom.query('.js-page');
      this.$datePanel = dom.query('.js-date-panel');
      this.$subNav = dom.query('.js-sub-nav');

      this.$bgSection = dom.queryAll('.js-bg-section');
      this.$videoItem = dom.queryAll('.js-video-item');
      this.$changeHeaderColor = dom.queryAll('.js-change-header-color');

      (0, _utils$setTimeout2['default'])(function () {
        this.$at = document.querySelectorAll('.js-at');
        if (this.$at) {
          for (var i = 0; i < this.$at.length; i++) {
            this.$at[i].innerHTML = '@';
          }
        }
      }, 700, this);

      this.headerHeight = this.$header.offsetHeight;
      this.headerY = this.$header.offsetTop;
      this.footerY = this.$footer.offsetTop;
      this.linkAnchorY = this.$linkAnchor.offsetTop;

      _applicationLayout2['default'].set({
        footer: this.$footer,
        footerY: this.footerY,
        linkAnchor: dom.query('.js-link-anchor'),
        linkAnchorY: this.$linkAnchor.offsetTop
      });

      this.bgSectionNum = this.$bgSection.length;

      this.changeHeaderColorNum = this.$changeHeaderColor.length;

      this.bgSectionArr = new Array(this.bgSectionNum);

      this.dateArr = new Array(this.dateChangeNum);

      this.$listDateItem = dom.queryAll('.js-list-date');
      this.listDateItemNum = this.$listDateItem.length;
      this.listDateArr = new Array(this.listDateItemNum);

      this.page = _applicationLayout2['default'].$body.getAttribute('data-page');

      _applicationLayout2['default'].set({
        page: this.page
      });

      if (this.page == 'director-details' || this.page == 'photographers-details') {
        (0, _utils$setTimeout2['default'])(function () {
          this.scrollToArticle();
        }, 700, this);
      }

      // EventDispatcher.on('linkAnchor:clicked', this.onlinkAnchorClicked, this);

      _utilsEventDispatcher2['default'].on('search:opened', this.onSearchOpened, this);
      _utilsEventDispatcher2['default'].on('search:closed', this.onSearchClosed, this);

      _utilsEventDispatcher2['default'].on('subnav:opened', this.onSubNavOpened, this);
      _utilsEventDispatcher2['default'].on('subnav:closed', this.onSubNavClosed, this);

      this.coreYoutube = document.getElementsByTagName('core-youtube');
      this.coreYoutubeNum = this.coreYoutube.length;

      this.onReady();

      for (var i = 0; i < this.listDateItemNum; i++) {

        this.listDateArr[i] = {
          el: this.$listDateItem[i],
          date: this.$listDateItem[i].getAttribute('data-list-date')
        };

        if (i > 0) {
          if (this.listDateArr[i].date == this.listDateArr[i - 1].date) {
            this.$listDateItem[i].style.visibility = 'hidden';
          }
        }
      }

      for (var i = 0; i < this.bgSectionNum; i++) {
        this.bgSectionArr[i] = {
          el: this.$bgSection[i],
          offsetY: this.$bgSection[i].offsetTop - this.$bgSection[i].scrollTop + this.$bgSection[i].clientTop - (this.headerY - this.headerHeight),
          height: this.$bgSection[i].height,
          state: this.$bgSection[i].getAttribute('data-state')
        };
      };

      this.$dateChange = dom.queryAll('.js-date');
      this.dateChangeNum = this.$dateChange.length;

      // objectFit.polyfill({
      //   selector: 'img', // this can be any CSS selector
      //   fittype: 'cover', // either contain, cover, fill or none
      //   disableCrossDomain: 'true' // either 'true' or 'false' to not parse external CSS files.
      // });

      this.resizeTimer = null;
      this.winHeight = 0;
      this.winWidth = 0;
      this.halfWinHeight = 0;
      this.halfWinWidth = 0;

      this.winWidth = this.$win.innerWidth || this.$doc.clientWidth || _applicationLayout2['default'].$body.clientWidth;
      this.winHeight = this.$win.innerHeight || this.$doc.clientHeight || _applicationLayout2['default'].$body.clientHeight;

      this.bodyHeight = 0;
      this.windowHeight = 0;
      this.windowWidth = 0;
      this.scrollTop = 0;
      this.scrollRatio = 0;
      this.currScrollTop = 0;
      this.prevScroll = 0;
      this.currScroll = this.scrollTop;

      (0, _utils$setTimeout2['default'])(function () {
        this.$preloadImages = dom.queryAll('.js-preload-image');
        this.preloadImagesNum = this.$preloadImages.length;

        this.imgLoaded = 0;

        for (var _i = 0; _i < this.preloadImagesNum; _i++) {

          var img = new Image();
          var self = this;

          img.onload = function () {
            this.onload = null;

            self.onResize();
          };

          img.src = this.$preloadImages[_i].src;
        }
      }, 100, this);

      document.addEventListener("scroll", this.onScroll.bind(this));
      document.addEventListener("load", this.onLoad.bind(this));

      dom.addEventListener(this.$win, 'resize', this.onResize.bind(this));
      dom.addEventListener(this.$win, 'orientationchange', this.onResize.bind(this));

      _utilsEventDispatcher2['default'].on('video:enterfullscreen', this.disableScroll, this);
      _utilsEventDispatcher2['default'].on('video:leavefullscreen', this.enableScroll, this);

      this.searchOpened = false;

      if (dom.hasClass('is-sub-nav-active', _applicationLayout2['default'].$body)) {
        this.isSubNavOpening = true;
      } else {
        this.isSubNavOpening = false;
      }

      _utilsEventDispatcher2['default'].on('filter:one', this.onResize, this);

      window.onload = (function () {
        this.updatescrollHeight();
      }).bind(this);
    }
  }, {
    key: 'onReady',
    value: function onReady() {

      (0, _utils$setTimeout2['default'])(function () {
        dom.addClass('is-ready', document.getElementsByTagName("body")[0]);
        this.onResize();
      }, 50, this);
    }
  }, {
    key: 'disableScroll',
    value: function disableScroll() {
      this.isScrollDisabled = true;
    }
  }, {
    key: 'enableScroll',
    value: function enableScroll() {
      this.isScrollDisabled = false;
    }
  }, {
    key: 'getOffsetTop',
    value: function getOffsetTop(elem) {
      var offsetTop = 0;
      do {
        if (!isNaN(elem.offsetTop)) {
          offsetTop += elem.offsetTop;
        }
      } while (elem = elem.offsetParent);
      return offsetTop;
    }
  }, {
    key: 'onLoad',
    value: function onLoad() {}
  }, {
    key: 'onResize',
    value: function onResize() {

      _get(Object.getPrototypeOf(OnirimApplication.prototype), 'onResize', this).call(this);

      var winWidth = this.$win.innerWidth || this.$doc.clientWidth || _applicationLayout2['default'].$body.clientWidth;
      var winHeight = this.$win.innerHeight || this.$doc.clientHeight || _applicationLayout2['default'].$body.clientHeight;

      //IE 7/8 willt triggerresize event on dom node size change
      if (winWidth == this.lastWinWidth && winHeight == this.lastWinHeight) {
        return;
      }

      this.winWidth = winWidth;
      this.winHeight = winHeight;
      this.lastWinWidth = this.winWidth;
      this.lastWinHeight = this.winHeight;

      //working alternative for ios7 height issue but hardcoded :
      if (support.isIOS7 && this.winWidth > this.winHeight) this.winHeight -= 20;

      this.halfWinHeight = this.winHeight >> 1;
      this.halfWinWidth = this.winWidth >> 1;
      this.quarterWinWidth = this.halfWinWidth >> 1;
      this.quarterWinHeight = this.halfWinHeight >> 1;
      this.thirdWinHeight = this.winHeight / 3;
      this.winRatio = this.winWidth / this.winHeight;

      _applicationLayout2['default'].set({
        winWidth: winWidth,
        winHeight: winHeight,
        lastWinWidth: this.winWidth,
        lastWinHeight: this.winHeight,
        halfWinHeight: this.winHeight >> 1,
        halfWinWidth: this.winWidth >> 1,
        thirdWinHeight: this.thirdWinHeight,
        quarterWinWidth: this.halfWinWidth >> 1,
        quarterWinHeight: this.halfWinHeight >> 1,
        winRatio: this.winWidth / this.winHeight
      });

      _utilsEventDispatcher2['default'].trigger('customresize');
    }
  }, {
    key: 'afterResize',
    value: function afterResize() {
      _get(Object.getPrototypeOf(OnirimApplication.prototype), 'afterResize', this).call(this);

      if (this.winWidth > 1024) {
        this.updatescrollHeight();
      }
    }
  }, {
    key: 'updatescrollHeight',
    value: function updatescrollHeight() {
      if ('page-content' in this.refs == false) {
        return;
      }

      if (this.isSubNavOpening == false) {
        this.scrollContentHeight = this.refs['page-content'].height;
      } else {
        if ('page-content' in this.refs == false) {} else {
          this.scrollContentHeight = this.refs['page-nav'].height;
        }
      }

      (0, _utils$setTimeout2['default'])(function () {
        this.$fakeScroll.style.height = this.scrollContentHeight + 'px';
      }, 50, this);
    }
  }, {
    key: 'onSubNavOpened',
    value: function onSubNavOpened() {
      this.isSubNavOpening = true;
      this.updatescrollHeight();
    }
  }, {
    key: 'onSubNavClosed',
    value: function onSubNavClosed() {
      this.isSubNavOpening = false;
      this.updatescrollHeight();
    }
  }, {
    key: 'onScroll',
    value: function onScroll() {
      if (this.isScrollDisabled) {
        return;
      }

      this.scrollTop = window.pageYOffset || document.documentElement.scrollTop;

      _applicationLayout2['default'].set({
        scrollTop: this.scrollTop = window.pageYOffset || document.documentElement.scrollTop
      });
    }
  }, {
    key: 'onSearchOpened',
    value: function onSearchOpened() {
      this.searchOpened = true;
    }
  }, {
    key: 'onSearchClosed',
    value: function onSearchClosed() {
      this.searchOpened = false;
    }
  }, {
    key: 'enterFrame',
    value: function enterFrame() {

      _get(Object.getPrototypeOf(OnirimApplication.prototype), 'enterFrame', this).call(this);

      this.currScrollTop += (this.scrollTop - this.currScrollTop) * 0.09;

      if (this.page === 'photographers-details') {
        //console.log(this.scrollTop + this.winHeight, this.scrollContentHeight);
        if (this.scrollTop + this.winHeight === this.scrollContentHeight) {
          //this.updatescrollHeight();
          this.onResize();
        }
      }

      //lopp through each subView
      //if it's a scroll-object, then update it's position

      if (this.winWidth > 1024 && 'page-content' in this.refs != false) {

        if (this.isSubNavOpening == false) {
          this.refs['page-content'].setPosition(0, -this.currScrollTop);
        } else {
          this.refs['page-nav'].setPosition(0, -this.currScrollTop);
        }
      };
    }
  }, {
    key: 'scrollToArticle',
    value: function scrollToArticle() {
      this.id = location.search.substring(1);

      if (this.id != '') {
        this.article = document.getElementById(this.id);
        this.articleY = this.getOffsetTop(this.article) - 100;
        this.$win.scrollTo(0, this.articleY);
      }
    }
  }]);

  return OnirimApplication;
})(_coreComponent2['default']);

exports['default'] = OnirimApplication;

window.onpageshow = function (event) {
  if (event.persisted) {
    window.location.reload();
  }
};
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],37:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimBurger = (function (_Component) {
  _inherits(OnirimBurger, _Component);

  function OnirimBurger(options) {
    _classCallCheck(this, OnirimBurger);

    _get(Object.getPrototypeOf(OnirimBurger.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimBurger, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimBurger.prototype), 'afterRender', this).call(this);

      this.$btnBurger = dom.query('.js-btn-burger');
      this.$mainHeader = dom.query('.js-main-header');

      this.onBtnBurgerClick = this.onBtnBurgerClick.bind(this);

      dom.addEventListener(this.$btnBurger, 'click', this.onBtnBurgerClick);
    }
  }, {
    key: 'onBtnBurgerClick',
    value: function onBtnBurgerClick() {

      if (dom.hasClass('is-active', this.$mainHeader)) {
        dom.removeClass('is-active', this.$mainHeader);
      } else {
        dom.addClass('is-active', this.$mainHeader);
      }
    }
  }]);

  return OnirimBurger;
})(_coreComponent2['default']);

exports['default'] = OnirimBurger;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],38:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimDates = (function (_Component) {
  _inherits(OnirimDates, _Component);

  function OnirimDates(options) {
    _classCallCheck(this, OnirimDates);

    _get(Object.getPrototypeOf(OnirimDates.prototype), 'constructor', this).call(this, options);
    this.visible = true;
  }

  _createClass(OnirimDates, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimDates.prototype), 'afterRender', this).call(this);

      this.$dates = this.$('.js-date');
      this.datesObj = {};

      this.currentDate = null;

      for (var i = 0, l = this.$dates.length; i < l; i++) {
        var date = this.$dates[i].getAttribute('data-date');
        this.datesObj[date] = {
          el: this.$dates[i],
          date: this.$dates[i].getAttribute('data-date')
        };
      }

      _utilsEventDispatcher2['default'].on('date:change', this.onDateChange, this);
    }
  }, {
    key: 'onDateChange',
    value: function onDateChange(date) {

      if (date == this.currentDate) {
        return;
      }

      //we check if the date exists
      if (date in this.datesObj == false) {
        return;
      }

      this.currentDate = date;
      console.log('__ON_DATE_CHANGE__', this.currentDate);

      for (var k in this.datesObj) {
        if (date < k) {
          this.addClass('is-leaving-top', this.datesObj[k].el);
          this.removeClass('is-leaving-bottom', this.datesObj[k].el);
          this.removeClass('is-active', this.datesObj[k].el);
        } else if (date > k) {
          this.addClass('is-leaving-bottom', this.datesObj[k].el);
          this.removeClass('is-leaving-top', this.datesObj[k].el);
          this.removeClass('is-active', this.datesObj[k].el);
        } else {
          this.removeClass('is-leaving-top', this.datesObj[k].el);
          this.removeClass('is-leaving-bottom', this.datesObj[k].el);
          this.addClass('is-active', this.datesObj[k].el);
        }
      }
    }
  }]);

  return OnirimDates;
})(_coreComponent2['default']);

exports['default'] = OnirimDates;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],39:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsDom = require('utils/dom');

var _utilsDom2 = _interopRequireDefault(_utilsDom);

var _utilsEventDispatcher = require('../../utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _applicationLayout = require('../../applicationLayout');

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var OnirimElevator = (function (_Component) {
  _inherits(OnirimElevator, _Component);

  function OnirimElevator(options) {
    _classCallCheck(this, OnirimElevator);

    _get(Object.getPrototypeOf(OnirimElevator.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimElevator, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimElevator.prototype), 'afterRender', this).call(this);

      this.$body = document.getElementsByTagName("body")[0];
      this.$win = window;

      this.$elevatorLink = _utilsDom2['default'].query('.js-elevator-link');
      this.$elevatorItems = _utilsDom2['default'].queryAll('.js-elevator-item');
      this.elevatorItemsNum = this.$elevatorItems.length;
      this.elevatorItemsArr = [];

      this.onElevatorItemsHover = this.onElevatorItemsHover.bind(this);
      this.onElevatorItemsOut = this.onElevatorItemsOut.bind(this);

      console.log(_utilsEventDispatcher2['default'].request('application', ['tokenKey']));
      _applicationLayout2['default'].on('change:foo', this.onFooChanged, this);

      if (this.$elevatorLink !== null) {
        for (var i = 0; i < this.elevatorItemsNum; i++) {
          _utilsDom2['default'].addEventListener(this.$elevatorItems[i], 'mouseover', this.onElevatorItemsHover);
          _utilsDom2['default'].addEventListener(this.$elevatorItems[i], 'mouseout', this.onElevatorItemsOut);
        };

        //dom.addEventListener( this.$win, 'resize', this.onResize);
        _utilsEventDispatcher2['default'].on('customresize', this.onResize, this);
        this.onResize();
      }
    }
  }, {
    key: 'onResize',
    value: function onResize() {

      _get(Object.getPrototypeOf(OnirimElevator.prototype), 'onResize', this).call(this);

      console.log('__onResize');

      for (var i = 0; i < this.elevatorItemsNum; i++) {
        var el = {
          offsetTop: this.$elevatorItems[i].offsetTop - this.$elevatorItems[i].scrollTop + this.$elevatorItems[i].clientTop + this.$elevatorItems[i].offsetHeight / 2
        };
        this.elevatorItemsArr.push(el);
      };

      this.$elevatorLink.style.top = this.elevatorItemsArr[0].offsetTop + 'px';

      console.log(this.elevatorItemsArr[2].offsetTop + 'px');
    }
  }, {
    key: 'onFooChanged',
    value: function onFooChanged() {
      console.log('The Key foo has changed to: ', _applicationLayout2['default'].foo);
    }
  }, {
    key: 'onElevatorItemsHover',
    value: function onElevatorItemsHover(e) {
      this.index = e.currentTarget.getAttribute('data-index');
      this.$elevatorLink.style[_utilsBrowserSupport2['default'].transform] = 'translateY(' + (this.elevatorItemsArr[this.index].offsetTop - this.elevatorItemsArr[0].offsetTop + 5) + 'px) rotate(-90deg) translateZ(0)';
    }
  }, {
    key: 'onElevatorItemsOut',
    value: function onElevatorItemsOut(e) {
      this.$elevatorLink.style[_utilsBrowserSupport2['default'].transform] = 'translateY(' + 0 + 'px) rotate(-90deg) translateZ(0)';
    }
  }]);

  return OnirimElevator;
})(_coreComponent2['default']);

exports['default'] = OnirimElevator;
module.exports = exports['default'];

},{"../../applicationLayout":28,"../../utils/EventDispatcher":68,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/dom":71}],40:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimFilters = (function (_Component) {
  _inherits(OnirimFilters, _Component);

  function OnirimFilters(options) {
    _classCallCheck(this, OnirimFilters);

    _get(Object.getPrototypeOf(OnirimFilters.prototype), 'constructor', this).call(this, options);

    this.events.set({
      'click .js-link-filter': 'onFilterLinkClick'
    });
  }

  _createClass(OnirimFilters, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(OnirimFilters.prototype), 'afterRender', this).call(this);

      this.$filters = dom.queryAll('.js-tile-filter');

      this.$tileFilter = dom.queryAll('.js-tile-filter');

      this.$listFilter = dom.query('.js-list-filter');

      this.$linkFilterAll = dom.query('.js-link-filter-all');

      this.currFilter = [];

      this.filterArr = new Array();

      for (var i = 0; i < this.$filters.length; i++) {

        this.currFilter[i] = this.$filters[i].getAttribute('data-filter');

        if (this.currFilter[i] !== this.currFilter[i - 1] && this.currFilter[i] !== "") {
          this.filterArr.push(this.currFilter[i]);
        }
      }

      this.filterArr = this.filterArr.filter(function (elem, index, self) {
        return index == self.indexOf(elem);
      });

      for (var k = 0; k < this.filterArr.length; k++) {
        var saveHTML = this.$listFilter.innerHTML;
        this.$listFilter.innerHTML = saveHTML + '<li><a href="javascript:void(0)" class="c-link c-link--filter u-upper c-link--small-rmarged js-link-filter" data-filter-by="' + this.filterArr[k] + '">' + this.filterArr[k] + '</a></li>';
      }

      (0, _utils$setTimeout2['default'])(function () {
        this.$linkFilters = dom.queryAll('.js-link-filter');
      }, 10, this);
    }
  }, {
    key: 'onFilterLinkClick',
    value: function onFilterLinkClick(event) {

      this.resetAllFilter();

      dom.addClass('is-filtering', _applicationLayout2['default'].$body);

      var el = event.currentTarget;
      var filter = event.currentTarget.getAttribute('data-filter-by');

      el.classList.add('is-active');

      if (filter == 'all') {
        (0, _utils$setTimeout2['default'])(function () {
          for (var i = 0; i < this.$tileFilter.length; i++) {

            this.$tileFilter[i].style.display = 'inline-block';
            this.$tileFilter[i].style.marginTop = '0';
            this.resetAllFilter();
          }
        }, 450, this);
      } else {
        (0, _utils$setTimeout2['default'])(function () {
          for (var i = 0; i < this.$tileFilter.length; i++) {

            this.$tileFilter[i].style.display = 'none';

            if (this.$tileFilter[i].getAttribute('data-filter') == filter) {
              this.$tileFilter[i].style.display = 'inline-block';
              this.$tileFilter[i].classList.add('is-tile-active');

              this.resetMarge();
            }
          }
        }, 450, this);
      }

      (0, _utils$setTimeout2['default'])(function () {
        dom.removeClass('is-filtering', _applicationLayout2['default'].$body);
        _utilsEventDispatcher2['default'].publish('filter:one');
      }, 700, this);
    }
  }, {
    key: 'resetAllFilter',
    value: function resetAllFilter() {
      console.log('resetAllFilter');

      for (var i = 0; i < this.$linkFilters.length; i++) {
        this.$linkFilters[i].classList.remove('is-active');
      }

      for (var i = 0; i < this.$tileFilter.length; i++) {
        this.$tileFilter[i].classList.remove('is-tile-active');
      }
    }
  }, {
    key: 'resetMarge',
    value: function resetMarge() {
      for (var i = 0; i < this.$tileFilter.length; i++) {
        this.$tileFilter[i].style.marginTop = '0';
      }

      this.els = document.getElementsByClassName('is-tile-active');
      this.els[0].style.marginTop = '-40%';

      console.log(this.els[0]);
    }
  }]);

  return OnirimFilters;
})(_coreComponent2['default']);

exports['default'] = OnirimFilters;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],41:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimGradient = (function (_Component) {
  _inherits(OnirimGradient, _Component);

  function OnirimGradient(options) {
    _classCallCheck(this, OnirimGradient);

    _get(Object.getPrototypeOf(OnirimGradient.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimGradient, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(OnirimGradient.prototype), 'afterRender', this).call(this);
    }
  }]);

  return OnirimGradient;
})(_coreComponent2['default']);

exports['default'] = OnirimGradient;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],42:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsDom = require('utils/dom');

var _utilsDom2 = _interopRequireDefault(_utilsDom);

var _applicationLayout = require('../../applicationLayout');

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var OnirimGridVideo = (function (_Component) {
  _inherits(OnirimGridVideo, _Component);

  function OnirimGridVideo(options) {
    _classCallCheck(this, OnirimGridVideo);

    console.log('OnirimGridVideo');

    _get(Object.getPrototypeOf(OnirimGridVideo.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimGridVideo, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimGridVideo.prototype), 'afterRender', this).call(this);
    }
  }, {
    key: 'enterFrame',
    value: function enterFrame() {

      _get(Object.getPrototypeOf(OnirimGridVideo.prototype), 'enterFrame', this).call(this);
    }
  }]);

  return OnirimGridVideo;
})(_coreComponent2['default']);

exports['default'] = OnirimGridVideo;
module.exports = exports['default'];

},{"../../applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/dom":71}],43:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsDom = require('utils/dom');

var _utilsDom2 = _interopRequireDefault(_utilsDom);

var _applicationLayout = require('../../applicationLayout');

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var OnirimGrid = (function (_Component) {
  _inherits(OnirimGrid, _Component);

  function OnirimGrid(options) {
    _classCallCheck(this, OnirimGrid);

    _get(Object.getPrototypeOf(OnirimGrid.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimGrid, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimGrid.prototype), 'afterRender', this).call(this);

      this.$page = _utilsDom2['default'].query('.js-page');
      this.$linkGrid = _utilsDom2['default'].query('.js-toggle-body');

      if (this.$page.getAttribute('data-grid') !== null) {
        _utilsDom2['default'].removeClass('is-disable', this.$linkGrid);
      } else {
        _utilsDom2['default'].addClass('is-disable', this.$linkGrid);
      }
    }
  }]);

  return OnirimGrid;
})(_coreComponent2['default']);

exports['default'] = OnirimGrid;
module.exports = exports['default'];

},{"../../applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/dom":71}],44:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var DELAY_CHANGE_SLIDE = 2200;
var TIMER_STEP = 10000; //10000;

var OnirimHomeSlides = (function (_Component) {
	_inherits(OnirimHomeSlides, _Component);

	function OnirimHomeSlides(options) {
		_classCallCheck(this, OnirimHomeSlides);

		_get(Object.getPrototypeOf(OnirimHomeSlides.prototype), 'constructor', this).call(this, options);
	}

	_createClass(OnirimHomeSlides, [{
		key: 'afterRender',
		value: function afterRender() {

			_get(Object.getPrototypeOf(OnirimHomeSlides.prototype), 'afterRender', this).call(this);

			this.coreYoutube = document.getElementById('core-youtube');
			this.playerYoutube = document.getElementById('youtube-player');

			this.$posterPhoto = dom.query('.js-poster-photo');

			this.$wheelContainer = dom.query('.js-wheel-container');
			this.$slideActive = dom.query('.is-slide-active');
			this.$btnGrid = dom.query('.js-btn-grid');

			this.$slides = dom.queryAll('.js-slide');
			this.$slidesVideos = dom.query('.js-video-item');
			this.$slideCounter = dom.queryAll('.js-slide-counter');
			this.$slideHover = dom.queryAll('.js-slide-hover');

			this.$slideDescription = dom.queryAll('.js-slide-description');

			this.slideNum = this.$slides.length;

			this.activeIndex = Number(Array.prototype.indexOf.call(this.$slides, this.$slideActive)) || 0;
			this.isAnimating = false;

			this.mousewheelevt = /Firefox/i.test(navigator.userAgent) ? 'DOMMouseScroll' : 'mousewheel';

			dom.addEventListener(this.$wheelContainer, this.mousewheelevt, this.onWheelContainerScroll.bind(this));
			document.addEventListener('keydown', this.onKeyboardClick.bind(this));

			if (_applicationLayout2['default'].winWidth <= 1024) {
				document.addEventListener('touchstart', this.handleTouchStart.bind(this), false);
				document.addEventListener('touchmove', this.handleTouchMove.bind(this), false);
				document.addEventListener('touchend', this.handleTouchEnd.bind(this), false);
			}

			for (var i = 0; i < this.$slideHover.length; i++) {

				dom.addEventListener(this.$slideHover[i], 'mouseover', this.onSlideHover.bind(this));
				dom.addEventListener(this.$slideHover[i], 'mouseout', this.onSlideOut.bind(this));
			}

			//this.isHover = false;
			this.counter = 0;

			this.goToCurrentSlide();

			this.autoplayTimer = (0, _utils$setTimeout2['default'])(function () {
				this.nextSlide();
			}, TIMER_STEP, this);

			_utilsEventDispatcher2['default'].on('subnav:opened', this.disableSlideAuto, this);
			_utilsEventDispatcher2['default'].on('subnav:closed', this.enableSlideAuto, this);
		}
	}, {
		key: 'enterFrame',
		value: function enterFrame() {
			_get(Object.getPrototypeOf(OnirimHomeSlides.prototype), 'enterFrame', this).call(this);
		}
	}, {
		key: 'onResize',
		value: function onResize() {
			_get(Object.getPrototypeOf(OnirimHomeSlides.prototype), 'onResize', this).call(this);

			this.spaceY = _applicationLayout2['default'].winHeight - (this.$slidesVideos.offsetTop + this.$slidesVideos.offsetHeight);
			this.spaceX = this.$slidesVideos.offsetLeft;

			this.heightSlideVideo = this.$slidesVideos.offsetHeight;

			this.descriptionY = this.$slidesVideos.offsetTop + this.$slidesVideos.offsetHeight;

			for (var i = 0; i < this.$slideDescription.length; i++) {
				this.$slideDescription[i].style.top = this.descriptionY + this.spaceY / 2 + 'px';
				//this.$slideCounter[i].style.top = (this.descriptionY - 50) + 'px';
				//this.$slideCounter[i].style.left = (this.spaceX + 90) + 'px';
			};
		}
	}, {
		key: 'onWheelContainerScroll',
		value: function onWheelContainerScroll(e) {

			var e = window.event || e;
			var delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));

			if (this.isAnimating || dom.hasClass('is-sub-nav-active', _applicationLayout2['default'].$body)) {
				return;
			} else {

				if (delta > 0) {
					this.isAnimating = true;
					this.prevSlide();
				} else {
					this.isAnimating = true;
					this.nextSlide();
				}

				(0, _utils$setTimeout2['default'])(function () {
					this.isAnimating = false;
				}, DELAY_CHANGE_SLIDE, this);
			}
		}
	}, {
		key: 'prevSlide',
		value: function prevSlide(e) {

			clearTimeout(this.autoplayTimer);
			this.autoplayTimer = (0, _utils$setTimeout2['default'])(function () {
				this.nextSlide();
			}, TIMER_STEP, this);

			if (this.activeIndex <= 0) {
				this.activeIndex = this.$slides.length - 1;
			} else {
				this.activeIndex--;
			}
			this.goToCurrentSlide();
		}
	}, {
		key: 'nextSlide',
		value: function nextSlide(e) {

			clearTimeout(this.autoplayTimer);
			this.autoplayTimer = (0, _utils$setTimeout2['default'])(function () {
				this.nextSlide();
			}, TIMER_STEP, this);

			if (this.activeIndex >= this.$slides.length - 1) {
				this.activeIndex = 0;
			} else {
				this.activeIndex++;
			}

			this.goToCurrentSlide();
		}
	}, {
		key: 'goToCurrentSlide',
		value: function goToCurrentSlide() {

			this.counter++;

			if (this.counter == 1) {
				dom.addClass('is-active', this.coreYoutube);
			} else {
				dom.removeClass('is-active', this.coreYoutube);
				dom.addClass('is-leaving-bottom', this.coreYoutube);
			}

			// if(this.$slides[this.activeIndex - 1]){
			// 	if(dom.hasClass('is-hover', this.$slides[this.activeIndex - 1])){
			// 		dom.removeClass('is-hover', this.$slides[this.activeIndex - 1]);
			// 	}
			// }

			this.videoId = this.$slides[this.activeIndex].getAttribute('data-video-id');
			this.posterId = this.$slides[this.activeIndex].getAttribute('data-poster-id');
			this.posterPhotoId = this.$slides[this.activeIndex].getAttribute('data-photo-src');

			this.$btnGrid.classList.add('is-leaving');
			this.$btnGrid.classList.add('is-disable');

			(0, _utils$setTimeout2['default'])(function () {
				for (var i = 0; i < this.slideNum; i++) {

					if (i < this.activeIndex) {
						this.addClass('is-slide-leaving-top', this.$slides[i]);
						this.removeClass('is-slide-leaving-bottom', this.$slides[i]);
						this.removeClass('is-slide-active', this.$slides[i]);
					} else if (i > this.activeIndex) {
						this.removeClass('is-slide-leaving-top', this.$slides[i]);
						this.removeClass('is-slide-active', this.$slides[i]);
						this.addClass('is-slide-leaving-bottom', this.$slides[i]);

						//dom.removeClass('is-leaving-bottom', this.coreYoutube);
					} else {
							this.removeClass('is-slide-leaving-top', this.$slides[i]);
							this.removeClass('is-slide-leaving-bottom', this.$slides[i]);
							this.addClass('is-slide-active', this.$slides[i]);
						}
				}
			}, 50, this);

			clearTimeout(this.resetSlidePostimer);
			this.resetSlidePostimer = (0, _utils$setTimeout2['default'])(function () {
				for (var i = 0; i < this.slideNum; i++) {
					this.removeClass('is-video-leaving-top', this.$slides[i]);
					this.removeClass('is-video-leaving-bottom', this.$slides[i]);
				}

				for (var i = 0; i < this.slideNum; i++) {
					if (i !== this.activeIndex) {
						this.$slides[i].style.display = "none";
					}
				}

				if (this.posterPhotoId != '') {
					this.$posterPhoto.style.backgroundImage = 'url(' + this.posterPhotoId + ')';
					this.playerYoutube.style.display = 'none';
					// console.log('laaa')
					this.$posterPhoto.style.display = 'inline-block';
				}

				if (this.videoId) {

					if (_applicationLayout2['default'].winWidth <= 1024) {
						this.$posterPhoto.style.backgroundImage = 'url(' + this.posterId + ')';
					} else {
						this.$posterPhoto.style.display = 'none';
						this.playerYoutube.style.display = 'inline-block';

						if (this.counter > 1) {
							if (typeof YoutubeCustomPlayer !== "undefined") {
								YoutubeCustomPlayer.changeSrc(this.videoId);
								console.log('change src', this.videoId);
							}
						}
					}
				}

				// if(this.videoId){
				// 	this.refs['video-youtube'].props.set('autoplay',true);
				// }
				// else{
				// 	this.refs['video-youtube'].pause();
				// }

				dom.removeClass('is-leaving', this.$btnGrid);

				dom.removeClass('is-leaving-bottom', this.coreYoutube);
				dom.addClass('is-active', this.coreYoutube);

				// if(this.counter == 1){
				// 	this.addClass('is-first-over', this.$slides[this.activeIndex]);
				// }

				// if(this.activeIndex > 0){
				// 	dom.setStyle(this.$slideCounter[this.activeIndex - 1], {
				// 		transform: 'translateY(' + 0 + 'px)' + support.translateZ
				// 	});				
				// } else {
				// 	dom.setStyle(this.$slideCounter[this.activeIndex], {
				// 		transform: 'translateY(' + 0 + 'px)' + support.translateZ
				// 	});					
				// }
			}, this.counter === 1 ? 0 : 1750, this);

			(0, _utils$setTimeout2['default'])(function () {
				this.$btnGrid.classList.remove('is-disable');
			}, 2000, this);

			this.$slides[this.activeIndex].style.display = "inline-block";
		}
	}, {
		key: 'handleTouchStart',
		value: function handleTouchStart(evt) {
			this.isPointerDown = true;
			this.pointerStartX = evt.touches[0].clientX;
			this.pointerStartY = evt.touches[0].clientY;

			this.pointerX = this.pointerStartX;
			this.pointerY = this.pointerStartY;
		}
	}, {
		key: 'handleTouchMove',
		value: function handleTouchMove(evt) {

			if (!this.isPointerDown) {
				return;
			}

			//avoid native scroll as we cannot do anything on IOS during native scroll
			evt.preventDefault();

			this.pointerX = evt.touches[0].clientX;
			this.pointerY = evt.touches[0].clientY;
		}
	}, {
		key: 'handleTouchEnd',
		value: function handleTouchEnd(evt) {

			if (this.isAnimating || !this.isPointerDown) {
				return;
			}

			this.isPointerDown = false;

			if (Math.abs(this.pointerStartX - this.pointerX) > 10) {

				if (_applicationLayout2['default'].winWidth <= 720) {
					if (this.pointerX > this.pointerStartX) {
						this.prevSlide();
					} else {
						this.nextSlide();
					}
				}

				if (_applicationLayout2['default'].winWidth > 720 && _applicationLayout2['default'].winWidth <= 1024) if (this.pointerY > this.pointerStartY) {
					this.prevSlide();
				} else {
					this.nextSlide();
				}
			}
		}
	}, {
		key: 'onKeyboardClick',
		value: function onKeyboardClick(e) {

			if (this.isAnimating) {
				return;
			} else {

				if (e.keyCode == '38') {
					this.isAnimating = true;
					this.prevSlide();
				}

				if (e.keyCode == '40') {
					this.isAnimating = true;
					this.nextSlide();
				}

				(0, _utils$setTimeout2['default'])(function () {
					this.isAnimating = false;
				}, DELAY_CHANGE_SLIDE, this);
			}
		}
	}, {
		key: 'disableSlideAuto',
		value: function disableSlideAuto() {
			clearTimeout(this.autoplayTimer);
		}
	}, {
		key: 'enableSlideAuto',
		value: function enableSlideAuto() {
			this.autoplayTimer = (0, _utils$setTimeout2['default'])(function () {
				this.nextSlide();
			}, TIMER_STEP, this);
		}
	}, {
		key: 'onSlideHover',
		value: function onSlideHover() {
			//this.isHover = true;

			if (this.isAnimating) {
				(0, _utils$setTimeout2['default'])(function () {
					dom.addClass('is-hover', this.$slides[this.activeIndex]);
				}, 1500, this);
			} else {
				dom.addClass('is-hover', this.$slides[this.activeIndex]);
			}
		}
	}, {
		key: 'onSlideOut',
		value: function onSlideOut() {
			dom.removeClass('is-hover', this.$slides[this.activeIndex]);
			// $setTimeout(function(){
			// 	this.isHover = false;
			// }, 1000, this)
		}
	}]);

	return OnirimHomeSlides;
})(_coreComponent2['default']);

exports['default'] = OnirimHomeSlides;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],45:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var homeNavEasing = 'cubic-bezier(0.855, 0.100, 0.355, 1.000)';

var OnirimHomeSubNav = (function (_Component) {
	_inherits(OnirimHomeSubNav, _Component);

	function OnirimHomeSubNav(options) {
		_classCallCheck(this, OnirimHomeSubNav);

		_get(Object.getPrototypeOf(OnirimHomeSubNav.prototype), 'constructor', this).call(this, options);
	}

	_createClass(OnirimHomeSubNav, [{
		key: 'afterRender',
		value: function afterRender() {
			_get(Object.getPrototypeOf(OnirimHomeSubNav.prototype), 'afterRender', this).call(this);

			this.$btnGrid = dom.query('.js-btn-grid');
			this.$subNav = dom.query('.js-home-subnav');

			this.$slides = dom.queryAll('.js-slide');
			this.$videoItems = dom.query('.js-video-item');
			this.$videos = dom.queryAll('.js-slide-video');
			this.$videoGrids = dom.queryAll('.js-video-grid');
			this.$slideTitle = dom.queryAll('.js-slide-title');
			this.$titlesGrids = dom.queryAll('.js-home-subnav-title');
			this.$overlay = dom.queryAll('.js-home-subnav-overlay');

			this.$translateWrapper = dom.query('.js-translate');
			this.$translateWrapperInversed = dom.query('.js-translate-inversed');
			this.$youtube = dom.query('.js-wrapper-youtube');

			this.$slideYoutube = dom.query('.js-slide-youtube');

			this.slidesNum = this.$slides.length;

			this.videoGridsNum = this.$videoGrids.length;

			this.indexActiveSlide = null;

			for (var i = 0; i < this.videoGridsNum; i++) {
				dom.addEventListener(this.$videoGrids[i], 'mouseover', this.onOverlayOver.bind(this));
			};

			_utilsEventDispatcher2['default'].on('subnav:opened', this.onSubNavOpened, this);
			_utilsEventDispatcher2['default'].on('subnav:closed', this.onSubNavClosed, this);
		}
	}, {
		key: 'enterFrame',
		value: function enterFrame() {
			_get(Object.getPrototypeOf(OnirimHomeSubNav.prototype), 'enterFrame', this).call(this);
		}
	}, {
		key: 'onResize',
		value: function onResize() {

			_get(Object.getPrototypeOf(OnirimHomeSubNav.prototype), 'onResize', this).call(this);

			this.$activeSlide = dom.query('.is-slide-active');
			this.indexActiveSlide = Number(this.$activeSlide.getAttribute('data-index'));

			this.translateRatio = this.$videoItems.offsetWidth * .5 + 2;

			this.translateY = _applicationLayout2['default'].thirdWinHeight - this.$videoItems.offsetTop + 0.5;

			this.translateYInversed = this.translateY * 2;

			if (dom.hasClass('is-sub-nav-active', _applicationLayout2['default'].$body)) {

				this.$activeSlide = dom.query('.is-slide-active');
				this.indexActiveSlide = this.$activeSlide.getAttribute('data-index');

				dom.setStyle(this.$translateWrapper, {
					transform: 'translateX(' + this.translateRatio + 'px)' + 'translateY(' + this.translateY + 'px)' + support.translateZ
				});

				dom.setStyle(this.$translateWrapperInversed, {
					transform: 'translateX(' + 0 + '%)' + 'translateY(' + -this.translateYInversed + 'px)' + support.translateZ
				});

				dom.setStyle(this.$youtube, {
					transform: 'scaleX(' + 1.3 + ') scaleY(' + 1.3 + ') translateY(25%)' + support.translateZ
				});
			}
		}
	}, {
		key: 'onOverlayOver',
		value: function onOverlayOver(e) {
			var el = e.currentTarget;
			var index = el.getAttribute('data-index');

			for (var i = 0; i < this.videoGridsNum; i++) {

				dom.setStyle(this.$titlesGrids[i], {
					transitionDelay: '0s',
					transitionDuration: '0s',
					opacity: '0'
				});

				dom.setStyle(this.$titlesGrids[index - 1], {
					transitionDelay: '0s',
					transitionDuration: '.45s',
					transitionTimingFunction: 'cubic-bezier(0.165, 0.840, 0.440, 1.000)',
					opacity: '1'
				});
			}
		}
	}, {
		key: 'onSubNavOpened',
		value: function onSubNavOpened() {

			_utilsEventDispatcher2['default'].publish('homesubnav:opened');

			dom.addClass('is-disable', this.$btnGrid);

			(0, _utils$setTimeout2['default'])(function () {
				dom.removeClass('is-disable', this.$btnGrid);
			}, 1300, this);

			this.$subNav.style.display = 'inline-block';

			for (var k in this.subViews) {
				this.subViews[k].onResize();
			}

			this.$activeSlide = dom.query('.is-slide-active');
			this.indexActiveSlide = Number(this.$activeSlide.getAttribute('data-index'));

			dom.removeClass('o-wrapper--o', this.$videoItems);

			this.$titlesGrids[this.indexActiveSlide].style.opacity = 1;

			(0, _utils$setTimeout2['default'])(function () {

				dom.removeClass('is-dark', _applicationLayout2['default'].header);
				dom.removeClass('is-dark', _applicationLayout2['default'].footer);
				dom.removeClass('is-dark', _applicationLayout2['default'].linkAnchor);
				dom.addClass('is-clear', _applicationLayout2['default'].header);
				dom.addClass('is-clear', _applicationLayout2['default'].footer);
				dom.addClass('is-clear', _applicationLayout2['default'].linkAnchor);

				dom.addClass('is-grid-' + this.indexActiveSlide, this.$subNav);
				dom.addClass('is-dark', this.$slideYoutube);
				dom.addClass('is-active', this.$slideTitle[this.indexActiveSlide]);
				dom.addClass('is-clear', _applicationLayout2['default'].header);

				dom.setStyle(this.$translateWrapper, {
					transitionDelay: '0.05s',
					transitionDuration: '1s',
					transitionTimingFunction: 'cubic-bezier(0.230, 1.000, 0.320, 1.000)',
					transformOrigin: '50% 50%',
					transform: 'translateX(' + this.translateRatio + 'px)' + 'translateY(' + this.translateY + 'px)' + support.translateZ
				});

				dom.setStyle(this.$translateWrapperInversed, {
					transitionDelay: '0.05s',
					transitionDuration: '1s',
					transformOrigin: '50% 0',
					transitionTimingFunction: 'cubic-bezier(0.230, 1.000, 0.320, 1.000)',
					transform: 'translateX(' + 0 + '%)' + 'translateY(' + -this.translateYInversed + 'px)' + support.translateZ
				});

				dom.setStyle(this.$youtube, {
					transitionDelay: '0.05s',
					transitionDuration: '1s',
					transitionTimingFunction: 'cubic-bezier(0.230, 1.000, 0.320, 1.000)',
					transform: 'scaleX(' + 1.3 + ') scaleY(' + 1.3 + ') translateY(25%)' + support.translateZ
				});
			}, 100, this);

			(0, _utils$setTimeout2['default'])(function () {
				this.isNavAnimating = false;
			}, 1500, this);
		}
	}, {
		key: 'onSubNavClosed',
		value: function onSubNavClosed() {

			if (this.isNavAnimating) {
				return;
			}

			dom.addClass('is-disable', this.$btnGrid);

			(0, _utils$setTimeout2['default'])(function () {
				dom.removeClass('is-disable', this.$btnGrid);
			}, 1300, this);

			this.isNavAnimating = true;

			_utilsEventDispatcher2['default'].publish('homesubnav:closed');

			(0, _utils$setTimeout2['default'])(function () {
				this.$subNav.style.display = 'none';
				for (var i = 0; i < this.$titlesGrids.length; i++) {
					this.$titlesGrids[i].style.opacity = 0;
				}
			}, 2000, this);

			(0, _utils$setTimeout2['default'])(function () {
				dom.removeClass('is-clear', _applicationLayout2['default'].header);
				dom.removeClass('is-clear', _applicationLayout2['default'].footer);
				dom.removeClass('is-clear', _applicationLayout2['default'].linkAnchor);
			}, 400, this);

			dom.addClass('is-dark', _applicationLayout2['default'].footer);
			dom.addClass('is-dark', _applicationLayout2['default'].header);
			dom.addClass('is-dark', _applicationLayout2['default'].linkAnchor);

			this.$activeSlide = dom.query('.is-slide-active');
			this.indexActiveSlide = this.$activeSlide.getAttribute('data-index');

			dom.removeClass('is-dark', this.$slideYoutube);
			dom.removeClass('is-active', this.$slideTitle[this.indexActiveSlide]);

			// $setTimeout(function(){
			// 	this.$slideTitle[this.indexActiveSlide].style.opacity					        = '1';
			// 	this.$slideTitle[this.indexActiveSlide].style[support.transitionDuration]       = '.45s';
			// 	this.$slideTitle[this.indexActiveSlide].style[support.transitionTimingFunction] = '';
			// }, 850, this)

			(0, _utils$setTimeout2['default'])(function () {

				dom.setStyle(this.$translateWrapper, {
					transitionDelay: '0s',
					transitionDuration: '1s',
					transitionTimingFunction: 'cubic-bezier(0.230, 1.000, 0.320, 1.000)',
					transform: 'translateX(' + 0 + '%)' + 'translateY(' + 0 + '%)' + support.translateZ
				});

				dom.setStyle(this.$translateWrapperInversed, {
					transitionDelay: '0s',
					transitionDuration: '1s',
					transitionTimingFunction: 'cubic-bezier(0.230, 1.000, 0.320, 1.000)',
					transform: 'translateX(' + 0 + '%)' + 'translateY(' + 0 + '%)' + support.translateZ
				});

				dom.setStyle(this.$youtube, {
					transitionDelay: '0s',
					transitionDuration: '1s',
					transitionTimingFunction: 'cubic-bezier(0.230, 1.000, 0.320, 1.000)',
					transform: 'scaleX(' + 1 + ') scaleY(' + 1 + ')' + support.translateZ
				});
			}, 600, this);

			// for (var i = 0; i < this.videoGridsNum; i++) {
			// 	this.$videoItems[i].style.opacity = 1;				
			// }

			(0, _utils$setTimeout2['default'])(function () {
				for (var i = 0; i < this.slidesNum; i++) {
					if (dom.hasClass('is-grid-' + i, this.$subNav)) {
						dom.removeClass('is-grid-' + i, this.$subNav);
					}
				};
				dom.addClass('o-wrapper--o', this.$videoItems);

				this.isNavAnimating = false;
			}, 1500, this);
		}
	}]);

	return OnirimHomeSubNav;
})(_coreComponent2['default']);

exports['default'] = OnirimHomeSubNav;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],46:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
		value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimHome = (function (_Component) {
		_inherits(OnirimHome, _Component);

		function OnirimHome(options) {
				_classCallCheck(this, OnirimHome);

				_get(Object.getPrototypeOf(OnirimHome.prototype), 'constructor', this).call(this, options);
				this.visible = true;
		}

		_createClass(OnirimHome, [{
				key: 'afterRender',
				value: function afterRender() {

						_get(Object.getPrototypeOf(OnirimHome.prototype), 'afterRender', this).call(this);
						console.log('afterRender::home');

						// this.$video            = document.getElementById('video');
						// this.$frame            = document.getElementById('frame');
						// this.$logo 			   = document.getElementById('logoIntro');

						// console.log(this.$video.offsetWidth);
						// console.log(this.$video.offsetHeight);

						// this.$frame.style.width  = this.$video.offsetWidth + 'px';
						// this.$frame.style.height = this.$video.offsetHeight + 'px';

						// this.$frame.style.left   = ((this.$video.offsetWidth / 2) - this.$logo.offsetWidth / 2) * -1 + 'px';
						// this.$frame.style.top    = ((this.$video.offsetHeight / 2) - this.$logo.offsetHeight / 2) * -1 + 'px';

						// this.$canvasTxt        = document.getElementById('canvasText');
						// this.$canvasTxt.width  = this.$video.offsetWidth;
						// this.$canvasTxt.height = this.$video.offsetHeight;
						// this.ctxTxt            = this.$canvasTxt.getContext('2d');

						// this.imageWidth  = this.$video.clientWidth;
						// this.imageHeight = this.$video.clientHeight;
						// this.imageComputedWidth  = 0;
						// this.imageComputedHeight = 0;
						// this.imageX = 0;
						// this.imageY = 0;
						// this.imageScale  = 0.5;
						// this.currImageScale = 0.5;

						// this.imagesList = {};

						// this.imageToLoad = ['wp-content/themes/immersivegarden/images/onirim-text.png', 'wp-content/themes/immersivegarden/videos/poster.png'];

						// this.numImagestoLoad = this.imageToLoad.length;
						// this.numImagesLoaded = 0;

						// var imageName;

						// for(var i=0;i<this.numImagestoLoad; i++){

						// 	//imageName = this.imageToLoad[i].match(/\/([^.]*)\./)[1];
						// 	imageName = this.imageToLoad[i];

						// 	//create a scope to use closure to save image name
						// 	(function scope( name ){

						// 		this.imagesList[ name ] = {
						// 			source: new Image(),
						// 			ratio: 0,
						// 			height: 0,
						// 			width: 0
						// 		}

						// 		this.imagesList[ name ].source.onload = function(){

						// 			 this.imagesList[ name ].width = this.imagesList[ name ].source.width;
						// 			 this.imagesList[ name ].height = this.imagesList[ name ].source.height;
						// 			 this.imagesList[ name ].ratio = this.imagesList[ name ].source.width / this.imagesList[ name ].source.height;

						// 			this.numImagesLoaded++;

						// 	    	if( this.numImagesLoaded >= this.numImagestoLoad ){
						// 		    	this.isImageLoaded = true;
						// 				this.imageScale = 1;
						// 	    	}

						// 		}.bind(this);

						// 		this.imagesList[ name ].source.src = this.imageToLoad[i];

						// 	}).call( this, imageName );

						// }
				}
		}, {
				key: 'enterFrame',
				value: function enterFrame() {

						_get(Object.getPrototypeOf(OnirimHome.prototype), 'enterFrame', this).call(this);

						// if( !this.isImageLoaded ){
						// 	return;
						// }

						// $setTimeout(function(){

						// 	// on incremente de la diffence entre currScale et imageScale(qui ici est notre scale de destination)
						// 	// multiplié par un ratio(qui donnera la vitesse de decceleration)
						// 	this.currImageScale += (this.imageScale - this.currImageScale) * 0.05;

						// 	//hack to replace clearRect
						// 	this.$canvasTxt.width = this.$canvasTxt.width;

						// 	//this.ctxTxt.globalCompositeOperation = "source-over";

						//     this.ctxTxt.drawImage( this.imagesList["wp-content/themes/immersivegarden/images/onirim-text.png"].source, ((this.$canvasTxt.width / 2) - this.imagesList["wp-content/themes/immersivegarden/images/onirim-text.png"].width / 2 ), (this.$canvasTxt.height / 2) - this.imagesList["wp-content/themes/immersivegarden/images/onirim-text.png"].height / 2 );

						//     this.ctxTxt.globalCompositeOperation = "source-atop";

						//     this.imageComputedWidth  = this.imageWidth * this.currImageScale;
						//     this.imageComputedHeight = this.imageHeight * this.currImageScale;

						// 	this.imageX = (this.imageComputedWidth - this.imageWidth) * .5 * -1;
						// 	this.imageY = (this.imageComputedHeight - this.imageHeight) * .5 * -1;

						//     this.ctxTxt.drawImage(
						//     	this.imagesList["wp-content/themes/immersivegarden/videos/poster.png"].source,
						//     	this.imageX,
						//     	this.imageY,
						//     	this.imageComputedWidth,
						//     	this.imageComputedHeight
						//     );

						//     if (this.currImageScale.toFixed(1) == 0.5) {
						//     	this.endIntro();
						//     }

						//    }, 1500, this)
				}
		}]);

		return OnirimHome;
})(_coreComponent2['default']);

exports['default'] = OnirimHome;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],47:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsDom = require('utils/dom');

var _utilsDom2 = _interopRequireDefault(_utilsDom);

var _applicationLayout = require('../../applicationLayout');

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var DECCELERATION_RATE = 0.1;

var OnirimListHover = (function (_Component) {
  _inherits(OnirimListHover, _Component);

  function OnirimListHover(options) {
    _classCallCheck(this, OnirimListHover);

    _get(Object.getPrototypeOf(OnirimListHover.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimListHover, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimListHover.prototype), 'afterRender', this).call(this);

      this.$listItemHover = _utilsDom2['default'].queryAll('.js-list-item-hover');
      this.$listItem = _utilsDom2['default'].queryAll('.js-list-item');
      this.$test = _utilsDom2['default'].queryAll('.js-test');

      this.$subNav = _utilsDom2['default'].query('.js-sub-nav');

      this.listItemsNum = this.$listItemHover.length;

      this.index = null;
      this.cursorX = 0;
      this.cursorY = 0;
      this.currentCursorX = 0;
      this.currentCursorY = 0;
      this.decceleration = 0.08;

      this.onlistItemsHover = this.onlistItemsHover.bind(this);
      this.onlistItemsOut = this.onlistItemsOut.bind(this);
      this.onSubNavMove = this.onSubNavMove.bind(this);

      for (var i = 0; i < this.listItemsNum; i++) {
        _utilsDom2['default'].addEventListener(this.$listItemHover[i], 'mouseover', this.onlistItemsHover);
        _utilsDom2['default'].addEventListener(this.$listItemHover[i], 'mouseout', this.onlistItemsOut);
      };

      if (this.$subNav !== null) {
        _utilsDom2['default'].addEventListener(this.$subNav, 'mousemove', this.onSubNavMove);
      };
    }
  }, {
    key: 'onlistItemsHover',
    value: function onlistItemsHover(e) {
      this.index = Number(e.currentTarget.getAttribute('data-index'));
      _utilsDom2['default'].addClass('is-active', this.$listItem[this.index]);
    }
  }, {
    key: 'onlistItemsOut',
    value: function onlistItemsOut(e) {
      this.index = Number(e.currentTarget.getAttribute('data-index'));
      _utilsDom2['default'].removeClass('is-active', this.$listItem[this.index]);
    }
  }, {
    key: 'enterFrame',
    value: function enterFrame() {

      _get(Object.getPrototypeOf(OnirimListHover.prototype), 'enterFrame', this).call(this);

      if (_utilsDom2['default'].hasClass('is-sub-nav-active', _applicationLayout2['default'].$body)) {} else {
        for (var i = 0; i < this.$test.length; i++) {
          this.decceleration = 0.09 * i;
          this.$test[i].style[_utilsBrowserSupport2['default'].transitionDuration] = '0s';
        };
      }
    }
  }, {
    key: 'onSubNavMove',
    value: function onSubNavMove(e) {

      this.cursorX = e.pageX;
      this.cursorY = e.pageY;

      this.currentCursorX += (this.cursorX - this.currentCursorX) * this.decceleration;
      this.currentCursorY += (this.cursorY - this.currentCursorY) * this.decceleration;

      for (var i = 0; i < this.$test.length; i++) {
        this.decceleration = 0.09 * i;
        this.$test[i].style[_utilsBrowserSupport2['default'].transitionDuration] = '1.6s';
        this.$test[i].style[_utilsBrowserSupport2['default'].transitionDelay] = '0s';
        this.$test[i].style[_utilsBrowserSupport2['default'].transitionTimingFunction] = 'cubic-bezier(0.230, 1.000, 0.320, 1.000)';
        this.$test[i].style[_utilsBrowserSupport2['default'].transform] = 'translate3d(' + this.currentCursorX / 100 + 'px,' + this.currentCursorY / 100 + 'px,' + 0 + ')';
      };
    }
  }]);

  return OnirimListHover;
})(_coreComponent2['default']);

exports['default'] = OnirimListHover;
module.exports = exports['default'];

},{"../../applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/dom":71}],48:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimPage = (function (_Component) {
  _inherits(OnirimPage, _Component);

  function OnirimPage(options) {
    _classCallCheck(this, OnirimPage);

    _get(Object.getPrototypeOf(OnirimPage.prototype), 'constructor', this).call(this, options);
    this.visible = true;
  }

  _createClass(OnirimPage, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(OnirimPage.prototype), 'afterRender', this).call(this);

      this.setPosition();
    }
  }, {
    key: 'onResize',
    value: function onResize() {
      _get(Object.getPrototypeOf(OnirimPage.prototype), 'onResize', this).call(this);
    }
  }, {
    key: 'setPosition',
    value: function setPosition(x, y) {
      for (var k in this.subViews) {
        if (this.subViews[k].setPosition) {
          this.subViews[k].setPosition(x, y);
        }
      }
    }
  }]);

  return OnirimPage;
})(_coreComponent2['default']);

exports['default'] = OnirimPage;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],49:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimRootLink = (function (_Component) {
  _inherits(OnirimRootLink, _Component);

  function OnirimRootLink(options) {
    _classCallCheck(this, OnirimRootLink);

    _get(Object.getPrototypeOf(OnirimRootLink.prototype), 'constructor', this).call(this, options);

    this.events.set({
      'click': 'onClickLinkRoot'
    });
  }

  _createClass(OnirimRootLink, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(OnirimRootLink.prototype), 'afterRender', this).call(this);

      var href = this.$el.getAttribute('href');
      this.$el.setAttribute('data-href', href);
      this.$el.setAttribute('href', 'javascript:void(0);');
    }
  }, {
    key: 'onClickLinkRoot',
    value: function onClickLinkRoot(e) {

      var redirectLink = e.currentTarget.getAttribute('data-href');

      (0, _utils$setTimeout2['default'])(function () {
        dom.addClass('is-leaving', _applicationLayout2['default'].$body);
      }, 10, this);

      setTimeout(function () {
        window.location = redirectLink;
      }, 500);
    }
  }]);

  return OnirimRootLink;
})(_coreComponent2['default']);

exports['default'] = OnirimRootLink;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],50:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimScrollContent = (function (_Component) {
  _inherits(OnirimScrollContent, _Component);

  function OnirimScrollContent(options) {
    _classCallCheck(this, OnirimScrollContent);

    _get(Object.getPrototypeOf(OnirimScrollContent.prototype), 'constructor', this).call(this, options);
    this.visible = true;
  }

  _createClass(OnirimScrollContent, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(OnirimScrollContent.prototype), 'afterRender', this).call(this);
    }
  }, {
    key: 'onResize',
    value: function onResize() {
      _get(Object.getPrototypeOf(OnirimScrollContent.prototype), 'onResize', this).call(this);
      this.height = this.el.offsetHeight;
      this.width = this.el.offsetWidth;
    }
  }, {
    key: 'setPosition',
    value: function setPosition(x, y) {
      for (var k in this.subViews) {
        if (this.subViews[k].setPosition) {
          this.subViews[k].setPosition(x, y);
        }
      }
    }
  }]);

  return OnirimScrollContent;
})(_coreComponent2['default']);

exports['default'] = OnirimScrollContent;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],51:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('../../utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utilsClamp = require('../../utils/clamp');

var _utilsClamp2 = _interopRequireDefault(_utilsClamp);

var _utils$setTimeout = require('../../utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var dom = require('utils/dom');

var OnirimScrollObject = (function (_Component) {
  _inherits(OnirimScrollObject, _Component);

  function OnirimScrollObject(options) {
    _classCallCheck(this, OnirimScrollObject);

    _get(Object.getPrototypeOf(OnirimScrollObject.prototype), 'constructor', this).call(this, options);

    this.visible = false;

    this.position = { x: 0, y: 0 };
    this.deltaPosition = { x: 0, y: 0 };
    this.offset = { x: 0, y: 0 };
    this.screenPosition = { x: 0, y: 0 };

    this.parallaxOffset = { x: 0, y: 0 };
    this.parallaxRatio = { x: 0, y: 0 };

    this.props.set({
      activate: false,
      enterClass: false,
      leaveClass: false,
      animate: false,
      triggerClassOnce: true,
      ratio: false,
      friction: 0,
      activeDate: false,
      index: null,
      date: null,
      changeUi: false
    });
  }

  _createClass(OnirimScrollObject, [{
    key: 'afterRender',
    value: function afterRender() {
      _get(Object.getPrototypeOf(OnirimScrollObject.prototype), 'afterRender', this).call(this);

      if (_applicationLayout2['default'].winWidth > 1024) {
        this.addClass('u-force-hide');

        if (this.props.absolute) {
          this.setAbsolute();
        }
      };

      this.securityMerge = _applicationLayout2['default'].page == 'projects' || _applicationLayout2['default'].page == 'photographers-details' ? _applicationLayout2['default'].winHeight : 0;
    }
  }, {
    key: 'setPosition',
    value: function setPosition(x, y) {

      this.position.x += (x - this.position.x) * (1 - this.props.friction);
      this.position.y += (y - this.position.y) * (1 - this.props.friction);

      this.screenPosition.x = this.position.x + this.offset.x;
      this.screenPosition.y = this.position.y + this.offset.y;

      if (this.options.parent.screenPosition) {
        this.screenPosition.x += this.options.parent.screenPosition.x;
        this.screenPosition.y += this.options.parent.screenPosition.y;
      }

      if (this.props.ratio) {
        this.parallaxRatio.y = ((this.screenPosition.y + this.height * .5) / _applicationLayout2['default'].winHeight - .5) * 2;
        this.deltaPosition.y = this.parallaxRatio.y * this.parallaxOffset.y;
      };

      for (var k in this.subViews) {
        if (this.subViews[k].setPosition) {
          this.subViews[k].setPosition(0, 0);
        }
      }

      this.computeTillingOrActiveState();
      if (this.props.date) {
        this.computeDate();
      }

      if (this.props.changeUi) {
        this.changeUi();
      }
    }
  }, {
    key: 'computeDate',
    value: function computeDate() {

      if (this.screenPosition.y < _applicationLayout2['default'].winHeight * .33 && this.screenPosition.y > _applicationLayout2['default'].winHeight * .33 - 50) {
        _utilsEventDispatcher2['default'].trigger('date:change', [this.props.date]);
      }
    }
  }, {
    key: 'changeUi',
    value: function changeUi() {

      if (this.screenPosition.y < 0) {
        // if(this.screenPosition.y > (-this.height) && this.screenPosition.y > (Layout.winHeight)){
        if (this.$el.getAttribute('data-state') == 'is-clear') {
          dom.removeClass('is-dark', _applicationLayout2['default'].header);
        } else {
          dom.removeClass('is-clear', _applicationLayout2['default'].header);
        }
        dom.addClass(this.$el.getAttribute('data-state'), _applicationLayout2['default'].header);
      }

      if (this.screenPosition.y > -this.height && this.screenPosition.y < _applicationLayout2['default'].winHeight - 10) {
        if (this.$el.getAttribute('data-state') == 'is-clear') {
          dom.removeClass('is-dark', _applicationLayout2['default'].footer);
        } else {
          dom.removeClass('is-clear', _applicationLayout2['default'].footer);
        }
        dom.addClass(this.$el.getAttribute('data-state'), _applicationLayout2['default'].footer);
      }

      if (this.screenPosition.y > -this.height && this.screenPosition.y < _applicationLayout2['default'].winHeight - 10) {
        if (this.$el.getAttribute('data-state') == 'is-clear') {
          dom.removeClass('is-dark', _applicationLayout2['default'].linkAnchor);
        } else {
          dom.removeClass('is-clear', _applicationLayout2['default'].linkAnchor);
        }
        dom.addClass(this.$el.getAttribute('data-state'), _applicationLayout2['default'].linkAnchor);
      }
    }
  }, {
    key: 'computeTillingOrActiveState',
    value: function computeTillingOrActiveState() {

      if (this.screenPosition.y > -this.height - this.securityMerge && this.screenPosition.y < _applicationLayout2['default'].winHeight) {

        this.enable();

        if (this.props.activate) {
          this.addClass('is-active');
        }

        if (this.props.enterClass) {
          (0, _utils$setTimeout2['default'])(function () {
            this.addClass(this.props.enterClass);
          }, 100, this);
        }

        if (this.props.leaveClass) {
          this.removeClass(this.props.leaveClass);
        }
      } else {

        this.disable();

        if (this.props.leaveClass) {
          (0, _utils$setTimeout2['default'])(function () {
            this.addClass(this.props.leaveClass);
          }, 100, this);
        }

        if (!this.props.triggerClassOnce) {

          if (this.props.activate) {
            this.removeClass('is-active');
          }

          if (this.props.enterClass) {
            this.removeClass(this.props.enterClass);
          }
        }
      }
    }
  }, {
    key: 'getParentOffset',
    value: function getParentOffset() {

      var targetObjects = this;
      var parentOffset = { x: 0, y: 0 };

      while (targetObjects.options.parent) {
        if (targetObjects.options.parent.offset) {
          parentOffset = targetObjects.options.parent.offset;
          break;
        }
        targetObjects = targetObjects.options.parent;
      }

      return parentOffset;
    }
  }, {
    key: 'onResize',
    value: function onResize() {

      _get(Object.getPrototypeOf(OnirimScrollObject.prototype), 'onResize', this).call(this);

      var offset = this.getPosition(this.el, true); //true == get position from document node

      var parentOffset = this.getParentOffset();

      this.offset.x = offset.left - parentOffset.x;
      this.offset.y = offset.top - parentOffset.y;

      this.width = this.$el.offsetWidth;
      this.height = this.$el.offsetHeight;

      if (this.props.ratio) {
        this.parallaxOffset.y = _applicationLayout2['default'].winHeight * this.props.ratio;
      };
    }
  }, {
    key: 'disable',
    value: function disable() {
      if (this.visible) {
        this.addClass('u-force-hide');
        _get(Object.getPrototypeOf(OnirimScrollObject.prototype), 'disable', this).call(this);
      }
    }
  }, {
    key: 'enable',
    value: function enable() {
      if (!this.visible) {
        this.removeClass('u-force-hide');
        _get(Object.getPrototypeOf(OnirimScrollObject.prototype), 'enable', this).call(this);
      }
    }
  }, {
    key: 'enterFrame',
    value: function enterFrame() {

      _get(Object.getPrototypeOf(OnirimScrollObject.prototype), 'enterFrame', this).call(this);

      if (this.props.animate) {
        this.$el.style[_utilsBrowserSupport2['default'].transform] = 'translate(' + 0 + 'px,' + (this.position.y + this.deltaPosition.y) + 'px) ' + _utilsBrowserSupport2['default'].translateZ;
      }
    }
  }]);

  return OnirimScrollObject;
})(_coreComponent2['default']);

exports['default'] = OnirimScrollObject;
module.exports = exports['default'];

},{"../../utils/$setTimeout":65,"../../utils/BrowserSupport":67,"../../utils/clamp":70,"applicationLayout":28,"core/Component":57,"utils/EventDispatcher":68,"utils/dom":71}],52:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimSearch = (function (_Component) {
  _inherits(OnirimSearch, _Component);

  function OnirimSearch(options) {
    _classCallCheck(this, OnirimSearch);

    _get(Object.getPrototypeOf(OnirimSearch.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimSearch, [{
    key: 'afterRender',
    value: function afterRender() {

      _get(Object.getPrototypeOf(OnirimSearch.prototype), 'afterRender', this).call(this);

      this.$body = document.getElementsByTagName("body")[0];
      this.$btnSearch = dom.query('.js-btn-search');
      this.$inputSearch = dom.query('.js-input-search');
      this.$wrapperSearch = dom.query('.js-wrapper-search');
      this.$header = dom.query('.js-main-header');
      this.$wrapperHeader = dom.query('.js-wrapper-header');

      this.onBtnSearchClick = this.onBtnSearchClick.bind(this);
      this.onInputSearchFocus = this.onInputSearchFocus.bind(this);
      this.onDocumentDown = this.onDocumentDown.bind(this);

      document.addEventListener('click', this.onDocumentDown, this);

      dom.addEventListener(this.$btnSearch, 'click', this.onBtnSearchClick);
      dom.addEventListener(this.$inputSearch, 'focus', this.onInputSearchFocus);

      this.isFocused = false;
      this.currentNode = null;
    }
  }, {
    key: 'onBtnSearchClick',
    value: function onBtnSearchClick() {

      if (dom.hasClass('is-searching', this.$body)) {

        _utilsEventDispatcher2['default'].publish('search:closed');

        dom.removeClass('is-searching', this.$body);

        //shame
        dom.removeClass('force-clear', this.$header);

        if (_applicationLayout2['default'].winWidth < 721) {
          dom.removeClass('is-hiding', this.$wrapperHeader);
        }
      } else {
        dom.addClass('is-searching', this.$body);

        _utilsEventDispatcher2['default'].publish('search:opened');

        if (typeof YoutubeCustomPlayer !== "undefined") {
          YoutubeCustomPlayer.stopAllVideos();
        }

        //shame
        dom.addClass('force-clear', this.$header);

        if (_applicationLayout2['default'].winWidth < 721) {
          dom.addClass('is-hiding', this.$wrapperHeader);
        }
      }
    }
  }, {
    key: 'onInputSearchFocus',
    value: function onInputSearchFocus(e) {
      dom.addClass('is-focused', this.$wrapperSearch);

      this.isFocused = true;
      this.currentNode = this.$inputSearch;
    }
  }, {
    key: 'outInputSearchFocusOut',
    value: function outInputSearchFocusOut() {
      dom.removeClass('is-focused', this.$wrapperSearch);

      this.isFocused = false;
      this.currentNode = null;
    }
  }, {
    key: 'onDocumentDown',
    value: function onDocumentDown(e) {

      if (this.isFocused && this.currentNode) {

        var node = e.target;
        var match = false;

        while (node.parentNode) {
          if (node == this.currentNode) {
            match = true;
            break;
          }
          node = node.parentNode;
        }

        //if we click outside the currentPopin, close it
        if (!match) {
          this.outInputSearchFocusOut();
        }
      } else {}
    }
  }]);

  return OnirimSearch;
})(_coreComponent2['default']);

exports['default'] = OnirimSearch;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],53:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsDom = require('utils/dom');

var _utilsDom2 = _interopRequireDefault(_utilsDom);

var _applicationLayout = require('../../applicationLayout');

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var OnirimSlideshow = (function (_Component) {
  _inherits(OnirimSlideshow, _Component);

  function OnirimSlideshow(options) {
    _classCallCheck(this, OnirimSlideshow);

    _get(Object.getPrototypeOf(OnirimSlideshow.prototype), 'constructor', this).call(this, options);
  }

  _createClass(OnirimSlideshow, [{
    key: 'afterRender',
    value: function afterRender() {

      this.$body = document.getElementsByTagName("body")[0];

      this.$preview = _utilsDom2['default'].query('.js-slideshow-preview');

      this.$items = _utilsDom2['default'].queryAll('.js-slideshow-item');
      this.$slides = _utilsDom2['default'].queryAll('.js-slide');

      this.$slideActive = _utilsDom2['default'].query('.is-slide-active');

      this.$arrowLeft = _utilsDom2['default'].query('.js-arrow-left');
      this.$arrowRight = _utilsDom2['default'].query('.js-arrow-right');

      this.$currentSlide = _utilsDom2['default'].query('.js-current-slide');
      this.$totalSlide = _utilsDom2['default'].query('.js-total-slide');

      this.$media = _utilsDom2['default'].queryAll('.js-media-slide');

      // imgLoaded++;
      // if (imgLoaded >= imgtoLoad -1) {
      //  this.onAllImagesLoaded();
      // }

      console.log('preloadImagesNum :', this.preloadImagesNum);

      //console.log(this.$media);

      this.test = _utilsDom2['default'].query('.js-test');

      this.itemsNum = this.$items.length;
      this.slidesNum = this.$slides.length;

      this.$totalSlide.innerHTML = this.slidesNum;

      this.index = null;

      this.activeIndex = Number(Array.prototype.indexOf.call(this.$slides, this.$slideActive)) || 0;

      this.$mediaSlide = _utilsDom2['default'].queryAll('.js-media-slide');

      this.onItemsClick = this.onItemsClick.bind(this);
      this.onArrowRightClick = this.onArrowRightClick.bind(this);
      this.onArrowLeftClick = this.onArrowLeftClick.bind(this);

      for (var i = 0; i < this.itemsNum; i++) {
        _utilsDom2['default'].addEventListener(this.$items[i], 'click', this.onItemsClick);
      };

      _utilsDom2['default'].addEventListener(this.$arrowLeft, 'click', this.onArrowLeftClick);
      _utilsDom2['default'].addEventListener(this.$arrowRight, 'click', this.onArrowRightClick);

      this.setAspectRatio();
    }
  }, {
    key: 'setAspectRatio',
    value: function setAspectRatio() {

      for (var i = 0; i < this.$mediaSlide.length; i++) {

        var width = Number(this.$mediaSlide[i].getAttribute('width'));
        var height = Number(this.$mediaSlide[i].getAttribute('height'));

        var _parent = this.$mediaSlide[i].parentElement;

        if (width > height) {
          _parent.classList.add('is-landscape');
        } else if (width < height) {
          _parent.classList.add('is-portrait');
        } else if (width == height) {
          _parent.classList.add('is-square');
        }
      }
    }
  }, {
    key: 'onItemsClick',
    value: function onItemsClick(e) {

      for (var i = 0; i < this.slidesNum; i++) {
        _utilsDom2['default'].removeClass('is-slide-active', this.$slides[i]);
      };

      this.activeIndex = Number(e.currentTarget.getAttribute('data-index'));

      _utilsDom2['default'].addClass('is-slide-active', this.$slides[this.activeIndex]);

      this.changeCurrentSlide();
    }
  }, {
    key: 'onArrowLeftClick',
    value: function onArrowLeftClick() {

      if (this.activeIndex <= 0) {
        return;
      } else {
        this.activeIndex--;
        this.prevSlide();
      }
    }
  }, {
    key: 'onArrowRightClick',
    value: function onArrowRightClick() {

      if (this.activeIndex >= this.$slides.length - 1) {
        return;
      } else {
        this.activeIndex++;
        this.nextSlide();
      }
    }
  }, {
    key: 'prevSlide',
    value: function prevSlide() {
      for (var i = 0; i < this.slidesNum; i++) {
        _utilsDom2['default'].removeClass('is-slide-active', this.$slides[i]);
      };

      _utilsDom2['default'].addClass('is-slide-active', this.$slides[this.activeIndex]);
      this.changeCurrentSlide();
    }
  }, {
    key: 'nextSlide',
    value: function nextSlide() {
      for (var i = 0; i < this.slidesNum; i++) {
        _utilsDom2['default'].removeClass('is-slide-active', this.$slides[i]);
      };

      _utilsDom2['default'].addClass('is-slide-active', this.$slides[this.activeIndex]);
      this.changeCurrentSlide();
    }
  }, {
    key: 'changeCurrentSlide',
    value: function changeCurrentSlide() {
      this.$currentSlide.innerHTML = this.activeIndex + 1;
    }
  }]);

  return OnirimSlideshow;
})(_coreComponent2['default']);

exports['default'] = OnirimSlideshow;
module.exports = exports['default'];

},{"../../applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/dom":71}],54:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _utilsBrowserSupport = require('utils/BrowserSupport');

var _utilsBrowserSupport2 = _interopRequireDefault(_utilsBrowserSupport);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _utilsDom = require('utils/dom');

var _utilsDom2 = _interopRequireDefault(_utilsDom);

var _applicationLayout = require('../../applicationLayout');

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var Component = require('../../core/Component');
var _ = require('utils/utils');

var OnirimSubNav = (function (_Component) {
  _inherits(OnirimSubNav, _Component);

  function OnirimSubNav(options) {
    _classCallCheck(this, OnirimSubNav);

    _get(Object.getPrototypeOf(OnirimSubNav.prototype), 'constructor', this).call(this, options);

    this.$body = document.getElementsByTagName("body")[0];
    this.$btnGrid = _utilsDom2['default'].query('.js-btn-grid');
    this.$navSection = _utilsDom2['default'].queryAll('.js-nav-section');

    this.$navStacked = _utilsDom2['default'].query('.js-nav-stacked');

    this.$subNavScrollable = _utilsDom2['default'].query('.js-sub-nav-scrollable');

    this.navSectionArr = new Array();

    this.$linkSubNavAnchor = _utilsDom2['default'].queryAll('.js-sub-nav-link-anchor');
    this.linkSubNavAnchorNum = this.$linkSubNavAnchor.length;

    this.onBtnGridClick = this.onBtnGridClick.bind(this);

    _utilsDom2['default'].addEventListener(this.$btnGrid, 'click', this.onBtnGridClick);

    for (var i = 0; i < this.linkSubNavAnchorNum; i++) {
      _utilsDom2['default'].addEventListener(this.$linkSubNavAnchor[i], 'click', this.onLinkSubNavClick.bind(this));
    }

    if (_utilsDom2['default'].hasClass('is-sub-nav-active', this.$body)) {
      this.isOpened = true;
      _utilsDom2['default'].addClass('u-scrollable', this.$subNavScrollable);
    } else {
      this.isOpened = false;
    }

    this.page = this.$body.getAttribute('data-page');
  }

  _createClass(OnirimSubNav, [{
    key: 'getOffsetTop',
    value: function getOffsetTop(elem) {
      var offsetTop = 0;
      do {
        if (!isNaN(elem.offsetTop)) {
          offsetTop += elem.offsetTop;
        }
      } while (elem = elem.offsetParent);
      return offsetTop;
    }
  }, {
    key: 'onBtnGridClick',
    value: function onBtnGridClick() {

      if (this.isOpened) {

        _utilsEventDispatcher2['default'].publish('subnav:closed');
        this.isOpened = false;

        if (this.$navStacked) {
          (0, _utils$setTimeout2['default'])(function () {
            _utilsDom2['default'].addClass('is-stacked', this.$navStacked);
          }, 500, this);
        }

        (0, _utils$setTimeout2['default'])(function () {

          if (this.page == 'post-production') {
            _utilsDom2['default'].removeClass('force-clear', _applicationLayout2['default'].header);
          }

          _utilsDom2['default'].removeClass('is-sub-nav-active', this.$body);

          if (this.$subNavScrollable) {
            _utilsDom2['default'].addClass('u-scrollable', this.$subNavScrollable);
          }
        }, 100, this);
      } else {

        _utilsEventDispatcher2['default'].publish('subnav:opened');
        this.isOpened = true;

        window.scrollTo(0, 0);

        if (this.$navStacked) {
          (0, _utils$setTimeout2['default'])(function () {
            _utilsDom2['default'].removeClass('is-stacked', this.$navStacked);
          }, 1200, this);
        }

        if (this.page != 'home') {
          if (typeof YoutubeCustomPlayer !== "undefined") {
            YoutubeCustomPlayer.stopAllVideos();
          }
        }

        (0, _utils$setTimeout2['default'])(function () {

          if (this.page == 'post-production') {
            _utilsDom2['default'].addClass('force-clear', _applicationLayout2['default'].header);
          }

          if (this.$subNavScrollable) {
            _utilsDom2['default'].addClass('u-scrollable', this.$subNavScrollable);
          }

          _utilsDom2['default'].addClass('is-sub-nav-active', this.$body);
        }, 100, this);
      }
    }
  }, {
    key: 'onLinkSubNavClick',
    value: function onLinkSubNavClick(e) {

      var index = Number(e.currentTarget.getAttribute('data-index'));

      this.scrollToY = this.$navSection[index].offsetTop;
      _utilsEventDispatcher2['default'].publish('subnav:closed');
      _utilsDom2['default'].removeClass('is-sub-nav-active', _applicationLayout2['default'].$body);
      _utilsDom2['default'].removeClass('force-clear', _applicationLayout2['default'].header);
      this.isOpened = false;

      window.scrollTo(0, this.scrollToY);

      //EventDispatcher.publish('linkAnchor:clicked');
    }
  }]);

  return OnirimSubNav;
})(Component);

exports['default'] = OnirimSubNav;
module.exports = exports['default'];

},{"../../applicationLayout":28,"../../core/Component":57,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],55:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _coreComponent = require('core/Component');

var _coreComponent2 = _interopRequireDefault(_coreComponent);

var _utils$setTimeout = require('utils/$setTimeout');

var _utils$setTimeout2 = _interopRequireDefault(_utils$setTimeout);

var _utilsEventDispatcher = require('utils/EventDispatcher');

var _utilsEventDispatcher2 = _interopRequireDefault(_utilsEventDispatcher);

var _apiStateProvider = require("api/StateProvider");

var _apiStateProvider2 = _interopRequireDefault(_apiStateProvider);

var _applicationLayout = require("applicationLayout");

var _applicationLayout2 = _interopRequireDefault(_applicationLayout);

var _utils$xhr = require('utils/$xhr');

var _utils$xhr2 = _interopRequireDefault(_utils$xhr);

var _ = require('utils/utils');
var dom = require('utils/dom');
var support = require('utils/BrowserSupport');

var OnirimYoutubeOverlay = (function (_Component) {
  _inherits(OnirimYoutubeOverlay, _Component);

  function OnirimYoutubeOverlay(options) {
    _classCallCheck(this, OnirimYoutubeOverlay);

    _get(Object.getPrototypeOf(OnirimYoutubeOverlay.prototype), 'constructor', this).call(this, options);

    this.events.set({
      'click .js-link-overlay-youtube': 'onLinkOverlayClick'
    });
  }

  _createClass(OnirimYoutubeOverlay, [{
    key: 'onLinkOverlayClick',
    value: function onLinkOverlayClick() {
      if (dom.hasClass('is-overlay-active', this.el)) {
        this.removeClass('is-overlay-active', this.el);
      } else {
        this.addClass('is-overlay-active', this.el);
      }
    }
  }]);

  return OnirimYoutubeOverlay;
})(_coreComponent2['default']);

exports['default'] = OnirimYoutubeOverlay;
module.exports = exports['default'];

},{"api/StateProvider":25,"applicationLayout":28,"core/Component":57,"utils/$setTimeout":65,"utils/$xhr":66,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/dom":71,"utils/utils":73}],56:[function(require,module,exports){
'use strict';

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

var _utilsUtils = require('utils/utils');

var _ = _interopRequireWildcard(_utilsUtils);

var events = require('api/events');
var $setTimeout = require('utils/$setTimeout');
var $xhr = require('utils/$xhr');

var $Object = function $Object(attributes, options) {

  var attrs = attributes || {};

  options = _.extend({}, options || {
    autoFetch: false,
    url: null
  });

  this.addNonEnumerableProperty('id', null);
  this.addNonEnumerableProperty('idAttribute', null);
  this.addNonEnumerableProperty("callbackStack", {}); //used by the view event chanel
  this.addNonEnumerableProperty("attributes", {});
  this.addNonEnumerableProperty("changed", {});
  this.addNonEnumerableProperty("_previousAttributes", {});
  this.addNonEnumerableProperty("_changing", false);
  this.addNonEnumerableProperty("_pending", false);

  // this.callbackStack = {};
  // this.attributes = {};
  // this.changed = {};
  // this._previousAttributes = {};
  // this._changing = false;
  // this._pending = false;

  attrs = _.extend({}, attrs || {});

  this.set(attrs, options);

  if (options.url && options.autoFetch) {
    $setTimeout(function () {
      this.fetch(options.url);
    }, 10, this);
  }
};

$Object.prototype = _.extend({

  addNonEnumerableProperty: function addNonEnumerableProperty(name, value) {
    Object.defineProperty(this, name, {
      writable: true,
      enumerable: false,
      value: value
    });
  },

  fetch: function fetch(url) {

    if (typeof url === "undefined") {
      _.warn('cannot fetech datas, no url specified', this);
    }

    var model = this;

    $xhr.get(url).then(function (resp) {
      console.log('ajax callback', JSON.parse(resp));
      model.set(JSON.parse(resp));
    }, function (resp) {
      _.warn('ajax error on fetch', model);
    });
  },

  get: function get(attr) {
    return this[attr] || null;
  },

  toJSON: function toJSON(options) {
    return _.clone(this);
  },

  has: function has(attr) {
    return this.get(attr) != null;
  },

  hasChanged: function hasChanged(attr) {
    return attr in this.changed;
  },

  /**
   * Extracted from Backbone.Model
   *
   * Backbone.js 1.2.1
   * (c) 2010-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Backbone may be freely distributed under the MIT license.
   * For all details and documentation:
   * http://backbonejs.org
   * 
   * TODO: rewrite this and check any perf optimization that can be done
   */

  set: function set(key, val, options) {

    if (key == null) return this;

    // Handle both `"key", value` and `{key: value}` -style arguments.
    var attrs;
    if (typeof key === 'object') {
      attrs = key;
      options = val;
    } else {
      (attrs = {})[key] = val;
    }

    options || (options = {});

    // Extract attributes and options.
    var unset = options.unset;
    var silent = options.silent || false;
    var changes = [];
    var changing = this._changing;
    this._changing = true;

    if (!changing) {
      this._previousAttributes = _.clone(this);
      this.changed = {};
    }

    var current = this;
    var changed = this.changed;
    var prev = this._previousAttributes;

    // For each `set` attribute, update or delete the current value.
    for (var attr in attrs) {
      val = attrs[attr];
      if (!_.isEqual(current[attr], val)) changes.push(attr);
      if (!_.isEqual(prev[attr], val)) {
        changed[attr] = val;
      } else {
        delete changed[attr];
      }
      unset ? delete current[attr] : current[attr] = val;
    }

    // Update the `id`.
    this.id = this.get(this.idAttribute);

    // Trigger all relevant attribute changes.
    if (!silent) {
      if (changes.length) this._pending = options;
      for (var i = 0; i < changes.length; i++) {
        this.trigger('change:' + changes[i], this, current[changes[i]], options);
      }
    }

    // You might be wondering why there's a `while` loop here. Changes can
    // be recursively nested within `"change"` events.
    if (changing) return this;
    if (!silent) {
      while (this._pending) {
        options = this._pending;
        this._pending = false;
        this.trigger('change', this, options);
      }
    }

    this._pending = false;
    this._changing = false;

    return this;
  }

}, events);

module.exports = $Object;

},{"api/events":26,"utils/$setTimeout":65,"utils/$xhr":66,"utils/utils":73}],57:[function(require,module,exports){
//require("lib/handlebarHelpers");

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _modelsBaseModel = require('models/BaseModel');

var _modelsBaseModel2 = _interopRequireDefault(_modelsBaseModel);

var _utilsUtils = require('utils/utils');

var _ = _interopRequireWildcard(_utilsUtils);

//contain components when monted

var _apiComponentProvider = require('api/ComponentProvider');

var _apiComponentProvider2 = _interopRequireDefault(_apiComponentProvider);

var _core$Object = require('core/$Object');

var _core$Object2 = _interopRequireDefault(_core$Object);

var EventDispatcher = require('utils/EventDispatcher');
var EventList = require('utils/EventList');
var support = require('utils/BrowserSupport');
var $setTimeout = require('utils/$setTimeout');
var dom = require('utils/dom');
var events = require('api/events');

var directives = require('directives');
var Directive = require('directives/Directive');

var GUID = 0;
var BINDING_GUI = 0;

//TODO:: add multiple selector on events manager
//TODO setStyle: add multiple selector/nodes
function Component(options) {

  var self = this;

  this.options = _.extend({
    tagName: 'div',
    attributes: {},
    id: null,
    className: null,
    el: null,
    behaviours: null,
    model: {},
    index: 0
  }, options || {});

  this.visible = true;

  //set the el element
  this.setElement(this.options.el);

  this.template = this.options.template || null;

  this.props = new _core$Object2['default']();

  this.events = new _core$Object2['default']();

  this.index = this.options.index;
  //a way to access child component based on data-ref attribute
  //you can also access child component with .children[ childrenIndex ]
  this.refs = {};

  // Get app config for all views
  // TODO: seperate model(rename it as context)
  if (typeof this.options.model.toJSON != "undefined") {
    this.model = this.options.model;
    //this.model.set(EventDispatcher.request('app:config'));
  } else {
      //if no parent, create a new context
      this.model = new _core$Object2['default'](); //EventDispatcher.request('app:config');
      //this.model.set(this.options.model);
    }

  this.imagesLoaded = false;

  this.subViews = {}; //store list of subviews
  this.numSubViews = 0;
  this.numSubViewsLoaded = 0;

  this.behaviours = {}; //store list of initated behaviours

  /**
   *  used by image preloader to choose the right src
   */
  // this.winWidth = EventDispatcher.request('layout', ['winWidth']);
  // this.winHeight = EventDispatcher.request('layout', ['winHeight']);
  // this.scaleFactor = EventDispatcher.request('layout', ['scaleFactor']);
  // this.winRatio = EventDispatcher.request('layout', ['winRatio']);

  this._enterFrame = this.enterFrame.bind(this);

  //keep ref to Views Directives
  this._directives = [];

  this.bindMethods(["_onImageLoaded", "_onDelegateEvent"]);
  this.preloadImages = [];

  //check attributes values and find callback
  //store the result in this.props
  //merge all this.events

  $setTimeout(function () {
    this.getPropsFromAttributes();
  }, 0, this);
}

Component.prototype = _.extend({

  constructor: Component,

  getNumChildren: function getNumChildren() {
    var num = 0;
    for (var k in this.subViews) {
      num++;
    }
    return num;
  },

  getPropsFromAttributes: function getPropsFromAttributes() {

    // console.log("Component::getPropsFromAttributes()", this);
    var scope = this;

    var callbackProps = {};

    //first we loop through attribute
    //if any of them match data-on , consider it it a callbackProps
    for (var i = 0, l = this.el.attributes.length; i < l; i++) {
      var formatedPropName = _.camelCalise(this.el.attributes[i].nodeName.replace("data-", ""));
      if (/^on/.test(formatedPropName)) {
        callbackProps[formatedPropName] = this.el.attributes[i].nodeValue;
      } else {
        if (this.el.attributes[i].nodeValue !== "") {
          this.props.set(formatedPropName, this.el.attributes[i].nodeValue);
        }
      }
    }

    //no we loop through each parents to find any method matching the callbackProps
    while (scope.options.parent) {
      for (var k in callbackProps) {
        if (callbackProps[k] in scope.options.parent) {
          this.props.set(k, scope.options.parent[callbackProps[k]].bind(scope.options.parent));
          delete callbackProps[k];
        }
      }
      scope = scope.options.parent;
    }

    //if  there are any callback left:
    for (var k in callbackProps) {
      _.warn('no \'' + k + '\’ method found in any parent scope', callbackProps[k]);
      this.props.set(k, function () {}); //we set a null function to avoid js errors
    }
  },

  open: function open() {},

  close: function close() {},

  setElement: function setElement(el) {

    el = typeof el === 'string' ? this.$once(el) : el;

    /**
     * if not dom node as been found, create a new element using tagName , className and id options
     */
    if (!el) {
      var attr = _.clone(this.options.attributes);
      if (this.options.id) {
        attr.id = this.options.id;
      }
      if (this.options.className) {
        attr.className = this.options.className;
      }
      el = document.createElement(this.options.tagName);
      this.el = this.$el = el;
      this.setAttributes(attr);
    } else {
      this.el = this.$el = el;
    }
  },

  template: null,

  setAttributes: function setAttributes(attr) {
    for (var a in attr) {
      if (a == "className") {
        this.el.setAttribute('class', attr[a]);
      } else {
        this.el.setAttribute(a, attr[a]);
      }
    }
  },

  serializeData: function serializeData() {
    var model = this.model ? this.model.toJSON() : {};
    var props = this.props ? this.props.toJSON() : {};
    return _.extend(model, props);
  },

  getTemplate: function getTemplate() {
    return this.template;
  },

  /** 
   * create/retrieve inital DOM
   */
  render: function render() {

    if (this.hasRendered) {

      clearTimeout(this.afterResizeTimer);
      clearTimeout(this.parseTimer);
      clearTimeout(this.afterRenderTimer);
      clearTimeout(this.renderComponentsTimer);
      clearTimeout(this.resizeTimer);
      clearTimeout(this.afterShowTimer);
      clearTimeout(this.showTimer);

      for (var i = 0; i < this.preloadImages.length; i++) {
        if (this.preloadImages[i] && this.preloadImages[i].onload) {
          this.preloadImages[i].onload = null;
        }
        this.preloadImages[i] = null;
      }
      this.preloadImages = [];

      for (var k in this.subViews) {
        if (this.subViews[k].destroy) {
          this.subViews[k].destroy(true);
          delete this.subViews[k];
        }
      }

      for (var k in this.refs) {
        delete this.subViews[k];
      }

      for (var d = 0; d < this._directives.length; d++) {
        this._directives[d].destroy();
      }

      this._directives = [];

      for (var behaviour in this.behaviours) {
        this.behaviours[behaviour].destroy();
        delete this.behaviours[behaviour];
      }
    }

    var template = this.getTemplate();
    var datas = this.serializeData();

    if (template) {
      if (!this.hasRendered /*&& !this.el.hasChildNodes()*/ || this.hasRendered) {
        this.attachElContent(_.isFunction(this.template) ? this.template(datas) : this.template);
      }
    }

    this.hasRendered = true;

    this.afterRenderTimer = $setTimeout(function () {
      this.parseView();
    }, 0, this);

    return this;
  },

  attachElContent: function attachElContent(html) {
    this.el.innerHTML = html;
  },

  attachHtml: function attachHtml(node) {
    this.el.appendChild(node);
  },

  /** 
   * init subViews
   * manipulate DOM
   * cache selectors
   */
  afterRender: function afterRender() {
    this.canResize = true;

    for (var k in this.subViews) {
      if (this.subViews[k].props.ref) {
        this.refs[this.subViews[k].props.ref] = this.subViews[k];
      }
    }

    this.resizeTimer = $setTimeout(function () {
      this.onResize();
      this.loadImages();
    }, 0, this);
  },

  addToPreloadQueue: function addToPreloadQueue(image) {
    this.preloadImages.push(image.src);
  },

  loadImages: function loadImages() {

    var img;

    for (var i = 0, l = this.preloadImages.length; i < l; i++) {
      img = new Image();
      img.onload = this._onImageLoaded;
      img.src = this.preloadImages[i];
      this.preloadImages[i] = img;
    }
  },

  _onImageLoaded: function _onImageLoaded() {
    this.trigger('image:loaded');
    this.onResize();
  },

  parseView: function parseView() {
    this.transcludeContent();
    this.recursiveParseNodes(this.el);
    this.initBehaviours();
    this.initEvents();
    if (this.numSubViews === 0) {
      this.trigger('after:render');
    }

    //trigger render async  because some Component.constructor()
    //stuffs are done after the execution stack ended
    this.renderComponentsTimer = $setTimeout(function () {
      this._renderComponents();
    }, 0, this);

    this.afterRenderTimer = $setTimeout(function () {
      this.afterRender();
    }, 0, this);
  },

  transcludeContent: function transcludeContent() {

    if (!this.template && this.options.content) {
      while (this.options.content.hasChildNodes()) {
        this.el.appendChild(this.options.content.childNodes[0]);
      }
      return;
    }

    //content must only appear once in a template
    var $content = this.$('content');

    if ($content.length > 1) {
      _.warn("<content> tag must only appear once in a template");
    }

    if ($content.length > 0) {
      $content = $content[0];
      if (this.options.content && this.options.content.hasChildNodes()) {
        while (this.options.content.hasChildNodes()) {
          $content.parentNode.insertBefore(this.options.content.childNodes[0], $content);
        }
      } else {
        while ($content.hasChildNodes()) {
          $content.parentNode.insertBefore($content.childNodes[0], $content);
        }
      }
      $content.parentNode.removeChild($content);
    }
  },

  recursiveParseNodes: function recursiveParseNodes(node) {

    //if the node is binded to a child view, let the child view parse it to retrieve directive
    //this scope the dom to the closets view
    //and avoid double event bind from ParentView + childView
    if (this.numSubViews !== 0) {
      for (var s in this.subViews) {
        if (node == this.subViews[s].el) {
          return;
        }
      }
    }

    if (this.hasClass('js-preload', node) && node instanceof HTMLImageElement) {
      this.addToPreloadQueue(node);
    }

    if (node != this.el) {

      if (this.hasAttr('data-component', node)) {

        var componentName = this.getAttr('data-component', node);
        if (componentName in _apiComponentProvider.components) {
          //_.warn( "data-component=\""+componentName+"\" is depreciated. Please use custom tag instead (remember that a custom tag need a '-' in it's name to avoid any conflicts with futur native html tag)" );
          this._bindComponent(componentName, _apiComponentProvider.components[componentName], node);
          return; //don't check for subNodes as the component will do it
        }
      } else if (node.tagName.toLowerCase() in _apiComponentProvider.components) {
          var componentName = node.tagName.toLowerCase();
          var newNode = document.createElement("div");
          for (var i = 0, l = node.attributes.length; i < l; i++) {
            newNode.setAttribute(node.attributes[i].nodeName, node.attributes[i].nodeValue);
          }

          var content = document.createDocumentFragment();

          while (node.hasChildNodes()) {
            content.appendChild(node.childNodes[0]);
          }

          node.parentNode.insertBefore(newNode, node);
          node.parentNode.removeChild(node);
          this._bindComponent(componentName, _apiComponentProvider.components[componentName], newNode, content);
          return; //don't check for subNodes as the component will do it
        }
    }

    for (var i = 0, l = node.childNodes.length; i < l; i++) {
      //childNode will include textNode
      //we cannot use .children as it is not supported on svg elements on safari
      //so we check if we're dealing with textNode(nodetype == 3)
      //check if it is a comment (nodetype == 8)
      if (node.childNodes[i].nodeType != 3 && node.childNodes[i].nodeType != 8) {
        this._checkDirective(node.childNodes[i]);
        this.recursiveParseNodes(node.childNodes[i]);
      }
    }
  },

  _bindComponent: function _bindComponent(name, componentClass, node, content) {

    this.setAttr('data-component', '__' + name, node);

    //if a subview already exists with this component name
    if (typeof this.subViews[name] !== "undefined") {
      name += GUID;
      GUID++;
    }

    var componentModel;
    var scope = node.getAttribute('data-scope') !== null ? node.getAttribute('data-scope') : "inherit";

    //INHERIT MODEL FROM PARENT
    if (scope == "inherit") {
      this.addSubView(name, componentClass, {
        el: node,
        model: this.model,
        content: content
      });
    } else {
      this.addSubView(name, componentClass, {
        el: node,
        model: new _modelsBaseModel2['default'](this.model.toJSON()),
        content: content
      });
    }
  },

  _renderComponents: function _renderComponents() {
    for (var k in this.subViews) {
      this.subViews[k].render();
    }
  },

  _checkDirective: function _checkDirective(node) {
    if (this.hasAttr('data-directive', node)) {
      for (var d in directives) {
        if (this.getAttr('data-directive', node) == d) {
          this._bindDirective(d, node, this.model, directives[d]);
        }
      }
    }
  },

  _bindDirective: function _bindDirective(name, node, desc, def) {

    this.setAttr('data-directive', '__' + name, node);

    this._directives.push(new Directive(name, node, desc, def));
  },

  addSubView: function addSubView(viewInstanceName, viewClass, viewOptions) {
    var options = viewOptions || {};
    options._id = viewInstanceName;
    options.parent = this;
    options.index = this.numSubViews;
    this.numSubViews++;
    this.subViews[viewInstanceName] = new viewClass(options);
    return this.subViews[viewInstanceName];
  },

  /**
   *  initalize the view behaviours(scroll/popin/overlay etc)
   */
  initBehaviours: function initBehaviours() {
    if (this.options.behaviours) {
      for (var behaviour in this.options.behaviours) {
        var behaviourClass = this.options.behaviours[behaviour].behaviorClass;
        var behaviourOptions = this.options.behaviours[behaviour].options;
        this.behaviours[behaviour] = new behaviourClass(behaviourOptions, this);
      }
    }
  },

  initEvents: function initEvents() {

    this._delegateEvents = {};

    for (var eventKey in this.events) {

      if (this.events.hasOwnProperty(eventKey)) {

        var methodName = this.events[eventKey];
        var eventElements = eventKey.split(" ");

        //pointerenter and pointerleave do not propagate so replace it with pointerover/pointerout
        //we need to do a check on the callback not to trigger enter/leave if we roll over/out a child of the node
        if (eventElements[0] == "pointerenter") {
          eventElements[0] = "pointerover";
        } else if (eventElements[0] == "pointerleave") {
          eventElements[0] = "pointerout";
        }

        var eventName = /pointer/.test(eventElements[0]) ? support[eventElements[0]] : eventElements[0];

        var selector = eventElements[1];

        if (typeof this[methodName] !== 'undefined') {

          this.bindMethods(methodName);

          if (typeof this._delegateEvents[eventName] === "undefined") {
            this._delegateEvents[eventName] = [];
            dom.addEventListener(this.el, eventName, this._onDelegateEvent);
          }

          this._delegateEvents[eventName].push({
            selector: selector,
            callback: this[methodName]
          });
        } else {
          throw new Error('oups, there isnt any ' + methodName + ' method defined for this view');
        }
      }
    }
  },

  _onDelegateEvent: function _onDelegateEvent(originalEvent) {

    var delegateEvents = this._delegateEvents[originalEvent.type];
    var stopPropagation = false;

    for (var i = 0; i < delegateEvents.length; i++) {

      if (stopPropagation) {
        _.notify('Event propagation has been stopped');
        break;
        return;
      }

      //if delegation
      if (delegateEvents[i].selector) {

        var related = originalEvent.relatedTarget;
        var node = originalEvent.target;

        while (node && node !== this.el) {
          if (this.hasClass(delegateEvents[i].selector.replace('.', ''), node)) {
            /**
             * special case for mouseover/mouseout as it is triggered by subnode mouseover/mouseout
             * which is in most case the deisred behaviour
             * we check if the related (it is the element matched by the mouse after the mouseout/mouseover event)
             * is inside the node. If it is, it means we did not really "over" or "out" the node so no need to trigger callback
             */
            if (originalEvent.type !== "mouseover" && originalEvent.type !== "mouseout" || !_.contains(node, related)) {
              var originalEventCopy = _.clone(originalEvent);
              originalEventCopy.currentTarget = node;
              originalEventCopy.stopPropagation = function () {
                stopPropagation = true;
              };
              delegateEvents[i].callback(originalEventCopy);
            }
          }

          node = node.parentNode;
        }
      }

      //else it's binded to this.el so just trigger the callback
      else {
          delegateEvents[i].callback(originalEvent);
        }
    }
  },

  removeEvents: function removeEvents() {

    for (var eventName in this._delegateEvents) {
      dom.removeEventListener(this.el, eventName, this._onDelegateEvent);
    }

    // for (var eventKey in this.events) {
    //   var methodName = this.events[eventKey];
    //   var eventElements = eventKey.split(" ");
    //   var eventName = /pointer/.test(eventElements[0]) ? support[eventElements[0]] : eventElements[0];
    //   var selector = eventElements[1];
    //   if (typeof this[methodName] != 'undefined') {
    //     var el = selector ? this.$(selector) : this.el;
    //     if (_.isArray(el) || (el.length && el instanceof NodeList) ) {
    //       for (var i = 0, l = el.length; i < l; i++) {
    //         dom.removeEventListener( el[i], eventName, this[methodName] );
    //       }
    //     } else {
    //       dom.removeEventListener( el, eventName, this[methodName]);
    //     }
    //   }
    // }
  },

  destroy: function destroy(keepParentNode) {

    clearTimeout(this.afterResizeTimer);
    clearTimeout(this.parseTimer);
    clearTimeout(this.afterRenderTimer);
    clearTimeout(this.renderComponentsTimer);

    clearTimeout(this.resizeTimer);
    clearTimeout(this.afterShowTimer);
    clearTimeout(this.showTimer);

    for (var i = 0, l = this.preloadImages.length; i < l; i++) {
      if (this.preloadImages[i] && this.preloadImages[i].onload) {
        this.preloadImages[i].onload = null;
      }
      this.preloadImages[i] = null;
    }

    for (var k in this.subViews) {
      if (this.subViews[k].destroy) {
        this.subViews[k].destroy();
      }
    }

    //     delete this.subViews[k];
    // for (let k in this.refs) {
    //   delete this.subViews[k];
    // }

    this.removeEvents();

    if (!keepParentNode) {
      this.removeEl();
    }

    if (this.options.ref && this.options.ref in this.options.parent.refs) {
      delete this.options.parent.refs[this.options.ref];
    }

    //console.log('this.options.parent', this.options.parent);
    if (this.options._id in this.options.parent.subViews) {
      delete this.options.parent.subViews[this.options._id];
    }
  },

  removeEl: function removeEl() {
    this.el.parentNode.removeChild(this.el);
  },

  /**
   *  reflow the view/ set DOM elemnts size
   *  call resize on subviews
   * attch the resize method to the global chanel resize event
   */
  onResize: function onResize() {

    if (!this.canResize) {
      return;
    }

    // this.winWidth = EventDispatcher.request('layout', ['winWidth']);
    // this.winHeight = EventDispatcher.request('layout', ['winHeight']);

    // this.halfWinWidth = EventDispatcher.request('layout', ['halfWinWidth']);
    // this.halfWinHeight = EventDispatcher.request('layout', ['halfWinHeight']);

    // this.quarterWinWidth = EventDispatcher.request('layout', ['quarterWinWidth']);
    // this.quarterWinHeight = EventDispatcher.request('layout', ['quarterWinHeight']);

    // this.winRatio = EventDispatcher.request('layout', ['winRatio']);
    // this.scaleFactor = EventDispatcher.request('layout', ['scaleFactor']);

    // this.scaleLayerHeight = this.scaleFactor / this.winRatio;
    // this.halfScaleFactor = this.scaleFactor >> 1;
    // this.halfScaleLayerHeight = this.scaleLayerHeight >> 1;

    //we use asynch not to force calling super()
    //at thevery end of each overwritten methods
    //super can be called at the begining
    this.afterResizeTimer = $setTimeout(function () {

      if (this.numSubViews > 0) {
        for (var k in this.subViews) {
          if (this.subViews[k].hasRendered && this.subViews[k].canResize) {
            this.subViews[k].triggerMethod('resize');
          }
        }
      }

      this.hasResized = true;
      this.afterResize();
    }, 0, this);
  },

  afterResize: function afterResize() {
    // for (var k in this.subViews) {
    //   if( this.subViews[k] && this.subViews[k].off ){
    //     this.subViews[k].off('after:resize', this.onSubViewResized, this);
    //   }
    // }
    //
    //
    this.trigger('after:resize');

    this.showTimer = $setTimeout(function () {
      if (this.numSubViews > 0) {
        for (var k in this.subViews) {
          if (this.subViews[k].hasResized) {
            this.subViews[k].triggerMethod('show');
          }
        }
      }

      this.onShow();
    }, 0, this);
  },

  /**
   * call 'show' method on subviews for them to render/push textures to the GPU
   * then call the view show method to render/push its textures to the GPU
   * we do this from subview to parentview because the browser
   * compositor need to create all child layer before rendering the parent layer.
   * This prevent the browser from pushing textures multiple times to the GPU.
   */
  onShow: function onShow() {

    this.afterShowTimer = $setTimeout(function () {

      if (this.numSubViews > 0) {
        for (var k in this.subViews) {
          this.subViews[k].triggerMethod('show');
        }
      }

      this.afterShow();
    }, 0, this);
  },

  /**
   * call 'show' method on subviews for them to render/push textures to the GPU
   * then call the view show method to render/push its textures to the GPU
   * we do this from subview to parentview because the browser
   * compositor need to create all child layer before rendering the parent layer.
   * This prevent the browser from pushing textures multiple times to the GPU.
   */
  afterShow: function afterShow() {

    this.trigger('after:show');
  },

  enterFrame: function enterFrame() {
    if (this.visible) {
      for (var k in this.subViews) {
        if (this.subViews[k].visible && this.subViews[k].hasResized) {
          this.subViews[k].enterFrame(); //no event chanel to limit intermediires
        }
      }
    }
  },

  disable: function disable() {
    this.visible = false;
    for (var k in this.subViews) {
      this.subViews[k].disable();
    }
  },

  enable: function enable() {
    this.visible = true;
    for (var k in this.subViews) {
      this.subViews[k].enable();
    }
  },

  $: function $(selector) {
    return dom.queryAll(selector, this.el);
  },

  $once: function $once(selector) {
    return dom.query(selector, this.el);
  },

  hasAttr: function hasAttr(attrName, node) {
    if (node.hasAttribute) {
      return node.hasAttribute(attrName);
    } else {
      return typeof node.getAttribute(attrName) !== "undefined" ? true : false;
    }
  },

  getAttr: function getAttr(attrName, node) {
    return typeof node.getAttribute(attrName) !== "undefined" ? node.getAttribute(attrName) : false;
  },

  setAttr: function setAttr(attrName, value, node) {
    if (_.isObject(attrName)) {
      node = value;
      for (var a in attrName) {
        node.setAttribute(a, attrName[a]);
      }
    } else {
      node.setAttribute(attrName, value);
    }
  },

  toggleClass: function toggleClass(className, selector) {
    var el = selector ? this.$(selector) : this.el;
    if (_.isArray(el) || el.length && el instanceof NodeList) {
      for (var i = 0, l = el.length; i < l; i++) {
        if (dom.hasClass(className, el[i])) {
          dom.removeClass(className, el[i]);
        } else {
          dom.addClass(className, el[i]);
        }
      }
    } else {
      if (dom.hasClass(className, el)) {
        dom.removeClass(className, el);
      } else {
        dom.addClass(className, el);
      }
    }
  },

  hasClass: function hasClass(className, selector) {
    var el = selector ? this.$once(selector) : this.$el;
    return dom.hasClass(className, el);
  },

  addClass: function addClass(className, selector) {

    if (this.hasClass(className, selector)) {
      return;
    }

    var el = selector ? this.$(selector) : this.el;

    if (_.isArray(el) || el.length && el instanceof NodeList) {
      for (var i = 0, l = el.length; i < l; i++) {
        dom.addClass(className, el[i]);
      }
      return;
    }

    return dom.addClass(className, el);
  },

  removeClass: function removeClass(className, selector) {
    var el = selector ? this.$(selector) : this.el;
    if (_.isArray(el) || el.length && el instanceof NodeList) {
      for (var i = 0, l = el.length; i < l; i++) {
        dom.removeClass(className, el[i]);
      }
      return;
    }
    return dom.removeClass(className, el);
  },

  getStyle: function getStyle(node, styleProp) {
    var value;
    if (node.currentStyle) {
      value = node.currentStyle[styleProp];
    } else if (window.getComputedStyle) value = document.defaultView.getComputedStyle(node, null).getPropertyValue(styleProp);
    return value;
  },

  getPosition: function getPosition(node, fromBaseNode) {

    var root = !fromBaseNode ? this.el : document;
    var offsetTop = node.offsetTop;
    var offsetLeft = node.offsetLeft;

    while (node && node.parentNode && (!fromBaseNode && node != this.el || fromBaseNode && node.parentNode != document)) {
      if (this.getStyle(node.parentNode, 'position') != 'static') {
        offsetTop += node.parentNode.offsetTop;
        offsetLeft += node.parentNode.offsetLeft;
      }
      node = node.parentNode;
    }

    return {
      top: offsetTop,
      left: offsetLeft
    };
  },

  setStyle: function setStyle(selector, props) {
    var el = selector ? this.$(selector) : this.el;
    if (_.isArray(el) || el.length && el instanceof NodeList) {
      for (var i = 0, l = el.length; i < l; i++) {
        dom.setStyle(el[i], props);
      }
    } else {
      dom.setStyle(el, props);
    }
  },

  transform: function transform(selector, transformPorps) {
    var el = selector ? this.$(selector) : this.el;
    if (_.isArray(el) || el.length && el instanceof NodeList) {
      for (var i = 0, l = el.length; i < l; i++) {
        dom.transform(el[i], transformPorps);
      }
    } else {
      dom.transform(el, transformPorps);
    }
  },

  /**
   * bind method to workd with EventDispatcher without changing it's name
   * no more need to do :
   *    this._methodName = this.methodName.bind(this)
   * simply do :
   *    this.bindMethods("myMethod", "myOtherMethod", "myLastMethod");
   */
  bindMethods: function bindMethods(methodNames) {
    var methodNames = typeof methodNames == "object" && methodNames.length ? methodNames : [methodNames];
    for (var i = 0, l = methodNames.length; i < l; i++) {
      var oldMethod = this[methodNames[i]];
      this[methodNames[i]] = oldMethod.bind(this);
    }
  }

}, events);

Component.mount = function (name, componentClass) {
  _apiComponentProvider2['default'].mount(name, componentClass);
};

exports['default'] = Component;
module.exports = exports['default'];

},{"api/ComponentProvider":24,"api/events":26,"core/$Object":56,"directives":60,"directives/Directive":58,"models/BaseModel":64,"utils/$setTimeout":65,"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/EventList":69,"utils/dom":71,"utils/utils":73}],58:[function(require,module,exports){
'use strict';

var _ = require('utils/utils');

function Directive(name, el, model, def) {

	this.name = name;
	this.el = el;
	this.model = model;

	if (_.isFunction(def)) {
		this.update = def;
	} else {
		_.extend(this, def);
	}

	this.bind();
}

Directive.prototype = {
	bind: function bind() {},
	update: function update() {},
	destroy: function destroy() {}
};

module.exports = Directive;

},{"utils/utils":73}],59:[function(require,module,exports){
'use strict';

var EventDispatcher = require('utils/EventDispatcher');
var EventList = require('utils/EventList');
var dom = require('utils/dom');
var _ = require('utils/utils');
var support = require('utils/BrowserSupport');

module.exports = {

	bind: function bind() {

		this.x = 0;
		this.y = 0;
		this.scale = 1;

		dom.setStyle(this.el, {
			position: 'absolute',
			top: 0, left: 0,
			transformOrigin: "0 0"
		});

		EventDispatcher.subscribe(EventList.RESIZE, this.onResize, this);
		this.onResize();
	},

	update: function update(value) {},

	unbind: function unbind() {},

	onResize: function onResize() {

		this.coverWidth = this.el.clientWidth;
		this.coverHeight = this.el.clientHeight;
		this.coverParent = this.el.parentNode;
		this.coverParentWidth = this.coverParent.clientWidth;
		this.coverParentHeight = this.coverParent.clientHeight;

		if (this.coverWidth !== 0 && this.coverHeight !== 0) {
			this.coverRatio = this.coverWidth / this.coverHeight;
			this.coverSwitcher = this.coverParentWidth / this.coverRatio > this.coverParentHeight;
			this.coverComputedWidth = this.coverSwitcher ? this.coverParentWidth : this.coverParentHeight * this.coverRatio;
			this.coverComputedHeight = this.coverSwitcher ? this.coverParentWidth / this.coverRatio : this.coverParentHeight;
			this.scale = this.coverComputedWidth / this.coverWidth;
			this.x = (this.coverParentWidth - this.coverComputedWidth) / 2;
			this.y = (this.coverParentHeight - this.coverComputedHeight) / 2;
		}

		dom.setStyle(this.el, {
			transform: 'translate(' + this.x + 'px, ' + this.y + 'px) scale(' + this.scale + ')' + support.translateZ
		});
	},

	destroy: function destroy() {}

};

},{"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/EventList":69,"utils/dom":71,"utils/utils":73}],60:[function(require,module,exports){
'use strict';

exports.sref = require('./sref');
exports["toggle-class"] = require('./toggleClass');
exports["cover"] = require('./cover');

},{"./cover":59,"./sref":61,"./toggleClass":62}],61:[function(require,module,exports){
'use strict';

var EventDispatcher = require('utils/EventDispatcher');
var EventList = require('utils/EventList');
var dom = require('utils/dom');
var _ = require('utils/utils');
var support = require('utils/BrowserSupport');

module.exports = {

	bind: function bind() {

		this.originalHref = this.el.getAttribute('href');
		this._onPointerDown = this.onPointerDown.bind(this);
		this.el.setAttribute('href', 'javascript:void(0)');

		this.el.setAttribute('target', '');

		dom.addEventListener(this.el, "click", this._onPointerDown);

		this.sref = this.el.getAttribute('data-state');

		if (this.el.getAttribute('data-state-params', false)) {
			this.sparams = JSON.parse(this.el.getAttribute('data-state-params'));
		} else {
			this.sparams = {};
		}
	},

	update: function update(value) {},

	unbind: function unbind() {
		dom.removeEventListener(this.el, support.pointerdown, this._onPointerDown);
		this.el.setAttribute('href', this.originalHref);
	},

	onPointerDown: function onPointerDown(e) {
		e.preventDefault();
		//e.stopPropagation();
		EventDispatcher.trigger('statechange', [this.sref, this.sparams]);
	},

	destroy: function destroy() {
		dom.removeEventListener(this.el, support.pointerdown, this._onPointerDown);
	}

};

},{"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/EventList":69,"utils/dom":71,"utils/utils":73}],62:[function(require,module,exports){
'use strict';

var EventDispatcher = require('utils/EventDispatcher');
var EventList = require('utils/EventList');
var dom = require('utils/dom');
var _ = require('utils/utils');
var support = require('utils/BrowserSupport');

module.exports = {

	bind: function bind() {

		this._onPointerDown = this.onPointerDown.bind(this);
		dom.addEventListener(this.el, "click", this._onPointerDown);

		this.useBody = this.el.getAttribute('data-use-body') ? this.el.getAttribute('data-use-body') == "true" ? true : false : false;
		this.target = this.useBody ? document.getElementsByTagName("body")[0] : this.el;
		this.classList = this.target.getAttribute('data-class') ? this.target.getAttribute('data-class').split(' ') : ["is-sub-nav-active"];
	},

	update: function update(value) {},

	unbind: function unbind() {
		dom.removeEventListener(this.el, support.pointerdown, this._onPointerDown);
	},

	onPointerDown: function onPointerDown() {

		for (var i = 0; i < this.classList.length; i++) {
			var className = this.classList[i];
			if (dom.hasClass(className, this.target)) {
				dom.removeClass(className, this.target);
			} else {
				dom.addClass(className, this.target);
			}
		}
	},

	destroy: function destroy() {
		dom.removeEventListener(this.el, support.pointerdown, this._onPointerDown);
	}

};

},{"utils/BrowserSupport":67,"utils/EventDispatcher":68,"utils/EventList":69,"utils/dom":71,"utils/utils":73}],63:[function(require,module,exports){
'use strict';

var Application = require('./application');
Application.initialize();

},{"./application":27}],64:[function(require,module,exports){
'use strict';

var _ = require('utils/utils');
var events = require('api/events');
var $setTimeout = require('utils/$setTimeout');
var $xhr = require('utils/$xhr');

var BaseModel = function BaseModel(attributes, options) {

  this.callbackStack = {}; //used by the view event chanel

  var attrs = attributes || {};

  options = _.extend({}, options || {
    autoFetch: false,
    url: null
  });

  this.attributes = {};

  attrs = _.extend({}, attrs || {});

  this.set(attrs, options);

  this.changed = {};

  if (options.url && options.autoFetch) {
    $setTimeout(function () {
      this.fetch(options.url);
    }, 10, this);
  }
};

BaseModel.prototype = _.extend({

  fetch: function fetch(url) {

    if (typeof url === "undefined") {
      _.warn('cannot fetech datas, no url specified', this);
    }

    var model = this;

    $xhr.get(url).then(function (resp) {
      console.log('ajax callback', JSON.parse(resp));
      model.set(JSON.parse(resp));
    }, function (resp) {
      _.warn('ajax error on fetch', model);
    });
  },

  get: function get(attr) {
    return this.attributes[attr] || null;
  },

  toJSON: function toJSON(options) {
    return _.clone(this.attributes);
  },

  has: function has(attr) {
    return this.get(attr) != null;
  },

  hasChanged: function hasChanged(attr) {
    return attr in this.changed;
  },

  // set: function(key, val, options) {

  //   if( typeof key === "object" && _.isEmptyObject(key) ){
  //     return;
  //   }

  //   var curr, hasChanged = false;

  //   if( typeof key === "object" ){

  //     for( var k in key){

  //       curr = this.attributes[k];

  //     }

  //   } else {

  //     curr = this.attributes[key];

  //     if( ( _.isObject( curr ) &&  _.isObject( val ) ) || ( _.isArray( curr ) || _.isArray( val ) ) ){

  //       this.deepEqual( curr, val );

  //     } else {

  //       if( curr !== val ){
  //         hasChanged = true;
  //         curr = val;
  //       }

  //     }

  //   }

  //   if( hasChanged ){
  //     clearTimeout( this.changeTimer );
  //     this.changeTimer = $setTimeout(function(){
  //       this.trigger('change');
  //     }, 100, this );
  //   }

  // },

  /**
   * Extracted from Backbone.Model
   *
   * Backbone.js 1.2.1
   * (c) 2010-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
   * Backbone may be freely distributed under the MIT license.
   * For all details and documentation:
   * http://backbonejs.org
   * 
   * TODO: rewrite this and check any perf optimization that can be done
   */

  set: function set(key, val, options) {

    if (key == null) return this;

    // Handle both `"key", value` and `{key: value}` -style arguments.
    var attrs;
    if (typeof key === 'object') {
      attrs = key;
      options = val;
    } else {
      (attrs = {})[key] = val;
    }

    options || (options = {});

    // Extract attributes and options.
    var unset = options.unset;
    var silent = options.silent || false;
    var changes = [];
    var changing = this._changing;
    this._changing = true;

    if (!changing) {
      this._previousAttributes = _.clone(this.attributes);
      this.changed = {};
    }

    var current = this.attributes;
    var changed = this.changed;
    var prev = this._previousAttributes;

    // For each `set` attribute, update or delete the current value.
    for (var attr in attrs) {
      val = attrs[attr];
      if (!_.isEqual(current[attr], val)) changes.push(attr);
      if (!_.isEqual(prev[attr], val)) {
        changed[attr] = val;
      } else {
        delete changed[attr];
      }
      unset ? delete current[attr] : current[attr] = val;
    }

    // Update the `id`.
    this.id = this.get(this.idAttribute);

    // Trigger all relevant attribute changes.
    if (!silent) {
      if (changes.length) this._pending = options;
      for (var i = 0; i < changes.length; i++) {
        this.trigger('change:' + changes[i], this, current[changes[i]], options);
      }
    }

    // You might be wondering why there's a `while` loop here. Changes can
    // be recursively nested within `"change"` events.
    if (changing) return this;
    if (!silent) {
      while (this._pending) {
        options = this._pending;
        this._pending = false;
        this.trigger('change', this, options);
      }
    }

    this._pending = false;
    this._changing = false;

    return this;
  }

}, events);

module.exports = BaseModel;

},{"api/events":26,"utils/$setTimeout":65,"utils/$xhr":66,"utils/utils":73}],65:[function(require,module,exports){
/**
 * Helper to set a context for the setTimeout callback
 */
"use strict";

module.exports = function (callback, delay, ctx) {
	return setTimeout(function () {
		callback.call(ctx);
	}, delay);
};

},{}],66:[function(require,module,exports){
/**
 * Helper to manage xhr request
 *
 * TODO: integrate promises
 */

'use strict';

var _ = require('utils/utils');

function xhrRequest(url, options) {

	this.xhr = new XMLHttpRequest();

	this.options = _.extend({
		method: 'GET',
		dataType: "application/json; charset=utf-8",
		success: function success() {},
		fail: function fail() {},
		params: null,
		elem: null
	}, options || {});

	this.sendParams = null;

	var params = "";
	var index = 0;
	for (var k in this.options.params) {
		params += index == 0 ? "" : "&";
		params += k + "=" + this.options.params[k];
		index++;
	}

	if (this.options.method == "POST") {
		this.sendParams = params;
	}

	if (this.options.method == "GET" && params != "") {
		url += "?" + params;
	}

	this.xhr.onreadystatechange = this.onreadystatechange.bind(this);
	this.xhr.open(this.options.method, url, true);

	this.xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

	// todo fix
	// multipart
	if (this.options.elem && typeof window.FormData !== "undefined") {
		var formData = new FormData(this.options.elem);
		this.xhr.send(formData);
	} else {

		if (this.options.method == "POST") {
			this.xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		} else {
			this.xhr.setRequestHeader("Content-type", this.options.dataType);
		}

		this.xhr.send(this.sendParams);
	}

	return this;
}

xhrRequest.prototype = {

	onreadystatechange: function onreadystatechange() {
		this.checkRequest();
	},

	checkRequest: function checkRequest() {
		if (this.xhr.readyState == 4 && (this.xhr.status == 200 || this.xhr.status == 0)) {
			this.options.success(this.xhr.responseText);
		} else if (this.xhr.readyState == 1) {
			// added this to fix error c00c023f in IE9
			return;
		} else if (this.xhr.readyState < 4 && this.xhr.status == 404) {
			this.options.fail(this.xhr.responseText);
		}
	},

	then: function then(success, fail) {

		if (typeof success !== "undefined") {
			this.options.success = success;
		}
		if (typeof fail !== "undefined") {
			this.options.fail = fail;
		}

		this.checkRequest();

		return this;
	},

	abort: function abort() {
		this.xhr.onreadystatechange = null;
		// this.options.success = null;
		// this.options.fail = null;
		this.xhr.abort();
	}

};

module.exports = {
	post: function get(url, options) {
		var options = options || {};
		options.method = 'POST';
		return new xhrRequest(url, options);
	},
	get: function get(url, options) {
		var options = options || {};
		options.method = 'GET';
		return new xhrRequest(url, options);
	}
};

},{"utils/utils":73}],67:[function(require,module,exports){
'use strict';

var support = {},

/** tests list */
tests = null,
    docElement = document.documentElement,

/** the div to test properties on */
testDiv = document.createElement('div'),
    testDivStyle = testDiv.style,
    prefixes = ' Webkit WebKit Moz O Ms ms'.split(' '),
    cssprefixes = ' -webkit- -webkit- -moz- -o- -ms- -ms-'.split(' '),
    prefixesLength = prefixes.length,
    useCache = false,

/**
 * check if the given property exists in div.style or window
 * and returns the prefixed version or if not found, false;
 *
 * @param {string} prop Name of the property to check
 * @param {Boolean} cssformat return css prefixed version ? default: false
 * @return {String|Boolean} prefixed version, or if not found, false.
 */
testProperty = function testProperty(prop, cssformat) {

  if (useCache & prop in support) {
    return support[prop];
  }

  if (prop in testDivStyle) {
    return prop;
  }

  var formatForCss = typeof cssformat != "undefined" ? cssformat : false,
      propd,
      l = prefixesLength;

  propd = prop.replace(/(^[a-z])/g, function (val) {
    return val.toUpperCase();
  }).replace(/\-([a-z])/g, function (val, a) {
    return a.toUpperCase();
  });

  while (l--) {

    // if( prop == "transform" ){
    //   console.log('checkTransform ' + prefixes[l] + propd, ' ? ', prefixes[l] + propd in testDivStyle );
    // }

    if (prefixes[l] + propd in testDivStyle) {
      return formatForCss ? cssprefixes[l] + prop.toLowerCase() : prefixes[l] + propd;
    } else if (typeof window[prefixes[l].toLowerCase() + propd] !== 'undefined') {
      return prefixes[l].toLowerCase() + propd;
    } else if (typeof window[prefixes[l] + propd] != 'undefined') {
      return prefixes[l] + propd;
    }
  }

  return false;
};

tests = {

  prefix: function prefix() {
    var prefixedProp = testProperty('transform');
    return !!prefixedProp ? prefixedProp.replace('Transform', '') : '';
  },

  cssprefix: function cssprefix() {
    var prefixedProp = testProperty('transform', true);
    return !!prefixedProp ? prefixedProp.replace('transform', '') : '';
  },

  transform: function transform() {
    return testProperty('transform');
  },

  transformCss: function transformCss() {
    return testProperty('transform', true);
  },

  transform3d: function transform3d() {
    return 'WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix() || !!testProperty('perspective');
  },

  translateZ: function translateZ() {
    return this.transform3d() ? 'translateZ(0)' : '';
  },

  transformOrigin: function transformOrigin() {
    return testProperty('transformOrigin');
  },

  backfaceVisibility: function backfaceVisibility() {
    return testProperty('backfaceVisibility');
  },

  perspective: function perspective() {
    return testProperty('perspective');
  },

  perspectiveOrigin: function perspectiveOrigin() {
    return testProperty('perspectiveOrigin');
  },

  transition: function transition() {
    return testProperty('transition');
  },

  transitionProperty: function transitionProperty() {
    return testProperty('transitionProperty');
  },

  transitionDuration: function transitionDuration() {
    return testProperty('transitionDuration');
  },

  transitionTimingFunction: function transitionTimingFunction() {
    return testProperty('transitionTimingFunction');
  },

  transitionDelay: function transitionDelay() {
    return testProperty('transitionDelay');
  },

  transitionEvent: function transitionEvent() {
    return testProperty('transitionEvent');
  },

  transitionEventPrefix: function transitionEventPrefix() {
    return !!testProperty('transitionEvent') ? testProperty('transitionEvent').replace('TransitionEvent', '').toLowerCase() : '';
  },

  transitionEnd: function transitionEnd() {
    return this.transitionEventPrefix() !== '' ? this.transitionEventPrefix() + 'TransitionEnd' : 'transitionend';
  },

  touch: function touch() {
    return 'ontouchstart' in window || /*|| (navigator.maxTouchPoints > 0)*/navigator.msMaxTouchPoints > 0;
  },

  //IE10 Pointers
  msPointer: function msPointer() {
    return !!window.navigator.msPointerEnabled;
  },

  //IE11 Pointers
  pointer: function pointer() {
    return !!window.navigator.pointerEnabled;
  },

  ipad: function ipad() {
    return navigator.userAgent.match(/.*(iPad).*/) ? true : false;
  },
  iphone: function iphone() {
    return navigator.userAgent.match(/.*(iPhone).*/) ? true : false;
  },
  android: function android() {
    return navigator.userAgent.match(/.*(Android).*/) ? true : false;
  },
  firefox: function firefox() {
    return navigator.userAgent.match(/.*((f|F)irefox).*/) ? true : false;
  },

  /** browser */
  ltIE9: function ltIE9() {
    return window.attachEvent && !window.addEventListener;
  },

  pointerdown: function pointerdown() {
    return this.touch() ? 'touchstart' : this.pointer() ? 'pointerdown' : this.msPointer() ? 'MSPointerDown' : 'mousedown';
  },
  pointerup: function pointerup() {
    return this.touch() ? 'touchend' : this.pointer() ? 'pointerup' : this.msPointer() ? 'MSPointerUp' : 'mouseup';
  },
  pointermove: function pointermove() {
    return this.touch() ? 'touchmove' : this.pointer() ? 'pointermove' : this.msPointer() ? 'MSPointerMove' : 'mousemove';
  },
  pointerenter: function pointerenter() {
    return this.touch() ? 'touchstart' : this.pointer() ? 'pointerenter' : this.msPointer() ? 'MSPointerEnter' : 'mouseenter';
  },
  pointerleave: function pointerleave() {
    return this.touch() ? 'touchend' : this.pointer() ? 'pointerleave' : this.msPointer() ? 'MSPointerLeave' : 'mouseleave';
  },
  pointerover: function pointerover() {
    return this.touch() ? 'touchstart' : this.pointer() ? 'pointerover' : this.msPointer() ? 'MSPointerOver' : 'mouseover';
  },
  pointerout: function pointerout() {
    return this.touch() ? 'touchend' : this.pointer() ? 'pointerout' : this.msPointer() ? 'MSPointerOut' : 'mouseout';
  },

  smallscreen: function smallscreen() {
    return window.innerWidth < 700;
  },

  canvas: function canvas() {
    var elem = document.createElement('canvas');
    return !!(elem.getContext && elem.getContext('2d'));
  },

  video: function video() {
    var video = document.createElement("video");
    return typeof video.play != "undefined";
  },

  audio: function audio() {
    var audio = document.createElement("audio");
    return typeof audio.play != "undefined";
  }

};

var featureName;
for (var feature in tests) {
  if (tests.hasOwnProperty(feature)) {
    featureName = feature;
    support[featureName] = tests[feature]();
  }
}

/**
 * check if the given property exists in div.style or window
 * and returns a boolean
 *
 * @param {string} prop Name of the property to check
 * @return {Boolean} true, or if not found, false.
 */
support['test'] = function (prop) {
  return !!testProperty(prop);
};

/**
 * check if the given property exists in div.style or window and
 * returns it with the vendor prefixe
 *
 * @param {string} prop Name of the property to check
 * @return {String|Boolean} prefix + prop, or if not found, false.
 */
support['getPrefixed'] = function (prop) {

  if (prop in testDivStyle) {
    return prop;
  }

  return testProperty(prop);
};

/**
 * same as getPrefixed but it returns the css perfixed version
 *
 * @param {string} prop Name of the property to check
 * @return {String|Boolean} prefix + prop, or if not found, false.
 */
support['getCssPrefixed'] = function (prop) {
  return testProperty(prop, true);
};

useCache = true;

module.exports = support;

},{}],68:[function(require,module,exports){
'use strict';

var _ = require('utils/utils');

var EventDispatcher = {

    /**
     *  normalise event names 
     */
    normalize: function normalize(eventName) {
        return eventName.replace(':', '');
    },

    /**
     *  list of events callbacks
     */
    listeners: [],

    on: function on(eventName, fn, target) {
        var eventName = this.normalize(eventName);
        this.listeners.push({ name: eventName, closure: fn, target: target });
    },

    /**
     *  remove callback to a global event chanel
     */
    off: function off(eventName, fn) {

        var evt,
            i = 0,
            l = this.listeners.length,
            eventName = this.normalize(eventName);

        for (; i < l; i++) {
            evt = this.listeners[i];
            if (evt.name === eventName && evt.closure === fn) {
                this.listeners.splice(i, 1);
                i--;
                l--;
            }
        }
    },

    /**
     *  trigger callbacks from a global event chanel
     */
    trigger: function trigger(eventName, params) {

        var evt,
            i = 0,
            l = this.listeners.length,
            eventName = this.normalize(eventName);

        for (; i < l; i++) {
            evt = this.listeners[i];
            if (!evt) continue;
            if (evt.name === eventName) {
                if (typeof params != "undefined") {
                    evt.closure.apply(evt.target, params);
                } else {
                    evt.closure.call(evt.target);
                }
            }
        }
    },

    /**
     *  list of request callbacks
     */
    handlers: {},

    /**
     * create global request chanel handler
     */
    setHandler: function setHandler(handlerName, handler) {
        this.handlers[this.normalize(handlerName)] = handler;
    },

    /**
     * trigger global request chanel
     */
    request: function request(handlerName, params) {
        var handlerName = this.normalize(handlerName);
        if (typeof this.handlers[handlerName] != 'undefined') {
            return typeof params != "undefined" ? this.handlers[handlerName].apply(this, params) : this.handlers[handlerName]();
        } else {
            return { error: 'request error :: no handler found', handler: handlerName };
        }
    },

    /**
     * COMPATIBILITY
     */
    subscribe: function subscribe(eventName, fn, target) {
        _.warn("EventDispatcher.subscribe is depreciated and will be removed. Please use EventDispatcher.on instead");
        this.subscribe(eventName, fn, target);
    },
    unsubscribe: function unsubscribe(eventName, fn) {
        _.warn("EventDispatcher.unsubscribe is depreciated and will be removed. Please use EventDispatcher.off instead");
        this.off(eventName, fn, target);
    },
    publish: function publish(eventName, params) {
        _.warn("EventDispatcher.publish is depreciated and will be removed. Please use EventDispatcher.trigger) instead");
        this.trigger(eventName, params);
    }

};

module.exports = EventDispatcher;

},{"utils/utils":73}],69:[function(require,module,exports){
'use strict';

var EventList = {

	ENTER_FRAME: 'enterframe',

	DOM_READY: 'ready',
	APLLICATION_READY: 'applicationready',

	RESIZE: 'resize',
	RESIZE_END: 'resizeend',
	CUSTOM_RESIZE: 'customresize',

	SLIDESHOW_READY: 'slideshowready',

	VIEW_OPENED: 'viewopened',
	VIEW_CLOSED: 'viewclosed',
	VIEW_READY: 'viewready',

	OPEN_POPIN: 'openpopin',
	CLOSE_POPIN: 'closepopin',
	SWITCH_OPEN_POPIN: 'switchopenpopin',
	SWITCH_CLOSE_POPIN: 'switchclosepopin',
	POPIN_CLOSED: 'popinclosed',
	POPIN_OPENED: 'popinopened',
	BEFORE_OPEN_POPIN: 'beforeopenpopin',

	SCROLL: 'scroll',
	SCROLL_END: 'scrollend',
	SCROLL_DISABLE: 'disablescroll',
	SCROLL_ENABLE: 'enablescroll',
	SCROLL_TARGET_CHANGE: 'scrolltargetchange',
	SCROLL_PREV: 'scrollprev',
	SCROLL_JUMP_TO: 'scrolljumpto',
	SCROLL_TO: 'scrollto',
	SCROLL_UPDATE: 'scrollupdate',

	ROUTER_PREV: 'routerprev',

	SEQUENCE_LOADING: 'document.SEQUENCE_LOADING',
	SEQUENCE_LOADED: 'document.SEQUENCE_LOADED'

};

EventList.add = function (key, value) {
	EventList[key] = value;
};

module.exports = EventList;

},{}],70:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports["default"] = clamp;

function clamp(value, min, max) {
	return value < min ? min : value > max ? max : value;
}

module.exports = exports["default"];

},{}],71:[function(require,module,exports){
'use strict';

var support = require('utils/BrowserSupport');
var _ = require('utils/utils');

function queryAll(selector, scope) {
  if (typeof selector !== "string") return selector;
  scope = scope || document;
  return scope.querySelectorAll(selector);
};

function query(selector, scope) {
  if (typeof selector !== "string") return selector;
  scope = scope || document;
  return scope.querySelector(selector);
}

function addClass(cls, node) {
  if (node.classList) {
    node.classList.add(cls);
  } else {
    var cur = ' ' + (node.getAttribute('class') || '') + ' ';
    if (cur.indexOf(' ' + cls + ' ') < 0) {
      node.setAttribute('class', (cur + cls).trim());
    }
  }
};

function removeClass(cls, node) {
  if (node.classList) {
    node.classList.remove(cls);
  } else {
    var cur = ' ' + (node.getAttribute('class') || '') + ' ';
    var tar = ' ' + cls + ' ';
    while (cur.indexOf(tar) >= 0) {
      cur = cur.replace(tar, ' ');
    }
    node.setAttribute('class', cur.trim());
  }
};

function hasClass(className, node) {
  return new RegExp(' ' + className + ' ').test(' ' + node.getAttribute('class') + ' ');
};

function getOrSetAttribute(node, attrName, attrValue) {
  if (typeof attrValue !== "undefined") {
    node.setAttribute(attrName, attrValue);
  }
  return node.getAttribute(attrName);
};

function fixEvent(event) {

  var doc = document.documentElement,
      body = document.body,
      pageX = event.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0),
      pageY = event.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);

  var fixedEvnt = {
    target: event.srcElement,
    currentTarget: event.srcElement,
    pageX: pageX,
    pageY: pageY,
    stopPropagation: function stopPropagation() {
      this.cancelBubble = true;
    },
    preventDefault: function preventDefault() {
      this.returnValue = false;
    }
  };

  for (var k in fixedEvnt) {
    if (typeof event[k] === 'undefined') {
      event[k] = fixedEvnt[k];
    }
  }

  return event;
}

function addEventListener(node, eventName, callback) {
  if (node.addEventListener) {
    node.addEventListener(eventName, callback, false);
  } else if (node.attachEvent) {
    node.attachEvent("on" + eventName, callback);
  }
}

function removeEventListener(node, eventName, callback) {

  if (!callback || typeof callback === "undefined") {
    return;
  }

  if (node.removeEventListener) {
    node.removeEventListener(eventName, callback);
  } else if (node.detachEvent) {
    node.detachEvent("on" + eventName, callback);
  }
}

function setStyle(node, props) {
  for (var k in props) {
    var prop = support.getPrefixed(k);
    if (_.isArray(node) || node.length) {
      for (var i = 0, l = node.length; i < l; i++) {
        node[i].style[prop] = props[k];
      }
    } else {
      node.style[prop] = props[k];
    }
  }
}

function transform(node, transformPorps) {

  if (support.transform) {

    var transform = '';

    if ('translateX' in transformPorps) transform += 'translateX(' + transformPorps.translateX + ') ';

    if ('translateY' in transformPorps) transform += 'translateY(' + transformPorps.translateY + ') ';

    if ('scale' in transformPorps) transform += 'scale(' + transformPorps.scale + ') ';

    if ('force3D' in transformPorps) transform += " " + support.translateZ;

    this.setStyle(node, {
      transform: transform
    });
  } else {

    var styles = {};

    if ('translateX' in transformPorps) styles.left = transformPorps.translateX;

    if ('translateY' in transformPorps) styles.top = transformPorps.translateY;

    if ('scale' in transformPorps) styles.zoom = transformPorps.scale;

    this.setStyle(node, styles);
  }
}

module.exports = {
  query: query,
  queryAll: queryAll,
  attr: getOrSetAttribute,
  addClass: addClass,
  removeClass: removeClass,
  hasClass: hasClass,
  addEventListener: addEventListener,
  removeEventListener: removeEventListener,
  fixEvent: fixEvent,
  setStyle: setStyle,
  transform: transform
};

},{"utils/BrowserSupport":67,"utils/utils":73}],72:[function(require,module,exports){
/**
 * Check for native FullScreen API support
 * TODO: add this to BrowserSupport
 */
"use strict";

var vendors = ["moz", "webkit", "", "ms", "o"],
    l = vendors.length,
    fs,
    requestFn,
    cancelFn,
    eventName,
    isFullScreen;

if (document.cancelFullscreen !== undefined) {
    requestFn = "requestFullscreen";
    cancelFn = "exitFullscreen";
    eventName = "fullscreenchange";
} else {
    while (l--) {
        if ((vendors[l] != "moz" || document.mozFullScreenEnabled) && document[vendors[l] + "CancelFullScreen"] !== undefined) {
            requestFn = vendors[l] + "RequestFullScreen";
            cancelFn = vendors[l] + "CancelFullScreen";
            eventName = vendors[l] + "fullscreenchange";
            isFullScreen = vendors[l] == "webkit" ? vendors[l] + "IsFullScreen" : vendors[l] + "FullScreen";
        }
    }
}

module.exports = {
    requestFn: requestFn,
    cancelFn: cancelFn,
    eventName: eventName,
    isFullScreen: isFullScreen
};

},{}],73:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
exports.isArray = isArray;
exports.isObject = isObject;
exports.extend = extend;
exports.contains = contains;
exports.move = move;
exports.has = has;
exports.clone = clone;
exports.isFunction = isFunction;
exports.warn = warn;
exports.notify = notify;
exports.formatIndex = formatIndex;
exports.isEmptyObject = isEmptyObject;
exports.objectToString = objectToString;
exports.nop = nop;
exports.has = has;
exports.camelCalise = camelCalise;
exports.shuffle = shuffle;
exports.isEqual = isEqual;

function isArray(object) {
  return Object.prototype.toString.call(object) === '[object Array]' || Object.prototype.toString.call(object) === '[object NodeList]';
}

function isObject(object) {
  return Object.prototype.toString.call(object) === '[object Object]';
}

function extend() {

  var options,
      k,
      baseObject = arguments[0] || {},
      i = 1,
      length = arguments.length;

  if (typeof baseObject !== "object" && typeof baseObject !== 'function') baseObject = {};

  for (; i < length; i++) {
    if ((options = arguments[i]) != null) {
      for (k in options) {
        if (options[k] !== undefined) {
          baseObject[k] = options[k];
        }
      }
    }
  }

  return baseObject;
}

;

function contains(parentNode, childNode) {
  var isChild = false;
  while (childNode) {
    //console.log("contains loop", childNode, childNode == parentNode );
    if (childNode == parentNode) {
      isChild = true;
      break;
    }
    childNode = childNode.parentNode;
  }
  return isChild;
}

function move(arr, old_index, new_index) {
  if (new_index >= arr.length) {
    var k = new_index - arr.length;
    while (k-- + 1) {
      arr.push(undefined);
    }
  }
  arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
  return arr;
}

function has(key, scope) {
  return key in scope;
}

function clone(obj) {
  return isArray(obj) ? obj.slice() : extend({}, obj);
}

function isFunction(fn) {
  return typeof fn === "function";
}

function warn(msg, ctx) {
  console.log("%c" + "[warn]: " + msg, "color:#ff0084", ctx);
}

function notify(msg, ctx) {
  console.log("%c" + "[notify]: " + msg, "color:#00aeff", ctx);
}

function formatIndex(index, decimal) {
  return (index + Math.pow(10, decimal)).toString(10).substr(1);
}

function isEmptyObject(obj) {
  return Object.keys(obj).length === 0;
}

function recursiveParseNode(obj, callback) {
  for (var k in obj) {
    if (isObject(k)) {
      recursiveParseNode(k, callback);
    } else {
      callback(k, obj[k]);
    }
  }
}

function objectToString(obj) {
  var str = '{';
  recursiveParseNode(obj, function (key, value) {
    str += '"' + key + '":"' + value + '"';
  });
  str += '}';
  return str;
}

function nop() {}

;

var nob = {};

exports.nob = nob;

function has(obj, key) {
  return obj != null && hasOwnProperty.call(obj, key);
}

;

function camelCalise(string) {
  return string.replace(/-([a-z])/g, function (g) {
    return g[1].toUpperCase();
  });
}

function shuffle(array) {
  var currentIndex = array.length,
      temporaryValue,
      randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

/**
 * Extracted from Underscore.js
 *
 * Underscore.js 1.8.3
 * http://underscorejs.org
 * (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 * Underscore may be freely distributed under the MIT license.
 *
 * TODO: rewrite this and check any perf optimization that can be done
 */

function isEqual(a, b, aStack, bStack) {

  var toString = ({}).toString;
  // Identical objects are equal. `0 === -0`, but they aren't identical.
  // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
  if (a === b) return a !== 0 || 1 / a === 1 / b;
  // A strict comparison is necessary because `null == undefined`.
  if (a == null || b == null) return a === b;

  // Compare `[[Class]]` names.
  if (typeof a !== "undefined") {
    var className = toString.call(a);
  } else {
    _.warn('isEqual, first argument is undefined');
    return false;
  }

  if (className !== toString.call(b)) return false;

  switch (className) {
    // Strings, numbers, regular expressions, dates, and booleans are compared by value.
    case '[object RegExp]':
    // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
    case '[object String]':
      // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
      // equivalent to `new String("5")`.
      return '' + a === '' + b;
    case '[object Number]':
      // `NaN`s are equivalent, but non-reflexive.
      // Object(NaN) is equivalent to NaN
      if (+a !== +a) return +b !== +b;
      // An `egal` comparison is performed for other numeric values.
      return +a === 0 ? 1 / +a === 1 / b : +a === +b;
    case '[object Date]':
    case '[object Boolean]':
      // Coerce dates and booleans to numeric primitive values. Dates are compared by their
      // millisecond representations. Note that invalid dates with millisecond representations
      // of `NaN` are not equivalent.
      return +a === +b;
  }

  var areArrays = className === '[object Array]';
  if (!areArrays) {
    if (typeof a != 'object' || typeof b != 'object') return false;

    // Objects with different constructors are not equivalent, but `Object`s or `Array`s
    // from different frames are.
    var aCtor = a.constructor,
        bCtor = b.constructor;
    if (aCtor !== bCtor && !(isFunction(aCtor) && aCtor instanceof aCtor && isFunction(bCtor) && bCtor instanceof bCtor) && 'constructor' in a && 'constructor' in b) {
      return false;
    }
  }
  // Assume equality for cyclic structures. The algorithm for detecting cyclic
  // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

  // Initializing stack of traversed objects.
  // It's done here since we only need them for objects and arrays comparison.
  aStack = aStack || [];
  bStack = bStack || [];
  var length = aStack.length;
  while (length--) {
    // Linear search. Performance is inversely proportional to the number of
    // unique nested structures.
    if (aStack[length] === a) return bStack[length] === b;
  }

  // Add the first object to the stack of traversed objects.
  aStack.push(a);
  bStack.push(b);

  // Recursively compare objects and arrays.
  if (areArrays) {
    // Compare array lengths to determine if a deep comparison is necessary.
    length = a.length;
    if (length !== b.length) return false;
    // Deep compare the contents, ignoring non-numeric properties.
    while (length--) {
      if (!isEqual(a[length], b[length], aStack, bStack)) return false;
    }
  } else {
    // Deep compare objects.
    var keys = Object.keys(a),
        key;
    length = keys.length;
    // Ensure that both objects contain the same number of properties before comparing deep equality.
    if (Object.keys(b).length !== length) return false;
    while (length--) {
      // Deep compare each member
      key = keys[length];
      if (!(has(b, key) && isEqual(a[key], b[key], aStack, bStack))) return false;
    }
  }
  // Remove the first object from the stack of traversed objects.
  aStack.pop();
  bStack.pop();
  return true;
}

;

},{}]},{},[63])


//# sourceMappingURL=bundle.js.map

;!function(a,b){"use strict";function c(){if(!e){e=!0;var a,c,d,f,g=-1!==navigator.appVersion.indexOf("MSIE 10"),h=!!navigator.userAgent.match(/Trident.*rv:11\./),i=b.querySelectorAll("iframe.wp-embedded-content");for(c=0;c<i.length;c++)if(d=i[c],!d.getAttribute("data-secret")){if(f=Math.random().toString(36).substr(2,10),d.src+="#?secret="+f,d.setAttribute("data-secret",f),g||h)a=d.cloneNode(!0),a.removeAttribute("security"),d.parentNode.replaceChild(a,d)}else;}}var d=!1,e=!1;if(b.querySelector)if(a.addEventListener)d=!0;if(a.wp=a.wp||{},!a.wp.receiveEmbedMessage)if(a.wp.receiveEmbedMessage=function(c){var d=c.data;if(d.secret||d.message||d.value)if(!/[^a-zA-Z0-9]/.test(d.secret)){var e,f,g,h,i,j=b.querySelectorAll('iframe[data-secret="'+d.secret+'"]'),k=b.querySelectorAll('blockquote[data-secret="'+d.secret+'"]');for(e=0;e<k.length;e++)k[e].style.display="none";for(e=0;e<j.length;e++)if(f=j[e],c.source===f.contentWindow){if(f.removeAttribute("style"),"height"===d.message){if(g=parseInt(d.value,10),g>1e3)g=1e3;else if(200>~~g)g=200;f.height=g}if("link"===d.message)if(h=b.createElement("a"),i=b.createElement("a"),h.href=f.getAttribute("src"),i.href=d.value,i.host===h.host)if(b.activeElement===f)a.top.location.href=d.value}else;}},d)a.addEventListener("message",a.wp.receiveEmbedMessage,!1),b.addEventListener("DOMContentLoaded",c,!1),a.addEventListener("load",c,!1)}(window,document);