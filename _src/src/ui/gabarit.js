var $ = require('jquery');
var imagesLoaded = require('imagesloaded');
import domUtils from '../tools/dom-utils';
import resizer from '../tools/resizer';


var init = function(isDesktop) {


    // THIS IS FOR EVERY PAGES    
        
    // // Fix gabarit 6-2, postion image right
    var gabarit6PositionImage = (instance) => {
        nWindow = window.innerWidth;
        var $gabarit, small, big, smallTop;
        for(var i = 0, nb = instance.elements.length; i < nb; ++i) {
            $gabarit = $(instance.elements[i]);
            small = $gabarit.find('.image-small').get(0);
            big = $gabarit.find('.image-big').get(0);

            // var smallHeight = small.offsetHeight;
            // console.log('smallHeight', smallHeight)

            var bigHeight = big.offsetHeight; 
            // console.log('bigHeight', bigHeight)

            // var smallWidth = small.offsetWidth; 
            // console.log('smallWidth', smallWidth)

            var borderHeight = domUtils.getStyles($gabarit.find('.image-small img').get(0)).borderTop;
            // console.log('borderHeight', borderHeight)

            var pcent = 100 * ((bigHeight - small.offsetHeight) / bigHeight) / 2;
            var relation = bigHeight / small.offsetWidth;
            // console.log('pcent', pcent)
            // console.log('relation', relation)
            
            smallTop = pcent * relation;
            // console.log('smallTop', smallTop)
            small.style.marginTop = `${smallTop}%`;
        }
    }
    var gab = document.querySelectorAll('.gabarit-6-2');
    imagesLoaded(gab, (instance) => {
        gabarit6PositionImage(instance);
        if(isDesktop) {
            resizer.listen(gabarit6PositionImage, instance);
        }
    });
    

    // ONLY FOR DESKTOP, ARTIST DETAILS, CREATION, PLAYLIST
    if(isDesktop && (document.getElementsByClassName('template-artist-details').length || document.getElementsByClassName('template-creation').length || document.getElementsByClassName('template-playlist'))) {


        var nWindow = window.innerWidth;

        // // Fix gabarit 4-0, postion legend
        var gab = document.querySelectorAll('.gabarit-4-0');
        imagesLoaded(gab, (instance) => {
            var $gabarit, nWidthBig, nWidthSmall, nLeft;
            for(var i = 0, nb = instance.elements.length; i < nb; ++i) {
                $gabarit = $(instance.elements[i]);
                nWidthBig = $gabarit.find('.image-big').get(0).offsetWidth;
                nWidthSmall = $gabarit.find('.small').get(0).offsetWidth;
                nLeft = $gabarit.hasClass('quarter') ? nWidthSmall * 0.25 : nWidthSmall * 0.5;
                $gabarit.find('.legend').get(0).style.marginLeft = `${100 * nLeft / nWidthBig}%`;
            }
        });
        
        // // Fix gabarit 4-1, postion related
        var gab = document.querySelectorAll('.gabarit-4-1');
        imagesLoaded(gab, (instance) => {
            nWindow = window.innerWidth;
            var $gabarit, small, smallWidth, smallLeft, elRelated;
            for(var i = 0, nb = instance.elements.length; i < nb; ++i) {
                $gabarit = $(instance.elements[i]);
                small = $gabarit.find('.image-small').get(0);
                smallWidth = $gabarit.hasClass('quarter') ? small.offsetWidth * 0.25 : small.offsetWidth * 0.5;
                smallLeft = 100 - 100 * (small.offsetLeft - smallWidth) / nWindow;    
                // console.log('MARGIN RIGHT ', parseInt(domUtils.getStyles(elRelated).marginRight, 10));
                $gabarit.find('.related').get(0).style.right = `${smallLeft}%`;
            }
        });
    }
}

var constrainHeight = (elGabarit, maxHeight, currentWindowHeight) => {
    imagesLoaded(elGabarit, (instance) => {

        // if(instance.elements[0].dataset.id == 1638 || instance.elements[0].dataset.id == 1710) {
            // console.log('--------ALL IMAGES OF ', instance.elements[0].dataset.id);
            // RESIZE IMAGE CONTENT
            var elCenter = $(elGabarit).find('.js-center-height').get(0);

            if(elCenter) {
                // var itemWidth = item.offsetWidth;
                // var itemHeight = item.offsetHeight;
                var itemWidth = elCenter.getBoundingClientRect().width;
                var itemHeight = elCenter.getBoundingClientRect().height;
                // console.log('offsetWidth', elCenter.offsetWidth);
                // console.log(itemHeight);
                var prevScale = elCenter.dataset.newScale || 1;
                var scale = maxHeight/itemHeight;
                // console.log('offsetHeight', elCenter.offsetHeight);
                // if(itemHeight > maxHeight) {
                console.log($(elCenter).parent().get(0))
                console.log(itemWidth, itemHeight, maxHeight, scale, prevScale,  elCenter.dataset.newScale);
                //1740 1000.6666870117188 730 0.7295136427295206 "0.1507302809419219" "0.1507302809419219"
                var newScale = scale * prevScale;


                // console.log('itemHeight : '+ itemHeight);
                // console.log('maxHeight : '+ maxHeight);
                // console.log('item scaled height : '+(prevScale * itemHeight));
                if(newScale <= 1) {
                    // var newW = itemWidth * scale;
                    // var newH = itemHeight * scale;
                    // console.log(`new size : ${newW} x ${newH}`);
        
                    // console.log('prevScale scale', prevScale);
                    // console.log('scale', scale);
                    // console.log('newScale', newScale);



                    // modif par moi j'espere que ca pete pas tout
                    // ca
                    // elCenter.style.transformOrigin = `100% 0%`;
                    // par ca
                    elCenter.style.transformOrigin = `center center`;


                    elCenter.style.transform = `scale(${newScale})`;
                    //////////////////////////
                    // je vire le prevScale ca bug sur le gabarit 9-1 je ne sais pkoi
                    ///////////////////////////
                    elCenter.dataset.newScale = newScale;

                    // elCenter.dataset.prevScale = prevScale;
                }else{
                    elCenter.style.transformOrigin ='';
                    elCenter.style.transform = '';
                }
            }
        // }

        // SHOW TITLE
        $(elGabarit).find('.title-home').get(0).classList.add('show');
    
        // POSITION GABARIT IN HEIGHT
        var itemHeight = elGabarit.getBoundingClientRect().height;
        // console.log('itemHeight : '+ itemHeight, elGabarit);
        // console.log('maxHeight : '+ maxHeight);
        var newMarginTop = (maxHeight - elGabarit.getBoundingClientRect().height) * 0.5;
        if(newMarginTop < 0) {
            newMarginTop = 0;
        }
        elGabarit.style.marginTop = `${newMarginTop}px`;
    });
}

// Only center in height, no resize
var centerInHeightMobile = (elGabarit, maxHeight) => {
    imagesLoaded(elGabarit, (instance) => {
        if(elGabarit) {
            // console.log('Center Height : ', elGabarit.offsetHeight);    
            var nTop = (maxHeight - elGabarit.offsetHeight) * .5 - 30;
            if(nTop < 0) nTop = 0;
            elGabarit.style.transform = `translateY(${nTop}px)`;
        }   
    });
}

module.exports = {
    init: init,
    constrainHeight: constrainHeight,
    centerInHeightMobile: centerInHeightMobile
};