var $ = require('jquery');

var init = function() {
    
    if(document.getElementsByClassName('template-playlist').length > 0) {        
        console.log('Playlist init()');

        var title = document.getElementById('inputname');
        if(title) {
            // Give focus to title
            if(title.value == '')
                title.focus();
    
            // When playlist title loses focus
            title.addEventListener('blur', (evt) => {
                sendName(title.value);
            });
        }

        var inputlink = document.getElementById('input-link');
        if(inputlink) {
            inputlink.addEventListener('focus', (evt) => {
                inputlink.setSelectionRange(0, inputlink.value.length);
                document.execCommand( 'copy' );
            });
        }

        var sendName = (sName) => {
            $.post(
                window.adminAjaxUrl,
                {
                    'action': 'edit_name_playlist',
                    'name': sName
                },
                function(response){
                    $('#inputname').val(response);
                }
            );
        }

      // If is not shared playlist
      if(document.getElementsByClassName('template-playlist-shared').length == 0) {
        // Get share link on page load
        $.post(
          window.adminAjaxUrl,
          {
            'action': 'get_bitly_and_save_playlist'
          },
          function(response){
            document.getElementById('input-link').value = response;
          }
        );
      }

        // Open share link
        $('#open-share').click( (evt) => {
            evt.currentTarget.classList.add('hide');
            $('#share-link').addClass('show');
            var inputlink = document.getElementById('input-link');
                inputlink.focus();
                inputlink.select();
                inputlink.setSelectionRange(0, inputlink.value.length);
                document.execCommand('copy');
        });

        // Empty playlist
        $('#empty-playlist').click( (evt) => {
            $.post(
                window.adminAjaxUrl,
                {
                    'action': 'empty_playlist'
                },
                function(response){                    
                    emptyPlaylist();
                    updateMenu();
                    document.getElementById('input-link').value = '';
                    $('#share-link').removeClass('show').addClass('hide');
                    $('#open-share').addClass('show').removeClass('hide');// If is not shared playlist
                  if(document.getElementsByClassName('template-playlist-shared').length == 0) {
                    // Get share link on page load
                    $.post(
                      window.adminAjaxUrl,
                      {
                        'action': 'get_bitly_and_save_playlist'
                      },
                      function(response){
                        document.getElementById('input-link').value = response;
                      }
                    );
                  }
                }
            );
        });
    }

    // Add to playlist, used in artist and creation
    if(document.getElementsByClassName('template-artist-details').length > 0 || document.getElementsByClassName('template-creation').length > 0) {
        
        $('.js-add-playlist').click( function(evt) {
            var el = evt.currentTarget;
            while(el.dataset.id === undefined) {
                el = el.parentNode;
            }
            console.log('id', el.dataset.id);
            $.post(
                window.adminAjaxUrl,
                {
                    'action': 'add_to_playlist',
                    'id':   el.dataset.id
                },
                function(response){
                    // Show remove from playlist button
                    $(el).find('.js-add-playlist').hide();
                    $(el).find('.js-remove-playlist').show();

                    addToPlaylistData(el.dataset.id);
                    updateMenu();
                }
            );
        });
    }
    
    // All pages with 'remove from playlist'
    if(document.getElementsByClassName('template-playlist').length > 0 || document.getElementsByClassName('template-artist-details').length > 0 || document.getElementsByClassName('template-creation').length > 0) {

        // Remove from playlist
        $('.js-remove-playlist').click( function(evt) {
            var el = evt.currentTarget;
            while(el.dataset.id === undefined) {
                el = el.parentNode;
            }
            console.log('id', el.dataset.id);
            
            $.post(
                window.adminAjaxUrl,
                {
                    'action': 'remove_from_playlist',
                    'id':  el.dataset.id
                },
                function(response){                    
                    // Show add to playlist button
                    $(el).find('.js-add-playlist').show();
                    $(el).find('.js-remove-playlist').hide();
                    // window.playlistData.push(el.dataset.id);
                    removeFromPlaylistData(el.dataset.id);
                    updateMenu();
                }
            );
        });

        // Update add/remove buttons        
        console.log('window.playlistData', window.playlistData);
        var data = window.playlistData;
        document.querySelectorAll(`[data-id]`).forEach(el => {
            var b = false;
            var id = el.dataset.id;
            // console.log('Check if '+id+' is in playlist')
            for (var i = 0, nb = data.length; i < nb; i++) {
                if(data[i] == id) {
                    // console.log('  -> '+id+' is in playlist');
                    b = true;
                    break;
                }
            }
            if(b) {
                $(el).find('.js-add-playlist').hide();
            } else {
                $(el).find('.js-remove-playlist').hide();
            }
        });
    }
    
    var updateMenu = () => {
        // console.log('updateMenu', window.playlistData.length);
        // Update menu
        var txt = $('.menu-desktop .menu-item:last-child a').text();
        var reg = new RegExp(/([a-zA-Z]*)(\([0-9]*\))/i);
        var newTxt;
        if(!window.playlistData || window.playlistData.length == 0)
            newTxt = txt.replace(reg, '$1');
        else
            newTxt = txt.replace(reg, '$1')+'('+window.playlistData.length+')';
        $('.menu-desktop .menu-item:last-child a').text(newTxt);
    }

    var emptyPlaylist = () => {        
        window.playlistData = [];
        $('.gabarit').hide();
        $('h1.title-artist').hide();        
        $('#inputname').val('');
    }

    var removeFromPlaylistData = (id) => {
        // console.log('remove - window.playlistData.indexOf('+id+')', window.playlistData.indexOf(id));
        window.playlistData.splice(window.playlistData.indexOf(id), 1);   
        if(document.getElementsByClassName('template-playlist').length > 0)  {
            // $(`[data-id='${id}'`).hide();
            var elGabarit = $(`[data-id='${id}'`).get(0);

            console.log('$(elGabarit.parentNode).find(\'.gabarit\').length', $(elGabarit.parentNode).find('.gabarit').length);
            if($(elGabarit.parentNode).find('.gabarit').length <= 1) {
                elGabarit.parentNode.remove();
            } else {
                elGabarit.remove();
            }
        }   
        // console.log('window.playlistData', window.playlistData)
    }

    var addToPlaylistData = (id) => {
        window.playlistData.push(id);
        console.log('add - window.playlistData '+id+')', window.playlistData);
    }

    updateMenu();

}

module.exports = {
    init: init
};