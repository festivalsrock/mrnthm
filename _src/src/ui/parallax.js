var ScrollMagic = require('scrollmagic');
var URLUtils = require('../tools/url-utils');
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import {TweenLite, Power0, Power1} from 'gsap';
import {ScrollToPlugin} from 'gsap/ScrollToPlugin';

var init = function(isDesktop) { 

    // SCROLL APPARITION, ONLY DESKTOP

    if(isDesktop) {

        if(document.getElementsByClassName('template-artist-details').length || 
            document.getElementsByClassName('template-creation').length ||
            document.getElementsByClassName('template-playlist').length) {
    
            console.log('Parallax init()');
    
            var controller = new ScrollMagic.Controller({addIndicators: false});
            
            var scene;
            [].forEach.call(document.getElementsByClassName('gabarit'), (item, id)=>{
                // console.log('Gabarit ', id);
                scene = new ScrollMagic.Scene({
                    triggerElement: item,
                    // duration: '25%',	// the scene should last for a scroll distance of 100px 
                    offset: '-0%',		// start this scene after scrolling for 50px 
                    triggerHook: 0.8
                })
                .on('enter', (evt) => {
                    // console.log('enter gabarit', evt);
                    TweenLite.to(item, 1, {autoAlpha: 1, ease:Power0.easeNone});
                })
                // .setTween(TweenLite.to(item, 1, {autoAlpha: 1, ease:Power0.easeNone}));
                .addTo(controller);
            });
            
            // controller.scrollto((newpos) => {
            //     console.log('scrollto', newpos);
            //     TweenLite.to(window, 1, {scrollTo: {y: newpos, autokill: false}, ease:Power1.easeInOut})
            // });
        }
    }


    // SCROLL TO SHARED PROJECT
    var gid = URLUtils.getSearchParam('gid');
    if(gid != '') {
        console.log('scroll to gid', gid);
        var els =  document.querySelectorAll(`[data-id='${URLUtils.getSearchParam('gid')}']`);
        // Menu is 90px height desktop, 60px mobile
        var space = (window.innerHeight - 90 - els[0].offsetHeight) * 0.3; 
        TweenLite.to(window, 1, {scrollTo: {y: isDesktop ? els[0].offsetTop - space - 90 : els[0].offsetTop - 60, autokill: false}, ease:Power1.easeInOut});
    }


}

module.exports = {
    init: init
};

