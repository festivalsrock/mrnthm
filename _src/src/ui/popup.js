const $	= require('jquery');
const mfp = require('magnific-popup');
require('jquery-mousewheel')($);
require('malihu-custom-scrollbar-plugin')($);

// DOC about mfp
// http://dimsemenov.com/plugins/magnific-popup/documentation.html
// don't forget to add the .mfp-hide in the class of the main popup container

module.exports = function(){


	// Youtube url parameters
	// https://developers.google.com/youtube/player_parameters?hl=fr
	var trailer = (idVideo) => {

		// Fix to add "rel=0" to the iframe src.
		// magnificPopup is not magnific at all
		var youTubeSrcPattern = $.magnificPopup.defaults['iframe'].patterns.youtube.src;
		$.magnificPopup.defaults['iframe'].patterns.youtube.src = '//www.youtube.com/embed/%id%?autoplay=1&rel=0';

		$.magnificPopup.open({
	      prependTo:$('#dcw-app-root'),
		  type: 'iframe',
		  items:{
		       src: `//www.youtube.com/watch?v=${idVideo}`
		  }
		  ,closeMarkup:'<button title="%title%" type="button" class="mfp-close bt-close-yellow"></button>'
	  	  ,removalDelay: 600
	  	  ,mainClass: 'mfp-fade'
		});

		$.magnificPopup.defaults['iframe'].patterns.youtube.src = youTubeSrcPattern;
	}

	var termsAndConditions = () => {

		$.mCustomScrollbar.defaults.scrollButtons.enable=false; //enable scrolling buttons by default
		$.mCustomScrollbar.defaults.axis='y'; //enable 2 axis scrollbars by default
		$('#dcw-popup-terms .js-terms-content').mCustomScrollbar({
			setHeight:$(window).height() * .65,
			autoExpandScrollbar: false,
			autoDraggerLength:false
		});

	    $.magnificPopup.open({
	      prependTo:$('#dcw-app-root'),
		  type: 'inline',
		  items:{
		       src: '#dcw-popup-terms'
		  }
		  ,closeMarkup:'<button title="%title%" type="button" class="mfp-close bt-close-yellow"></button>'
		  ,closeBtnInside:true
	  	  ,removalDelay: 600
	  	  ,mainClass: 'mfp-fade'
	  	  ,closeOnBgClick:false
		});
	}

	return {
		openTrailer:trailer,
		openTerms:termsAndConditions
	}
}
