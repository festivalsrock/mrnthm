var $ = require('jquery');


var init = function() {
    console.log('Related init()');



    $('.js-share').on('mouseover', (evt) => {
        $(evt.currentTarget).find('.links-mask').addClass('show');
    });
    
    $('.js-share').on('mouseout', (evt) => {
        $(evt.currentTarget).find('.links-mask').removeClass('show');
    });

    // Menu mobile
    // var menuBurger = document.getElementsByClassName('js-menu-burger')[0];
    // menuBurger.addEventListener('click', _openMenuBurger);
    // var menuBurgerClose = document.getElementsByClassName('js-menu-close')[0];
    // menuBurgerClose.addEventListener('click', _closeMenuBurger);

}

// var _openMenuBurger = function(e) {
//     var menuContent = document.getElementsByClassName('js-menu-content')[0];
//     $(menuContent).show();
// }
// var _closeMenuBurger = function(e) {
//     var menuContent = document.getElementsByClassName('js-menu-content')[0];
//     $(menuContent).hide();
// }


module.exports = {
    init: init
};