var $ = require('jquery');


var init = function() {
    console.log('Menu init()');

    // Menu mobile
    var menuBurger = document.getElementsByClassName('js-menu-burger')[0];
    menuBurger.addEventListener('click', _openMenuBurger);
    var menuBurgerClose = document.getElementsByClassName('js-menu-close')[0];
    menuBurgerClose.addEventListener('click', _closeMenuBurger);
}

var _openMenuBurger = function(e) {
    var menuContent = document.getElementsByClassName('js-menu-content')[0];
    $(menuContent).show();

    // Toggle logo
    $('#logo').removeClass('to-black');

    // Center in height, added after show() so that height() has a value
    var $menu = $($(menuContent).find('ul.menu').get(0));
    var freeSpaceY = (parseFloat($(window).height(), 10) - 100 - parseFloat($menu.height(), 10)) * 0.5;
    if(freeSpaceY < 0) freeSpaceY = 0;
    $menu.css('margin-top', `${freeSpaceY}px`);
}
var _closeMenuBurger = function(e) {
    var menuContent = document.getElementsByClassName('js-menu-content')[0];
    $(menuContent).hide();
    
    // Toggle logo
    $('#logo').addClass('to-black');
}


module.exports = {
    init: init
};