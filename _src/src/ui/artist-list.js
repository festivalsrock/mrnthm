var $ = require('jquery');
var gabaritUtils = require('./gabarit');
var Cookies = require('js-cookie');
var videoPlayer = require('../tools/video-player');

import { TweenMax, Power2 } from 'gsap';
import ScrollToPlugin from 'gsap/ScrollToPlugin';


var init = function(isDesktop) {    
    
    if(document.getElementsByClassName('template-artist-list').length > 0) {        
        console.log('Artist List init()');

        
        var isArtistListPage = document.getElementsByClassName('page-template-artist-list').length > 0;

        if(isArtistListPage) {

            // Center cover
            var filters = document.getElementsByClassName('menu-filter')[0];  
            var maxHeight = filters.offsetTop - 90 - 0;
    
            var constrainHeight = () => {
                var gabarits = document.getElementsByClassName('gabarit');
                Array.from(gabarits).forEach((item, id) => {
                    gabaritUtils.constrainHeight(item, maxHeight);
                });
            }
            
            var resizeTimer;
            // Gabarits
            if(isDesktop) {
                constrainHeight();
                window.addEventListener('resize', (evt) => {
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        constrainHeight();               
                    }, 400);
                });
            }
        }
        

        // Artists list
        $('.menu-artist a').on('mouseover', (evt) => {
            var className = evt.target.className;
            if(isDesktop) {
                constrainHeight();
                videoPlayer.startVideoCover( $(`.artist-list-cover.${evt.currentTarget.dataset.artist}`).find('.video-js').get(0));
            }
            if(className.indexOf('active') > -1) {
                $(`.artist-list-cover.${evt.currentTarget.dataset.artist}`).addClass('show');      
                updateFiltersRollover(className);
            }
        });

        $('.menu-artist a').on('mouseout', (evt) => {            
            $(`.artist-list-cover.${evt.currentTarget.dataset.artist}`).removeClass('show');
            if(evt.target.className.indexOf('active') > -1) {
                resetFilters();
            }
        });

        $('.menu-artist a').on('click', (evt) => {            
            if(evt.target.className.indexOf('active') < 0) {
                evt.preventDefault();
            }
        });

        var updateFiltersRollover = (elClasses) => {
            if($('.menu-filter input.selected').length == 0) {
                var aClasses = elClasses.split(' ');
                for(var i = 0, nb = aClasses.length; i < nb; ++i) {
                    $('.menu-filter input#'+aClasses[i]).addClass('highlight');
                }                
            }
        }

        var resetFilters = () => {
            $('.menu-filter input').removeClass('highlight');
        }

        // Filter list
        $('.menu-filter input').on('click', (evt) => {
            var el = evt.currentTarget;
            if(el.classList.contains('selected')) {
                el.classList.remove('selected');
            } else {
                el.classList.add('selected');
            }
            updateCookie();
            updateList();
        });

        var updateList = () => {            
            var $selected = $('.menu-filter input.selected');                
            // Remove all actives
            $(`.menu-artist a`).removeClass('active');
            // Add active by filters
            var c = '';
            $('.menu-filter input.selected').each((i, el) => {
                c += '.'+el.id;
            }); 
            $(`.menu-artist a${c}`).addClass('active');

            if(!isDesktop) {
                $(`.menu-artist a`).each((i, el) => {
                    if(!el.classList.contains('active')) {
                        el.parentElement.classList.add('hide');
                    } else {
                        el.parentElement.classList.remove('hide');
                    }
                });
            }
        }

        // Cookies for filters
        var updateCookie = () => {
            var oFilters = {};
            $('.menu-filter input').each((j, el) => {
                oFilters[el.id] = el.classList.contains('selected');
            });
            Cookies.set('filters', oFilters);
        }

        var cookies = Cookies.getJSON('filters');
        if(cookies) {
            // Pre select filters
            for(var keys in cookies) {
                if(cookies[keys]) {
                    $('.menu-filter input#'+keys).trigger('click');
                }
            }
        }
        
        updateList();


        /*******************
         * DROP DOWN LIST
         */
        var dropdown = document.getElementsByClassName
        ('dropdown');
        var bOpened = false;
        if(dropdown.length > 0) {

            var dropdownLabel = document.getElementsByClassName('dropdown-label')[0];
            dropdownLabel.addEventListener('click', (evt) => {
                if(!bOpened) {
                    document.body.classList.add('no-scroll');
                    dropdown[0].classList.add('show');
                    // setTimeout(()=>{
                    //     $('.menu-filter').css('bottom', '-90px');
                    // }, 1000);
                } else {
                    document.body.classList.remove('no-scroll');
                    dropdown[0].classList.remove('show');
                }
                bOpened = !bOpened;
            });
            
            // Page Creation, click on list item should close the list, and scroll to the id of the gabarit
            var isCreationPage = document.getElementsByClassName('template-creation').length > 0;
            if(isCreationPage) {
                $('.menu-artist li').on('click', (evt) => {
                    var el = evt.currentTarget;
                    // console.log('click', el.innerText);
                    // window.location.hash = el.dataset.goto;   
                    // var elToGo = document.querySelectorAll(`[data-id='${el.dataset.goto}']`)[0];
                    var elToGo = document.getElementById(`goto-gabarit-${el.dataset.goto}`);
                    TweenMax.to(window, 1.2, {scrollTo:elToGo.offsetTop - 120, delay:0.8});
                    bOpened = false;
                    document.body.classList.remove('no-scroll');
                    dropdown[0].classList.remove('show');      
                    document.querySelectorAll('.dropdown .dropdown-label')[0].innerText = el.innerText;
                });
            }
        }
    }
}

module.exports = {
    init: init
};