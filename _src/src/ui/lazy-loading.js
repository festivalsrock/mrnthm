// Taken from
// https://www.sitepoint.com/five-techniques-lazy-load-images-website-performance/#robin-osborne-progressively-enhanced-lazy-loading

var detectIt = require('detect-it');
var imagesLoaded = require('imagesloaded');


var init = function() {
    
    // Everywhere but home, because swiper js handles lazy loading
    if(document.getElementsByClassName('js-home-swiper').length == 0) {
        console.log('Lazy Loading init()');

        var lazy = [];
    
        registerListener('load', setLazy);
        registerListener('load', lazyLoad);
        // registerListener('scroll', lazyLoad);
        // registerListener('resize', lazyLoad);

        
        var scrollTimer;
        var lazyLoadTimer = (evt) => {
            if(document.getElementsByClassName('lazy').length == 0) {
                // console.log('REMOVE LAZY LOADING ON SCROLL');
                window.removeEventListener('scroll', lazyLoadTimer);            
            } else {
                setLazy();
                lazyLoad();        
            }

            // clearTimeout(scrollTimer);
            // scrollTimer = setTimeout(function() { 
            //     if(document.getElementsByClassName('lazy').length == 0) {
            //         console.log('REMOVE LAZY LOADING ON SCROLL');
            //         window.removeEventListener('scroll', lazyLoadTimer);            
            //     } else {
            //         setLazy();
            //         lazyLoad();        
            //     }
            // }, 200);
        }
        window.addEventListener('scroll', lazyLoadTimer, detectIt.passiveEvents ? {passive:true} : false);
        
        var resizeTimer;
        window.addEventListener('resize', (evt) => {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() { 
                setLazy();
                lazyLoad();        
            }, 400);
        }, detectIt.passiveEvents ? {passive:true} : false);

    
        function setLazy(){            
            lazy = document.getElementsByClassName('lazy');
            // console.log('Found ' + lazy.length + ' lazy images');
        } 
    
        function lazyLoad(){
            for(let i=0, nb = lazy.length; i<nb; i++){
                let myImgLazy = lazy[i];
                if(isInViewport(myImgLazy)){
                    if (myImgLazy.getAttribute('data-src')){
                        myImgLazy.src = myImgLazy.getAttribute('data-src');
                        // myImgLazy.removeAttribute('data-src');
                        imagesLoaded(myImgLazy, (instance) => {
                            // console.log('imagesLoaded', myImgLazy.src);
                            let el = instance.elements[0];
                            el.classList.remove('lazy');
                            el.classList.add('lazy-loaded');
                        });
                    }
                }
            }
            cleanLazy();
        }
    
        function cleanLazy(){
            lazy = Array.prototype.filter.call(lazy, function(l){ return l.getAttribute('data-src');});
        }
    
        function isInViewport(el){
            var rect = el.getBoundingClientRect();
            
            return (
                rect.bottom >= 0 && 
                rect.right >= 0 && 
                rect.top <= (window.innerHeight || document.documentElement.clientHeight) && 
                rect.left <= (window.innerWidth || document.documentElement.clientWidth)
            );
        }
    
        function registerListener(event, func) {
            window.addEventListener(event, func, detectIt.passiveEvents ? {passive:true} : false);
        }

    }


}


module.exports = {
    init: init,
};

