var $ = require('jquery');
var videoPlayer = require('../tools/video-player');
var detectIt = require('detect-it');

import { TweenMax, Power2 } from 'gsap';
// import ScrollToPlugin from 'gsap/ScrollToPlugin';
import { setTimeout } from 'timers';


var init = (isDesktop) => {
    console.log('Transitions init()');   

    var elTransition = document.getElementById('transition');
    var elLogo = document.getElementById('logo');

    // Transition introduction
    if(document.getElementById('intro')) {
        setTimeout(() => {
            // $('#intro').addClass('hide');
            // elLogo.classList.add('to-black');
            elTransition.classList.remove('enter');
            elTransition.classList.add('leave');
            
            setTimeout(() => {
                $('#intro').addClass('hide-quick');
                // elLogo.classList.add('to-black');
                elTransition.classList.remove('leave');
                elTransition.classList.add('enter');
            }, 1000);

        }, 4000);
    } else {
        elLogo.classList.add('to-black');
    }
    
    // Animation OUT
    var ignore_onbeforeunload = false;
    $('a[href^=mailto]').on('click',function(){
        // console.log('click on a mail to');
        ignore_onbeforeunload = true;
    });

    // https://css-tricks.com/add-page-transitions-css-smoothstate-js/
    window.addEventListener('beforeunload', (e) => {
        console.log('before unload', ignore_onbeforeunload);
        if (ignore_onbeforeunload){
            return "Halt! you are not supposed to leave!";
        } else {
            elTransition.classList.remove('enter');
            elTransition.classList.add('leave');
            elLogo.classList.remove('to-black');
        }
        ignore_onbeforeunload = false;
    });

    // Safari caches the page when clicking back button, and do not dispatch event onload 
    window.onpageshow = function(event) {
        if (event.persisted) {
            // alert('persited');
            hideTransition();
        }
    };

    var hideTransition = function() {
        if(!elTransition.classList.contains('enter')) {
            elTransition.classList.add('enter');
            elTransition.classList.remove('leave');
        }
    }
    // setTimeout(hideTransition, 3000);
    window.addEventListener("hashchange", hideTransition);
    

    // Transition Anchor
    // Smooth scrolling when the document is loaded and ready
    // If the target is a video then autoplay
    if(document.getElementsByClassName('template-homepage').length == 0 && window.location.hash) {
        console.log('-> SHOULD GO TO ANCHOR');

        // setTimeout(()=>{
            window.scrollTo(0, 0);
        // }, 200);

        window.addEventListener('load', () => {
            var elAnchor = document.getElementById('goto-'+window.location.hash.split('#')[1]);
            console.log('HAS ELEMENT FOR ANCHOR', elAnchor);
            if(elAnchor) {
                let nGap = (isDesktop ? 90 : 120);

                console.log('ANCHOR OFFSET', elAnchor.offsetTop);
                TweenMax.to(window, 0.5, {scrollTo: elAnchor.offsetTop - nGap, delay: 1});
                
                // Autoplay video, if gabarit is a video player
                let elGabarit = document.querySelector(`[data-id='${window.location.hash.split('#')[1]}']`);
                $(elGabarit).find('.title-project').hide();
                if(elGabarit.classList.contains('gabarit-1') || elGabarit.classList.contains('gabarit-2'))
                    videoPlayer.startVideo($(elGabarit).find('.video-js').get(0));
            }
        }, detectIt.passiveEvents ? {passive:true} : false);
    }

    // $(document).on('click', 'a[href^="#"]', function (event) {
    //     event.preventDefault();
    //     var elToGo = document.getElementById(`goto-gabarit-${el.dataset.goto}`);
    //     TweenMax.to(window, 1.2, {scrollTo:elToGo.offsetTop - (isDesktop ? -90 : -160), delay:0.1});
    // });

}

module.exports = {
    init: init
};