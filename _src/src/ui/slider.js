var $ = require('jquery');
var Swiper = require('swiper');
var videoPlayer = require('../tools/video-player');
var gabaritUtils = require('./gabarit');

var init = function(isDesktop, isPortrait) { 

    // isDesktop = false;
    console.log('IS DESKTOP', isDesktop);
    console.log('IS PORTRAIT', isPortrait);

    // HOMEPAGE SLIDER
    if($('.js-home-swiper').length) {

        var arrows = document.getElementsByClassName('js-home-swiper-custom-arrows')[0];
        var mySwiper = new Swiper(`.js-home-swiper`, {
            init: false,
            slidesPerView: 1,
            direction: 'horizontal',
            loop: true,
            speed: 500,
            mousewheel: true,
            effect: 'fade',
            hashNavigation: true,
            setWrapperSize: true,
            shortSwipes : !isDesktop,
            // allowTouchMove: !isDesktop,
            // autoHeight: true,
            fadeEffect: {
                crossFade: true
            },
            // autoplay: {
            //     delay: 12000
            // },
            preloadImages: false,
            lazy: {
                loadPrevNext: true,
                elementClass: 'lazy',
                loadedClass: 'lazy-loaded',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.js-home-swiper-custom-arrows .swiper-button-next',
                prevEl: '.js-home-swiper-custom-arrows .swiper-button-prev',
            }
        });        

        var resizeTimer;
        mySwiper.on('init', (evt) => {
            console.log('SWIPER INITIALIZED');

            // Need to mute videos here, because I guess, swiperjs make a copy of the video without the 'muted' attribute
            videoPlayer.muteSmallVideos();

            if(isDesktop) {
                constrainHeight();
                window.addEventListener('resize', (evt) => {
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function() {
                        constrainHeight(); 
                        mySwiper.update();                      
                    }, 400);
                });
            } else if(isPortrait) {
                centerInHeightMobile();                
            } else {
                constrainHeight();
            }
            mySwiper.update();
        });  

        mySwiper.on('lazyImageReady', (target) => {
            console.log('LAZY IMAGE LOADED', target);            
            if(isPortrait) {
                centerInHeightMobileBySlide(target);                
            } else {
                // constrainHeight();
                constrainHeightBySlide(target);
            }
        });

        var goToArtistURL = (id) => {
            window.location = $(mySwiper.slides[mySwiper.activeIndex]).find('[data-artist-url]').get(0).dataset.artistUrl+'#'+id;
        }

        mySwiper.on('tap', (evt) => {
             console.log('TAP SWIPER', evt);
            goToArtistURL($(mySwiper.slides[mySwiper.activeIndex]).find('[data-id]').get(0).dataset.id);
        });

        mySwiper.on('click', (evt) => {
             console.log('CLICK SWIPER', evt);
            goToArtistURL($(mySwiper.slides[mySwiper.activeIndex]).find('[data-id]').get(0).dataset.id);
        });

        mySwiper.on('slideChange', (target) => {
            console.log('---> CHANGE SLIDE SWIPER', mySwiper);
            console.log('Current slide is '+mySwiper.activeIndex);
            console.log('Previsou slide is '+mySwiper.previousIndex);

            var $activeSlide = $(mySwiper.slides[mySwiper.activeIndex]);
            var $previousSlide = $(mySwiper.slides[mySwiper.previousIndex]);
            console.log('ACTIVE SLIDE $ ', $activeSlide);
            
            // VIDEO START/STOP
            var elVideoCurrent = $activeSlide.find('.video-js').get(0);
            videoPlayer.startVideoHome(elVideoCurrent);            
            var elVideoPrev = $previousSlide.find('.video-js').get(0);
            // console.log('elVideoPrev', elVideoPrev);

            if(elVideoCurrent && elVideoPrev) {              
                if(elVideoCurrent.getAttribute('id') !== elVideoPrev.getAttribute('id')){
                    videoPlayer.stopVideo(elVideoPrev);
                }
            } else if(elVideoPrev) {
                videoPlayer.stopVideo(elVideoPrev);
            }
        });

        
        var constrainHeightBySlide = (target) => {
            console.log('---constrainHeightBySlide', target);
            var maxHeight = arrows.offsetTop - 90 - 40;
            // var gabarits = document.getElementsByClassName('gabarit');
            // Array.from(gabarits).forEach((item, id) => {
                gabaritUtils.constrainHeight($(target).find('.gabarit').get(0), maxHeight);
                /*let $el = $(target).find('.gabarit .js-center-height');
                let w = ($el.closest('.gabarit').width() - $el.width()) / 2;
                console.log(w, $el, $el.closest('.gabarit').width(), $el.width())

                $el.css('margin-left', `${w}px`);*/

            // });
        }   
        
        var constrainHeight = () => {
            console.log('---constrainHeight');
            var maxHeight = arrows.offsetTop - 90 - 40;
            var gabarits = document.getElementsByClassName('gabarit');
            Array.from(gabarits).forEach((item, id) => {
                gabaritUtils.constrainHeight(item, maxHeight);
            });
        }       

        // CENTER SLIDER IN HEIGHT,
        // Slider takes the height of the tallest slide
        var centerInHeightDesktop = () => {
            var freeSpaceY = (parseFloat($(window).height(), 10) - 90 - parseFloat($('.js-home-swiper').height(), 10) - 40) * 0.5;
            console.log('freeSpaceY', freeSpaceY);
            if(freeSpaceY < 0) freeSpaceY = 0;
            $('.js-home-swiper').css('margin-top', `${freeSpaceY}px`);
        }  
        
        var centerInHeightMobile = () => {  
            console.log('---centerInHeightMobile');
            var $currentSlide;
            for(var i = 0, nbSlides = mySwiper.slides.length; i < nbSlides; ++i) {
                $currentSlide = $(mySwiper.slides[i]);
                // CENTER IN HEIGHT
                var elCenter = $currentSlide.find('.js-center-height').get(0);
                console.log('---CENTER CONTENT', elCenter);
                if(elCenter) {
                    var $elTitle = $currentSlide.find('.title-home');
                    gabaritUtils.centerInHeightMobile(elCenter, arrows.offsetTop - $elTitle.offset().top - $elTitle.height());
                }    
            }

        }
        
        var centerInHeightMobileBySlide = (target) => {  
            console.log('---centerInHeightMobileBySlide');
            var $currentSlide;
            // for(var i = 0, nbSlides = mySwiper.slides.length; i < nbSlides; ++i) {
                $currentSlide = $(target);
                // CENTER IN HEIGHT
                var elCenter = $currentSlide.find('.js-center-height').get(0);
                console.log('---CENTER CONTENT', elCenter);
                if(elCenter) {
                    var $elTitle = $currentSlide.find('.title-home');
                    // je retire le cnetrage vertical mobile
                    // gabaritUtils.centerInHeightMobile(elCenter, arrows.offsetTop - $elTitle.offset().top - $elTitle.height());
                }    
            // }

        }

        // mySwiper.on('slidePrevTransitionStart', (evt) => {
        //     console.log('SLIDE PREV TRANSITION START SWIPER', mySwiper);
        //     // mySwiper.slides[mySwiper.realIndex]
        //     // mySwiper.slides[mySwiper.realIndex]
        // });

        // mySwiper.on('slideNextTransitionStart', (evt) => {
        //     console.log('SLIDE NEXT TRANSITION START SWIPER', mySwiper);
        //     // mySwiper.slides[mySwiper.realIndex]
        //     // mySwiper.slides[mySwiper.realIndex]
        // });


        console.log('INIT SWIPER');
        mySwiper.init();
        
    } 
    // ABOUT SLIDER
    /*else if($('.js-about-swiper').length > 0) {

        var mySwiper = new Swiper(`.js-about-swiper`, {
            init: false,
            slidesPerView: 7,
            // direction: 'horizontal',
            freeMode: true,
            loop: true,
            speed: 1000,
            // mousewheel: true,
            // effect: 'fade',
            // autoHeight: true,
            // fadeEffect: {
            //     crossFade: true
            // },
            autoplay: {
                delay: 2000
            },

            // Navigation arrows
            navigation: {
                nextEl: '.js-about-swiper-custom-arrows .swiper-button-next',
                prevEl: '.js-about-swiper-custom-arrows .swiper-button-prev',
            }
        });        

        mySwiper.on('init', (evt) => {
            console.log('INIT SWIPER', mySwiper);
        });  

        // mySwiper.on('click', (evt) => {
        //     console.log('CLICK SWIPER', evt);
        //     $(mySwiper.slides[mySwiper.activeIndex]).find('[data-id]').get(0);
        //     console.log('URL ARTIST', $(mySwiper.slides[mySwiper.activeIndex]).find('[data-artist-url]').get(0).dataset.artistUrl);
        //     window.location = $(mySwiper.slides[mySwiper.activeIndex]).find('[data-artist-url]').get(0).dataset.artistUrl;
        // });

        mySwiper.init();

    }*/ else {
        // Loop on swipers, for artist, collection and playlist
        $('.swiper-container').each(function (i, el) {
            var reg;
            el.classList.forEach(element => {    
                // Swiper in artist, collection and playlist
                reg = new RegExp(/multiple-swiper-(.*)/, 'i');
                if (reg.test(element)) {
                    // console.log(`.multiple-swiper-${element.split(reg)[1]}`);
                    var mySwiper = new Swiper(`.multiple-swiper-${element.split(reg)[1]}`, {
                        slidesPerView: 3, //'auto',
                        direction: 'horizontal',
                        loop: true,
                        // loopedSlides: 3,
                        speed: 400,
                        autoplay: {delay: 3000},
                        
                        preloadImages: false,
                        lazy: {
                            loadPrevNext: true,
                            elementClass: 'lazy',
                            loadedClass: 'lazy-loaded',
                        },
    
                        // Navigation arrows
                        navigation: {
                            nextEl: '.swiper-custom-arrows-'+element.split(reg)[1]+' .swiper-button-next',
                            prevEl: '.swiper-custom-arrows-'+element.split(reg)[1]+' .swiper-button-prev',
                        },
    
                        breakpoints: {
                            // when window width is <= 320px
                            320: {
                                slidesPerView: 1
                                // loopedSlides: 3,
                            },
                            // when window width is <= 480px
                            720: {
                                slidesPerView: 2
                                // loopedSlides: 3,
                            }
                        }
                    });
                    if(!isDesktop) {
                        mySwiper.autoplay.stop();
                    }
                }
            });
        });
    }


}

module.exports = {
    init: init
};