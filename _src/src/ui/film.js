const $             = require('jquery');
const filmTPL       = require('../../templates/film.hbs');
const createHeader  = require('./header');
const analytics		= require('../tools/analytics');
const header        = createHeader();

var isPhone,
    isTablet,
    cdnUrl,
    lang,
    $film;

var player;
var bPlay = false;

var init = ($elem, _cdnUrl, _isPhone, _lang, _isTablet) => {
    cdnUrl = _cdnUrl;
    lang = _lang;
    isPhone = _isPhone;
    isTablet = _isTablet;
    console.log('INIT FILM');

    let filmHTML = filmTPL({
        lang: lang,
        device: !isPhone ? 'desktop' : 'mobile'
    });
    $elem.append(filmHTML);

    $film = $elem.find('#film-main');

    initYoutubeVideo();
}

var initFooter = () => {
    var $menuItems = $film.find('.menu li');
    $menuItems.on('click', (evt) => {
        if(evt.currentTarget.className == 'shop')
        {
            window.open(lang.LINK_shop_url, 'Davidoff_CW_shop');
        } else {
            updateLocation(evt.currentTarget.className);
        }
    });

	var updateLocation = (name) => {
        console.log('updateLocation : '+name);
		window.location.hash = name;
	}
}

var initYoutubeVideo = () => {
    // This code loads the IFrame Player API code asynchronously
    var tag = document.createElement('script');

    tag.src = 'https://www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}


var start = () => {
    console.log('START film');

    // $('#film-youtube').css('opacity', 0);
    // let bPlay = false;

    // This function creates an <iframe> (and YouTube player) after the API code downloads.
    // https://developers.google.com/youtube/iframe_api_reference?hl=fr#Operations
    function onYouTubeIframeAPIReady() {
        if (document.getElementById('film-youtube') === null) {
            console.log('---CREATE THE YOUTUBE DIV--');
            const ytdiv = document.createElement('div');
            ytdiv.setAttribute('id', 'film-youtube');
            document.getElementById('youtube-player').appendChild(ytdiv);
        }
        player = new YT.Player('film-youtube', {
            height: '360',
            width: '640',
            videoId: lang.FILM_youtube_video_id,
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            },
            playerVars: { 'autoplay': 0, 'controls': 1, rel:0 },
        });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
        // event.target.playVideo();
        TweenMax.to($('#film-youtube'), .3, {opacity:1});
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    // var done = false;

    function onPlayerStateChange(event) {
        // if (event.data == YT.PlayerState.PLAYING && !done) {
        //     setTimeout(stopVideo, 6000);
        //     done = true;
        // }
        console.info(`onPlayerStateChange  ${event.data}`);
        if (event.data === 0) {
            bPlay = false;
            analytics.track('Content', 'The_film_Complete');
        } else if (event.data === 1) {
            if (bPlay === false)
                analytics.track('Content', 'The_film_Start');
            bPlay = true;
        }
    }
    // TODO
    onYouTubeIframeAPIReady(); // Uncomment this when video is released and remove image <img> in #film-youtube
    // onPlayerReady(null); // Remove this when video is released
    initFooter();
}



var stopVideo = () => {
    console.log(`♀♀♀ STOP VIDEO, bPlay = ${bPlay}`);
    if (bPlay) {
        bPlay = false;
        if (player.stopVideo) {
            player.stopVideo();
        } else {
            console.log('---ERASE THE YOUTUBE DIV--');
            document.getElementById('film-youtube').parentNode.removeChild(document.getElementById('film-youtube'));
        }
    }
}


const NAME = 'film';
var open = (onEndOpen) => {
    console.log('open '+NAME);
    analytics.track('Content', 'Watch_the_film');

    start();
    TweenMax.to($film, .3, {opacity:1, display:'block', onComplete:onEndOpen});

    // Move footer down
    if(!isPhone) {
        positionFooter();
        $(window).resize(positionFooter);
    }
}

var positionFooter = ()=> {
    console.log('getMenuIsOpened : '+header.getMenuIsOpened());
    if(!header.getMenuIsOpened())
        $('.dcw-footer').css('top', $('#film-main').height() + 53);
    else {
        $('.dcw-footer').css('top', $(window).height() - 40 + 9);
    }
}

var close = (onEndClose) => {
    console.log('close '+NAME);
    stopVideo();
    $(window).off('resize', positionFooter);
    TweenMax.to($film, .3, {opacity:0, display:'none', onComplete:onEndClose});
}


module.exports = {
    init: init,
    open:open,
    close:close
}
