var $ = require('jquery');
var imagesLoaded = require('imagesloaded');


var init = function() {    

    if(document.getElementsByClassName('template-aboutus').length > 0) {        

        // Animation of letters
        $('.resume-over b').each((id, el) => {
            setTimeout(() => {
                el.classList.add('show');
            }, id * 500);
        });
        // Animation of letters
        $('.resume b').each((id, el) => {
            setTimeout(() => {
                el.classList.add('hide');
            }, id * 500);
        });


        // Instagram full width
        var instagram = document.getElementsByClassName('instagram')[0];

        var resizeInstagram = () => {            
            instagram.style.transform = `scale(1)`;   
            var scale = $(window).width() / instagram.getBoundingClientRect().width;
            var nH = instagram.getBoundingClientRect().height * scale;

            instagram.querySelectorAll('img').forEach(el => {
                el.style.height = `${nH}px`;
            });
        };

        var resizeTimer;
        window.addEventListener('resize', (evt) => {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {     
                resizeInstagram();        
            }, 400);
        });

        
        imagesLoaded(instagram, (instance) => {
            resizeInstagram();
        });

    }

}

module.exports = {
    init: init
};