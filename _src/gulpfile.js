'use strict';

const gulp = require('gulp');
const exit = require('gulp-exit');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const csso = require('gulp-csso');
const stripDebug = require('gulp-strip-debug');

const browserify = require('browserify');
const watchify = require('watchify');
const babelify = require('babelify');

const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

// LIVE RELOAD PHP, WORK THROUGH
// https://github.com/vohof/gulp-livereload/issues/122
const browserSync = require('browser-sync').create();
const localProxy = 'local.onirim.fr';

const cssDestFolder = '../css/';
const cssMinDestFolder = '../css/';

const jsDestFolder = '../js/';


gulp.task('sass', function () {
  // console.log('SASS');
  return gulp.src('./scss/**/*.scss')
  .pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError)) // compressed, expanded, compact
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest(cssDestFolder))
  .pipe(browserSync.stream());
});


gulp.task('minify', function () {
  // console.log('MINIFY');
  return gulp.src(cssDestFolder + 'style.css')
  // .pipe(sourcemaps.init())
  // .pipe(csso())
  // .pipe(sourcemaps.write())
  // .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(cssMinDestFolder));
});
  

function compile(watch) {
  var bundler = watchify(browserify('./src/index.js', {debug: true}).transform(babelify, {
      presets: ["env"],
      sourceMaps: false
  }));  
  
  function rebundle() {
    return bundler
    .bundle()
    .on('error', function (err) {
      console.error(err);
      this.emit('end');
    })
    .pipe(source('temp.js'))
    .pipe(buffer())
    .pipe(rename('main.js'))
    .pipe(gulp.dest(jsDestFolder));
  }
  
  function rebundleDist() {
    return bundler
    .bundle()
    .on('error', function (err) {
      console.error(err);
      this.emit('end');
    })
    .pipe(source('temp.js'))
    .pipe(buffer())
    .pipe(rename('main.js'))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(sourcemaps.write(jsDestFolder))
    .pipe(gulp.dest(jsDestFolder));
  }
  
  if (watch) {
    bundler.on('update', function () {
      console.log('-> bundling...');
      rebundle().pipe(browserSync.stream());
    });
    rebundle();
  } else {
    rebundleDist().pipe(exit());
  }
}
  


gulp.task('watch', ['build'], function () {
  browserSync.init({ proxy: localProxy, online: true });
  gulp.watch('./scss/**/*.scss', ['build']);
  compile(true);
});

// Tâche "build"
gulp.task('build', ['sass']);


// Tâche "prod" = Build + minify
gulp.task('dist', ['build',  'minify'], function () {
  compile(false);
});

// Tâche par défaut
gulp.task('default', ['watch']);
