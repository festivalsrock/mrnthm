<?php
/*
 * Fonctions ajax permettant l'édition d'une playlist
 */

require_once locate_template('inc/plugins/bitly/bitly.php');

// ADD GABARIT TO PLAYLIST
// Action : add_to_playlist
add_action( 'wp_ajax_add_to_playlist', 'onirim_add_to_playlist' );
add_action( 'wp_ajax_nopriv_add_to_playlist', 'onirim_add_to_playlist' );

function onirim_add_to_playlist() {

    $playlist_id = onirim_get_playlist_id(false);
    $gabarit_id = intval($_POST['id']);

    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));

    if(!isset($currentPlaylist['data']) || !is_array($currentPlaylist['data'])) {
        if($gabarit_id > 0) {
            $currentPlaylist = array(
                'name' => 'My playlist',
                'data' => array($gabarit_id)
            );
        }else {
            $currentPlaylist = array(
                'name' => 'My playlist',
                'data' => array()
            );
        }
    }

    if($gabarit_id > 0) {
        $gabarit = get_post( $gabarit_id );

        if($gabarit->post_type == 'onirim_gabarit' && $gabarit_id > 0) {
            $currentPlaylist['data'][] = $gabarit_id;
        }

    }

    $currentPlaylist['data'] = array_unique($currentPlaylist['data']);

    setcookie('current_playlist', base64_encode(serialize($currentPlaylist)), time() + (365 * 24 * 60 * 60), '/');

    echo sizeof($currentPlaylist['data']);

    if($playlist_id > 0) {
        update_field( 'gabarits_playlist', array_merge($currentPlaylist['data']), $playlist_id );
    }

    wp_die();
}

// REMOVE GABARIT FROM PLAYLIST
// Action : remove_from_playlist
add_action( 'wp_ajax_remove_from_playlist', 'onirim_remove_from_playlist' );
add_action( 'wp_ajax_nopriv_remove_from_playlist', 'onirim_remove_from_playlist' );

function onirim_remove_from_playlist() {

    $playlist_id = onirim_get_playlist_id(false);
    $gabarit_id = intval($_POST['id']);

    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
    if(!isset($currentPlaylist['data']) || !is_array($currentPlaylist['data'])) {
        $currentPlaylist = array(
            'name' => 'My playlist',
            'data' => array($gabarit_id)
        );
    }

    foreach($currentPlaylist['data'] as $k=>$v) {
        if($v == $gabarit_id) unset($currentPlaylist['data'][$k]);
    }


    $currentPlaylist['data'] = array_unique($currentPlaylist['data']);
    setcookie('current_playlist', base64_encode(serialize($currentPlaylist)), time() + (365 * 24 * 60 * 60), '/');

    echo sizeof($currentPlaylist['data']);

    if($playlist_id > 0) {
        update_field( 'gabarits_playlist', array_merge($currentPlaylist['data']), $playlist_id );
    }

    wp_die();
}

// EMPTY PLAYLIST
// Action : empty_playlist
add_action( 'wp_ajax_empty_playlist', 'onirim_empty_playlist' );
add_action( 'wp_ajax_nopriv_empty_playlist', 'onirim_empty_playlist' );

function onirim_empty_playlist() {

    $playlist_id = onirim_get_playlist_id(false);

    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
    if(!isset($currentPlaylist['data']) || !is_array($currentPlaylist['data'])) {
        $currentPlaylist = array(
            'name' => 'My playlist',
            'data' => array()
        );
    }

    $currentPlaylist['name'] = '';
    $currentPlaylist['data'] = array();
    $currentPlaylist['id'] = 0;
    setcookie('current_playlist', base64_encode(serialize($currentPlaylist)), time() + (365 * 24 * 60 * 60), '/');

    echo sizeof($currentPlaylist['data']);

    if($playlist_id > 0) {
        //update_field( 'gabarits_playlist', array(), $playlist_id );
        //update_field( 'name', '', $playlist_id );
    }

    wp_die();
}

// EDIT NAME OF GABARIT
// Action : edit_name_playlist
add_action( 'wp_ajax_edit_name_playlist', 'onirim_edit_name_playlist' );
add_action( 'wp_ajax_nopriv_edit_name_playlist', 'onirim_edit_name_playlist' );

function onirim_edit_name_playlist() {

    $name = trim(htmlspecialchars(strip_tags($_POST['name'])));

    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
    if(!isset($currentPlaylist['data']) || !is_array($currentPlaylist['data'])) {
        $currentPlaylist = array(
            'name' => 'My playlist',
            'data' => array()
        );
        setcookie('current_playlist', base64_encode(serialize($currentPlaylist)), time() + (365 * 24 * 60 * 60), '/');
    }
    $currentPlaylist['name'] = $name;

    $playlist_id = onirim_get_playlist_id(false);
    if($playlist_id > 0) {
        update_field( 'name', $name, $playlist_id );
        $post = array(
            'ID'           => $playlist_id,
            'post_title'   => $name
        );
        wp_update_post( $post );
    }

    setcookie('current_playlist', base64_encode(serialize($currentPlaylist)), time() + (365 * 24 * 60 * 60), '/');

    echo $name;

    wp_die();
}



// GET BITLY URL AND SAVE PLAYLIST
// Action : get_bitly_and_save_playlist
add_action( 'wp_ajax_get_bitly_and_save_playlist', 'onirim_get_bitly_and_save_playlist' );
add_action( 'wp_ajax_nopriv_get_bitly_and_save_playlist', 'onirim_get_bitly_and_save_playlist' );

function onirim_get_bitly_and_save_playlist() {

    // Création du post wp playlist si n'existe pas
    $playlist_id = onirim_get_playlist_id(true);

    if($playlist_id > 0) {
        $short_link = get_field('short_link', $playlist_id);

        // Si URL raccourcie existe déjà
        if (filter_var($short_link, FILTER_VALIDATE_URL)) {
            wp_die($short_link);

        // Si URL raccourcie n'existe pas on la crée
        } else {

            // Création du lien raccourci bitly et stockage sur le post wp
            $params = array();
            $params['access_token'] = '7a95dac6e15b657d4561a56df0d49309fa1cf2fd';
            $params['longUrl'] = get_permalink($playlist_id);
            $results = bitly_get('shorten', $params);

            if($results['status_code'] == 200 && isset($results['data']['url']) && filter_var($results['data']['url'], FILTER_VALIDATE_URL)) {
                update_field( 'short_link', $results['data']['url'], $playlist_id );
                wp_die($results['data']['url']);
            } else {
                wp_die(get_permalink($playlist_id));
            }
        }

    }
    wp_die();
}


function onirim_get_playlist_id($create = false) {

    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
    if(isset($currentPlaylist['id']) && intval($currentPlaylist['id']) > 0) {
        $playlist = get_post( intval($currentPlaylist['id']) );
        if(isset($playlist->post_type) && $playlist->post_type == 'onirim_playlist') {
            return $playlist->ID;
        }
    }

    if($create) {
        $playlist_id = wp_insert_post( array(
            'post_type' => 'onirim_playlist',
            'post_status' => 'publish',
            'post_content' => '',
            'post_title' => (($currentPlaylist['name'] != '') ? $currentPlaylist['name'] : 'My playlist'),
            'comment_status' => 'closed',
            'ping_status' => 'closed'
        ));
        if($playlist_id > 0) {
            $currentPlaylist['id'] = $playlist_id;
            setcookie('current_playlist', base64_encode(serialize($currentPlaylist)), time() + (365 * 24 * 60 * 60), '/');
            update_field( 'name', 'My playlist', $playlist_id );
            update_field( 'gabarits_playlist', array_merge($currentPlaylist['data']), $playlist_id );
            return $playlist_id;
        }
    }

    // Si la playlist existe deja en base on stocke son ID dans la variable de session
//    if(isset($query[0]->post_type) && $query[0]->post_type == 'onirim_playlist') {
//        $_COOKIE['current_playlist'] = array(
//            'id'   => $query[0]->ID,
//            'name' => get_field('name', $query[0]->ID),
//            'data' => array()
//        );
//        return $query[0]->ID;
//
//    // Si la playlist n'existe pas en base on la créé et on stocke son ID dans la variable de session (si $create == true)
//    }else {
////        if($create) {
////        }
//    }
    return '0';
}