<?php
wp_cache_flush();

function isa_delete_all_transients() {

    global $wpdb;

    $sql = 'DELETE FROM ' . $wpdb->options . ' WHERE option_name LIKE "_transient_%"';
    $wpdb->query($sql);

}
add_action( 'init', 'isa_delete_all_transients' );