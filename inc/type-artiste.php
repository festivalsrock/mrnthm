<?php

function onirim_create_artiste_type() {
//    $args = array(
//        'labels' => array(
//            'name' => __( 'Artistes' ),
//            'singular_name' => __( 'Artiste' )
//        ),
//        'hierarchical'        => false,
//        'public'              => true,
//        'show_ui'             => true,
//        'show_in_menu'        => true,
//        'show_in_nav_menus'   => true,
//        'show_in_admin_bar'   => true,
//        'menu_position'       => 5,
//        'menu_icon'           => 'dashicons-admin-users',
//        'can_export'          => true,
//        'has_archive'         => true,
//        'exclude_from_search' => false,
//        'publicly_queryable'  => true,
//        'capability_type'     => 'page',
//    );
//
//    register_post_type( 'onirim_artiste', $args );

        // create a new taxonomy
    $labels = array(
        'name' => __('Artistes'),
        'singular_name' => __('Artiste'),
        'all_items' => __('Tous les artistes'),
        'edit_item' => __('Éditer l\'artiste'),
        'view_item' => __('Voir l\'artiste'),
        'update_item' => __('Mettre à jour l\'artiste'),
        'add_new_item' => __('Ajouter un artiste'),
        'new_item_name' => __('Nouveau artiste'),
        'search_items' => __('Rechercher parmi les artistes'),
        'popular_items' => __('Artistes les plus utilisés')
    );
    register_taxonomy(
        'onirim_artiste',
        'onirim_gabarit',
        array(
            'label' => __( 'Artistes' ),
            'labels' => $labels,
            'rewrite' => array( 'slug' => 'artist' ),
            'capabilities' => array(
//                    'assign_terms' => 'edit_guides',
//                    'edit_terms' => 'publish_guides'
            ),
            'show_in_quick_edit' => false,
            'meta_box_cb' => false,
        )
    );


    $labels = array(
        'name' => __('Catégories d\'artiste'),
        'singular_name' => __('Catégorie d\'artiste'),
        'all_items' => __('Toutes les catégories d\'artiste'),
        'edit_item' => __('Éditer la catégorie d\'artiste'),
        'view_item' => __('Voir la catégorie d\'artiste'),
        'update_item' => __('Mettre à jour la catégorie d\'artiste'),
        'add_new_item' => __('Ajouter une catégorie d\'artiste'),
        'new_item_name' => __('Nouvelle catégorie d\'artiste'),
        'search_items' => __('Rechercher parmi les catégories d\'artiste'),
        'popular_items' => __('Catégories d\'artiste les plus utilisées')
    );
    register_taxonomy(
        'onirim_categorie_artiste',
        'onirim_gabarit',
        array(
            'label' => __( 'Catégories d\'artiste' ),
            'labels' => $labels,
            'rewrite' => array( 'slug' => 'category_artist' ),
            'capabilities' => array(
//                    'assign_terms' => 'edit_guides',
//                    'edit_terms' => 'publish_guides'
            ),
            'show_in_quick_edit' => false,
            'meta_box_cb' => false,
        )
    );

}
add_action( 'init', 'onirim_create_artiste_type' );


function onirim_artist_page_related_post_object_query( $args, $field, $post_id ) {

    $args['meta_query']	= array(
        array(
            'key'   => '_wp_page_template',
            'value' => 'views/artist-list.php'
        )
    );

    // return
    return $args;

}

add_filter('acf/fields/post_object/query/name=page_liste_artistes', 'onirim_artist_page_related_post_object_query', 10, 3);
