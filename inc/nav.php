<?php
function onirim_main_menu() {
    register_nav_menu('main-menu',__( 'Menu principal' ));
}
add_action( 'init', 'onirim_main_menu' );


function onirim_artist_detail_active_nav_class( $classes, $item ) {
    if ( is_tax( 'onirim_artiste' )) {
        $term = get_queried_object();
        $page_liste_artistes_id = ONIRIM_DEFAULT_PAGE_ARTIST_ID;
        $page_liste_artistes = get_field('page_liste_artistes', $term);
        if(is_object($page_liste_artistes) && isset($page_liste_artistes->ID)) {
            $page_liste_artistes_id = $page_liste_artistes->ID;
        }
        if(intval($item->object_id) == $page_liste_artistes_id) {
            $classes[] = 'current_page_item';
        }
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'onirim_artist_detail_active_nav_class', 10, 2 );