<?php
// Remove default image sizes here.
function onirim_remove_default_images( $sizes, $metadata ) {
    unset( $sizes['small']); // 150px
    unset( $sizes['medium']); // 300px
    unset( $sizes['large']); // 1024px
    unset( $sizes['medium_large']); // 768px
    return $sizes;
}

add_filter( 'intermediate_image_sizes_advanced', 'onirim_remove_default_images', 10, 1 );

// Disable WordPRess responsive srcset images
add_filter('max_srcset_image_width', create_function('', 'return 1;'));

// Remove accents for filenames
add_filter('sanitize_file_name', 'remove_accents' );

// Custom sizes
function onirim_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'mobile-small' => __( 'Mobile (small)' ),
        'mobile' => __( 'Mobile' )
    ) );
}
add_filter( 'image_size_names_choose', 'onirim_custom_sizes' );

add_image_size( 'lazy', 50, 50, false );
add_image_size( 'mobile', 1125, null, false );
add_image_size( 'mobile-small', 640, null, false );
