<?php

define('VIMEO_CLIENT_ID', '86f14acd171e6a4c5a273d8431ac9a23ead10686');
define('VIMEO_CLIENT_SECRET', '+Y6+X1hJjyxMMt5GaRd10Z9qVWsGWCgur+N4LOV6Ofh6PCys0yNC01M4StLKeg7kOccF5ra2Xbc9NrBq4SeCV5kQPtJRNiXYpiQ4lUvrvV0aW6PD3swMV0LEA89Aghtk');
define('VIMEO_CLIENT_TOKEN', 'dacb9683b0e9f80cee13b744a584f8a9');

header("Authorization: Basic " . base64_encode(VIMEO_CLIENT_ID.":".VIMEO_CLIENT_SECRET));

function onirim_get_vimeo($video_id) {

    $cache_key = 'vimeo_'.$video_id;
    $cache_group = 'onirim_vimeo';
    $out = wp_cache_get( $cache_key, $cache_group );
    if ( false === $out ) {

        $out = [];
        require_once locate_template("/inc/plugins/vimeo/autoload.php");

        $apiVimeo = new \Vimeo\Vimeo(VIMEO_CLIENT_ID, VIMEO_CLIENT_SECRET);

        // use the token
        $apiVimeo->setToken(VIMEO_CLIENT_TOKEN);

        $response = $apiVimeo->request('/videos/'.$video_id.'?fields=files,pictures', null, 'GET');
        if(isset($response['body']['files']) && is_array($response['body']['files'])) {
            foreach($response['body']['files'] as $file) {
                if($file['quality'] == 'hls') {
                    $out['url'] = $file['link'];
                    $out['url'] = str_replace('http:', '', $out['url']);
                    $out['url'] = str_replace('https:', '', $out['url']);
                }
                if(is_int($file['width']) && is_int($file['height']) && $file['width'] > 0 && $file['height'] > 0) {
                    $out['ratio'] = $file['width'] / $file['height'];
                }
            }
        }

        // Picture
        if(isset($response['body']['pictures']['sizes']) && is_array($response['body']['pictures']['sizes'])) {
            $size_width = 0;
            foreach($response['body']['pictures']['sizes'] as $size) {
                if($size['width'] > $size_width) {
                    $size_width = $size['width'];
                    $out['picture'] = $size['link'];
                    $out['picture'] = str_replace('http:', '', $out['picture']);
                    $out['picture'] = str_replace('https:', '', $out['picture']);
                }
                if($size_width >= 1280) break;
            }
        }

        // MP4
        if(isset($response['body']['files']) && is_array($response['body']['files'])) {
            $size_width = 0;
            foreach($response['body']['files'] as $file) {
                if($file['type'] == 'video/mp4' && $file['quality'] == 'hd') {
                    if($size['width'] > $size_width) {
                        $out['url_mp4'] = $file['link'];
                        $out['url_mp4'] = str_replace('http:', '', $out['url_mp4']);
                        $out['url_mp4'] = str_replace('https:', '', $out['url_mp4']);
                        $size_width = $size['width'];
                    }
                }
            }
        }

        wp_cache_set( $cache_key, $out, $cache_group, (3600 * 24 * 7) ); // 7 jours
    }

    return $out;
}
