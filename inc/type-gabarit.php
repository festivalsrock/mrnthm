<?php

function onirim_create_gabarit_type() {
    $labels = array(
        'name' => __('Gabarits'),
        'singular_name' => __('Gabarit'),
        'all_items' => __('Tous les gabarits'),
        'edit_item' => __('Éditer le gabarit'),
        'view_item' => __('Voir le gabarit'),
        'update_item' => __('Mettre à jour le gabarit'),
        'add_new_item' => __('Ajouter un gabarit'),
        'new_item_name' => __('Nouveau gabarit'),
        'search_items' => __('Rechercher parmi les gabarits'),
        'popular_items' => __('Gabarits les plus utilisés')
    );
    $args = array(
                'label' => __( 'Gabarits' ),
                'labels' => $labels,
                'hierarchical'        => false,
                'public'              => true,
                'rewrite'             => array('slug' => 'creation'),
                'has_archive'         => false,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_position'       => 5,
                'menu_icon'           => 'dashicons-format-gallery',
                'can_export'          => true,
                'exclude_from_search' => false,
                'publicly_queryable'  => true,
                'capability_type'     => 'page',
            );

    // Registering your Custom Post Type
    register_post_type( 'onirim_gabarit', $args );

//    $labels = array(
//        'name' => __('Catégories de gabarit'),
//        'singular_name' => __('Catégorie de gabarit'),
//        'all_items' => __('Toutes les catégories de gabarit'),
//        'edit_item' => __('Éditer la catégorie de gabarit'),
//        'view_item' => __('Voir la catégorie de gabarit'),
//        'update_item' => __('Mettre à jour la catégorie de gabarit'),
//        'add_new_item' => __('Ajouter une catégorie de gabarit'),
//        'new_item_name' => __('Nouvelle catégorie de gabarit'),
//        'search_items' => __('Rechercher parmi les catégories de gabarit'),
//        'popular_items' => __('Catégories de gabarit les plus utilisées')
//    );
//    register_taxonomy(
//        'onirim_categorie_gabarit',
//        'onirim_gabarit',
//        array(
//            'label' => __( 'Catégories de gabarit' ),
//            'labels' => $labels,
//            'rewrite' => array( 'slug' => 'category_gabarit' ),
//            'capabilities' => array(
////                    'assign_terms' => 'edit_guides',
////                    'edit_terms' => 'publish_guides'
//            ),
//            'show_in_quick_edit' => false,
//            'meta_box_cb' => false,
//        )
//    );

}
add_action( 'init', 'onirim_create_gabarit_type' );
// Add the custom columns to the book post type:
add_filter( 'manage_onirim_gabarit_posts_columns', 'set_onirim_edit_onirim_gabarit_columns' );
function set_onirim_edit_onirim_gabarit_columns($columns) {
    $date = $columns['date'];
    $columns['type_gabarit'] = 'Type gabarit';
    unset($columns['date']);
    $columns['date'] = $date;

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_onirim_gabarit_posts_custom_column' , 'onirim_gabarit_column', 10, 2 );
function onirim_gabarit_column( $column, $post_id ) {
    switch ( $column ) {
        case 'type_gabarit' :
            if(get_field('type_gabarit', $post_id) > 0) echo 'Type '.get_field('type_gabarit', $post_id);
            break;
    }
}

/*
 * Custom display and query pour le champ de sélection de gabarits
 */

function onirim_gabarits_relationship_result( $title, $post, $field, $post_id ) {

    // load a custom field from this $object and show it in the $result
//    $page_views = get_field('page_views', $post->ID);
    $url = get_edit_post_link( $post->ID );

    // Nom de l'artiste
    $artiste = get_field( 'artiste',  $post->ID );
    if(isset($artiste->name)) $title = '<span class="artiste">' . $artiste->name . ' &rsaquo; </span>' . $title;

    // Nom du projet
    $projet = get_field( 'projet',  $post->ID );
    if(isset($projet->name)) $title = '<span class="projet">' . $projet->name . ' &rsaquo; </span>' . $title;


    // append to title
    $title .= ' <span class="artiste">['.$post->ID.']</span> <a href="'.$url.'" title="Modifier le gabarit" class="edit-gabarit"><i class="dashicons dashicons-edit"></i></a>';


    // return
    return $title;

}
add_filter('acf/fields/relationship/result/name=gabarits', 'onirim_gabarits_relationship_result', 10, 4);
add_filter('acf/fields/relationship/result/name=gabarits_playlist', 'onirim_gabarits_relationship_result', 10, 4);


function onirim_gabarits_relationship_query( $args, $field, $post_id ) {

    // only show children of the current post being edited
    //$args['post_parent'] = $post_id;

    $args['meta_query']	= array(
        array(
            'key'		=> 'display_on_page',
            'value'		=> $post_id,
            'compare'	=> 'LIKE'
        )
    );

    // return
    return $args;

}
add_filter('acf/fields/relationship/query/name=gabarits', 'onirim_gabarits_relationship_query', 10, 3);


function onirim_gabarits_artiste_relationship_result( $title, $post, $field, $post_id ) {

    // load a custom field from this $object and show it in the $result
//    $page_views = get_field('page_views', $post->ID);
    $url = get_edit_post_link( $post->ID );

    // Nom de l'artiste
    $artiste = get_field( 'artiste',  $post->ID );
    if(isset($artiste->name)) $title = '<span class="artiste">' . $artiste->name . ' &rsaquo; </span>' . $title;

    // Nom du projet
    $projet = get_field( 'projet',  $post->ID );
    if(isset($projet->name)) $title = '<span class="projet">' . $projet->name . ' &rsaquo; </span>' . $title;


    // append to title
    $title .= ' <span class="artiste">['.$post->ID.']</span> <a href="'.$url.'" title="Modifier le gabarit" class="edit-gabarit"><i class="dashicons dashicons-edit"></i></a>';


    // return
    return $title;

}
add_filter('acf/fields/relationship/result/name=gabarits_artiste', 'onirim_gabarits_artiste_relationship_result', 10, 4);


function onirim_gabarits_artiste_relationship_query( $args, $field, $post_id ) {

    // only show children of the current post being edited
    //$args['post_parent'] = $post_id;


    $args['tax_query']	= array(
        array(
            'taxonomy' => 'onirim_artiste',
            'field' => 'term_taxonomy_id',
            'terms' => array(intval(str_replace('term_', '', $post_id))),
        )
    );

    // return
    return $args;

}
add_filter('acf/fields/relationship/query/name=gabarits_artiste', 'onirim_gabarits_artiste_relationship_query', 10, 3);




function onirim_gabarit_cover_post_object_query( $args, $field, $post_id ) {

    $args['tax_query']	= array(
        array(
            'taxonomy' => 'onirim_artiste',
            'field' => 'term_taxonomy_id',
            'terms' => array(intval(str_replace('term_', '', $post_id))),
        )
    );

    // return
    return $args;

}

add_filter('acf/fields/post_object/query/name=gabarit_cover', 'onirim_gabarit_cover_post_object_query', 10, 3);

function onirim_gabarit_cover_post_object_result( $title, $post, $field, $post_id ) {

    $artiste = get_field( 'artiste',  $post->ID );
    if(isset($artiste->name)) $title = '<span class="artiste">' . $artiste->name . ' &rsaquo; </span>' . $title;

    // Nom du projet
    $projet = get_field( 'projet',  $post->ID );
    if(isset($projet->name)) $title = $projet->name . ' &rsaquo; ' . $title;

    // append to title
    $title .= ' ['.$post->ID.']';


    // return
    return $title;

}
add_filter('acf/fields/post_object/result/name=gabarit_cover', 'onirim_gabarit_cover_post_object_result', 10, 4);


// SEO YOAST REMOVE METAS WHEN 'gid' defined in url

add_action('template_redirect','onirim_remove_gabarit_wpseo');
function onirim_remove_gabarit_wpseo(){
    if(isset($_REQUEST['gid']) && intval($_REQUEST['gid']) > 0) {
        global $wpseo_front;
        if(defined($wpseo_front)){
            remove_action('wp_head',array($wpseo_front,'head'),1);
        }
        else {
            $wp_thing = WPSEO_Frontend::get_instance();
            remove_action('wp_head',array($wp_thing,'head'),1);
        }
    }
}

add_filter( 'wp_title', function( $title )
{
    return trim( $title );
} );

/**
 * Ajout bouton suppression projet
 */

function onirim_gabarits_projet_relationship_result( $title, $post, $field, $post_id ) {

    $title .= ' ('.$post->count.')';
    if($post->count == 0) $title .= '<i class="acf-icon -minus small dark remove-projet" data-name="remove_item" data-id="'.$post->term_id.'"></i>';

    // return
    return $title;

}
add_filter('acf/fields/taxonomy/result/name=projet', 'onirim_gabarits_projet_relationship_result', 10, 4);



/**
 * SUPPRESSION AJAX DE PROJET DEPUIS UN GABARIT
 */
add_action( 'wp_ajax_onirim_remove_project', 'onirim_remove_project' );

function onirim_remove_project() {

    $param = intval($_POST['id_projet']);
    if($param > 0) {
        wp_delete_term( $param, 'onirim_projet' );
    }

    die(1);
}

/**
 * Consignes gabarits
 */
function onirim_custom_consignes( $field ) {
    $key = $field['key'];
    if($key == 'field_5a19483686e8d') echo get_field('consigne_gabarit_1', 'onirim_options');
    if($key == 'field_5a1a37fac2e88') echo get_field('consigne_gabarit_2', 'onirim_options');
    if($key == 'field_5a1a38936df43') echo get_field('consigne_gabarit_3', 'onirim_options');
    if($key == 'field_5a1d61088dbc8') echo get_field('consigne_gabarit_4', 'onirim_options');
    if($key == 'field_5a1d6a09a113b') echo get_field('consigne_gabarit_5', 'onirim_options');
    if($key == 'field_5a1d672e9b00b') echo get_field('consigne_gabarit_6', 'onirim_options');
    if($key == 'field_5a1d683081c1b') echo get_field('consigne_gabarit_7', 'onirim_options');
    if($key == 'field_5a1d690857241') echo get_field('consigne_gabarit_8', 'onirim_options');
    if($key == 'field_5a1d694f46fb5') echo get_field('consigne_gabarit_9', 'onirim_options');
    if($key == 'field_5a3001ab26947') echo get_field('consigne_gabarit_10', 'onirim_options');
}
add_action( 'acf/render_field', 'onirim_custom_consignes', 10, 1 );

