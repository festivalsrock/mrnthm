<?php

add_action('admin_menu','onirim_remove_links_admin_menu');

function onirim_remove_links_admin_menu() {
    remove_menu_page( 'edit.php' );                   //Posts
    remove_menu_page( 'edit-comments.php' );          //Comments
}

if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 10);

/*
 * ACF OPTION PAGE
 */
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array('page_title' => 'Config Onirim', 'capability' => 'delete_others_pages', 'post_id' => 'onirim_options'));

}

/*
 * CUSTOM ADMIN CSS
 */
function onirim_admin_style_scripts() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
    wp_enqueue_style('admin-lightbox-styles', get_template_directory_uri().'/css/admin-lightbox.min.css');
    wp_enqueue_script( 'admin-scripts', get_template_directory_uri() . '/js/admin.js' );
    wp_localize_script('admin-scripts', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_enqueue_script( 'admin-lightbox-scripts', get_template_directory_uri() . '/js/admin-lightbox.min.js' );
}
add_action('admin_enqueue_scripts', 'onirim_admin_style_scripts');

/*
 * ROLES UTILISATEURS
 */

// Remove roles existants
remove_role( 'subscriber' );
remove_role( 'contributor' );
remove_role( 'author' );
remove_role( 'editor' );
remove_role( 'moderator' );
remove_role( 'onirim_admin' );

// Create new specific roles for Onirim
// Admin
add_role('onirim_admin', __(
    'ONIRIM Administrateur'),
    array(
        'activate_plugins' => false,
        'delete_others_pages' => true,
        'delete_others_posts' => true,
        'delete_pages' => true,
        'delete_posts' => true,
        'delete_private_pages' => true,
        'delete_private_posts' => true,
        'delete_published_pages' => true,
        'delete_published_posts' => true,
        'edit_dashboard' => true,
        'edit_others_pages' => true,
        'edit_others_posts' => true,
        'edit_pages' => true,
        'edit_posts' => true,
        'edit_private_pages' => true,
        'edit_private_posts' => true,
        'edit_published_pages' => true,
        'edit_published_posts' => true,
        'edit_theme_options' => true,
        'export' => false,
        'import' => false,
        'list_users' => true,
        'manage_categories' => true,
        'manage_links' => false,
        'manage_options' => true,
        'moderate_comments' => false,
        'promote_users' => true,
        'publish_pages' => true,
        'publish_posts' => true,
        'read_private_pages' => true,
        'read_private_posts' => true,
        'read' => true,
        'remove_users' => true,
        'switch_themes' => false,
        'upload_files' => true,
        'customize' => false,
        'delete_site' => false
    )
);
// Editor : can only edit pages
add_role('onirim_editor', __(
    'ONIRIM Editeur'),
    array(
        'delete_others_pages' => false,
        'delete_others_posts' => false,
        'delete_pages' => false,
        'delete_posts' => false,
        'delete_private_pages' => false,
        'delete_private_posts' => false,
        'delete_published_pages' => false,
        'delete_published_posts' => false,
        'edit_others_pages' => true,
        'edit_others_posts' => true,
        'edit_pages' => true,
        'edit_posts' => true,
        'edit_private_pages' => true,
        'edit_private_posts' => true,
        'edit_published_pages' => true,
        'edit_published_posts' => true,
        'manage_categories' => true,
        'manage_links' => false,
        'moderate_comments' => false,
        'publish_pages' => false,
        'publish_posts' => false,
        'read' => true,
        'read_private_pages' => true,
        'read_private_posts' => true,
        'unfiltered_html' => true,
        'upload_files' => false
    )
);

$onirim_admin = get_role('onirim_admin');
$onirim_admin->add_cap('activate_plugins');
$onirim_admin->add_cap('delete_others_pages');
$onirim_admin->add_cap('delete_others_posts');
$onirim_admin->add_cap('delete_pages');
$onirim_admin->add_cap('delete_posts');
$onirim_admin->add_cap('delete_private_pages');
$onirim_admin->add_cap('delete_private_posts');
$onirim_admin->add_cap('delete_published_pages');
$onirim_admin->add_cap('delete_published_posts');
$onirim_admin->add_cap('edit_dashboard');
$onirim_admin->add_cap('edit_others_pages');
$onirim_admin->add_cap('edit_others_posts');
$onirim_admin->add_cap('edit_pages');
$onirim_admin->add_cap('edit_posts');
$onirim_admin->add_cap('edit_private_pages');
$onirim_admin->add_cap('edit_private_posts');
$onirim_admin->add_cap('edit_published_pages');
$onirim_admin->add_cap('edit_published_posts');
$onirim_admin->add_cap('edit_theme_options');
$onirim_admin->remove_cap('export');
$onirim_admin->remove_cap('import');
$onirim_admin->add_cap('list_users');
$onirim_admin->add_cap('manage_categories');
$onirim_admin->remove_cap('manage_links');
$onirim_admin->add_cap('manage_options');
$onirim_admin->remove_cap('moderate_comments');
$onirim_admin->add_cap('promote_users');
$onirim_admin->add_cap('publish_pages');
$onirim_admin->add_cap('publish_posts');
$onirim_admin->add_cap('read_private_pages');
$onirim_admin->add_cap('read_private_posts');
$onirim_admin->add_cap('read');
$onirim_admin->add_cap('remove_users');
$onirim_admin->remove_cap('switch_themes');
$onirim_admin->add_cap('upload_files');
$onirim_admin->remove_cap('customize');
$onirim_admin->remove_cap('delete_site');


$onirim_editor = get_role('onirim_editor');
$onirim_editor->remove_cap('delete_others_pages');
$onirim_editor->remove_cap('delete_others_posts');
$onirim_editor->remove_cap('delete_pages');
$onirim_editor->remove_cap('delete_posts');
$onirim_editor->remove_cap('delete_private_pages');
$onirim_editor->remove_cap('delete_private_posts');
$onirim_editor->remove_cap('delete_published_pages');
$onirim_editor->remove_cap('delete_published_posts');
$onirim_editor->add_cap('edit_others_pages');
$onirim_editor->add_cap('edit_others_posts');
$onirim_editor->add_cap('edit_pages');
$onirim_editor->add_cap('edit_posts');
$onirim_editor->add_cap('edit_private_pages');
$onirim_editor->add_cap('edit_private_posts');
$onirim_editor->add_cap('edit_published_pages');
$onirim_editor->add_cap('edit_published_posts');
$onirim_editor->add_cap('manage_categories');
$onirim_editor->add_cap('manage_links');
$onirim_editor->remove_cap('moderate_comments');
$onirim_editor->remove_cap('publish_pages');
$onirim_editor->remove_cap('publish_posts');
$onirim_editor->add_cap('read');
$onirim_editor->add_cap('read_private_pages');
$onirim_editor->add_cap('read_private_posts');
$onirim_editor->add_cap('unfiltered_html');
$onirim_editor->remove_cap('upload_files');
