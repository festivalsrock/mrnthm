<?php

function onirim_create_playlist_type() {
    $labels = array(
        'name' => __('Playlists'),
        'singular_name' => __('Playlist'),
        'all_items' => __('Toutes les playlists'),
        'edit_item' => __('Éditer la playlist'),
        'view_item' => __('Voir la playlist'),
        'update_item' => __('Mettre à jour la playlist'),
        'add_new_item' => __('Ajouter une playlist'),
        'new_item_name' => __('Nouvelle playlist'),
        'search_items' => __('Rechercher parmi les playlists'),
        'popular_items' => __('Playlists les plus utilisées')
    );
    $args = array(
        'label' => __( 'Playlists' ),
        'labels' => $labels,
        'hierarchical'        => false,
        'public'              => true,
        'rewrite'             => array('slug' => 'playlist/%id%/%name%', 'with_front' => false),
        'has_archive'         => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-images-alt2',
        'can_export'          => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    add_rewrite_tag('%id%','(\d+)');
    add_rewrite_tag('%name%','(.+)');

    // Registering your Custom Post Type
    register_post_type( 'onirim_playlist', $args );

    add_rewrite_rule('^playlist/([0-9]+)/?','index.php?post_type=onirim_playlist&p=$matches[1]','top');

//    add_permastruct('onirim_playlist', '/playlist/%id%/%name%/');

}
add_action( 'init', 'onirim_create_playlist_type' );


function onirim_wpc_permalinks($permalink, $post, $leavename) {

    if (is_object( $post ) && $post->post_type == 'onirim_playlist') {
        $slug = sanitize_title(get_the_title($post->ID));
        if($slug == '') $slug = '-';
        $permalink = str_replace('%name%', $slug, $permalink);

        $permalink = str_replace('%id%', $post->ID, $permalink);
        $permalink = str_replace('/' . $post->post_name, '', $permalink);
    }

    return $permalink;
}
add_filter('post_type_link', 'onirim_wpc_permalinks', 10, 3);
