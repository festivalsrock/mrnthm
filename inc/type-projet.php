<?php

function onirim_create_projet_type() {
//    $args = array(
//        'labels' => array(
//            'name' => __( 'Projets' ),
//            'singular_name' => __( 'Projet' )
//        ),
//        'hierarchical'        => false,
//        'public'              => true,
//        'show_ui'             => true,
//        'show_in_menu'        => true,
//        'show_in_nav_menus'   => true,
//        'show_in_admin_bar'   => true,
//        'menu_position'       => 5,
//        'menu_icon'           => 'dashicons-media-default',
//        'can_export'          => true,
//        'has_archive'         => true,
//        'exclude_from_search' => false,
//        'publicly_queryable'  => true,
//        'capability_type'     => 'page',
//    );
//
//    register_post_type( 'onirim_projet', $args );
    $labels = array(
        'name' => __('Projets'),
        'singular_name' => __('Projet'),
        'all_items' => __('Tous les projets'),
        'edit_item' => __('Éditer le projet'),
        'view_item' => __('Voir le projet'),
        'update_item' => __('Mettre à jour le projet'),
        'add_new_item' => __('Ajouter un projet'),
        'new_item_name' => __('Nouveau projet'),
        'search_items' => __('Rechercher parmi les projets'),
        'popular_items' => __('Projets les plus utilisés')
    );
    register_taxonomy(
        'onirim_projet',
        'onirim_gabarit',
        array(
            'label' => __( 'Projets' ),
            'labels' => $labels,
            'rewrite' => array( 'slug' => 'projet' ),
            'capabilities' => array(
//                    'assign_terms' => 'edit_guides',
//                    'edit_terms' => 'publish_guides'
            ),
            'show_in_quick_edit' => false,
            'meta_box_cb' => false,
        )
    );
}
add_action( 'init', 'onirim_create_projet_type' );