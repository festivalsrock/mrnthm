<?php
/**
 * Template Name: Playlist
 */

$playlist = unserialize(base64_decode($_COOKIE['current_playlist']));
$playlist_name = $playlist['name'];
$playlist_gabarits = $playlist['data'];
/*echo '<hr><hr><hr><hr><hr><hr><pre>';
var_dump('$_COOKIE', $_COOKIE);
echo '</pre>';*/
$playlist_gabarits_by_artist = array();
foreach($playlist_gabarits as $gabarit_id) {
    $artiste = get_field('artiste', $gabarit_id);
    $playlist_gabarits_by_artist[intval($artiste->term_id)][] = $gabarit_id;
}
//echo '<hr><hr><hr><hr><hr><hr><pre>';
//var_dump($playlist_gabarits_by_artist);
//echo '</pre>';


get_header();

?>


<div class="template-playlist">

    <div class="playlist-top">
        <div class="playlist-top-title">YOUR PLAYLIST</div>
        <input type="text" autocorrect="off" autocapitalize="off" spellcheck="false" name="name" id="inputname" placeholder="YOUR TITLE" value="<?php echo $playlist_name; ?>" class="playlist-top-playlist-name" tabindex="-1">    
        <div class="playlist-top-menu">
            <div class="playlist-top-menu-share">
                <div class="playlist-top-menu-share-open" id="open-share"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt=""><span>SHARE MY PLAYLIST</span></div>
                <div class="playlist-top-menu-share-link" id="share-link"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-checked.svg" alt=""><span>LINK COPIED</span><input type="text" readonly value="<?php echo get_field('short_link'); ?>" id="input-link" tabindex="-1">
                </div>
            </div>
            <div class="separator"></div>
            <div class="playlist-top-menu-trash" id="empty-playlist"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-trash.svg" alt=""><span>EMPTY MY PLAYLIST</span></div>
        </div>
    </div>


    <?php
    foreach($playlist_gabarits_by_artist as $artist_id => $gabarits) {
        foreach($gabarits as $k => $gabarit_id) {

            $gabarit = get_post($gabarit_id);
            if ($gabarit->post_type == 'onirim_gabarit') {

                $type_gabarit = intval(get_field('type_gabarit', $gabarit_id));

                if ($type_gabarit > 0) {
                    // Si premier gabarit de l'artiste alors on affiche le nom de l'artiste
                    $artiste = get_field('artiste', $gabarit_id);
                    if($k == 0) { echo '<div><h1 class="title-artist">'.$artiste->name.'</h1>'; }
                    include(locate_template('views/partials/gabarit-' . $type_gabarit . '.php'));
                    if($k == 0) { echo '</div>'; }
                }
            }
        }
    }
    ?>

    
    <div class="gabarit-separator"></div>
    <div class="gabarit-separator"></div>
</div>



<?php get_footer(); ?>