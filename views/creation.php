<?php
/**
 * Template Name: Creation
 */

$gabarits = get_field('gabarits');

$array_projets = array();
foreach($gabarits as $gabarit) :
    if($gabarit->ID > 0) {
        $projet = get_field('projet', $gabarit->ID);

        if($projet && $projet->term_id > 0) {
            if(!array_key_exists($projet->term_id, $array_projets)) {
                $array_projets[$projet->term_id] = [
                    'gabarit_id' => $gabarit->ID,
                    'name' => $projet->name
                    ];
            }
        }
    }
endforeach;

// First project name for the dropdown list label
// Works localy but not online
//array_reduce($gabarits);
$firstProjet = get_field('projet', $gabarits[0]->ID);
$firstProjetName = $array_projets[$firstProjet->term_id]['name'];

// echo '<pre>';
// print_r($gabarits[0]->ID);
// echo '</pre>';

get_header(); ?>


<div class="template-creation">

    <!-- LISTE DES PROJETS -->
    <div class="dropdown">            
        <!-- LISTE DES ARTISTES -->
        <div class="dropdown-label"><?php echo $firstProjetName ?></div>
        <div class="template-artist-list dropdown-list">    

            <!-- MENU -->
            <div class="menu-artist">
                <ul class="col col-1">
                <?php foreach($array_projets as $data_projet) : ?>
                    <li data-goto="<?php echo $data_projet['gabarit_id']; ?>">
                        <?php echo $data_projet['name']; ?>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>

        </div>
    </div>    

    <?php
    $prec_projet = 0;
    $cpt = 0;
    foreach($gabarits as $k => $gabarit) {
        $type_gabarit = intval(get_field('type_gabarit', $gabarit->ID));
        ?>

        <?php if($type_gabarit > 0) {
            $projet = get_field('projet', $gabarit->ID);
            $current_projet = intval($projet->term_id);
            if($cpt > 0 && ($prec_projet != $current_projet || $prec_projet == 0)) echo '<div class="gabarit-separator"></div>';
            $prec_projet = $current_projet;
            echo '<span class="goto-gabarit" id="goto-'.$gabarit->ID.'"></span>';
            echo '<span class="goto-gabarit" id="goto-gabarit-'.$gabarit->ID.'">'.$array_projets[$projet->term_id]['name'].'</span>';
            include(locate_template('views/partials/gabarit-'.$type_gabarit.'.php'));
            $cpt ++;
        } ?>

        <?php
    }
    ?>
</div>

<?php get_footer(); ?>