<?php
/*
 * Détail d'une playlist existante
 */

get_header();
?>


<div class="template-playlist template-playlist-shared">

    <div class="playlist-top">
        <div class="playlist-top-playlist-name"><?php echo get_the_title(); ?></div>
    </div>

    <?php

    $gabarits = get_field('gabarits_playlist');

    $playlist_gabarits_by_artist = array();
    foreach($gabarits as $gabarit) {
        $artiste = get_field('artiste', $gabarit->ID);
        $playlist_gabarits_by_artist[intval($artiste->term_id)][] = $gabarit->ID;
    }

    foreach($playlist_gabarits_by_artist as $artist_id => $gabarits) {
        foreach($gabarits as $k => $gabarit_id) {
            $gabarit = get_post($gabarit_id);
            $type_gabarit = intval(get_field('type_gabarit', $gabarit_id));

            if ($type_gabarit > 0) {
                // Si premier gabarit de l'artiste alors on affiche le nom de l'artiste
                $artiste = get_field('artiste', $gabarit_id);
                if($k == 0) {
                    echo '<h1 class="title-artist">'.$artiste->name.'</h1>';
                }
                include(locate_template('views/partials/gabarit-' . $type_gabarit . '.php'));
            }
        }
    }
    ?>

</div>

<?php
get_footer();
