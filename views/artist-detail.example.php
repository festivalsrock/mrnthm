<?php

$term = get_queried_object();
$gabarits = get_field('gabarits_artiste', $term);
$artiste_name = $term->name;
$artiste_image_header = get_field('image_header', $term);
$artiste_image = get_field('image', $term);
$artiste_description = get_field('description', $term);
$artiste_social_links = get_field('social_links', $term);
$artiste_social_links = $artiste_social_links['social_links'];

//echo '<pre>';var_dump($artiste, $gabarits);echo '</pre>';
//exit;

/*********************************************

    Note :
    Rename this file to artist-detail.php 
    To see all the gabarit of artist page

*********************************************/

get_header();
?>

    <div class="template-artist-details">

        <div class="border">
            <div class="cover" style="background-image:url(<?php echo esc_url(get_template_directory_uri()); ?>/images/demo/artist-header.jpg)">
            
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" preload="auto" data-setup='{"autoplay":false, "loop":true}'>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="cover-title"><?php echo $artiste_name; ?></div>
                <div class="cover-scroll">SCROLL TO EXPLORE</div>
                <div class="cover-line"></div>
            </div>
        </div>

        <?php
        /*
        foreach($gabarits as $gabarit) {
            $type_gabarit = intval(get_field('type_gabarit', $gabarit->ID));
            ?>

            <?php if($type_gabarit > 0) include(locate_template('views/partials/gabarit-'.$type_gabarit.'.php')); ?>

            <?php
        } 
        */
        ?>

        <!--

            GABARIT 1

            -->
        <!-- GABARIT 1.0
            1 video 16/9, 1 image à droite
        -->
        <div>GABARIT 1.0</div>
        <div class="gabarit gabarit-1 gabarit-1-0">
            <div class="video-container">
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" controls preload="auto" poster='https://i.vimeocdn.com/video/617119633_1280x720.jpg?r=pad' data-setup=''>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit1-image1.jpg" alt="" class="video-bg">
            </div>
        </div>


        <!-- GABARIT 1.1
            1 video 2.35, 1 image à droite
        -->
        <div>GABARIT 1.1</div>
        <div class="gabarit gabarit-1 gabarit-1-1">
            <div class="video-container">
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-fluid vjs-2-35" controls preload="auto" poster='https://i.vimeocdn.com/video/617119633_1280x720.jpg?r=pad' data-setup=''>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit1-image2.jpg" alt="" class="video-bg">
            </div>
        </div>


        <!-- GABARIT 1.2
            1 video 16/9, 1 image à gauche
        -->
        <div>GABARIT 1.2</div>
        <div class="gabarit gabarit-1 gabarit-1-2">
            <div class="video-container">
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" controls preload="auto" poster='https://i.vimeocdn.com/video/617119633_1280x720.jpg?r=pad' data-setup=''>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit1-image3.jpg" alt="" class="video-bg">
            </div>
        </div>


        <!-- GABARIT 1.3
            1 video 2.35, 1 image à gauche
        -->
        <div>GABARIT 1.3</div>
        <div class="gabarit gabarit-1 gabarit-1-3">
            <div class="video-container">
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-fluid vjs-2-35" controls preload="auto" poster='https://i.vimeocdn.com/video/617119633_1280x720.jpg?r=pad' data-setup=''>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit1-image4.jpg" alt="" class="video-bg">
            </div>
        </div>
        
        <!--

            GABARIT 2

            -->
        <!-- GABARIT 2.0
            1 video 16/9
        -->
        <div>GABARIT 2.0</div>
        <div class="gabarit gabarit-2 gabarit-2-0">
            <div class="video-container">
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" controls preload="auto" poster='https://i.vimeocdn.com/video/617119633_1280x720.jpg?r=pad' data-setup=''>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- GABARIT 2.1
            1 video 2.35
        -->
        <div>GABARIT 2.1</div>
        <div class="gabarit gabarit-2 gabarit-2-1">
            <div class="video-container">
                <video class="video-js vjs-default-skin vjs-onirim-skin vjs-fluid vjs-2-35" controls preload="auto" poster='https://i.vimeocdn.com/video/617119633_1280x720.jpg?r=pad' data-setup=''>
                    <source src="https://player.vimeo.com/external/202964286.m3u8?s=8bbfb772c15a4376dc07858a1f780f5a109f6de5&oauth2_token_id=1021454627" type='application/x-mpegURL' />
                </video>
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Video 16/9 (1.77778) : https://vimeo.com/241879000/af8bde4e3f -->
        <!-- Video 2.35 : https://vimeo.com/125947740 -->
               


        <!--

            GABARIT 3

            -->
        <!-- GABARIT 3.0
            1 petite Horizontal à gauche et 2 vertical à droite
        -->
        <div>GABARIT 3.0</div>
        <div class="gabarit gabarit-3 gabarit-3-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big"><!--
                    --><div class="image-big-left"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1-small.jpg" alt="Titre image 1"></div><!--
                    --><div class="image-big-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2-small.jpg" alt="Titre image 1"></div><!--
                --></div><!--
                --><div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
            <div class="small">            
                <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image3-small.jpg" alt="Titre image 2"></div>
                <div class="title-project">Numéro</div>
            </div>
            <div style="clear: both;"></div>
        </div>
        
        <!-- GABARIT 3.0 - VIDEO
            1 petite Horizontal à gauche et 2 vertical à droite
        -->
        <div>GABARIT 3.0 VIDEO</div>
        <div class="gabarit gabarit-3 gabarit-3-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big"><!--
                    --><div class="image-big-left"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1.mp4" type="video/mp4">
                    </video></div><!--
                    --><div class="image-big-right"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2.mp4" type="video/mp4">
                    </video></div><!--
                --></div><!--
                --><div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
            <div class="small">            
                <div class="image-small"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-petite.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-petite.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-petite.mp4" type="video/mp4">
                    </video></div>
                <div class="title-project">Numéro</div>
            </div>
            <div style="clear: both;"></div>
        </div>

        
        <!-- GABARIT 3.1 
            1 petite Horizontal à droite et 2 vertical à gauche
         -->
        <div>GABARIT 3.1</div>
        <div class="gabarit gabarit-3 gabarit-3-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big">
                    <div class="image-big-left"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2-small.jpg" alt="Titre image 1"></div><!--
                 --><div class="image-big-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1-small.jpg" alt="Titre image 1"></div>
                </div>
            </div>
            <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image3-small.jpg" alt="Titre image 2"></div>
            <div class="title-project">Numéro</div>
            <div class="legend">Advertising fir Titre du gabarit</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>
        
        
        <!-- GABARIT 3.1  VIDEO
            1 petite Horizontal à droite et 2 vertical à gauche
         -->
        <div>GABARIT 3.1 VIDEO</div>
         <div class="gabarit gabarit-3 gabarit-3-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big">
                    <div class="image-big-left"><video muted="" playsinline="" autoplay loop src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image2.mp4" type="video/mp4">
                    </video></div><!--
                 --><div class="image-big-right"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-image1.mp4" type="video/mp4">
                    </video></div>
                </div>
            </div>
            <div class="image-small"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-petite.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-petite.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit3-petite.mp4" type="video/mp4">
                    </video></div>
            <div class="title-project">Numéro</div>
            <div class="legend">Advertising fir Titre du gabarit</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>



        <!--

            GABARIT 4

         -->
        <!-- GABARIT 4.0
            Grosse image à gauche, petite image à droite et position 1/2 | 1/4
            Ajouter 'quarter' à la class pour les positions alternatives de la petite image. 
            quarter = 25%
            sinon = 50%    
        -->
        <div>GABARIT 4.0</div>
        <div class="gabarit gabarit-4 gabarit-4-0 quarter"><!-- Ajouter class="gabarit-4-0 quarter" pour changer alignement -->
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-image1-small.jpg" alt="Titre image 1"></div>                
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
            <div class="small">
                <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-image2-small.jpg" alt="Titre image 2"></div>
                <div class="title-project">Numéro</div>
                <div class="legend">Advertising fir Titre du gabarit</div>
            </div>
            <div style="clear: both;"></div>
        </div>
        
        <!-- GABARIT 4.0 VIDEO
        -->
        <div>GABARIT 4.0 VIDEO</div>
        <div class="gabarit gabarit-4 gabarit-4-0 quarter"><!-- Ajouter class="gabarit-4-0 quarter" pour changer alignement -->
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-image1-small.jpg" alt="Titre image 1"></div>                
                <div class="legend">Advertising fir Titre du gabarit</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
            <div class="small">
                <div class="image-small"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-vignette.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-vignette.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-vignette.mp4" type="video/mp4">
                    </video></div>
                <div class="title-project">Numéro</div>
                <div class="legend">Advertising fir Titre du gabarit</div>
            </div>
            <div style="clear: both;"></div>
        </div>
        
        <!-- GABARIT 4.1 
            Grosse image à droite, petite image à gauche et position 1/2 | 1/4 -->
        <div>GABARIT 4.1</div>
        <div class="gabarit gabarit-4 gabarit-4-1 quarter"><!-- Ajouter class="gabarit-4-1 quarter" pour changer alignement -->
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-image1-small.jpg" alt="Titre image 1"></div>
            <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-image2-small.jpg" alt="Titre image 2"></div>
            <div class="title-project">Numéro</div>
            <div class="legend">Advertising fir Titre du gabarit</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>
        
        <!-- GABARIT 4.1  VIDEO-->
        <div>GABARIT 4.1 VIDEO</div>
        <div class="gabarit gabarit-4 gabarit-4-1 quarter"><!-- Ajouter class="gabarit-4-1 quarter" pour changer alignement -->
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-image1-small.jpg" alt="Titre image 1"></div>
            <div class="image-small"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-vignette.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-vignette.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit4-vignette.mp4" type="video/mp4">
                    </video></div>
            <div class="title-project">Numéro</div>
            <div class="legend">Advertising fir Titre du gabarit</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>




        <!--

            GABARIT 5

            -->
        <!-- GABARIT 5.0
            Slider à gauche
        -->
        <div>GABARIT 5.0 (GAB. 10)</div>
        <div class="gabarit gabarit-5 gabarit-5-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="swiper-container multiple-swiper-1"><!-- Ici donner un nom unique à multiple-swiper-1 -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image1.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image2.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image3.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image4.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image5.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image6.jpg" alt="Titre image 2"></div>
                </div>
            </div>
            <div class="swiper-custom-arrows swiper-custom-arrows-1"><!-- Ici donner un nom unique à swiper-custom-arrows-1 -->
                <div class="swiper-button-prev swiper-button-prev-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                    <polyline fill="none" stroke="#494949" stroke-linecap="square" stroke-miterlimit="10" points="11.354,19.188 5.167,13 11.354,6.813 "/>
                    <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="5" y1="13" x2="20" y2="13"/></svg></div>
                <div class="swiper-button-next swiper-button-next-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                    <polyline fill="none" stroke="#505050" stroke-linecap="square" stroke-miterlimit="10" points="13.146,6.813 19.334,13 13.146,19.188 "/>
                    <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="19.5" y1="13" x2="4.5" y2="13"/></svg></div> 
            </div>

            <div class="title-project">Numéro</div>
            <div class="legend">Advertising fir Titre du gabarit</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>

        </div>

        <!-- GABARIT 5.1
            Slider à droite
        -->
        <div>GABARIT 5.1 (GAB. 10)</div>
        <div class="gabarit gabarit-5 gabarit-5-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="swiper-container multiple-swiper-2"><!-- Ici donner un nom unique à multiple-swiper-2 -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image1.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image2.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image3.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image4.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image5.jpg" alt="Titre image 2"></div>
                    <div class="swiper-slide"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-image6.jpg" alt="Titre image 2"></div>
                </div>
            </div>
            <div class="swiper-custom-arrows swiper-custom-arrows-2"><!-- Ici donner un nom unique à swiper-custom-arrows-2 -->
                <div class="swiper-button-prev swiper-button-prev-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                    <polyline fill="none" stroke="#494949" stroke-linecap="square" stroke-miterlimit="10" points="11.354,19.188 5.167,13 11.354,6.813 "/>
                    <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="5" y1="13" x2="20" y2="13"/></svg></div>
                <div class="swiper-button-next swiper-button-next-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                    <polyline fill="none" stroke="#505050" stroke-linecap="square" stroke-miterlimit="10" points="13.146,6.813 19.334,13 13.146,19.188 "/>
                    <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="19.5" y1="13" x2="4.5" y2="13"/></svg></div> 
            </div>

            <div class="title-project">Numéro</div>
            <div class="legend">Advertising fir Titre du gabarit</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>

        </div>

        <!-- GABARIT 5.2 | 5.3 | 5.4
            Bloc à gauche : 'left' (default)  |  Bloc centré : 'center'  | Bloc à droite : 'right'
            Image ferrée en bas si class 'bottom', sinon ferrée en haut
        -->
        <div>GABARIT 5.2 | 5.3 | 5.4</div>
        <div class="gabarit gabarit-5 gabarit-5-2">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="image-big bottom left"><!-- Ici ajouter ou enlever class 'bottom' ET  'left' 'right' ou 'center' -->
                <div class="image-big-item"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-2-image1.jpg" alt="Titre image 1"></div><!--
                --><div class="image-big-item"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-2-image2.jpg" alt="Titre image 1"></div><!--
                --><div class="image-big-item"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-2-image3.jpg" alt="Titre image 1"><div class="title-project">Richemont</div></div>
                <div class="legend">Advertising fir Titre du gabarit 8</div>
                <div class="related">
                    <div class="related-item related-item-playlist js-add-playlist">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                        <span>Add to my playlist</span>
                    </div>
                    <div class="related-item related-item-separator"><div></div></div>
                    <div class="related-item related-item-social js-share">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                        <span>Share</span>
                    </div>
                </div>
            </div>
        </div>  






        <!--

            GABARIT 6

            -->
        <!-- GABARIT 6.0
            1 image horizontale et une image verticale
        -->
        <div>GABARIT 6.0</div>
        <div class="gabarit gabarit-6 gabarit-6-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.jpg" alt="Titre image 1"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                    <!-- <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div> -->
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image2.jpg" alt="Titre image 2"></div>
                    <div class="title-project">Numéro</div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        <!-- GABARIT 6.0 VIDEO
            1 image horizontale et une image verticale
        -->
        <div>GABARIT 6.0 VIDEO</div>
        <div class="gabarit gabarit-6 gabarit-6-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4">
                    </video></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image2.jpg" alt="Titre image 2"></div>
                    <div class="title-project">Numéro</div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>


        <!-- GABARIT 6.1
            1 image verticale et une image verticale
        -->
        <div>GABARIT 6.1</div>
        <div class="gabarit gabarit-6 gabarit-6-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-1-image1.jpg" alt="Titre image 1"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-1-image2.jpg" alt="Titre image 2"></div>
                    <div class="title-project">Numéro</div>
                    <!-- <div class="legend">Advertising fir Titre du gabarit</div> -->
                    
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

        <!-- GABARIT 6.1  VIDEO
            1 image verticale et une image verticale
        -->
        <div>GABARIT 6.1 VIDEO</div>
        <div class="gabarit gabarit-6 gabarit-6-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4">
                    </video></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-1-image2.jpg" alt="Titre image 2"></div>
                    <div class="title-project">Numéro</div>
                    <!-- <div class="legend">Advertising fir Titre du gabarit</div> -->
                    
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>





        <!-- GABARIT 6.2
            2 images horizontales
        -->
        <div>GABARIT 6.2</div>
        <div class="gabarit gabarit-6 gabarit-6-2">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.jpg" alt="Titre image 1"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit7-image1.jpg" alt="Titre image 2"></div>
                    <div class="title-project">Numéro</div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>

        <!-- GABARIT 6.3
            2 image verticales
        -->
        <div>GABARIT 6.3</div>
        <div class="gabarit gabarit-6 gabarit-6-3">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big"><!--
                    --><div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-2-image3.jpg" alt="Titre image 1"><div class="image-big-border" style="width:<?php echo $border_width ?>px; background-color:<?php echo $border_color ?>;"></div><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-1-image1.jpg" alt="Titre image 2"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div><!--
                    --><div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>


        <!-- GABARIT 6.4 (pas utilisé)
            2 image verticales supperposées
        -->
        <div>GABARIT 6.4</div>
        <div class="gabarit gabarit-6 gabarit-6-4">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big"><!--
                    --><div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit5-2-image3.jpg" alt="Titre image 1"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-1-image1.jpg" alt="Titre image 2"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div><!--
                    --><div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>


        
               
        <!--

            GABARIT 7

            -->
        <!-- GABARIT 7.0
            1 image horizontale - moyenne largeur
        -->
        <div>GABARIT 7.0</div>
        <div class="gabarit gabarit-7 gabarit-7-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit7-image1.jpg" alt="Titre image 1"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- GABARIT 7.1
            1 image horizontale - grande largeur
        -->
        <div>GABARIT 7.1</div>
        <div class="gabarit gabarit-7 gabarit-7-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="center">
                <div class="big">
                    <div class="image-big"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit7-image2.jpg" alt="Titre image 1"></div><!--                
                    --><div class="legend">Advertising fir Titre du gabarit</div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!--

            GABARIT 8

            -->        
        <!-- GABARIT 8.0
            1 grande Horizontal à gauche et 1 vertical à droite, CENTRE
        -->
        <div>GABARIT 8.0</div>
        <div class="gabarit gabarit-8 gabarit-8-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big">
                    <div class="image-big-left"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit8-image1.jpg" alt="Titre image 1"></div><!--
                --><div class="image-big-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit8-image2.jpg" alt="Titre image 1"><div class="title-project">Richemont</div></div>
                </div>                
            </div>
            <div class="legend">Advertising fir Titre du gabarit 8</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>   
           
        <!-- GABARIT 8.0  VIDEO
            1 grande Horizontal à gauche et 1 vertical à droite, CENTRE
        -->
        <div>GABARIT 8.0 VIDEO</div>
        <div class="gabarit gabarit-8 gabarit-8-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big">
                    <div class="image-big-left"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4">
                    </video></div><!--
                --><div class="image-big-right"><video muted="" playsinline="" autoplay src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4" media="(min-width: 769px)">
                        <source src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit6-image1.mp4" type="video/mp4">
                    </video><div class="title-project">Richemont</div></div>
                </div>                
            </div>
            <div class="legend">Advertising fir Titre du gabarit 8</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div> 

        <!-- GABARIT 8.1
            1 grande Horizontal à gauche et 1 vertical à droite, GAUCHE
        -->
        <div>GABARIT 8.1</div>
        <div class="gabarit gabarit-8 gabarit-8-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big">
                    <div class="image-big-left"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit8-image1.jpg" alt="Titre image 1"></div><!--
                    --><div class="image-big-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit8-image2.jpg" alt="Titre image 1"><div class="title-project">Richemont</div></div>
                </div>                
            </div>
            <div class="legend">Advertising fir Titre du gabarit 8</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>   

        <!-- GABARIT 8.2
            1 grande Horizontal à gauche et 1 vertical à droite, DROITE
        -->
        <div>GABARIT 8.2</div>
        <div class="gabarit gabarit-8 gabarit-8-2">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>
            <div class="big">
                <div class="image-big">
                    <div class="image-big-left"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit8-image1.jpg" alt="Titre image 1"></div><!--
                    --><div class="image-big-right"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit8-image2.jpg" alt="Titre image 1"><div class="title-project">Richemont</div></div>
                </div>                
            </div>
            <div class="legend">Advertising fir Titre du gabarit 8</div>
            <div class="related">
                <div class="related-item related-item-playlist js-add-playlist">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                    <span>Add to my playlist</span>
                </div>
                <div class="related-item related-item-separator"><div></div></div>
                <div class="related-item related-item-social js-share">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                    <span>Share</span>
                </div>
            </div>
        </div>



        <!--

            GABARIT 9

            -->        
        <!-- GABARIT 9.0
            Mosaic 5 images
        -->
        <div>GABARIT 9.0</div>
        <div class="gabarit gabarit-9 gabarit-9-0">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>

            <div class="center">
                <div class="column column-left">
                    <div class="top"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image1.jpg" alt="Titre image 1"></div><!--
                    --><div class="bottom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image2.jpg" alt="Titre image 1"></div>
                    <div class="legend">Advertising fir Titre du gabarit 9</div>
                </div><!--  

                --><div class="column column-center">
                    <div class="title-project">Richemont</div><!--
                    --><div class="top"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image3.jpg" alt="Titre image 1"></div>
                </div><!--  

                --><div class="column column-right">
                    <div class="top"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image5.jpg" alt="Titre image 1"></div><!--
                    --><div class="bottom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image6.jpg" alt="Titre image 1"></div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>                
            </div>            
        </div>

        <!-- GABARIT 9.1
            Mosaic 6 images
        -->
        <div>GABARIT 9.1</div>
        <div class="gabarit gabarit-9 gabarit-9-1">
            <div class="title-home">
                <div class="title-home-artist">Titre Artist</div>
                <div class="title-home-project">Titre Project</div>
            </div>

            <div class="center">
                <div class="column column-left">
                    <div class="top"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image1.jpg" alt="Titre image 1"></div><!--
                    --><div class="bottom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image2.jpg" alt="Titre image 1"></div>
                    <div class="legend">Advertising fir Titre du gabarit 9</div>
                </div><!--  

                --><div class="column column-center">
                    <div class="title-project">Richemont</div><!--
                    --><div class="top"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image3.jpg" alt="Titre image 1"></div><!--
                    --><div class="bottom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image4.jpg" alt="Titre image 1"></div>
                </div><!--  

                --><div class="column column-right">
                    <div class="top"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image5.jpg" alt="Titre image 1"></div><!--
                    --><div class="bottom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/demo/gabarit9-image6.jpg" alt="Titre image 1"></div>
                    <div class="related">
                        <div class="related-item related-item-playlist js-add-playlist">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
                            <span>Add to my playlist</span>
                        </div>
                        <div class="related-item related-item-separator"><div></div></div>
                        <div class="related-item related-item-social js-share">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                            <span>Share</span>
                        </div>
                    </div>
                </div>                
            </div>            
        </div>   




        <!--
            
            INFOS

         -->
        <div class="infos">
            <div class="infos-photo">
                <?php if(isset($artiste_image['url']) && filter_var($artiste_image['url'], FILTER_VALIDATE_URL)) { ?>
                    <img src="<?php echo $artiste_image['url']; ?>" alt="<?php echo $artiste_image['alt']; ?>">
                <?php } ?>
            </div>
            <div class="infos-description">
                <div class="infos-description-title">Description</div>
                <div class="infos-description-resume"><?php echo $artiste_description; ?></div>
                <div class="infos-description-projects">
                    <div class="left">
                        <div class="title">Filmography</div>
                        <ul class="links">
                            <li><a href="">Drone</a></li>
                            <li><a href="">Mugler</a></li>
                            <li><a href="">Yves Saint Laurent</a></li>
                            <li><a href="">Givenchy</a></li>
                            <li><a href="">Hung Lean</a></li>
                        </ul>
                    </div><!--
                    --><div class="right">
                        <div class="title">Photography</div>
                        <ul class="links">
                            <li><a href="">Drone</a></li>
                            <li><a href="">Mugler</a></li>
                            <li><a href="">Yves Saint Laurent</a></li>
                            <li><a href="">Givenchy</a></li>
                            <li><a href="">Hung Lean</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="social">
                    <?php if(isset($artiste_social_links['facebook']) && filter_var($artiste_social_links['facebook'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['facebook']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on Facebook"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook-white.svg" alt="Facebook"></a></li>
                    <?php } ?>
                    <?php if(isset($artiste_social_links['twitter']) && filter_var($artiste_social_links['twitter'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['twitter']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on Twitter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-twitter-white.svg" alt="Twitter"></a></li>
                    <?php } ?>
                    <?php if(isset($artiste_social_links['linkedin']) && filter_var($artiste_social_links['linkedin'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['linkedin']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on LinkedIn"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-linkedin-white.svg" alt="LinkedIn"></a></li>
                    <?php } ?>
                    <?php if(isset($artiste_social_links['instagram']) && filter_var($artiste_social_links['instagram'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['instagram']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on Instagram"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram-white.svg" alt="Instagram"></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

<?php
get_footer();
