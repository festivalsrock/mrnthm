<?php
/**
 * Template Name: About us
 */

$resume = get_field('intro');
$social_links = get_field('social_links');
$agences = get_field('agences');

// Heure Paris
date_default_timezone_set('Europe/Paris');

get_header(); ?>

    <div class="template-aboutus">

        <div class="resume"><?php echo $resume; ?></div>
        <div class="resume resume-over"><?php echo $resume; ?></div>

        <ul class="social">
            <?php if(isset($social_links['facebook']) && filter_var($social_links['facebook'], FILTER_VALIDATE_URL)) { ?>
                <li><a href="<?php echo $social_links['facebook']; ?>" target="_blank" title="Follow Onirim on Facebook"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook.svg" alt="Facebook"></a></li>
            <?php } ?>
            <?php if(isset($social_links['twitter']) && filter_var($social_links['twitter'], FILTER_VALIDATE_URL)) { ?>
                <li><a href="<?php echo $social_links['twitter']; ?>" target="_blank" title="Follow Onirim on Twitter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-twitter.svg" alt="Twitter"></a></li>
            <?php } ?>
            <?php if(isset($social_links['linkedin']) && filter_var($social_links['linkedin'], FILTER_VALIDATE_URL)) { ?>
                <li><a href="<?php echo $social_links['linkedin']; ?>" target="_blank" title="Follow Onirim on LinkedIn"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-linkedin.svg" alt="LinkedIn"></a></li>
            <?php } ?>
            <?php if(isset($social_links['instagram']) && filter_var($social_links['instagram'], FILTER_VALIDATE_URL)) { ?>
                <li><a href="<?php echo $social_links['instagram']; ?>" target="_blank" title="Follow Onirim on Instagram"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram.svg" alt="Instagram"></a></li>
            <?php } ?>
        </ul>

        <?php
        if( sizeof($agences) > 0) {

            // loop through the rows of data
            foreach($agences as $agence) {

                $heure =  date("H:i", strtotime('now '.(($agence['decalage_horaire'] >= 0) ? '+' : '').$agence['decalage_horaire'].' Hour'));
                $decalage_horaire = get_sub_field('decalage_horaire');
                $left = get_sub_field('left');
                $right = get_sub_field('right');

                ?>

                <div class="office">
                    <div class="office-title"><?php echo $agence['titre']; ?> <span class="separator"><div></div></span> <?php echo $heure; ?></div>
                    <div class="col col-1">
                        <div class="address"><?php echo $agence['left']['coordonnees']; ?></div>
                        <div class="phone"><?php echo $agence['left']['telephones']; ?></div>
                        <div class="links">
                            <?php if(isset($agence['left']['email']) && !empty($agence['left']['email'])) { ?>
                                <span class="contact"><a href="mailto:<?php echo $agence['left']['email']; ?>" class="button black center"><img
                                            src="<?php echo esc_url(get_template_directory_uri()); ?>/images/picto-address.gif"
                                            alt="Contact us"> <span>Contact us</span></a></span>
                                <span class="separator"><div></div></span>
                            <?php } ?>
                            <?php if(isset($agence['left']['google_map']) && !empty($agence['left']['google_map'])) { ?>
                                <span class="map"><a href="<?php echo $agence['left']['google_map']; ?>" target="_blank" class="button black center"><img
                                            src="<?php echo esc_url(get_template_directory_uri()); ?>/images/picto-map.gif"
                                            alt="View map"> View map</a></span>
                            <?php } ?>
                        </div>
                    </div><!--
        -->
                    <?php

                    $col_left = [];
                    $col_right = [];
                    if( sizeof($agence['right']['contacts']) > 0) {
                        $colonnes = array_chunk($agence['right']['contacts'], ceil(sizeof($agence['right']['contacts']) / 2));
//                        var_dump($agence['right']['contacts']);
                        $col_left = $colonnes[0];
                        $col_right = $colonnes[1];
                    }

                    ?>
                    <div class="col col-2">
                        <div class="left">
                            <?php foreach($colonnes[0] as $contact) { ?>
                                <div class="person">
                                    <div class="title"><?php echo $contact['fonction']; ?></div>
                                    <div class="name"><?php echo $contact['nom']; ?></div>
                                    <div class="email"><a href="mailto:<?php echo $contact['email']; ?>" class="button black center"><?php echo $contact['email']; ?></a></div>
                                </div>
                            <?php } ?>
                        </div><!--

            -->
                        <div class="right">
                            <?php foreach($colonnes[1] as $contact) { ?>
                                <div class="person">
                                    <div class="title"><?php echo $contact['fonction']; ?></div>
                                    <div class="name"><?php echo $contact['nom']; ?></div>
                                    <div class="email"><a href="mailto:<?php echo $contact['email']; ?>" class="button black center"><?php echo $contact['email']; ?></a></div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php

            } // foreach $agences

        } // if sizeof $agences > 0
        ?>

        <!-- INSTAGRAM -->
        <div class="instagram">            
            <?php include(locate_template('views/partials/instagram.php')); ?>
        </div>

        <div class="footer">
            <ul class="menu">
                <?php $id_page_legals = get_field('page_legals', 'onirim_options'); ?>
                <li><a href="<?php echo get_permalink($id_page_legals); ?>">LEGAL NOTICE</a></li>
                <!-- <li><a href="">PRIVACY POLICY</a></li> -->
                <li><a href="https://westonmills.com/" target="_blank">CREDITS</a></li>
            </ul>
            <div class="copyright">© <?php echo date('Y'); ?> <span>ONIRIM</span> - ALL RIGHTS RESERVED</div>
        </div>

    </div>

<?php get_footer(); ?>