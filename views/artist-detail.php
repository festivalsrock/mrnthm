<?php

$type_page = 'artist-detail';

$term = get_queried_object();
$gabarits = get_field('gabarits_artiste', $term);
$artiste_name = $term->name;
$artiste_header = get_field('artiste_header', $term);
$artiste_image = get_field('image', $term);
$artiste_image_lazy = 'http://spacergif.org/spacer.gif';
if(isset($artiste_image['sizes']['lazy'])) {
    $artiste_image_lazy = $artiste_image['sizes']['lazy'];
}
$artiste_description = get_field('description', $term);
$artiste_categories = get_field('categorie_artiste', $term);
$artiste_social_links = get_field('social_links', $term);
$artiste_social_links = $artiste_social_links['social_links'];

// --- HEADER
// Video
$header_video_id = intval($artiste_header['video']);
if($header_video_id > 0) {
    $header_video_data = onirim_get_vimeo($header_video_id);
    $header_video_url = $header_video_data['url'];
    $header_video_url_mp4 = $header_video_data['url_mp4'];
    $header_video_picture = $header_video_data['picture'];
    $header_video_ratio = $header_video_data['ratio'];
}

// Image
$header_image_type = $artiste_header['image']['type']; // video / image

$header_image_video_iframe = $artiste_header['image']['video'];

$header_image_alt = $artiste_header['image']['alt'];
$header_image_desktop = $artiste_header['image']['url'];
$header_image_desktop_width = $artiste_header['image']['width'];
$header_image_desktop_height = $artiste_header['image']['height'];

$header_image_mobile = $artiste_header['image']['sizes']['mobile'];
$header_image_mobile_width = $artiste_header['image']['sizes']['mobile-width'];
$header_image_mobile_height = $artiste_header['image']['sizes']['mobile-height'];

$header_image_mobile_small = $artiste_header['image']['sizes']['mobile-small'];
$header_image_mobile_small_width = $artiste_header['image']['sizes']['mobile-small-width'];
$header_image_mobile_small_height = $artiste_header['image']['sizes']['mobile-small-height'];

// ---FIN HEADER

$footer_projets = array();
foreach($gabarits as $gabarit) {
    $projet = get_field('projet', $gabarit->ID);
    $categories_gabarit = get_field('categorie_gabarit', $gabarit->ID);
    if(isset($projet->name)) {
        if(isset($categories_gabarit) && is_array($categories_gabarit)) {
            foreach($categories_gabarit as $categorie_gabarit) {
                if(in_array($categorie_gabarit, $artiste_categories)) {
                    $footer_projets[$categorie_gabarit->name][] = $projet->name;
                    $footer_projets[$categorie_gabarit->name] = array_unique($footer_projets[$categorie_gabarit->name]);
                }
            }
        }


    }
}
//Ordre catégories artiste
$tmp = array();
foreach($artiste_categories as $cat) {
    if(isset($footer_projets[$cat->name])) $tmp[$cat->name] = $footer_projets[$cat->name];
}

$footer_projets = $tmp;


// Filters
$id_page_artist = get_field('page_artist', 'onirim_options');
$artistes_col_1 = get_field('artistes_col_1', $id_page_artist);
$artistes_col_2 = get_field('artistes_col_2', $id_page_artist);
$artistes_filtres = get_field('filtres', $id_page_artist);



//exit;

get_header();
?>

    <div class="template-artist-details">

        <div class="dropdown">            
            <!-- LISTE DES ARTISTES -->
            <div class="dropdown-label"><span></span><?php echo $artiste_name ?></div>
            <div class="template-artist-list dropdown-list">    
    
                <!-- MENU -->
                <div class="menu-artist">
                    <ul class="col col-1">
                        <?php
                        foreach($artistes_col_1 as $artiste) :
                            include(locate_template('views/partials/artiste-name.php'));
                        endforeach;
                        ?>
                    </ul>
                    <ul class="col col-2">
                        <?php
                        foreach($artistes_col_2 as $artiste) :
                            include(locate_template('views/partials/artiste-name.php'));
                        endforeach;
                        ?>
                    </ul>
                </div>
                        
                <!-- FILTRES -->
                <div class="menu-filter">
                    <?php foreach($artistes_filtres as $filtre) : ?><!--
                    --><input type="submit" id="<?php echo $filtre->slug; ?>" name="<?php echo $filtre->name; ?>" value="<?php echo $filtre->name; ?>"><!--
                    <?php endforeach; ?>
                --></div>

            </div>
    
        </div>


        <div class="border">
            <?php if(!ONIRIM_IS_MOBILE) { ?> 
            <div id="artist-cover" class="cover" style="<?php if($header_image_desktop != '') { ?>background-image:url(<?php echo $header_image_desktop; ?><?php } ?>">

                <?php if(isset($header_video_url) && $header_video_url != '') { ?>
                    <video class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9 video-cover js-artist-video-header" loop playsinline autoplay webkit-playsinline muted preload="auto" poster="<?php echo $header_video_picture ?>" src="<?php echo $header_video_url_mp4; ?>">
                        <source src="<?php echo $header_video_url_mp4; ?>" type='video/mp4' />
                        <!-- <source src="<?php echo $header_video_url; ?>" type='application/x-mpegURL' /> -->
                    </video>
                <?php } ?>
                <h1 class="cover-title"><?php echo $artiste_name; ?></h1>
                <div class="cover-scroll">SCROLL TO EXPLORE</div>
                <div class="cover-line"></div>
            </div>
            <?php } ?>
        </div>

        <?php
        $prec_projet = 0;
        $cpt = 0;
        foreach($gabarits as $k => $gabarit) {
            $type_gabarit = intval(get_field('type_gabarit', $gabarit->ID));
            if($type_gabarit > 0) {
                $projet = get_field('projet', $gabarit->ID);
                $current_projet = intval($projet->term_id);
                if($cpt > 0 && ($prec_projet != $current_projet || $prec_projet == 0)) echo '<div class="gabarit-separator"></div>';
                $prec_projet = $current_projet;
                echo '<span class="goto-gabarit" id="goto-'.$gabarit->ID.'"></span>';
                include(locate_template('views/partials/gabarit-'.$type_gabarit.'.php'));
                $cpt ++;
            }
        }
        ?>


        <!--
            
            INFOS

         -->
        <div class="infos">
            <div class="infos-photo">
                <?php if(isset($artiste_image['url']) && filter_var($artiste_image['url'], FILTER_VALIDATE_URL)) { ?>
                    <img class="lazy" src="<?php echo $artiste_image_lazy; ?>" data-src="<?php echo $artiste_image['url']; ?>" alt="<?php echo $artiste_image['alt']; ?>">
                <?php } ?>
            </div>
            <div class="infos-description">
                <?php if(!empty($artiste_description)) { ?>
                <div class="infos-description-title">Biography</div>
                <div class="infos-description-resume"><?php echo $artiste_description; ?></div>
                <?php } ?>
                <div class="infos-description-projects">
                    <?php $i = 0; foreach($footer_projets as $cat => $projets) { ?>
                    <div class="<?php echo ($i == 0) ? 'left' : (($i == 1) ? 'center' : 'right'); ?>">
                        <div class="title"><?php echo $cat; ?></div>
                        <ul class="links">
                            <?php foreach($projets as $projet) { ?>
                                <li><?php echo $projet; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php $i++; if($i > 2) break; } ?>
                </div>
                <ul class="social">
                    <?php if(isset($artiste_social_links['facebook']) && filter_var($artiste_social_links['facebook'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['facebook']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on Facebook"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook-white.svg" alt="Facebook"></a></li>
                    <?php } ?>
                    <?php if(isset($artiste_social_links['twitter']) && filter_var($artiste_social_links['twitter'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['twitter']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on Twitter"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-twitter-white.svg" alt="Twitter"></a></li>
                    <?php } ?>
                    <?php if(isset($artiste_social_links['linkedin']) && filter_var($artiste_social_links['linkedin'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['linkedin']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on LinkedIn"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-linkedin-white.svg" alt="LinkedIn"></a></li>
                    <?php } ?>
                    <?php if(isset($artiste_social_links['instagram']) && filter_var($artiste_social_links['instagram'], FILTER_VALIDATE_URL)) { ?>
                        <li><a href="<?php echo $artiste_social_links['instagram']; ?>" target="_blank" title="Follow <?php echo $artiste_name; ?> on Instagram"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram-white.svg" alt="Instagram"></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

<?php
get_footer();
