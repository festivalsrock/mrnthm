<?php
/**
 * Template Name: Homepage
 */
//die(onirim_get_playlist());
get_header(); ?>

    <div class="template-homepage">
        <div class="swiper-container js-home-swiper">
            <div class="swiper-wrapper">
                <?php
                $gabarits = get_field('gabarits');

                $type_page = 'homepage';

                foreach($gabarits as $gabarit) {
                    $type_gabarit = intval(get_field('type_gabarit', $gabarit->ID));
                    $types_gabarits_forbidden = [10];
                    if($type_gabarit > 0 && !in_array($type_gabarit, $types_gabarits_forbidden)) {
                    ?>
                    <div class="swiper-slide" data-hash="<?php echo $gabarit->ID ?>">
                        <div class="center-flex"><?php if($type_gabarit > 0) include(locate_template('views/partials/gabarit-'.$type_gabarit.'.php')); ?></div>
                    </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>

        <div class="swiper-custom-arrows js-home-swiper-custom-arrows">
            <div class="swiper-button-prev swiper-button-prev-custom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-arrow-left.svg" alt=""></div>
            <div class="swiper-button-next swiper-button-next-custom"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-arrow-right.svg" alt=""></div> 
        </div>

    </div>

<?php get_footer(); ?>