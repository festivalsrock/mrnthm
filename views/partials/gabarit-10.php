<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_10', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit['medias']);
//echo '</pre>';

if( $gabarit ):

    $position = $gabarit['position']; // left / right

    // Bordure
    $border_width = intval($gabarit['border_width']);
    $border_color = $gabarit['border_color'];
    if(empty($border_color)) $border_color = '#ffffff';
    
    // $border_width = 10;
    // $border_color = '#ff00ff';

    // Médias
    $medias = $gabarit['medias'];

    ?>

    <!--

        GABARIT 10 (ex 5 with slider)

        -->
<?php if($position == 'left') { ?>

    <!-- GABARIT 5.0
        Slider à gauche
    -->
    <div class="gabarit gabarit-5 gabarit-5-0 clearfix" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="swiper-outside">
            <div class="swiper-container multiple-swiper-<?php echo $gabarit_id; ?>"><!-- Ici donner un nom unique à multiple-swiper-1 -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <?php foreach($medias as $media) :

                        $media = $media['media'];

                        $image_alt = $media['image']['alt'];

                        $image_desktop = $media['image']['url'];
                        $image_desktop_width = $media['image']['width'];
                        $image_desktop_height = $media['image']['height'];

                        $image_mobile = $media['image']['sizes']['mobile'];
                        $image_mobile_width = $media['image']['sizes']['mobile-width'];
                        $image_mobile_height = $media['image']['sizes']['mobile-height'];

                        $image_mobile_small = $media['image']['sizes']['mobile-small'];
                        $image_mobile_small_width = $media['image']['sizes']['mobile-small-width'];
                        $image_mobile_small_height = $media['image']['sizes']['mobile-small-height'];
                        
                        $image_lazy = $media['image']['sizes']['lazy'];
                        ?>
                        <div class="swiper-slide" style="border-left:<?php echo $border_width ?>px solid <?php echo $border_color ?>"><img class="lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>"></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="swiper-custom-arrows swiper-custom-arrows-<?php echo $gabarit_id; ?>"><!-- Ici donner un nom unique à swiper-custom-arrows-1 -->
                <div class="swiper-button-prev swiper-button-prev-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                        <polyline fill="none" stroke="#494949" stroke-linecap="square" stroke-miterlimit="10" points="11.354,19.188 5.167,13 11.354,6.813 "/>
                        <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="5" y1="13" x2="20" y2="13"/></svg></div>
                <div class="swiper-button-next swiper-button-next-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                        <polyline fill="none" stroke="#505050" stroke-linecap="square" stroke-miterlimit="10" points="13.146,6.813 19.334,13 13.146,19.188 "/>
                        <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="19.5" y1="13" x2="4.5" y2="13"/></svg></div>
            </div>
        </div>

        <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
        <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
        <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        <div style="clear: both;"></div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<?php if($position == 'right') { ?>
    <!-- GABARIT 5.1
        Slider à droite
    -->
    <div class="gabarit gabarit-5 gabarit-5-1 clearfix" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="swiper-outside">
            <div class="swiper-container multiple-swiper-<?php echo $gabarit_id; ?>"><!-- Ici donner un nom unique à multiple-swiper-2 -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <?php foreach($medias as $media) :

                        $media = $media['media'];

                        $image_alt = $media['image']['alt'];

                        $image_desktop = $media['image']['url'];
                        $image_desktop_width = $media['image']['width'];
                        $image_desktop_height = $media['image']['height'];

                        $image_mobile = $media['image']['sizes']['mobile'];
                        $image_mobile_width = $media['image']['sizes']['mobile-width'];
                        $image_mobile_height = $media['image']['sizes']['mobile-height'];

                        $image_mobile_small = $media['image']['sizes']['mobile-small'];
                        $image_mobile_small_width = $media['image']['sizes']['mobile-small-width'];
                        $image_mobile_small_height = $media['image']['sizes']['mobile-small-height'];
                        
                        $image_lazy = $media['image']['sizes']['lazy'];
                        ?>
                        <div class="swiper-slide" style="border-right:<?php echo $border_width ?>px solid <?php echo $border_color ?>"><img class="lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>"></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="swiper-custom-arrows swiper-custom-arrows-<?php echo $gabarit_id; ?>"><!-- Ici donner un nom unique à swiper-custom-arrows-2 -->
                <div class="swiper-button-prev swiper-button-prev-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                        <polyline fill="none" stroke="#494949" stroke-linecap="square" stroke-miterlimit="10" points="11.354,19.188 5.167,13 11.354,6.813 "/>
                        <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="5" y1="13" x2="20" y2="13"/></svg></div>
                <div class="swiper-button-next swiper-button-next-custom"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
                        <polyline fill="none" stroke="#505050" stroke-linecap="square" stroke-miterlimit="10" points="13.146,6.813 19.334,13 13.146,19.188 "/>
                        <line fill="none" stroke="#494949" stroke-miterlimit="10" x1="19.5" y1="13" x2="4.5" y2="13"/></svg></div>
            </div>
        </div>

        <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
        <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
        <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        <div style="clear: both;"></div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
<?php } ?>

<?php endif; ?>
