<div class="artist-list-cover <?php echo $artiste->slug; ?>">
    <?php 
    $gabarit = get_field('gabarit_cover', $artiste);
    $type_gabarit = intval(get_field('type_gabarit', $gabarit->ID));

    // Si gabarit en cover défini
    if($type_gabarit > 0) {
        include(locate_template('views/partials/gabarit-'.$type_gabarit.'.php'));

    // Si aucun gabarit en cover défini, alors on prend le premier de la liste
    }else {
        $gabarits = get_field('gabarits_artiste', $artiste);
        if(isset($gabarits[0])) {
            $gabarit = $gabarits[0];
            $type_gabarit = intval(get_field('type_gabarit', $gabarit->ID));

            if($type_gabarit > 0) {
                include(locate_template('views/partials/gabarit-' . $type_gabarit . '.php'));
            }
        }
    }
    ?>
</div>
