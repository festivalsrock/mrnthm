<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_7', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit);
//echo '</pre>';

if( $gabarit ):

    // Image
    $position_image = $gabarit['position_image']; // full-width / with-padding

    $image_alt = $gabarit['image']['alt'];
    $image_desktop = $gabarit['image']['url'];
    $image_desktop_width = $gabarit['image']['width'];
    $image_desktop_height = $gabarit['image']['height'];

    $image_mobile = $gabarit['image']['sizes']['mobile'];
    $image_mobile_width = $gabarit['image']['sizes']['mobile-width'];
    $image_mobile_height = $gabarit['image']['sizes']['mobile-height'];

    $image_mobile_small = $gabarit['image']['sizes']['mobile-small'];
    $image_mobile_small_width = $gabarit['image']['sizes']['mobile-small-width'];
    $image_mobile_small_height = $gabarit['image']['sizes']['mobile-small-height'];

    $image_lazy = $gabarit['image']['sizes']['lazy'];
    $image_lazy_width = $gabarit['image']['sizes']['lazy-width'];
    $image_lazy_height = $gabarit['image']['sizes']['lazy-height'];


    ?>


    <!--

        GABARIT 7

        -->

    <?php if($position_image == 'with-padding') { ?>


<!-- GABARIT 7.0
    1 image horizontale - moyenne largeur
-->
<div class="gabarit gabarit-7 gabarit-7-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
    <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
    <div class="js-center-height">
        <div class="center">
            <div class="big">
                <div class="image-big"><img class="lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>"></div><!--
                        --><?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
            </div>
        </div>
    </div>
    <?php echo getProjectTitleHTML($titre_gabarit); ?>
</div>

<?php } ?>

    <?php if($position_image == 'full-width') { ?>
<!-- GABARIT 7.1
    1 image horizontale - grande largeur
-->
<div class="gabarit gabarit-7 gabarit-7-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
    <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
    <div class="js-center-height">
        <div class="center">
            <div class="big">
                <div class="image-big"><img class="lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>"></div><!--
                        --><?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
            </div>
        </div>
    </div>
    <?php echo getProjectTitleHTML($titre_gabarit); ?>
</div>


<?php } ?>

<?php endif; // if gabarit ?>