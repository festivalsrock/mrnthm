<?php
    global $wp;
    $share_url = home_url(add_query_arg(array(),$wp->request));
    $share_url .= '?gid='.$gabarit_id;

    $share_url = urlencode($share_url);
?>
<div class="related">
    <div class="related-item related-item-playlist js-add-playlist">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-add.svg" alt="">
        <span>Add to playlist</span>
    </div>
    <div class="related-item related-item-playlist js-remove-playlist">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-remove.svg" alt="">
        <span>Remove from playlist</span>
    </div>
    <div class="related-item related-item-separator"><div></div></div>
    <div class="related-item related-item-social js-share">
        <div class="links">
            <div class="links-mask">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $share_url; ?>" target="_blank" rel="noopener"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook.svg" alt="Facebook"></a>
                <a href="https://twitter.com/intent/tweet/?url=<?php echo $share_url; ?>" target="_blank" rel="noopener"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-twitter.svg" alt="Twitter"></a>
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $share_url; ?>" target="_blank" rel="noopener"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-linkedin.svg" alt="LinkedIn"></a>
            </div>
            <div class="links-share">
                <img class="share" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-share.svg" alt="">
                <span>Share</span>
            </div>
        </div>
    </div>
</div>