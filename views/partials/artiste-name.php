<?php
$categories_artiste = get_field('categorie_artiste', $artiste);
$classes_artiste = [];
foreach($categories_artiste as $categorie_artiste) {
    $classes_artiste[] = $categorie_artiste->slug;    
}
// echo '<pre>';
// var_dump( $classes_artiste);
// echo '</pre>';

// $artiste->term_id   // unique
// $artiste->slug
?>
<li>
    <a href="<?php echo get_term_link($artiste); ?>" data-artist="<?php echo $artiste->slug; ?>" class="<?php echo implode(' ', $classes_artiste); ?> active">
        <?php echo $artiste->name; ?>
    </a>
</li>