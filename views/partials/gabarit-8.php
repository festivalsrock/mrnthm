<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_8', $gabarit_id);

//echo '<p.1

if( $gabarit ):

    $position = $gabarit['position']; // left / center / right
    // $position = 'left';
    $flex_position = $position == 'right' ? 'flex-end' : ($position == 'center' ? 'center' : 'flex-start');

    // Sélection image à afficher en version mobile - 1 (left) / 2  = (right)
    $image_mobile = 1;
    if($gabarit['image_mobile'] == '2') $image_mobile = '2';

    // Emplacement gauche
    $left_type = $gabarit['left']['type']; // video / image

    $left_video_id = $gabarit['left']['video'];
    if($type_page == 'artist-list' && intval($gabarit['left']['video_list_artist']) > 0) $left_video_id = $gabarit['left']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['left']['video_home']) > 0) $left_video_id = $gabarit['left']['video_home'];

    $left_video_data = onirim_get_vimeo($left_video_id);
    $left_video_url = $left_video_data['url_mp4'];
    $left_video_picture = $left_video_data['picture'];
    $left_video_ratio = $left_video_data['ratio'];
    $left_video_format = (abs(2.35 -$left_video_ratio) > abs(1.777777778 -$left_video_ratio)) ? '16/9' : '2.35';


    $left_image_alt = $gabarit['left']['image']['alt'];
    $left_image_desktop = $gabarit['left']['image']['url'];
    $left_image_desktop_width = $gabarit['left']['image']['width'];
    $left_image_desktop_height = $gabarit['left']['image']['height'];

    $left_image_mobile = $gabarit['left']['image']['sizes']['mobile'];
    $left_image_mobile_width = $gabarit['left']['image']['sizes']['mobile-width'];
    $left_image_mobile_height = $gabarit['left']['image']['sizes']['mobile-height'];

    $left_image_mobile_small = $gabarit['left']['image']['sizes']['mobile-small'];
    $left_image_mobile_small_width = $gabarit['left']['image']['sizes']['mobile-small-width'];
    $left_image_mobile_small_height = $gabarit['left']['image']['sizes']['mobile-small-height'];

    $left_image_lazy = $gabarit['left']['image']['sizes']['lazy'];
    $left_image_lazy_width = $gabarit['left']['image']['sizes']['lazy-width'];
    $left_image_lazy_height = $gabarit['left']['image']['sizes']['lazy-height'];

    // Emplacement droite

    $right_type = $gabarit['right']['type']; // video / image

    $right_video_id = $gabarit['right']['video'];
    if($type_page == 'artist-list' && intval($gabarit['right']['video_list_artist']) > 0) $right_video_id = $gabarit['right']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['right']['video_home']) > 0) $right_video_id = $gabarit['right']['video_home'];

    $right_video_data = onirim_get_vimeo($right_video_id);
    $right_video_url = $right_video_data['url_mp4'];
    $right_video_picture = $right_video_data['picture'];
    $right_video_ratio = $right_video_data['ratio'];
    $right_video_format = (abs(2.35 -$right_video_ratio) > abs(1.777777778 -$right_video_ratio)) ? '16/9' : '2.35';
    
    $right_image_alt = $gabarit['right']['image']['alt'];
    $right_image_desktop = $gabarit['right']['image']['url'];
    $right_image_desktop_width = $gabarit['right']['image']['width'];
    $right_image_desktop_height = $gabarit['right']['image']['height'];

    $right_image_mobile = $gabarit['right']['image']['sizes']['mobile'];
    $right_image_mobile_width = $gabarit['right']['image']['sizes']['mobile-width'];
    $right_image_mobile_height = $gabarit['right']['image']['sizes']['mobile-height'];

    $right_image_mobile_small = $gabarit['right']['image']['sizes']['mobile-small'];
    $right_image_mobile_small_width = $gabarit['right']['image']['sizes']['mobile-small-width'];
    $right_image_mobile_small_height = $gabarit['right']['image']['sizes']['mobile-small-height'];

    $right_image_lazy = $gabarit['right']['image']['sizes']['lazy'];
    $right_image_lazy_width = $gabarit['right']['image']['sizes']['lazy-width'];
    $right_image_lazy_height = $gabarit['right']['image']['sizes']['lazy-height'];



    ?>


    <!--

        GABARIT 8

        -->

    <?php if($position == 'center') { ?>
    <!-- GABARIT 8.0
        1 grande Horizontal à gauche et 1 vertical à droite, CENTRE
    -->
    <div class="gabarit gabarit-8 gabarit-8-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="big">
                <div class="image-big" style="display: flex; flex-direction:row;justify-content:<?php echo $flex_position ?>">
                    <?php if(!ONIRIM_IS_MOBILE || $image_mobile == 1) { ?>
                    <div class="image-big-left"><?php echo getImageOrVideo($left_type, $left_image_desktop, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture, $left_image_lazy); ?></div><!--
                    --><?php } 
                    if(!ONIRIM_IS_MOBILE || $image_mobile == 2) {  ?><div class="image-big-right"><?php echo getImageOrVideo($right_type, $right_image_desktop, $right_image_alt, $right_video_url, $right_video_url, $right_video_picture, $right_image_lazy); ?><?php include(locate_template('views/partials/gabarit-title-project.php')); ?></div><?php } ?>
                </div>
            </div>
            <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
            <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

    <?php } ?>

    <?php if($position == 'left') { ?>
    <!-- GABARIT 8.1
        1 grande Horizontal à gauche et 1 vertical à droite, GAUCHE
    -->
    <div class="gabarit gabarit-8 gabarit-8-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="big">
                <div class="image-big" style="display: flex; flex-direction:row;justify-content:<?php echo $flex_position ?>">
                    <?php if(!ONIRIM_IS_MOBILE || $image_mobile == 1) { ?>
                    <div class="image-big-left"><?php echo getImageOrVideo($left_type, $left_image_desktop, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture, $left_image_lazy); ?></div><!--
                    --><?php } 
                    if(!ONIRIM_IS_MOBILE || $image_mobile == 2) { ?><div class="image-big-right"><?php echo getImageOrVideo($right_type, $right_image_desktop, $right_image_alt, $right_video_url, $right_video_url, $right_video_picture, $right_image_lazy); ?>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?></div><?php } ?>
                </div>
            </div>
            <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
            <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

    <?php if($position == 'right') { ?>
    <!-- GABARIT 8.2
        1 grande Horizontal à gauche et 1 vertical à droite, DROITE
    -->
    <div class="gabarit gabarit-8 gabarit-8-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="big">
                <div class="image-big" style="justify-content:<?php echo $flex_position ?>">
                    <?php if(!ONIRIM_IS_MOBILE || $image_mobile == 1) { ?>
                    <div class="image-big-left"><?php echo getImageOrVideo($left_type, $left_image_desktop, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture, $left_image_lazy); ?></div><!--
                    --><?php } 
                    if(!ONIRIM_IS_MOBILE || $image_mobile == 2) { ?><div class="image-big-right"><?php echo getImageOrVideo($right_type, $right_image_desktop, $right_image_alt, $right_video_url, $right_video_url, $right_video_picture, $right_image_lazy); ?>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?></div><?php } ?>
                </div>
            </div>
            <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
            <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<?php endif; // if gabarit ?>