<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_4', $gabarit_id);

//echo '<pre>';
//var_dump($artiste->name);
//echo '</pre>';


if( $gabarit ):

    // Vignette
    $position_vignette = $gabarit['position_vignette']; // left / right
    $ferrage_vignette = $gabarit['ferrage_vignette']; // 1/2 / 1/4
    $class_quarter = '';
    if($ferrage_vignette == '1/4') $class_quarter = 'quarter';

    $vignette_type = $gabarit['vignette']['type']; // video / image

    $vignette_video_id = $gabarit['vignette']['video'];
    if($type_page == 'artist-list' && intval($gabarit['vignette']['video_list_artist']) > 0) $vignette_video_id = $gabarit['vignette']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['vignette']['video_home']) > 0) $vignette_video_id = $gabarit['vignette']['video_home'];

    $vignette_video_data = onirim_get_vimeo($vignette_video_id);
    $vignette_video_url = $vignette_video_data['url_mp4'];
    $vignette_video_picture = $vignette_video_data['picture'];
    $vignette_video_ratio = $vignette_video_data['ratio'];
    $vignette_video_format = (abs(2.35 -$vignette_video_ratio) > abs(1.777777778 -$vignette_video_ratio)) ? '16/9' : '2.35';

    $vignette_image_alt = $gabarit['vignette']['image']['alt'];
    $vignette_image_desktop = $gabarit['vignette']['image']['url'];
    $vignette_image_desktop_width = $gabarit['vignette']['image']['width'];
    $vignette_image_desktop_height = $gabarit['vignette']['image']['height'];

    $vignette_image_mobile = $gabarit['vignette']['image']['sizes']['mobile'];
    $vignette_image_mobile_width = $gabarit['vignette']['image']['sizes']['mobile-width'];
    $vignette_image_mobile_height = $gabarit['vignette']['image']['sizes']['mobile-height'];

    $vignette_image_mobile_small = $gabarit['vignette']['image']['sizes']['mobile-small'];
    $vignette_image_mobile_small_width = $gabarit['vignette']['image']['sizes']['mobile-small-width'];
    $vignette_image_mobile_small_height = $gabarit['vignette']['image']['sizes']['mobile-small-height'];
    
    $vignette_image_lazy = $gabarit['vignette']['image']['sizes']['lazy'];

    $vignette_image_lazy = $gabarit['vignette']['image']['sizes']['lazy'];
    $vignette_image_lazy_width = $gabarit['vignette']['image']['sizes']['lazy-width'];
    $vignette_image_lazy_height = $gabarit['vignette']['image']['sizes']['lazy-height'];

    // Grande image de fond
    $background_image_alt = $gabarit['background_image']['alt'];
    $background_image_desktop = $gabarit['background_image']['url'];
    $background_image_desktop_width = $gabarit['background_image']['width'];
    $background_image_desktop_height = $gabarit['background_image']['height'];

    $background_image_mobile = $gabarit['background_image']['sizes']['mobile'];
    $background_image_mobile_width = $gabarit['background_image']['sizes']['mobile-width'];
    $background_image_mobile_height = $gabarit['background_image']['sizes']['mobile-height'];

    $background_image_mobile_small = $gabarit['background_image']['sizes']['mobile-small'];
    $background_image_mobile_small_width = $gabarit['background_image']['sizes']['mobile-small-width'];
    $background_image_mobile_small_height = $gabarit['background_image']['sizes']['mobile-small-height'];

    $background_image_lazy = $gabarit['background_image']['image']['sizes']['lazy'];
    $background_image_lazy_width = $gabarit['background_image']['image']['sizes']['lazy-width'];
    $background_image_lazy_height = $gabarit['background_image']['image']['sizes']['lazy-height'];


?>


<!--

    GABARIT 4

 -->
<?php if($position_vignette == 'left') { ?>
<!-- GABARIT 4.0
    Grosse image à gauche, petite image à gauche et position 1/2 | 1/4
    Ajouter 'quarter' à la class pour les positions alternatives de la petite image.
    quarter = 25%
    sinon = 50%
-->
<div class="gabarit gabarit-4 gabarit-4-0 <?php echo $class_quarter; ?>" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
    <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
    <div class="js-center-height">
        <div class="big">
            <div class="image-big">
                <img class="lazy" src="<?php echo $background_image_lazy; ?>" data-src="<?php echo $background_image_desktop; ?>" alt="<?php echo $background_image_alt; ?>">
            </div>
            <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
            <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        </div>
         
        <?php if(!ONIRIM_IS_MOBILE) { ?> 
            <div class="small">
                <div class="image-small"><?php echo getImageOrVideo($vignette_type, $vignette_image_desktop, $vignette_image_alt, $vignette_video_url, $vignette_video_url, $vignette_video_picture, $vignette_image_lazy); ?></div>
                <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
            </div>
        <?php } ?>
        <div style="clear: both;"></div>        
    </div>
    <?php echo getProjectTitleHTML($titre_gabarit); ?>
</div>
<?php } ?>

<?php if($position_vignette == 'right') { ?>
<!-- GABARIT 4.1
    Grosse image à droite, petite image à droite et position 1/2 | 1/4 -->
<div class="gabarit gabarit-4 gabarit-4-1 <?php echo $class_quarter; ?>" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
    <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
    <div class="js-center-height">
        <div class="all">
            <div class="image-big">
                <img class="lazy" src="<?php echo $background_image_lazy; ?>" data-src="<?php echo $background_image_desktop; ?>" alt="<?php echo $background_image_alt; ?>">
            </div>
            <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
            <?php include(locate_template('views/partials/gabarit-related.php')); ?>
            
            <?php if(!ONIRIM_IS_MOBILE) { ?> 
                <div class="image-small"><?php echo getImageOrVideo($vignette_type, $vignette_image_desktop, $vignette_image_alt, $vignette_video_url, $vignette_video_url, $vignette_video_picture, $vignette_image_lazy); ?></div>
            <?php } ?>
        </div>
        <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
        <div style="clear: both;"></div>
    </div>
    <?php echo getProjectTitleHTML($titre_gabarit); ?>
</div>
<?php } ?>


<?php endif; // if gabarit ?>