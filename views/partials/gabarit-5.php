<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_5', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit);
//echo '</pre>';

if( $gabarit ):

    $position = $gabarit['position'];
    $flex_position = $position == 'right' ? 'flex-end' : ($position == 'center' ? 'center' : 'flex-start');
    $alignement_vertical = $gabarit['alignement_vertical'];

    // Bordure
    $border_width = intval($gabarit['border_width']);
    $border_color = $gabarit['border_color'];
    if(empty($border_color)) $border_color = '#ffffff';

    // Sélection image à afficher en version mobile - 1 / 2
    $image_mobile = 1;
    if($gabarit['image_mobile'] == '2') $image_mobile = '2';

    // Emplacement 1

    $media_1_image_alt = $gabarit['media_1']['image']['alt'];
    $media_1_image_desktop = $gabarit['media_1']['image']['url'];
    $media_1_image_desktop_width = $gabarit['media_1']['image']['width'];
    $media_1_image_desktop_height = $gabarit['media_1']['image']['height'];

    $media_1_image_mobile = $gabarit['media_1']['image']['sizes']['mobile'];
    $media_1_image_mobile_width = $gabarit['media_1']['image']['sizes']['mobile-width'];
    $media_1_image_mobile_height = $gabarit['media_1']['image']['sizes']['mobile-height'];

    $media_1_image_mobile_small = $gabarit['media_1']['image']['sizes']['mobile-small'];
    $media_1_image_mobile_small_width = $gabarit['media_1']['image']['sizes']['mobile-small-width'];
    $media_1_image_mobile_small_height = $gabarit['media_1']['image']['sizes']['mobile-small-height'];
    
    $media_1_image_lazy = $gabarit['media_1']['image']['sizes']['lazy'];

    $media_1_image_lazy = $gabarit['media_1']['image']['sizes']['lazy'];
    $media_1_image_lazy_width = $gabarit['media_1']['image']['sizes']['lazy-width'];
    $media_1_image_lazy_height = $gabarit['media_1']['image']['sizes']['lazy-height'];

    // Emplacement 2

    $media_2_image_alt = $gabarit['media_2']['image']['alt'];
    $media_2_image_desktop = $gabarit['media_2']['image']['url'];
    $media_2_image_desktop_width = $gabarit['media_2']['image']['width'];
    $media_2_image_desktop_height = $gabarit['media_2']['image']['height'];

    $media_2_image_mobile = $gabarit['media_2']['image']['sizes']['mobile'];
    $media_2_image_mobile_width = $gabarit['media_2']['image']['sizes']['mobile-width'];
    $media_2_image_mobile_height = $gabarit['media_2']['image']['sizes']['mobile-height'];

    $media_2_image_mobile_small = $gabarit['media_2']['image']['sizes']['mobile-small'];
    $media_2_image_mobile_small_width = $gabarit['media_2']['image']['sizes']['mobile-small-width'];
    $media_2_image_mobile_small_height = $gabarit['media_2']['image']['sizes']['mobile-small-height'];

    $media_2_image_lazy = $gabarit['media_2']['image']['sizes']['lazy'];
    $media_2_image_lazy_width = $gabarit['media_2']['image']['sizes']['lazy-width'];
    $media_2_image_lazy_height = $gabarit['media_2']['image']['sizes']['lazy-height'];

    // Emplacement 3

    $media_3_image_alt = $gabarit['media_3']['image']['alt'];
    $media_3_image_desktop = $gabarit['media_3']['image']['url'];
    $media_3_image_desktop_width = $gabarit['media_3']['image']['width'];
    $media_3_image_desktop_height = $gabarit['media_3']['image']['height'];

    $media_3_image_mobile = $gabarit['media_3']['image']['sizes']['mobile'];
    $media_3_image_mobile_width = $gabarit['media_3']['image']['sizes']['mobile-width'];
    $media_3_image_mobile_height = $gabarit['media_3']['image']['sizes']['mobile-height'];

    $media_3_image_mobile_small = $gabarit['media_3']['image']['sizes']['mobile-small'];
    $media_3_image_mobile_small_width = $gabarit['media_3']['image']['sizes']['mobile-small-width'];
    $media_3_image_mobile_small_height = $gabarit['media_3']['image']['sizes']['mobile-small-height'];

    $media_3_image_lazy = $gabarit['media_3']['image']['sizes']['lazy'];
    $media_3_image_lazy_width = $gabarit['media_3']['image']['sizes']['lazy-width'];
    $media_3_image_lazy_height = $gabarit['media_3']['image']['sizes']['lazy-height'];



        ?>

    <!-- GABARIT 5.2 | 5.3 | 5.4
        Bloc à gauche : 'left' (default)  |  Bloc centré : 'center'  | Bloc à droite : 'right'
        Image ferrée en bas si class 'bottom', sinon ferrée en haut
    -->
    <div class="gabarit gabarit-5 gabarit-5-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <?php 

                // $border_width = 20;
                // $border_color = '#ff00ff';

                $border_to_calc = ($border_width * 2) / 3;
                $image_width = 100 / 3.00;

            ?>
            <div class="image-big <?php echo $alignement_vertical; ?> <?php echo $position; ?>"><!-- Ici ajouter ou enlever class 'bottom' ET 'left' 'right' ou 'center' -->
                <div style="display: flex; flex-direction:row;justify-content:<?php echo $flex_position ?>">
                    
                    <?php if(!ONIRIM_IS_MOBILE || $image_mobile == 1) { ?>
                        <div class="image-big-item" style="flex-basis:<?php echo $image_width ?>%"><img class="lazy" src="<?php echo $media_1_image_lazy; ?>" data-src="<?php echo $media_1_image_mobile; ?>" alt="<?php echo $media_1_image_alt; ?>"></div><!--

                        --><div class="image-big-border" style="width:<?php echo $border_width ?>px; background-color:<?php echo $border_color ?>;"></div>
                    <?php } 
                    
                    if(!ONIRIM_IS_MOBILE || $image_mobile == 2) { ?><!--

                    --><div class="image-big-item" style="flex-basis:<?php echo $image_width ?>%"><img class="lazy" src="<?php echo $media_2_image_lazy; ?>" data-src="<?php echo $media_2_image_mobile; ?>" alt="<?php echo $media_2_image_alt; ?>"></div><!--

                    --><div class="image-big-border" style="width:<?php echo $border_width ?>px; background-color:<?php echo $border_color ?>;"></div><?php } ?><!--
                            
                    --><?php if(!ONIRIM_IS_MOBILE) { ?><div class="image-big-item" style="flex-basis:<?php echo $image_width ?>%"><img class="lazy" src="<?php echo $media_3_image_lazy; ?>" data-src="<?php echo $media_3_image_mobile; ?>" alt="<?php echo $media_3_image_alt; ?>"></div><?php } ?>
                </div>
                
                <div>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                </div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>



    <?php 
    // Those are other methods not working properly for the borders separator
    if(false) {?> 

    <div class="gabarit gabarit-5 gabarit-5-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="image-big <?php echo $alignement_vertical; ?> <?php echo $position; ?>"><!--    Ici ajouter ou enlever class 'bottom' ET 'left' 'right' ou 'center' -->
                <div class="image-big-item" style="width:<?php echo $image_width ?>%"><img class="lazy" src="<?php echo $media_1_image_lazy; ?>" data-src="<?php echo $media_1_image_mobile; ?>" alt="<?php echo $media_1_image_alt; ?>"></div><!--

                --><div class="image-big-item" style="width:<?php echo $image_width ?>%"><img class="lazy" src="<?php echo $media_2_image_lazy; ?>" data-src="<?php echo $media_2_image_mobile; ?>" alt="<?php echo $media_2_image_alt; ?>"></div><!--

                --><?php if(!ONIRIM_IS_MOBILE) { ?><div class="image-big-item" style="width:<?php echo $image_width ?>%"><img class="lazy" src="<?php echo $media_3_image_lazy; ?>" data-src="<?php echo $media_3_image_mobile; ?>" alt="<?php echo $media_3_image_alt; ?>"></div>
                <?php } ?>

                <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>


    
    <div class="gabarit gabarit-5 gabarit-5-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <?php 

                // $border_width = 1;
                // $border_color = '#ff00ff';

                $border_to_calc = ($border_width * 2) / 3;
                $image_width = 100 / 3.00;

            ?>
            <div class="image-big <?php echo $alignement_vertical; ?> <?php echo $position; ?>"><!-- Ici ajouter ou enlever class 'bottom' ET 'left' 'right' ou 'center' -->
                <div style="background-color:<?php echo $border_color ?>">
                    <div class="image-big-item" style="width:calc(<?php echo $image_width ?>% - <?php echo $border_to_calc ?>px)"><img class="lazy" src="<?php echo $media_1_image_lazy; ?>" data-src="<?php echo $media_1_image_mobile; ?>" alt="<?php echo $media_1_image_alt; ?>"></div><!--

                    --><div class="image-big-border" style="width:<?php echo $border_width ?>px; background-color:<?php echo $border_color ?>;"></div><!--

                    --><div class="image-big-item" style="width:calc(<?php echo $image_width ?>% - <?php echo $border_to_calc ?>px)"><img class="lazy" src="<?php echo $media_2_image_lazy; ?>" data-src="<?php echo $media_2_image_mobile; ?>" alt="<?php echo $media_2_image_alt; ?>"></div><!--

                    --><div class="image-big-border" style="width:<?php echo $border_width ?>px; background-color:<?php echo $border_color ?>;"></div><!--
                            
                    --><?php if(!ONIRIM_IS_MOBILE) { ?><div class="image-big-item" style="width:calc(<?php echo $image_width ?>% - <?php echo $border_to_calc ?>px)"><img class="lazy" src="<?php echo $media_3_image_lazy; ?>" data-src="<?php echo $media_3_image_mobile; ?>" alt="<?php echo $media_3_image_alt; ?>"></div><?php } ?>
                </div>
                
                <div>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                </div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <?php } ?>

<?php endif; // if gabarit ?>

