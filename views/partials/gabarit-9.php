<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_9', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit);
//echo '</pre>';

if( $gabarit ):

    $image_left_1 = $gabarit['left']['image_1'];
    $image_left_2 = $gabarit['left']['image_2'];

    $image_center_1 = $gabarit['center']['image_1'];
    $image_center_2 = $gabarit['center']['image_2'];

    $image_right_1 = $gabarit['right']['image_1'];
    $image_right_2 = $gabarit['right']['image_2'];

    // Image left 1
    $image_left_1_alt = $image_left_1['alt'];
    $image_left_1_desktop = $image_left_1['url'];
    $image_left_1_desktop_width = $image_left_1['width'];
    $image_left_1_desktop_height = $image_left_1['height'];

    $image_left_1_mobile = $image_left_1['sizes']['mobile'];
    $image_left_1_mobile_width = $image_left_1['sizes']['mobile-width'];
    $image_left_1_mobile_height = $image_left_1['sizes']['mobile-height'];

    $image_left_1_mobile_small = $image_left_1['sizes']['mobile-small'];
    $image_left_1_mobile_small_width = $image_left_1['sizes']['mobile-small-width'];
    $image_left_1_mobile_small_height = $image_left_1['sizes']['mobile-small-height'];

    $image_left_1_lazy = $image_left_1['sizes']['lazy'];
    $image_left_1_lazy_width = $image_left_1['sizes']['lazy-width'];
    $image_left_1_lazy_height = $image_left_1['sizes']['lazy-height'];

    // Image left 2
    $image_left_2_alt = $image_left_2['alt'];
    $image_left_2_desktop = $image_left_2['url'];
    $image_left_2_desktop_width = $image_left_2['width'];
    $image_left_2_desktop_height = $image_left_2['height'];

    $image_left_2_mobile = $image_left_2['sizes']['mobile'];
    $image_left_2_mobile_width = $image_left_2['sizes']['mobile-width'];
    $image_left_2_mobile_height = $image_left_2['sizes']['mobile-height'];

    $image_left_2_mobile_small = $image_left_2['sizes']['mobile-small'];
    $image_left_2_mobile_small_width = $image_left_2['sizes']['mobile-small-width'];
    $image_left_2_mobile_small_height = $image_left_2['sizes']['mobile-small-height'];

    $image_left_2_lazy = $image_left_2['sizes']['lazy'];
    $image_left_2_lazy_width = $image_left_2['sizes']['lazy-width'];
    $image_left_2_lazy_height = $image_left_2['sizes']['lazy-height'];

    // Image center 1
    $image_center_1_alt = $image_center_1['alt'];
    $image_center_1_desktop = $image_center_1['url'];
    $image_center_1_desktop_width = $image_center_1['width'];
    $image_center_1_desktop_height = $image_center_1['height'];

    $image_center_1_mobile = $image_center_1['sizes']['mobile'];
    $image_center_1_mobile_width = $image_center_1['sizes']['mobile-width'];
    $image_center_1_mobile_height = $image_center_1['sizes']['mobile-height'];

    $image_center_1_mobile_small = $image_center_1['sizes']['mobile-small'];
    $image_center_1_mobile_small_width = $image_center_1['sizes']['mobile-small-width'];
    $image_center_1_mobile_small_height = $image_center_1['sizes']['mobile-small-height'];

    $image_center_1_lazy = $image_center_1['sizes']['lazy'];
    $image_center_1_lazy_width = $image_center_1['sizes']['lazy-width'];
    $image_center_1_lazy_height = $image_center_1['sizes']['lazy-height'];

    // Image center 2
    $image_center_2_alt = $image_center_2['alt'];
    $image_center_2_desktop = $image_center_2['url'];
    $image_center_2_desktop_width = $image_center_2['width'];
    $image_center_2_desktop_height = $image_center_2['height'];

    $image_center_2_mobile = $image_center_2['sizes']['mobile'];
    $image_center_2_mobile_width = $image_center_2['sizes']['mobile-width'];
    $image_center_2_mobile_height = $image_center_2['sizes']['mobile-height'];

    $image_center_2_mobile_small = $image_center_2['sizes']['mobile-small'];
    $image_center_2_mobile_small_width = $image_center_2['sizes']['mobile-small-width'];
    $image_center_2_mobile_small_height = $image_center_2['sizes']['mobile-small-height'];

    $image_center_2_lazy = $image_center_2['sizes']['lazy'];
    $image_center_2_lazy_width = $image_center_2['sizes']['lazy-width'];
    $image_center_2_lazy_height = $image_center_2['sizes']['lazy-height'];

    // Image right 1
    $image_right_1_alt = $image_right_1['alt'];
    $image_right_1_desktop = $image_right_1['url'];
    $image_right_1_desktop_width = $image_right_1['width'];
    $image_right_1_desktop_height = $image_right_1['height'];

    $image_right_1_mobile = $image_right_1['sizes']['mobile'];
    $image_right_1_mobile_width = $image_right_1['sizes']['mobile-width'];
    $image_right_1_mobile_height = $image_right_1['sizes']['mobile-height'];

    $image_right_1_mobile_small = $image_right_1['sizes']['mobile-small'];
    $image_right_1_mobile_small_width = $image_right_1['sizes']['mobile-small-width'];
    $image_right_1_mobile_small_height = $image_right_1['sizes']['mobile-small-height'];

    $image_right_1_lazy = $image_right_1['sizes']['lazy'];
    $image_right_1_lazy_width = $image_right_1['sizes']['lazy-width'];
    $image_right_1_lazy_height = $image_right_1['sizes']['lazy-height'];

    // Image right 2
    $image_right_2_alt = $image_right_2['alt'];
    $image_right_2_desktop = $image_right_2['url'];
    $image_right_2_desktop_width = $image_right_2['width'];
    $image_right_2_desktop_height = $image_right_2['height'];

    $image_right_2_mobile = $image_right_2['sizes']['mobile'];
    $image_right_2_mobile_width = $image_right_2['sizes']['mobile-width'];
    $image_right_2_mobile_height = $image_right_2['sizes']['mobile-height'];

    $image_right_2_mobile_small = $image_right_2['sizes']['mobile-small'];
    $image_right_2_mobile_small_width = $image_right_2['sizes']['mobile-small-width'];
    $image_right_2_mobile_small_height = $image_right_2['sizes']['mobile-small-height'];

    $image_right_2_lazy = $image_right_2['sizes']['lazy'];
    $image_right_2_lazy_width = $image_right_2['sizes']['lazy-width'];
    $image_right_2_lazy_height = $image_right_2['sizes']['lazy-height'];


    // 5 images
    if($image_center_2 === false) {
?>
        <!-- GABARIT 9.0
            Mosaic 5 images
        -->
        <div class="gabarit gabarit-9 gabarit-9-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
            <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
            <div class="js-center-height">
                <div class="center">
                    <div class="column column-left">
                        <div class="top"><img class="lazy" src="<?php echo $image_left_1_lazy; ?>" data-src="<?php echo $image_left_1_desktop; ?>" alt="<?php echo $image_left_1_alt; ?>"></div><!--
                        --><div class="bottom"><img class="lazy" src="<?php echo $image_left_2_lazy; ?>" data-src="<?php echo $image_left_2_desktop; ?>" alt="<?php echo $image_left_2_alt; ?>"></div>
                        <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                    </div><!--
    
                    --><div class="column column-center">
                        <?php include(locate_template('views/partials/gabarit-title-project.php')); ?><!--
                        --><div class="top"><img class="lazy" src="<?php echo $image_center_1_lazy; ?>" data-src="<?php echo $image_center_1_desktop; ?>" alt="<?php echo $image_center_1_alt; ?>"></div>
                    </div><!--
    
                    --><div class="column column-right">                        
                        <?php if(!ONIRIM_IS_MOBILE) { ?> 
                        <div class="top"><img class="lazy" src="<?php echo $image_right_1_lazy; ?>" data-src="<?php echo $image_right_1_desktop; ?>" alt="<?php echo $image_right_1_alt; ?>"></div><!--
                        --><div class="bottom"><img class="lazy" src="<?php echo $image_right_2_lazy; ?>" data-src="<?php echo $image_right_2_desktop; ?>" alt="<?php echo $image_right_2_alt; ?>"></div>
                        <?php } ?>
                        <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                    </div>
                </div>
            </div>
            <?php echo getProjectTitleHTML($titre_gabarit); ?>
        </div>

<?php }else { // 6 images ?>

        <!-- GABARIT 9.1
            Mosaic 6 images
        -->
        <div class="gabarit gabarit-9 gabarit-9-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
            <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
            <div class="js-center-height">
                <div class="center">
                    <div class="column column-left">
                        <div class="top"><img class="lazy" src="<?php echo $image_left_1_lazy; ?>" data-src="<?php echo $image_left_1_desktop; ?>" alt="<?php echo $image_left_1_alt; ?>"></div><!--
                        --><div class="bottom"><img class="lazy" src="<?php echo $image_left_2_lazy; ?>" data-src="<?php echo $image_left_2_desktop; ?>" alt="<?php echo $image_left_2_alt; ?>"></div>
                        <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                    </div><!--
    
                    --><div class="column column-center">
                        <?php include(locate_template('views/partials/gabarit-title-project.php')); ?><!--
                        --><div class="top"><img class="lazy" src="<?php echo $image_center_1_lazy; ?>" data-src="<?php echo $image_center_1_desktop; ?>" alt="<?php echo $image_center_1_alt; ?>"></div><!--
                        --><div class="bottom"><img class="lazy" src="<?php echo $image_center_2_lazy; ?>" data-src="<?php echo $image_center_2_desktop; ?>" alt="<?php echo $image_center_2_alt; ?>"></div>
                    </div><!--
    
                    --><div class="column column-right">                        
                        <?php if(!ONIRIM_IS_MOBILE) { ?> 
                            <div class="top"><img class="lazy" src="<?php echo $image_right_1_lazy; ?>" data-src="<?php echo $image_right_1_desktop; ?>" alt="<?php echo $image_right_1_alt; ?>"></div><!--
                            --><div class="bottom"><img class="lazy" src="<?php echo $image_right_2_lazy; ?>" data-src="<?php echo $image_right_2_desktop; ?>" alt="<?php echo $image_right_2_alt; ?>"></div>
                            <?php } ?>
                        <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                    </div>
                </div>
            </div>
            <?php echo getProjectTitleHTML($titre_gabarit); ?>
        </div>
    <?php } ?>

<?php endif; // if gabarit ?>