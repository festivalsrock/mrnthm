<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_3', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit);
//echo '</pre>';

if( $gabarit ):

    // Vignette
    $position_vignette = $gabarit['position_vignette']; // left / right

    $vignette_type = $gabarit['vignette']['type']; // video / image

    $vignette_video_id = $gabarit['vignette']['video'];
    if($type_page == 'artist-list' && intval($gabarit['vignette']['video_list_artist']) > 0) $vignette_video_id = $gabarit['vignette']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['vignette']['video_home']) > 0) $vignette_video_id = $gabarit['vignette']['video_home'];

    $vignette_video_data = onirim_get_vimeo($vignette_video_id);
    $vignette_video_url = $vignette_video_data['url_mp4'];
    $vignette_video_picture = $vignette_video_data['picture'];
    $vignette_video_ratio = $vignette_video_data['ratio'];
    $vignette_video_format = (abs(2.35 - $vignette_video_ratio) > abs(1.777777778 - $vignette_video_ratio)) ? '16/9' : '2.35';


    $vignette_image_alt = $gabarit['vignette']['image']['alt'];
    $vignette_image_desktop = $gabarit['vignette']['image']['url'];
    $vignette_image_desktop_width = $gabarit['vignette']['image']['width'];
    $vignette_image_desktop_height = $gabarit['vignette']['image']['height'];

    $vignette_image_mobile = $gabarit['vignette']['image']['sizes']['mobile'];
    $vignette_image_mobile_width = $gabarit['vignette']['image']['sizes']['mobile-width'];
    $vignette_image_mobile_height = $gabarit['vignette']['image']['sizes']['mobile-height'];

    $vignette_image_mobile_small = $gabarit['vignette']['image']['sizes']['mobile-small'];
    $vignette_image_mobile_small_width = $gabarit['vignette']['image']['sizes']['mobile-small-width'];
    $vignette_image_mobile_small_height = $gabarit['vignette']['image']['sizes']['mobile-small-height'];

    $vignette_image_lazy = $gabarit['vignette']['image']['sizes']['lazy'];

    $vignette_image_lazy = $gabarit['vignette']['image']['sizes']['lazy'];
    $vignette_image_lazy_width = $gabarit['vignette']['image']['sizes']['lazy-width'];
    $vignette_image_lazy_height = $gabarit['vignette']['image']['sizes']['lazy-height'];

    // Emplacement vertical 1
    $vertical_1_type = $gabarit['vertical_1']['type']; // video / image

    $vertical_1_video_id = $gabarit['vertical_1']['video'];
    if($type_page == 'artist-list' && intval($gabarit['vertical_1']['video_list_artist']) > 0) $vertical_1_video_id = $gabarit['vertical_1']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['vertical_1']['video_home']) > 0) $vertical_1_video_id = $gabarit['vertical_1']['video_home'];

    $vertical_1_video_data = onirim_get_vimeo($vertical_1_video_id);
    $vertical_1_video_url = $vertical_1_video_data['url_mp4'];
    $vertical_1_video_picture = $vertical_1_video_data['picture'];
    $vertical_1_video_ratio = $vertical_1_video_data['ratio'];
    $vertical_1_video_format = (abs(2.35 -$vertical_1_video_ratio) > abs(1.777777778 -$vertical_1_video_ratio)) ? '16/9' : '2.35';

    $vertical_1_image_alt = $gabarit['vertical_1']['image']['alt'];
    $vertical_1_image_desktop = $gabarit['vertical_1']['image']['url'];
    $vertical_1_image_desktop_width = $gabarit['vertical_1']['image']['width'];
    $vertical_1_image_desktop_height = $gabarit['vertical_1']['image']['height'];

    $vertical_1_image_mobile = $gabarit['vertical_1']['image']['sizes']['mobile'];
    $vertical_1_image_mobile_width = $gabarit['vertical_1']['image']['sizes']['mobile-width'];
    $vertical_1_image_mobile_height = $gabarit['vertical_1']['image']['sizes']['mobile-height'];

    $vertical_1_image_mobile_small = $gabarit['vertical_1']['image']['sizes']['mobile-small'];
    $vertical_1_image_mobile_small_width = $gabarit['vertical_1']['image']['sizes']['mobile-small-width'];
    $vertical_1_image_mobile_small_height = $gabarit['vertical_1']['image']['sizes']['mobile-small-height'];

    $vertical_1_image_lazy = $gabarit['vertical_1']['image']['sizes']['lazy'];
    $vertical_1_image_lazy_width = $gabarit['vertical_1']['image']['sizes']['lazy-width'];
    $vertical_1_image_lazy_height = $gabarit['vertical_1']['image']['sizes']['lazy-height'];

    // Emplacement vertical 2
    $vertical_2_type = $gabarit['vertical_2']['type']; // video / image

    $vertical_2_video_id = $gabarit['vertical_2']['video'];
    if($type_page == 'artist-list' && intval($gabarit['vertical_2']['video_list_artist']) > 0) $vertical_2_video_id = $gabarit['vertical_2']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['vertical_2']['video_home']) > 0) $vertical_2_video_id = $gabarit['vertical_2']['video_home'];

    $vertical_2_video_data = onirim_get_vimeo($vertical_2_video_id);
    $vertical_2_video_url = $vertical_2_video_data['url_mp4'];
    $vertical_2_video_picture = $vertical_2_video_data['picture'];
    $vertical_2_video_ratio = $vertical_2_video_data['ratio'];
    $vertical_2_video_format = (abs(2.35 -$vertical_2_video_ratio) > abs(1.777777778 -$vertical_2_video_ratio)) ? '16/9' : '2.35';

    $vertical_2_image_alt = $gabarit['vertical_2']['image']['alt'];
    $vertical_2_image_desktop = $gabarit['vertical_2']['image']['url'];
    $vertical_2_image_desktop_width = $gabarit['vertical_2']['image']['width'];
    $vertical_2_image_desktop_height = $gabarit['vertical_2']['image']['height'];

    $vertical_2_image_mobile = $gabarit['vertical_2']['image']['sizes']['mobile'];
    $vertical_2_image_mobile_width = $gabarit['vertical_2']['image']['sizes']['mobile-width'];
    $vertical_2_image_mobile_height = $gabarit['vertical_2']['image']['sizes']['mobile-height'];

    $vertical_2_image_mobile_small = $gabarit['vertical_2']['image']['sizes']['mobile-small'];
    $vertical_2_image_mobile_small_width = $gabarit['vertical_2']['image']['sizes']['mobile-small-width'];
    $vertical_2_image_mobile_small_height = $gabarit['vertical_2']['image']['sizes']['mobile-small-height'];

    $vertical_2_image_lazy = $gabarit['vertical_2']['image']['sizes']['lazy'];
    $vertical_2_image_lazy_width = $gabarit['vertical_2']['image']['sizes']['lazy-width'];
    $vertical_2_image_lazy_height = $gabarit['vertical_2']['image']['sizes']['lazy-height'];



 ?>


<!--

    GABARIT 3

    -->

<?php if($position_vignette == 'left') { ?>
<!-- GABARIT 3.0
    1 petite Horizontal à gauche et 2 vertical à droite
-->
<div class="gabarit gabarit-3 gabarit-3-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
    <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
    <div class="js-center-height">
        <div class="big">
            <div class="image-big">
                <div class="image-big-left"><?php echo getImageOrVideo($vertical_1_type, $vertical_1_image_desktop, $vertical_1_image_alt, $vertical_1_video_url, $vertical_1_video_url, $vertical_1_video_picture, $vertical_1_image_lazy); ?></div><!--
                --><div class="image-big-right"><?php echo getImageOrVideo($vertical_2_type, $vertical_2_image_desktop, $vertical_2_image_alt, $vertical_2_video_url, $vertical_2_video_url, $vertical_2_video_picture, $vertical_2_image_lazy); ?></div>
            </div>
            <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
            <?php include(locate_template('views/partials/gabarit-related.php')); ?>
        </div>
        <?php if(!ONIRIM_IS_MOBILE) { ?>
            <div class="small">
                <div class="image-small"><?php echo getImageOrVideo($vignette_type, $vignette_image_desktop, $vignette_image_alt, $vignette_video_url, $vignette_video_url, $vignette_video_picture, $vignette_image_lazy); ?></div>
                <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
            </div>
        <?php } ?>
        <div style="clear: both;"></div>
    </div>
    <?php echo getProjectTitleHTML($titre_gabarit); ?>
</div>

<?php } ?>

<?php if($position_vignette == 'right') { ?>
<!-- GABARIT 3.1
    1 petite Horizontal à droite et 2 vertical à gauche
 -->
<div class="gabarit gabarit-3 gabarit-3-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
    <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
    <div class="js-center-height">
        <div class="big">
            <div class="image-big">
                <div class="image-big-left"><?php echo getImageOrVideo($vertical_2_type, $vertical_2_image_desktop, $vertical_2_image_alt, $vertical_2_video_url, $vertical_2_video_url, $vertical_2_video_picture, $vertical_2_image_lazy); ?></div><!--
                --><div class="image-big-right"><?php echo getImageOrVideo($vertical_1_type, $vertical_1_image_desktop, $vertical_1_image_alt, $vertical_1_video_url, $vertical_1_video_url, $vertical_1_video_picture, $vertical_1_image_lazy); ?></div>
            </div>
        </div>
        <?php if(!ONIRIM_IS_MOBILE) { ?>
            <div class="image-small"><?php echo getImageOrVideo($vignette_type, $vignette_image_desktop, $vignette_image_alt, $vignette_video_url, $vignette_video_url, $vignette_video_picture, $vignette_image_lazy); ?></div>
        <?php } ?>
        <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
        <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
        <?php include(locate_template('views/partials/gabarit-related.php')); ?>
    </div>
    <?php echo getProjectTitleHTML($titre_gabarit); ?>
</div>

<?php } ?>

<?php endif; // if gabarit ?>