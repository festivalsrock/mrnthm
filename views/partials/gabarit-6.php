<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_6', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit);
//echo '</pre>';

if( $gabarit ):

    // Format
    $format = $gabarit['format'];
    if(empty($format)) $format = 'horizontal';

    // $format = 'vertical-double';

    // Bordure
    $border_width = intval($gabarit['border_width']);
    $border_width = ONIRIM_IS_MOBILE ? ceil($border_width / 2) : $border_width;
    $border_color = $gabarit['border_color'];
    if(empty($border_color)) $border_color = '#ffffff';

    // Emplacement gauche
    $left_type = $gabarit['left']['type']; // video / image

    $left_video_id = $gabarit['left']['video'];
    if($type_page == 'artist-list' && intval($gabarit['left']['video_list_artist']) > 0) $left_video_id = $gabarit['left']['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['left']['video_home']) > 0) $left_video_id = $gabarit['left']['video_home'];
    $left_video_data = onirim_get_vimeo($left_video_id);
    $left_video_url = $left_video_data['url_mp4'];
    $left_video_picture = $left_video_data['picture'];
    $left_video_ratio = $left_video_data['ratio'];
    $left_video_format = (abs(2.35 -$left_video_ratio) > abs(1.777777778 -$left_video_ratio)) ? '16/9' : '2.35';

    $left_image_alt = $gabarit['left']['image']['alt'];
    $left_image_desktop = $gabarit['left']['image']['url'];
    $left_image_desktop_width = $gabarit['left']['image']['width'];
    $left_image_desktop_height = $gabarit['left']['image']['height'];

    $left_image_mobile = $gabarit['left']['image']['sizes']['mobile'];
    $left_image_mobile_width = $gabarit['left']['image']['sizes']['mobile-width'];
    $left_image_mobile_height = $gabarit['left']['image']['sizes']['mobile-height'];

    $left_image_mobile_small = $gabarit['left']['image']['sizes']['mobile-small'];
    $left_image_mobile_small_width = $gabarit['left']['image']['sizes']['mobile-small-width'];
    $left_image_mobile_small_height = $gabarit['left']['image']['sizes']['mobile-small-height'];
    
    $left_image_lazy = $gabarit['left']['image']['sizes']['lazy'];

    $left_image_lazy = $gabarit['left']['image']['sizes']['lazy'];
    $left_image_lazy_width = $gabarit['left']['image']['sizes']['lazy-width'];
    $left_image_lazy_height = $gabarit['left']['image']['sizes']['lazy-height'];

    // Emplacement droite

    $right_image_alt = $gabarit['right']['image']['alt'];
    $right_image_desktop = $gabarit['right']['image']['url'];
    $right_image_desktop_width = $gabarit['right']['image']['width'];
    $right_image_desktop_height = $gabarit['right']['image']['height'];

    $right_image_mobile = $gabarit['right']['image']['sizes']['mobile'];
    $right_image_mobile_width = $gabarit['right']['image']['sizes']['mobile-width'];
    $right_image_mobile_height = $gabarit['right']['image']['sizes']['mobile-height'];

    $right_image_mobile_small = $gabarit['right']['image']['sizes']['mobile-small'];
    $right_image_mobile_small_width = $gabarit['right']['image']['sizes']['mobile-small-width'];
    $right_image_mobile_small_height = $gabarit['right']['image']['sizes']['mobile-small-height'];

    $right_image_lazy = $gabarit['right']['image']['sizes']['lazy'];
    $right_image_lazy_width = $gabarit['right']['image']['sizes']['lazy-width'];
    $right_image_lazy_height = $gabarit['right']['image']['sizes']['lazy-height'];

    ?>


    <!--

        GABARIT 6

        -->

<?php if($format == 'horizontal') { ?>
    <!-- GABARIT 6.0
        1 image horizontale et une image verticale
    -->
    <div class="gabarit gabarit-6 gabarit-6-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="center">
                <div class="big">
                    <div class="image-big"><?php echo getImageOrVideo($left_type, $left_image_mobile, $left_image_lazy, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture); ?></div><!--
                        --><?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                </div><!--
             --><div class="small">
                    <div class="image-small"><img class="lazy" src="<?php echo $right_image_lazy; ?>" data-src="<?php echo $right_image_mobile; ?>" alt="<?php echo $right_image_alt; ?>"></div>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<?php if($format == 'vertical') { ?>

    <!-- GABARIT 6.1
        1 image verticale et une image verticale
    -->
    <div class="gabarit gabarit-6 gabarit-6-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="center">
                <div class="big">
                    <div class="image-big"><?php echo getImageOrVideo($left_type, $left_image_mobile, $left_image_lazy, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture); ?></div><!--
                        --><?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                </div><!--
                --><div class="small">
                    <div class="image-small"><img class="lazy" src="<?php echo $right_image_lazy; ?>" data-src="<?php echo $right_image_mobile; ?>" alt="<?php echo $right_image_alt; ?>"></div>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>    
                    <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<?php if($format == 'horizontal-double') {      
        // $border_width = 10;
        // $border_color = '#ff0000';
    ?>
    <!-- GABARIT 6.2
        2 images horizontales
    -->
    <div class="gabarit gabarit-6 gabarit-6-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="center">
                <div class="big">
                    <div class="image-big"><?php echo getImageOrVideo($left_type, $left_image_mobile, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture, $left_image_lazy); ?></div><!--
                    --><?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img style="border: <?php echo $border_width ?>px solid <?php echo $border_color ?>;border-right: 0px;" class="lazy" src="<?php echo $right_image_lazy; ?>" data-src="<?php echo $right_image_mobile; ?>" alt="<?php echo $right_image_alt; ?>"></div>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<!-- /!\ NEVER GOES HERE, OLD GABARIT-->
<?php if(false && $format == 'horizontal-double') { ?>
    <!-- GABARIT 6.2
        2 images horizontales
    -->
    <div class="gabarit gabarit-6 gabarit-6-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="center">
                <div class="big">
                    <div class="image-big"><?php echo getImageOrVideo($left_type, $left_image_mobile, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture, $left_image_lazy); ?></div><!--
                    --><?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                </div><!--
                --><div class="small">            
                    <div class="image-small"><img class="lazy" src="<?php echo $right_image_lazy; ?>" data-src="<?php echo $right_image_mobile; ?>" alt="<?php echo $right_image_alt; ?>"></div>
                    <?php include(locate_template('views/partials/gabarit-title-project.php')); ?>
                    <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<?php if($format == 'vertical-double') {         
    // $border_width = 10;
    // $border_color = '#ff0000';
    ?>
    <!-- GABARIT 6.3
        2 images verticales alignées en bas
    -->
    <div class="gabarit gabarit-6 gabarit-6-3" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="center">
                <div class="big">
                    <div style="display: flex; flex-direction:row;"><!--
                        --><div class="image-big"><?php echo getImageOrVideo($left_type, $left_image_mobile, $left_image_alt, $left_video_url, $left_video_url, $left_video_picture, $left_image_lazy); ?></div><!--
                        
                        --><div class="image-big-border" style="width:<?php echo $border_width ?>px; background-color:<?php echo $border_color ?>;"></div><!-- 
                        
                        --><div class="image-big"><img class="lazy" src="<?php echo $right_image_lazy; ?>" data-src="<?php echo $right_image_mobile; ?>" alt="<?php echo $right_image_alt; ?>"></div>
                    </div><!--       

                    --><?php include(locate_template('views/partials/gabarit-legend.php')); ?><!--
                    --><?php include(locate_template('views/partials/gabarit-related.php')); ?>
                    <div style="clear: both;"></div>
                </div>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>

<?php } ?>

<?php endif; // if gabarit ?>