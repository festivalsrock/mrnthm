<?php
if(!isset($type_page)) $type_page = '';
$legendeHtml = '';

if(!empty($legende)) {
    $legendeHtml = $legende;
}
if(!empty($projet->name)) {
    //$legendeHtml .= ' for '. $projet->name;
}
if($type_page != 'artist-detail') {
    if($legende_by == 'neutre') {

    }elseif($legende_by == 'libre') {
        if(!empty($legende_by_texte)) {
            $legendeHtml .= ' <br class="by-to-line"/>by&nbsp;<span class="default">'. $legende_by_texte .'</span>';
        }
    }else {
        if(!empty($artiste->name)) {
            $legendeHtml .= ' <br class="by-to-line"/>by&nbsp;&nbsp;<a href="'.$artiste_url.'"><span class="default">'. $artiste->name .'</span><span class="over">'. $artiste->name.'</span></a>';
        }
    }
}

?>
<div class="legend"><?php echo $legendeHtml; ?></div>