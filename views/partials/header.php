<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php
    if(isset($_REQUEST['gid']) && intval($_REQUEST['gid']) > 0) {
        remove_action( 'wp_head', '_wp_render_title_tag', 1 );
        echo onirim_curl_get_data(get_permalink( intval($_REQUEST['gid']) ).'?seo');
    }
    wp_head(); ?>

    <link rel="shortcut icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png" />

    <!-- COOKIE CONSENT -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
    window.addEventListener('load', function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                "background": "#000000",
                "text": "#ffffff"
                },
                "button": {
                "background": "transparent",
                "text": "#ffffff",
                "border": "#ffffff"
                }
            },
            "content": {
                "dismiss": "Accept",
                
                <?php $id_page_legals = get_field('page_legals', 'onirim_options'); ?>
                "href": "<?php echo get_permalink($id_page_legals); ?>"
            }
        });
    });
    </script>    
    <!-- END - COOKIE CONSENT -->
</head>

<body <?php body_class(onirim_get_support_classes()); ?>>

<div class="header">
    <?php
    wp_nav_menu( array(
        'theme_location' => 'main-menu',
        'container_class' => 'menu-desktop' ) );
    ?>

    <div class="menu-mobile">
        <div class="menu-mobile-burger js-menu-burger"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
        <rect fill="#FFFFFF" width="25" height="25"/>
                <rect x="5" y="8" width="15" height="1"/>
                <rect x="5" y="12" width="15" height="1"/>
                <rect x="5" y="16" width="15" height="1"/>
        </svg></div>

        <div class="menu-mobile-content js-menu-content">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'main-menu',
                'container_class' => 'menu-mobile-ul' ) );
            ?>
            <!-- <div class="logo">
                <a href="/"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/onirim-white.svg" alt="Logo Onirim"></a>
            </div> -->
            <?php
            $instagram_url = get_field('instagram_url', 'onirim_options');
            $facebook_url = get_field('facebook_url', 'onirim_options');
            $twitter_url = get_field('twitter_url', 'onirim_options');
            $linkedin_url = get_field('linkedin_url', 'onirim_options');
            ?>
          <ul class="social">
              <?php if(!empty($facebook_url)) { ?>
                <li><a href="<?php echo $facebook_url; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-facebook-white.svg" alt="Lien Facebook"></a></li>
              <?php } ?>
              <?php if(!empty($twitter_url)) { ?>
                <li><a href="<?php echo $twitter_url; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-twitter-white.svg" alt="Lien Twitter"></a></li>
              <?php } ?>
              <?php if(!empty($linkedin_url)) { ?>
                <li><a href="<?php echo $linkedin_url; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-linkedin-white.svg" alt="Lien LinkedIn"></a></li>
              <?php } ?>
              <?php if(!empty($instagram_url)) { ?>
                <li><a href="<?php echo $instagram_url; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/bt-instagram-white.svg" alt="Lien Instagram"></a></li>
              <?php } ?>
          </ul>
            <div class="bt-close js-menu-close"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="25px" height="25px" viewBox="0 0 25 25" enable-background="new 0 0 25 25" xml:space="preserve">
            <rect fill="none" width="25" height="25"/>
                    <rect x="5" y="12" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -5.1777 12.5)" fill="#FFFFFF" width="15" height="1"/>
                    <rect x="5" y="12" transform="matrix(0.7071 0.7071 -0.7071 0.7071 12.5 -5.1777)" fill="#FFFFFF" width="15" height="1"/>
            </svg></div>
        </div>
    </div>

    <?php if(strpos(get_page_template(), 'homepage.php') !== false) {

        $resume = get_field('intro', ONIRIM_DEFAULT_PAGE_ABOUTUS_ID);
        $texte_intro = get_field('texte_intro', 'onirim_options');

        if(is_null($texte_intro)) $texte_intro = "CREATIVE PRODUCTION COMMERCIAL, PHOTOS &amp; BRAND CONTENT"; ?>
    <!--mfunc 288fd6393358f20ff850df97fdd6f5d1 -->
        session_start();
        if(!isset($_SESSION['intro']) || (isset($_SESSION['intro']) && $_SESSION['intro'] !== false)) {
            if(is_null($resume)) $resume = "We are<br>The perfect Antidote to Boredom<br>and mass-production.";
            echo '<div id="intro" class="template-aboutus">';
            echo '<div class="resume">';
            echo $resume;
            echo '</div>';
            echo '<div class="resume resume-over">';
            echo $resume;
            echo '</div>';
            echo '</div>';

            $_SESSION['intro'] = false;
        }
    <!--/mfunc 288fd6393358f20ff850df97fdd6f5d1 -->
     <?php } ?>

    <?php /*
        if($_SESSION['intro'] !== false) {
            $texte_intro = get_field('texte_intro', 'onirim_options');
            if(is_null($texte_intro)) $texte_intro = "CREATIVE PRODUCTION COMMERCIAL, PHOTOS &amp; BRAND CONTENT";
    ?>
    <div id="intro">
        <div class="content"><?php echo $texte_intro; ?></div>
    </div>
    <?php
    }
    $_SESSION['intro'] = false;
    */ ?>

    <div id="transition" class="enter"></div>

    <div id="logo" class="">
        <a href="/"><img class="black" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/onirim.svg" alt="Logo Onirim"><img class="white" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/onirim-white.svg" alt="Logo Onirim"></a>
    </div>
</div>


<div id="page" class="hfeed site">
    <div id="content" class="site-content">

