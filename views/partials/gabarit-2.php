<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_2', $gabarit_id);

//echo '<pre>';
//var_dump($gabarit);
//echo '</pre>';

if( $gabarit ):

    // Video
    $video_id = $gabarit['video'];
    if($type_page == 'artist-list' && intval($gabarit['video_list_artist']) > 0) $video_id = $gabarit['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['video_home']) > 0) $video_id = $gabarit['video_home'];
    $video_data = onirim_get_vimeo($video_id);
    $video_url = $video_data['url'];
    $video_url_mp4 = $video_data['url_mp4'];
    $video_ratio = $video_data['ratio'];
    $video_picture = $video_data['picture'];

    // This is not really needed anymore, the padding-top is calculated for any ratio of video
    $video_format = (abs(2.35 - $video_ratio) > abs(1.777777778 - $video_ratio)) ? '16/9' : '2.35';


    ?>
    <!--

        GABARIT 2

        -->

    <?php if($video_format == '16/9') { ?>
    <!-- GABARIT 2.0
        1 video 16/9
    -->
    <div class="gabarit gabarit-2 gabarit-2-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="video-container">
                <video webkit-playsinline playsinline class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" poster="<?php echo $video_picture; ?>" src="<?php echo $video_url_mp4; ?>">
                    <source src="<?php echo $video_url_mp4; ?>" type='video/mp4'>
                    <!-- <source src="<?php echo $video_url; ?>" type='application/x-mpegURL' /> -->
                </video>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <?php } ?>


    <?php if($video_format == '2.35') { ?>
    <!-- GABARIT 2.1
        1 video 2.35
    -->
    <div class="gabarit gabarit-2 gabarit-2-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="video-container">
                <video webkit-playsinline playsinline style="padding-top:<?php echo 100 / $video_ratio; ?>%" class="video-js vjs-default-skin vjs-onirim-skin vjs-fluid vjs-2-35" poster="<?php echo $video_picture; ?>" src="<?php echo $video_url_mp4; ?>">
                        <source src="<?php echo $video_url_mp4; ?>" type='video/mp4'>
                    <!-- <source src="<?php echo $video_url; ?>" type='application/x-mpegURL' /> -->
                </video>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <!-- Video 16/9 (1.77778) : https://vimeo.com/241879000/af8bde4e3f -->
    <!-- Video 2.35 : https://vimeo.com/125947740 -->
    <?php } ?>



<?php endif; ?>
