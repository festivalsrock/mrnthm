<?php
// Flux Instagram Onirim
//$user_id = "1346067497";
//$access_token = "1346067497.3a81a9f.44cbea83999a481db47a1ce9ae82a369";

// On récupère les variable depuis la page d'options ACF, id et token issus du plugin Instagram Feed
$user_id = get_field('instagram_user_id', 'onirim_options');
$access_token = get_field('instagram_access_token', 'onirim_options');
$instagram_url = get_field('instagram_url', 'onirim_options');

// On récupère les 6 dernières images publiées sur le compte Instagram
$endpoint  = "https://api.instagram.com/v1/users/".$user_id."/media/recent/?count=6&access_token=".$access_token;
$curl = curl_init($endpoint);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 3);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$data = curl_exec($curl);

// On converit en pbjet le retour JSON d'Instagram
$publications = json_decode($data);

?>

<?php if(isset($publications->data) && is_array($publications->data)) { ?>
    <?php foreach($publications->data as $publication) { ?>
        <a href="<?php echo $publication->link; ?>" target="_blank">
            <img src="<?php echo $publication->images->standard_resolution->url; ?>">
            <?php //var_dump($slide->images); ?>
        </a>
    <?php } ?>
<?php } ?>
