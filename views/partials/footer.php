<?php
/**
 * The template for displaying the footer
 *
 */
?>

</div><!-- .site-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
</footer><!-- .site-footer -->

</div><!-- .site -->


<script>
  <!--mfunc 288fd6393358f20ff850df97fdd6f5d1 -->
  $playlistData = array();
  $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
  if(isset($currentPlaylist['data']) && is_array($currentPlaylist['data']) && sizeof($currentPlaylist['data']) > 0) {
    $playlistData = $currentPlaylist['data'];
    $tmp = '"'.implode('","', array_merge($playlistData)).'"';
  }else {
    $tmp = '';
  }
  echo 'var playlistData = ['.$tmp.'];';
  <!--/mfunc 288fd6393358f20ff850df97fdd6f5d1 -->
</script>

<?php wp_footer(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60417875-10', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
