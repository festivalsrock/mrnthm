<?php
include(locate_template('views/partials/gabarit-get-data.php'));

$gabarit = get_field('gabarit_1', $gabarit_id);

//echo '<pre>';
//var_dump($artiste);
//echo '</pre>';


if( $gabarit ):

    // Video
    $position_video = $gabarit['position_video']; // left / right
    $video_id = $gabarit['video'];
    if($type_page == 'artist-list' && intval($gabarit['video_list_artist']) > 0) $video_id = $gabarit['video_list_artist'];
    if($type_page == 'homepage' && intval($gabarit['video_home']) > 0) $video_id = $gabarit['video_home'];
    $video_data = onirim_get_vimeo($video_id);
    $video_url = $video_data['url'];
    $video_url_mp4 = $video_data['url_mp4'];
    $video_picture = $video_data['picture'];
    $video_ratio = $video_data['ratio'];

    // This is not really needed anymore, the padding-top is calculated for any ratio of video
    $video_format = (abs(2.35 - $video_ratio) > abs(1.777777778 - $video_ratio)) ? '16/9' : '2.35';

    // Image
    $image_alt = $gabarit['image']['alt'];

    $image_desktop = $gabarit['image']['url'];
    $image_desktop_width = $gabarit['image']['width'];
    $image_desktop_height = $gabarit['image']['height'];

    $image_mobile = $gabarit['image']['sizes']['mobile'];
    $image_mobile_width = $gabarit['image']['sizes']['mobile-width'];
    $image_mobile_height = $gabarit['image']['sizes']['mobile-height'];

    $image_mobile_small = $gabarit['image']['sizes']['mobile-small'];
    $image_mobile_small_width = $gabarit['image']['sizes']['mobile-small-width'];
    $image_mobile_small_height = $gabarit['image']['sizes']['mobile-small-height'];
    
    $image_lazy = $gabarit['image']['sizes']['lazy'];

    $image_lazy = $gabarit['image']['sizes']['lazy'];
    $image_lazy_width = $gabarit['image']['sizes']['lazy-width'];
    $image_lazy_height = $gabarit['image']['sizes']['lazy-height'];

?>

    <!--

        GABARIT 1

        -->
    <?php if($video_format == '16/9' && $position_video == 'left') { ?>
    <!-- GABARIT 1.0
        1 video 16/9, 1 image à droite
    -->
    <div class="gabarit gabarit-1 gabarit-1-0" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="video-container">
                <video webkit-playsinline playsinline style="padding-top:<?php echo 100 / $video_ratio; ?>%" class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" poster="<?php echo $video_picture; ?>" src="<?php echo $video_url_mp4; ?>">
                    <source src="<?php echo $video_url_mp4; ?>" type='video/mp4'>
                    <!-- <source src="<?php echo $video_url; ?>" type='application/x-mpegURL' /> -->
                </video>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                
                <?php if(!ONIRIM_IS_MOBILE && $image_desktop != '') { ?> 
                <img class="video-bg lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>">
                <?php } ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <?php } ?>


    <?php if($video_format == '2.35' && $position_video == 'left') { ?>
    <!-- GABARIT 1.1
        1 video 2.35, 1 image à droite
    -->
    <div class="gabarit gabarit-1 gabarit-1-1" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="video-container">
                <video webkit-playsinline playsinline style="padding-top:<?php echo 100 / $video_ratio; ?>%" class="video-js vjs-default-skin vjs-onirim-skin vjs-fluid vjs-2-35" poster="<?php echo $video_picture; ?>" src="<?php echo $video_url_mp4; ?>">
                    <source src="<?php echo $video_url_mp4; ?>" type='video/mp4'>
                    <!-- <source src="<?php echo $video_url; ?>" type='application/x-mpegURL' /> -->
                </video>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                
                <?php if(!ONIRIM_IS_MOBILE && $image_desktop != '') { ?> 
                <img class="video-bg lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>">
                <?php } ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <?php } ?>

    <?php if($video_format == '16/9' && $position_video == 'right') { ?>
    <!-- GABARIT 1.2
        1 video 16/9, 1 image à gauche
    -->
    <div class="gabarit gabarit-1 gabarit-1-2" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="video-container">
                <video webkit-playsinline playsinline style="padding-top:<?php echo 100 / $video_ratio; ?>%" class="video-js vjs-default-skin vjs-onirim-skin vjs-16-9" poster="<?php echo $video_picture; ?>" src="<?php echo $video_url_mp4; ?>">
                    <source src="<?php echo $video_url_mp4; ?>" type='video/mp4'>
                    <!-- <source src="<?php echo $video_url; ?>" type='application/x-mpegURL' /> -->
                </video>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                
                <?php if(!ONIRIM_IS_MOBILE && $image_desktop != '') { ?> 
                <img class="video-bg lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>">
                <?php } ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <?php } ?>


    <?php if($video_format == '2.35' && $position_video == 'right') { ?>
    <!-- GABARIT 1.3
        1 video 2.35, 1 image à gauche
    -->
    <div class="gabarit gabarit-1 gabarit-1-3" data-artist-url="<?php echo $artiste_url; ?>" data-id="<?php echo $gabarit_id; ?>">
        <?php include(locate_template('views/partials/gabarit-title-home.php')); ?>
        <div class="js-center-height">
            <div class="video-container">
                <video webkit-playsinline playsinline style="padding-top:<?php echo 100 / $video_ratio; ?>%" class="video-js vjs-default-skin vjs-onirim-skin vjs-fluid vjs-2-35" poster="<?php echo $video_picture; ?>" src="<?php echo $video_url_mp4; ?>">
                    <source src="<?php echo $video_url_mp4; ?>" type='video/mp4'>
                    <!-- <source src="<?php echo $video_url; ?>" type='application/x-mpegURL' /> -->
                </video>
                <?php include(locate_template('views/partials/gabarit-legend.php')); ?>
                <?php include(locate_template('views/partials/gabarit-related.php')); ?>
                
                <?php if(!ONIRIM_IS_MOBILE && $image_desktop != '') { ?> 
                <img class="video-bg lazy" src="<?php echo $image_lazy; ?>" data-src="<?php echo $image_desktop; ?>" alt="<?php echo $image_alt; ?>">
                <?php } ?>
            </div>
        </div>
        <?php echo getProjectTitleHTML($titre_gabarit); ?>
    </div>
    <?php } ?>

<?php endif; ?>
