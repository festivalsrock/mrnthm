<?php

$gabarit_id = (isset($gabarit->ID) && $gabarit->ID > 0) ? $gabarit->ID : null;

$type_gabarit = get_field('type_gabarit', $gabarit_id);
$categorie = get_field('categorie', $gabarit_id);
$display_on_page = get_field('display_on_page', $gabarit_id);
$artiste = get_field('artiste', $gabarit_id);
$artiste_libre = get_field('artiste_libre', $gabarit_id);
$artiste_titre_homepage = get_field('titre_homepage', $artiste);
if(empty(trim(strip_tags($artiste_titre_homepage)))) $artiste_titre_homepage = $artiste->name;
if(empty(trim(strip_tags($artiste_titre_homepage)))) $artiste_titre_homepage = $artiste_libre;

$artiste_url = '';
if($artiste) {
    $artiste_url = get_term_link($artiste);
}else {
    if(is_array($display_on_page) && sizeof($display_on_page) > 0) {
        $frontpage_id = get_option( 'page_on_front' );
        foreach($display_on_page as $display_on_page_id) {
            if($display_on_page_id != $frontpage_id) { // pas homepage
                $display_on_page_gabarits = get_field('gabarits', $display_on_page_id);
                if(is_array($display_on_page_gabarits) && sizeof($display_on_page_gabarits) > 0) {
                    foreach($display_on_page_gabarits as $display_on_page_gabarit) {
                        if($display_on_page_gabarit->ID == $gabarit_id) {
                            $artiste_url = get_permalink($display_on_page_id);
                            break;
                        }
                    }
                }
            }
        }
    }
}

$projet = get_field('projet', $gabarit_id);
$legende = get_field('legende', $gabarit_id);
$legende_by = get_field('by', $gabarit_id);
$legende_by_texte = get_field('by_texte', $gabarit_id);
//$position_titre_home = get_field('position_titre_home', $gabarit_id);

$titre_gabarit = get_field('titre_gabarit', $gabarit_id);
$home_titre_top = $titre_gabarit['home_top'];
$home_titre_color = $titre_gabarit['home_color'];
