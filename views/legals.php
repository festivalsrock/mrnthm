<?php
/**
 * Template Name: Legals
 */

get_header(); ?>

<div class="template-legals">
    
    <div class="title"><?php the_title(); ?></div>
    <?php the_content(); ?>
</div>

<div class="footer">
    <ul class="menu">
        <!-- <li><a href="">PRIVACY POLICY</a></li> -->
        <li><a href="https://westonmills.com/" target="_blank">CREDITS</a></li>
    </ul>
    <div class="copyright">© <?php echo date('Y'); ?> <span>ONIRIM</span> - ALL RIGHTS RESERVED</div>
</div>

<?php get_footer(); ?>