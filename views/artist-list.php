<?php
/**
 * Template Name: Artist List
 */

$type_page = 'artist-list';

$artistes_col_1 = get_field('artistes_col_1');
$artistes_col_2 = get_field('artistes_col_2');
$artistes_filtres = get_field('filtres');

get_header();
?>

    <div class="template-artist-list">

        <!-- MENU -->
        <div class="menu-artist">
            <ul class="col col-1">
                <?php
                foreach($artistes_col_1 as $artiste) :
                    include(locate_template('views/partials/artiste-name.php'));
                endforeach;
                ?>
            </ul>
            <ul class="col col-2">
                <?php
                foreach($artistes_col_2 as $artiste) :
                    include(locate_template('views/partials/artiste-name.php'));
                endforeach;
                ?>
            </ul>
       </div>

        <!-- FILTRES -->
        <div class="menu-filter">
            <?php foreach($artistes_filtres as $filtre) : ?><!--
            --><input type="submit" id="<?php echo $filtre->slug; ?>" name="<?php echo $filtre->name; ?>" value="<?php echo $filtre->name; ?>"><!--
            <?php endforeach; ?>
        --></div>

        <!-- GABARITS COVER -->
        <?php if(!ONIRIM_IS_MOBILE) { ?>
        <div class="content">
            <?php
            foreach($artistes_col_1 as $artiste) :
                include(locate_template('views/partials/artiste-cover.php'));
            endforeach;
            foreach($artistes_col_2 as $artiste) :
                include(locate_template('views/partials/artiste-cover.php'));
            endforeach;
            ?>
        </div>
        <?php } ?>

    </div>

<?php get_footer(); ?>