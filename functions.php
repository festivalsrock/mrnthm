<?php
// Disable HSTS
add_action( 'send_headers', 'add_header_xua' );
function add_header_xua() {
    header( 'Strict-Transport-Security: max-age=0;' );
}
//define( 'W3TC_DYNAMIC_SECURITY', '79aa9a9cc3073165fa5438f299e42c21' );

// Gestion URL PLAYSLISTS
$request_uri = $_SERVER['REQUEST_URI'];
$request_uri = explode('/', $request_uri);
if($request_uri[1] == 'playlist' && intval($request_uri[2]) > 0) {

    $playlist_post = get_post(intval($request_uri[2]));
    if($playlist_post->post_type == 'onirim_playlist' && $playlist_post->ID > 0) {
        $slug = sanitize_title(get_the_title($playlist_post->ID));
        if($slug == '') $slug = '-';
        $permalink = site_url().'/playlist/'.$playlist_post->ID.'/'.$slug.'/';

        if(site_url().$_SERVER['REQUEST_URI'] != $permalink) {
            wp_redirect($permalink, 301);
            exit;
        }
    }
}

$onirim_includes = [
    'inc/conf.php', // Variables globales de configuration
    'inc/admin.php', // Config backoffice
    'inc/vimeo.php', // API Vimeo
    'inc/nav.php', // Config nav
    'inc/clean.php', // Clean metas
    'inc/media.php', // Medias
    'inc/helpers.php', // Helpers
    'inc/playlist.php', // Playlist

    // Custom Post Type
    'inc/type-artiste.php',
    'inc/type-projet.php',
    'inc/type-gabarit.php',
    'inc/type-playlist.php',

    // EMPTY WP CACHE & TRANSIENTS
//    'inc/empty-cache.php'
];

foreach ($onirim_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'onirim'), $file), E_USER_ERROR);
    }
    require_once $filepath;
}
unset($file, $filepath);

function onirim_scripts() {
    wp_enqueue_style( 'onirim', get_template_directory_uri() . '/css/style.css',array(), '1.2' );
    wp_enqueue_script( 'onirim', get_template_directory_uri() . '/js/main.js', array(), '1.0.2', true );

    wp_localize_script('onirim', 'adminAjaxUrl', admin_url( 'admin-ajax.php' ) );
//    $playlistData = array();
//    $currentPlaylist = unserialize(base64_decode($_COOKIE['current_playlist']));
//    if(isset($currentPlaylist['data']) && is_array($currentPlaylist['data'])) $playlistData = $currentPlaylist['data'];
//
//    wp_localize_script('onirim', 'playlistData', array_merge($playlistData) );

}
add_action( 'wp_enqueue_scripts', 'onirim_scripts' );

function secure_api( $result ) {
    if ( ! empty( $result ) ) {
        return $result;
    }
    if ( ! is_user_logged_in() ) {
        return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
    }
    return $result;
}
add_filter('rest_authentication_errors', 'secure_api');

add_filter( 'xmlrpc_enabled', '__return_false' );
remove_action( 'wp_head', 'rsd_link' );